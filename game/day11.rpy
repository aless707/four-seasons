label day11:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "FINAL DAY" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "08:00" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg25 with dissolve
    play music "music/music23.ogg"
    call phone_start from _call_phone_start_31
    call message_start("SYSTEM", "Sayaka has entered the chatroom.") from _call_message_start_472
    call message("Sayaka", "............") from _call_message_3764
    call message("Sayaka", "..........") from _call_message_3765
    call message("Sayaka", "[name]...") from _call_message_3766
    call screen phone_reply("Sayaka, this isn't a dream, is it?", "cd111", "Could you please summarize for me what's going on right now?", "cd112")
label cd111:
    call phone_after_menu from _call_phone_after_menu_444
    call message_start("[name]", "Sayaka, this isn't a dream, is it?") from _call_message_start_473
    call message("Sayaka", "That's what I thought.") from _call_message_3767
    call message("Sayaka", "Looks like I'm not the only one wondering.") from _call_message_3768
    call message("Sayaka", "I'm still having a hard time wrapping my head around what's happening.") from _call_message_3769
    jump am111
label cd112:
    call phone_after_menu from _call_phone_after_menu_445
    call message_start("[name]", "Could you please summarize for me what's going on right now?") from _call_message_start_474
    call message("Sayaka", "Apologies. I spaced out for a moment.") from _call_message_3770
    call message("Sayaka", "I will gladly make a summary for you, if you'd like.") from _call_message_3771
    jump am111
label am111:
    call message("Sayaka", "So Levan, a member of the RFA,") from _call_message_3772
    call message("Sayaka", "has been kidnapped by his own father.") from _call_message_3773
    call message("Sayaka", "So since last night") from _call_message_3774
    call message("Sayaka", "I've been composing a script") from _call_message_3775
    call message("Sayaka", "that will disclose this gigantic list of scandals") from _call_message_3776
    call message("Sayaka", "committed by his father,") from _call_message_3777
    call message("Sayaka", "who is none other than the prime minister.") from _call_message_3778
    call message("Sayaka", "And I asked Shun") from _call_message_3779
    call message("Sayaka", "to rehearse for the announcement of the written script.") from _call_message_3780
    call message("Sayaka", "A total of 72 internet news companies have been hacked") from _call_message_3781
    call message("Sayaka", "and we're waiting for noon to come.") from _call_message_3782
    call message("Sayaka", "That's when the video for which Shun will star") from _call_message_3783
    call message("Sayaka", "will be posted on the main portals") from _call_message_3784
    call message("Sayaka", "of the said companies....") from _call_message_3785
    call screen phone_reply("What a blast...", "cd113", "All of you have done so well.", "cd114")
label cd113:
    call phone_after_menu from _call_phone_after_menu_446
    call message_start("[name]", "What a blast...") from _call_message_start_475
    call message("Sayaka", "True...now I'm starting to think") from _call_message_3786
    call message("Sayaka", "perhaps this is all just a dream...") from _call_message_3787
    call message("Sayaka", "Doesn't it all seem so unrealistic?") from _call_message_3788
    jump am112
label cd114:
    call phone_after_menu from _call_phone_after_menu_447
    call message_start("[name]", "All of you have done so well.") from _call_message_start_476
    call message("Sayaka", "And we're still working hard...") from _call_message_3789
    call message("Sayaka", "And by 'we,' that includes you.") from _call_message_3790
    jump am112
label am112:
    call message("SYSTEM", "Yoshiro has entered the chatroom.") from _call_message_3791
    call message("Yoshiro", "[name]!") from _call_message_3792
    call message("Yoshiro", "Sayaka...") from _call_message_3793
    call message("Sayaka", "Yoshiro.") from _call_message_3794
    call message("Yoshiro", "Shun's been practicing for hours until sunrise") from _call_message_3795
    call message("Yoshiro", "and I ended up memorizing all of his script.") from _call_message_3796
    call screen phone_reply("Wow! If Shun's not feeling well, you could substitute(?) him!", "cd115", "You're good at memorizing lolol", "cd116")
label cd115:
    call phone_after_menu from _call_phone_after_menu_448
    call message_start("[name]", "Wow! If Shun's not feeling well, you could substitute(?) him!") from _call_message_start_477
    call message("Yoshiro", "You're right lolol") from _call_message_3797
    call message("Yoshiro", "In case Shun gets stage fright...") from _call_message_3798
    call message_img("Yoshiro", "you might as well want to use me as his substitute lolol", "images/smile.png") from _call_message_img_238
    call message("Sayaka", "He's an actor.") from _call_message_3799
    call message("Yoshiro", "It's impossible for him to get stage fright ^^") from _call_message_3800
    call message("Yoshiro", "..Right.") from _call_message_3801
    jump am113
label cd116:
    call phone_after_menu from _call_phone_after_menu_449
    call message_start("[name]", "You're good at memorizing lolol") from _call_message_start_478
    call message("Yoshiro", "It was useful during my college entrance exam lolol") from _call_message_3802
    call message("Yoshiro", "I'm good at short-term memorization.") from _call_message_3803
    call message("Yoshiro", "Sayaka...") from _call_message_3804
    jump am113
label am113:
    call message("Yoshiro", "Give me something to do as well. Something big.") from _call_message_3805
    call message("Sayaka", "I would like you to know that remaining sane in this situation") from _call_message_3806
    call message("Sayaka", "is more than big a role.") from _call_message_3807
    call message("Yoshiro", "...Then should I tell you what happened to me today?") from _call_message_3808
    call message("Yoshiro", "This morning I found an acne on my cheek...") from _call_message_3809
    call screen phone_reply("...", "cd117", "I can feel my daily life...suddenly kicking in.", "cd118")
label cd117:
    call phone_after_menu from _call_phone_after_menu_450
    call message_start("[name]", "...") from _call_message_start_479
    call message("Sayaka", "No, thank you.") from _call_message_3810
    call message("Sayaka", "You do not need to talk about your daily life right now.") from _call_message_3811
    jump am114
label cd118:
    call phone_after_menu from _call_phone_after_menu_451
    call message_start("[name]", "I can feel my daily life...suddenly kicking in.") from _call_message_start_480
    call message("Yoshiro", "I think I had too much snack last night lolol") from _call_message_3812
    jump am114
label am114:
    call message("Sayaka", "Is Shun there with you?") from _call_message_3813
    call message("Sayaka", "How does he look? Is he alright?") from _call_message_3814
    call message("Yoshiro", "He just went out to buy some fruits.") from _call_message_3815
    call message("Yoshiro", "He says my cereal is nothing") from _call_message_3816
    call message("Yoshiro", "but a concoction of despicable ingredients.") from _call_message_3817
    call message("Yoshiro", "But that's what gives me the energy throughout the day T_T") from _call_message_3818
    call message("SYSTEM", "Ryou has entered the chatroom.") from _call_message_3819
    call message("Yoshiro", "Ryou!") from _call_message_3820
    call message("Sayaka", "Mr. Han") from _call_message_3821
    call message("Sayaka", "how is our current situation?") from _call_message_3822
    call message("Ryou", "All is well, according to Sora.") from _call_message_3823
    call screen phone_reply("That's good. Are we all ready for filming?", "cd119", "Did you hear anything from Sora?", "cd1110")
label cd119:
    call phone_after_menu from _call_phone_after_menu_452
    call message_start("[name]", "That's good. Are we all ready for filming?") from _call_message_start_481
    call message("Ryou", "Almost.") from _call_message_3824
    jump am115
label cd1110:
    call phone_after_menu from _call_phone_after_menu_453
    call message_start("[name]", "Did you hear anything from Sora?") from _call_message_start_482
    call message("Ryou", "Isn't he with you at the moment?") from _call_message_3825
    call message("Ryou", "I heard there's nothing peculiar.") from _call_message_3826
    jump am115
label am115:
    call message("Yoshiro", "Are you sure") from _call_message_3827
    call message("Yoshiro", "Shun won't get in trouble after showing his face for this?") from _call_message_3828
    call message("Ryou", "According to analysis by the advertisement team,") from _call_message_3829
    call message("Ryou", "the public's view of him would grow more positive.") from _call_message_3830
    call message("Ryou", "That's because we'll be exposing only about 14%") from _call_message_3831
    call message("Ryou", "of what we collected about the prime minister.") from _call_message_3832
    call screen phone_reply("What are they about?", "cd1111", "That's it? Only 14%?", "cd1112")
label cd1111:
    call phone_after_menu from _call_phone_after_menu_454
    call message_start("[name]", "What are they about?") from _call_message_start_483
    jump am116
label cd1112:
    call phone_after_menu from _call_phone_after_menu_455
    call message_start("[name]", "That's it? Only 14%?") from _call_message_start_484
    call message("Sayaka", "We determined") from _call_message_3833
    call message("Sayaka", "that we should minimize the number of related people.") from _call_message_3834
    call message("Ryou", "And 14% would be more than enough.") from _call_message_3835
    jump am116
label am116:
    call message("Ryou", "We'll reveal only about his illegitimate sons...") from _call_message_3836
    call message("Ryou", "and laundering of unreported property that took place outside the country.") from _call_message_3837
    call message("Sayaka", "He committed his crime solely for his benefit.") from _call_message_3838
    call message("Ryou", "He didn't have any related person to this, so the job was rather easy.") from _call_message_3839
    call message("Ryou", "If he had a lot of accomplices,") from _call_message_3840
    call message("Ryou", "we would've made all of them our enemies") from _call_message_3841
    call message("Ryou", "and thus rendered this battle a challenge.") from _call_message_3842
    call message("Sayaka", "He must have had no idea") from _call_message_3843
    call message("Sayaka", "his trick would backfire like this.") from _call_message_3844
    call message_img("Yoshiro", "The heck?", "images/shocked.png") from _call_message_img_239
    call message("Yoshiro", "You're saying that's only 14 of what he did??") from _call_message_3845
    call message("Ryou", "Yes.") from _call_message_3846
    call screen phone_reply("What about the remaining 86%...?", "cd1113", "I'm starting to admire K and Zero for actually doing research on them..", "cd1114")
label cd1113:
    call phone_after_menu from _call_phone_after_menu_456
    call message_start("[name]", "What about the remaining 86%...?") from _call_message_start_485
    call message("Ryou", "It seems to me we should be the only ones to know about them.") from _call_message_3847
    call message("Sayaka", "You're welcome to take a look, if you think you can handle them...") from _call_message_3848
    jump am117
label cd1114:
    call phone_after_menu from _call_phone_after_menu_457
    call message_start("[name]", "I'm starting to admire K and Zero for actually doing research on them..") from _call_message_start_486
    call message("Sayaka", "We were shocked as well...") from _call_message_3849
    call message("Ryou", "K....") from _call_message_3850
    call message("Ryou", "I never thought he'd be so bold.") from _call_message_3851
    call message("Ryou", "We're longtime friends, but I'd say my evaluation on him must go through a revision.") from _call_message_3852
    jump am117
label am117:
    call message("Yoshiro", "Ugh T_T") from _call_message_3853
    call message("Yoshiro", "Wish I could live in a world free from crimes and lies.") from _call_message_3854
    call message("Ryou", "Then join the C&R.") from _call_message_3855
    call message("Yoshiro", "Uh....") from _call_message_3856
    call message_img("Yoshiro", "should I?", "images/well.png") from _call_message_img_240
    call screen phone_reply("So C&R can be trusted?", "cd1115", "We get to learn how precious being virtuous is because we get to see cases of immorality...", "cd1116")
label cd1115:
    call phone_after_menu from _call_phone_after_menu_458
    call message_start("[name]", "So C&R can be trusted?") from _call_message_start_487
    call message("Ryou", "No. Don't trust it completely.") from _call_message_3857
    call message("Ryou", "There's no telling what my predecessors might have done before my birth.") from _call_message_3858
    call message("Ryou", "And there's no evidence that C&R is completely virtuous.") from _call_message_3859
    call message_img("Sayaka", "That is...utterly objective of you, sir.", "images/well.png") from _call_message_img_241
    jump am118
label cd1116:
    call phone_after_menu from _call_phone_after_menu_459
    call message_start("[name]", "We get to learn how precious being virtuous is because we get to see cases of immorality...") from _call_message_start_488
    call message("Sayaka", "[name]") from _call_message_3860
    call message("Sayaka", "you're positive.") from _call_message_3861
    call message("Ryou", "I wish people who can see the world in such view") from _call_message_3862
    call message("Ryou"," would join the C&R....") from _call_message_3863
    call message("Sayaka", "You'd soon get to see that, if you add only half of the current office stress on your employees.") from _call_message_3864
    call message("Yoshiro"," Ooooh bam lol") from _call_message_3865
    jump am118
label am118:
    call message("Yoshiro", "But") from _call_message_3866
    call message("Yoshiro", "why would we reveal only 14%?") from _call_message_3867
    call message("Yoshiro", "What happens if we reveal the remaining 86%?") from _call_message_3868
    call message("Yoshiro", "Why don't we") from _call_message_3869
    call message("Yoshiro", "just reveal everything") from _call_message_3870
    call message("Yoshiro", "and unleash the tornado all over the country?") from _call_message_3871
    call message("Ryou", "That is too risky.") from _call_message_3872
    call message("Sayaka", "In case that happens,") from _call_message_3873
    call message("Sayaka", "there's a chance that innocent would suffer.") from _call_message_3874
    call message_img("Yoshiro", "What's that supposed to mean?", "images/questioning.png") from _call_message_img_242
    call screen phone_reply("Let's just dig a pit to safeguard the remaining 86% and make them stay there forever.", "cd1117", "But still...you will unleash everything someday, won't you?", "cd1118")
label cd1117:
    call phone_after_menu from _call_phone_after_menu_460
    call message_start("[name]", "Let's just dig a pit to safeguard the remaining 86% and make them stay there forever.") from _call_message_start_489
    call message("Sayaka", "And we'll make sure that pit is a virtual one.") from _call_message_3875
    jump am119
label cd1118:
    call phone_after_menu from _call_phone_after_menu_461
    call message_start("[name]", "But still...you will unleash everything someday, won't you?") from _call_message_start_490
    jump am119
label am119:
    call message("Sayaka", "Let's just say") from _call_message_3876
    call message("Sayaka", "there's this Pandora's box that must remain untouched for now.") from _call_message_3877
    call message("Sayaka", "This Pandora's box is what") from _call_message_3878
    call message("Sayaka", "Mr. Han and I will take our time") from _call_message_3879
    call message("Sayaka", "in opening it") from _call_message_3880
    call message("Sayaka", "very slowly and carefully.") from _call_message_3881
    call message("Yoshiro", "Whoa...") from _call_message_3882
    call message("Yoshiro", "Ok..") from _call_message_3883
    call message("Yoshiro", "got it.") from _call_message_3884
    call message("Sayaka", "And Mr. Sora will join us for that, if he wants to, of course...") from _call_message_3885
    call screen phone_reply("I'm sure Sora is strong enough to embrace the truth.", "cd1119", "Only if that helps everyone to be happy...!", "cd1120")
label cd1119:
    call phone_after_menu from _call_phone_after_menu_462
    call message_start("[name]", "I'm sure Sora is strong enough to embrace the truth.") from _call_message_start_491
    call message_img("Sayaka", "We'll all believe that he is.", "images/smile.png") from _call_message_img_243
    jump am1110
label cd1120:
    call phone_after_menu from _call_phone_after_menu_463
    call message_start("[name]", "Only if that helps everyone to be happy...!") from _call_message_start_492
    call message("Sayaka", "A riddle to be unraveled") from _call_message_3886
    call message("Sayaka", "cannot always be split into black and white...") from _call_message_3887
    call message("Sayaka", "As for the remaining secrets") from _call_message_3888
    call message("Sayaka", "we will get a chance to discuss them again in the future.") from _call_message_3889
    jump am1110
label am1110:
    call message("SYSTEM", "Shun has entered the chatroom.") from _call_message_3890
    call message("Sayaka", "Shun!") from _call_message_3891
    call screen phone_reply("Shun, how are you feeling?", "cd1121", "Shun, did you get your fruits?", "cd1122")
label cd1121:
    call phone_after_menu from _call_phone_after_menu_464
    call message_start("[name]", "Shun, how are you feeling?") from _call_message_start_493
    call message_img("Shun", "good good", "images/smile.png") from _call_message_img_244
    call message("Shun", "but") from _call_message_3892
    jump am1111
label cd1122:
    call phone_after_menu from _call_phone_after_menu_465
    call message_start("[name]", "Shun, did you get your fruits?") from _call_message_start_494
    call message("Shun", "[name]") from _call_message_3893
    call message("Shun", "morning lmao") from _call_message_3894
    call message("Shun", "And yes, I'm at the convenience store to get some, but...") from _call_message_3895
    jump am1111
label am1111:
    call message("Shun", "This convenience store") from _call_message_3896
    call message("Shun", "doesn't sell organic bananas.") from _call_message_3897
    call message("Yoshiro", "Shun") from _call_message_3898
    call message("Yoshiro", "I have bananas in my fridge.") from _call_message_3899
    call message("Shun", "But you don't know if it's organic, do you?") from _call_message_3900
    call message("Shun", "You gotta make sure your banana is organic.") from _call_message_3901
    call message("Shun", "Because most bananas are imported") from _call_message_3902
    call message("Shun", "and spend several days on planes.") from _call_message_3903
    call message("Shun", "But if those bananas are drenched with chemicals....") from _call_message_3904
    call screen phone_reply("That's just a rumor! Though organic is good..", "cd1123", "Why not have banana flavored milk instead?", "cd1124")
label cd1123:
    call phone_after_menu from _call_phone_after_menu_466
    call message_start("[name]", "That's just a rumor! Though organic is good..") from _call_message_start_495
    call message("Shun", "oh") from _call_message_3905
    call message("Shun", "seriously?") from _call_message_3906
    call message("Shun", "It used to be a really viral tale on the web lmao") from _call_message_3907
    jump am1112
label cd1124:
    call phone_after_menu from _call_phone_after_menu_467
    call message_start("[name]", "Why not have banana flavored milk instead?") from _call_message_start_496
    call message("Shun", "no no no") from _call_message_3908
    call message("Shun", "Do you have any idea how unhealthy it is?") from _call_message_3909
    call message("Yoshiro", "Shun") from _call_message_3910
    call message("Yoshiro", "please don't ruin my precious memory") from _call_message_3911
    call message("Yoshiro", "with my cherished banana flavored milk...") from _call_message_3912
    call message("Shun", "Uh...sure.") from _call_message_3913
    jump am1112
label am1112:
    call message("Ryou", "If you need bananas") from _call_message_3914
    call message("Ryou", "I'll get them ready on the vehicle I'll soon send you.") from _call_message_3915
    call message("Shun", "Seriously?") from _call_message_3916
    call message("Ryou", "Of course. I'll contact the staff for that matter.") from _call_message_3917
    call screen phone_reply("How convenient lol", "cd1125", "Of course. For now, please make sure you're in your top shape!", "cd1126")
label cd1125:
    call phone_after_menu from _call_phone_after_menu_468
    call message_start("[name]", "How convenient lol") from _call_message_start_497
    call message("Sayaka", "Shun") from _call_message_3918
    call message("Sayaka", "if you have anything you need, do not hesitate to tell us.") from _call_message_3919
    call message("Shun", "So it's all ready? Just like that?") from _call_message_3920
    jump am1113
label cd1126:
    call phone_after_menu from _call_phone_after_menu_469
    call message_start("[name]", "Of course. For now, please make sure you're in your top shape!") from _call_message_start_498
    call message("Yoshiro", "I offered to be his assistant") from _call_message_3921
    call message("Yoshiro", "but he said no T_T") from _call_message_3922
    call message("Sayaka", "That's a very wise") from _call_message_3923
    call message("Yoshiro", "What?!") from _call_message_3924
    call message("Sayaka", "Uh") from _call_message_3925
    call message("Sayaka", "nothing.") from _call_message_3926
    jump am1113
label am1113:
    call message("Sayaka", "Shun, you will be representing RFA today.") from _call_message_3927
    call message("Sayaka", "So please stay focused for your upcoming stage.") from _call_message_3928
    call message("Yoshiro", "...Yeah.") from _call_message_3929
    call message("Yoshiro", "I agree.") from _call_message_3930
    call message("Shun", "I can take care of myself but...") from _call_message_3931
    call message("Shun", "In that case, let me owe you one just for the day.") from _call_message_3932
    call message_img("Shun", "Since today is a big day.", "images/smile.png") from _call_message_img_245
    call message_img("Sayaka", "Of course.", "images/smile.png") from _call_message_img_246
    call message("Shun", "So Sayaka") from _call_message_3933
    call message("Shun", "is C&R all ready to go?") from _call_message_3934
    call message("Sayaka", "We're almost at that stage.") from _call_message_3935
    call message("Yoshiro", "Once the world finds out") from _call_message_3936
    call message("Yoshiro", "that we're behind this scandalous revelation,") from _call_message_3937
    call message("Yoshiro", "the person who will get to hate us...") from _call_message_3938
    call message("Sayaka", "Will only be the prime minister.") from _call_message_3939
    call screen phone_reply("You surely chose well what you have to reveal.", "cd1127", "We need to make our chance of hurting the innocent as small as possible.", "cd1128")
label cd1127:
    call phone_after_menu from _call_phone_after_menu_470
    call message_start("[name]", "You surely chose well what you have to reveal.") from _call_message_start_499
    call message("Ryou", "You all did so well") from _call_message_3940
    call message("Ryou"," despite the fact that we didn't have much time.") from _call_message_3941
    jump am1114
label cd1128:
    call phone_after_menu from _call_phone_after_menu_471
    call message_start("[name]", "We need to make our chance of hurting the innocent as small as possible.") from _call_message_start_500
    call message("Ryou", "Right you are.") from _call_message_3942
    jump am1114
label am1114:
    call message("Shun", "Wait") from _call_message_3943
    call message("Shun", "but here's the thing -") from _call_message_3944
    call message("Shun", "We're not gonna have some friends of prime minister") from _call_message_3945
    call message("Shun", "coming for our throats for vengeance,") from _call_message_3946
    call message("Shun", "are we?") from _call_message_3947
    call message("Shun", "That's pretty common") from _call_message_3948
    call message("Shun", "in the scripts") from _call_message_3949
    call message("Shun", "for plays and musicals....") from _call_message_3950
    call message("Yoshiro", "You're worrying for nothing....") from _call_message_3951
    call message("Yoshiro", "Prime minister is the one who's at fault.") from _call_message_3952
    call message("Yoshiro", "His friends shouldn't be mad...") from _call_message_3953
    call screen phone_reply("That doesn't sound impossible...", "cd1129", "You're right... I hope justice is done.", "cd1130")
label cd1129:
    call phone_after_menu from _call_phone_after_menu_472
    call message_start("[name]", "That doesn't sound impossible...") from _call_message_start_501
    call message("Shun", "Ikr?") from _call_message_3954
    call message("Shun", "Yoshiro, you think reason works with a man like him??") from _call_message_3955
    call message("Shun", "He's committing something so unreasonable.") from _call_message_3956
    jump am1115
label cd1130:
    call phone_after_menu from _call_phone_after_menu_473
    call message_start("[name]", "You're right... I hope justice is done.") from _call_message_start_502
    call message("Shun", "He kidnapped his own son.") from _call_message_3957
    call message("Shun", "Do you think you can find reason and justice around him...?") from _call_message_3958
    jump am1115
label am1115:
    call message("Ryou", "You've got a point, Shun.") from _call_message_3959
    call message("Shun", "Ikr?") from _call_message_3960
    call message("Ryou", "So if there's any sign of trouble arising against your favor,") from _call_message_3961
    call message("Ryou", "why don't you join us under C&R's management") from _call_message_3962
    call message("Ryou", "and stay under our protection?") from _call_message_3963
    call message_img("Shun", "Listen", "images/well.png") from _call_message_img_247
    call message("Shun", "my answer") from _call_message_3964
    call message("Shun", "is no.") from _call_message_3965
    call screen phone_reply("You should give it a thought. I think it's a golden opportunity.", "cd1131", "I think you look as free as you can be when you're on your own lolol", "cd1132")
label cd1131:
    call phone_after_menu from _call_phone_after_menu_474
    call message_start("[name]", "You should give it a thought. I think it's a golden opportunity.") from _call_message_start_503
    call message("Yoshiro", "She's right.") from _call_message_3966
    call message("Yoshiro", "This is your chance to rise to fame!") from _call_message_3967
    jump am1116
label cd1132:
    call phone_after_menu from _call_phone_after_menu_475
    call message_start("[name]", "I think you look as free as you can be when you're on your own lolol") from _call_message_start_504
    call message("Shun", "yeah...") from _call_message_3968
    call message("Shun", "And whatever happens, there's no way") from _call_message_3969
    call message("Shun", "I'm becoming famous with his company behind me.") from _call_message_3970
    jump am1116
label am1116:
    call message("Ryou", "I'm offering you just the title of the C&R.") from _call_message_3971
    call message("Ryou", "And this is only a possibility, in case the worst unfolds.") from _call_message_3972
    call message("Shun", "I don't want to have just the title, either....") from _call_message_3973
    call message("Shun", "I don't want to get suddenly famous because of this.") from _call_message_3974
    call message("Shun", "I gotta make myself famous with my talent.") from _call_message_3975
    call message("Sayaka", "Shun...that's admirable.") from _call_message_3976
    call screen phone_reply("I'll be rooting for you! Always!", "cd1133", "Sora will also be famous, won't he?", 'cd1134')
label cd1133:
    call phone_after_menu from _call_phone_after_menu_476
    call message_start("[name]", "I'll be rooting for you! Always!") from _call_message_start_505
    call message_img("Yoshiro", "I'll protect you, Shun!", "images/smile.png") from _call_message_img_248
    call message("Shun", "I can protect myself") from _call_message_3977
    call message("Shun", "on my own...") from _call_message_3978
    jump am1117
label cd1134:
    call phone_after_menu from _call_phone_after_menu_477
    call message_start("[name]"," Sora will also be famous, won't he?") from _call_message_start_506
    call message("Sayaka", "I'm sure same could be said of Levan...") from _call_message_3979
    call message("Shun", "They'll be more famous than me lmao") from _call_message_3980
    call message("Shun", "Since my duty is only announcing the truth!") from _call_message_3981
    jump am1117
label am1117:
    call message("Shun", "And I think it's an honor to be featured in news.") from _call_message_3982
    call message("Ryou", "I'm glad you're treating this matter cooperatively.") from _call_message_3983
    call message("Shun", "Cooperatively?!") from _call_message_3984
    call message("Shun", "Of course I'll be cooperative. It's something that concerns my friend!") from _call_message_3985
    call message("Sayaka", "But") from _call_message_3986
    call message("Sayaka", "it appears we've lost contact with K since yesterday....") from _call_message_3987
    call message("Shun", "I'll say.") from _call_message_3988
    call message("Yoshiro"," We last heard him, like, in the chat room the other day, didn't we?") from _call_message_3989
    call screen phone_reply("Ryou, have you heard nothing from him?", "cd1135", "I had a feeling he'd still be watching us...", "cd1136")
label cd1135:
    call phone_after_menu from _call_phone_after_menu_478
    call message_start("[name]", "Ryou, have you heard nothing from him?") from _call_message_start_507
    call message("Ryou", "He did call me...") from _call_message_3990
    call message("Ryou", "And he told me") from _call_message_3991
    call message("Ryou", "that no matter what happens from here on,") from _call_message_3992
    call message("Ryou", "he will always fully support my decisions.") from _call_message_3993
    jump am1118
label cd1136:
    call phone_after_menu from _call_phone_after_menu_479
    call message_start("[name]", "I had a feeling he'd still be watching us...") from _call_message_start_508
    call message("Sayaka", "So did I, but...") from _call_message_3994
    call message("Sayaka", "It felt like he mysteriously made himself disappear...") from _call_message_3995
    jump am1118
label am1118:
    call message("Yoshiro", "K...") from _call_message_3996
    call message("Yoshiro","he's not going to leave us or something, is he?") from _call_message_3997
    call message("Shun", "Hey") from _call_message_3998
    call message("Shun", "come on") from _call_message_3999
    call message("Shun", "He's not the type to leave a single phone call before departure.") from _call_message_4000
    call message("Ryou", "...") from _call_message_4001
    call message("Ryou", "I believe he'll get back to us again soon.") from _call_message_4002
    call message("Ryou", "Though I'm not sure what news he'll bring with him.") from _call_message_4003
    call screen phone_reply("I also have a feeling this isn't the end of it.", "cd1137", "I wish Sora could join us right now and chat.", "cd1138")
label cd1137:
    call phone_after_menu from _call_phone_after_menu_480
    call message_start("[name]", "I also have a feeling this isn't the end of it.") from _call_message_start_509
    call message("Sayaka", "So do I...") from _call_message_4004
    jump am1119
label cd1138:
    call phone_after_menu from _call_phone_after_menu_481
    call message_start("[name]", "I wish Sora could join us right now and chat.") from _call_message_start_510
    call message("Yoshiro", "What's he doing right now?") from _call_message_4005
    jump am1119
label am1119:
    call message("SYSTEM", "Sora has entered the chatroom.") from _call_message_4006
    call message("Yoshiro", "Huh?") from _call_message_4007
    call message_img("Yoshiro", "It's Sora!", "images/expecting.png") from _call_message_img_249
    call message("Sayaka", "Mr. Sora.") from _call_message_4008
    call message("Sayaka", "Is there any problem with the unit?") from _call_message_4009
    call message("Sora", "No. Everything's fine.") from _call_message_4010
    call message("Ryou", "Good.") from _call_message_4011
    call screen phone_reply("Sora, you've done so well through the night.", "cd1139", "Sora, I hope we can reach the end soon and everyone would be happy.", "cd1140")
label cd1139:
    call phone_after_menu from _call_phone_after_menu_482
    call message_start("[name]", "Sora, you've done so well through the night.") from _call_message_start_511
    call message("Sora", "This is") from _call_message_4012
    call message("Sora", "nothing...") from _call_message_4013
    call message("Sora", "And I was lucky too.") from _call_message_4014
    jump am1120
label cd1140:
    call phone_after_menu from _call_phone_after_menu_483
    call message_start("[name]", "Sora, I hope we can reach the end soon and everyone would be happy.") from _call_message_start_512
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "That's what you've been wishing all this time...") from _call_message_4015
    call message("Sora", "I'm sure it'll soon come true.") from _call_message_4016
    jump am1120
label am1120:
    call message("Yoshiro", "But") from _call_message_4017
    call message("Sayaka", "how's the rescue plan for Zero going?") from _call_message_4018
    call message("Sora", "I'll explain that") from _call_message_4019
    call message("Sora", "right now.") from _call_message_4020
    call message("Yoshiro", "Ooooh, Sora!") from _call_message_4021
    call message("Sora", "1. Release the news. Send evidence to press and police stations.") from _call_message_4022
    call message("Sora", "2. Wait for people to gather to the deserted house.") from _call_message_4023
    call screen phone_reply("A deserted house...?", "cd1141", "Who would send the evidence?", "cd1142")
label cd1141:
    call phone_after_menu from _call_phone_after_menu_484
    call message_start("[name]", "A deserted house...?") from _call_message_start_513
    jump am1121
label cd1142:
    call phone_after_menu from _call_phone_after_menu_485
    call message_start("[name]", "Who would send the evidence?") from _call_message_start_514
    call message("Sayaka", "C&R has prepared the hard copies at the printing room. They're all set for shipment.") from _call_message_4024
    call message("Yoshiro", "But") from _call_message_4025
    call message_img("Yoshiro", "a deserted house?", "images/questioning.png") from _call_message_img_250
    jump am1121
label am1121:
    call message("Sayaka", "After our investigation, the house turned out to be a property without an owner, human presence long gone.") from _call_message_4026
    call message("Sora", "Which means there will be no problem even if we trespass.") from _call_message_4027
    call message("Sora", "...That's where me and my brother lived during childhood.") from _call_message_4028
    call message("Shun", "I knew it...") from _call_message_4029
    call message("Shun", "Just as I thought.") from _call_message_4030
    call message("Yoshiro", "I thought so") from _call_message_4031
    call message("Yoshiro", "after reading yesterday's chat log.") from _call_message_4032
    call message("Sora", "3. C&R's team arrives and investigates inside the house.") from _call_message_4033
    call message("Sora", "4. Results of the investigation will be handed over to the prosecution.") from _call_message_4034
    call message("Sayaka", "And the prime minister will soon be arrested") from _call_message_4035
    call message("Sayaka", "based on the files we will disclose.") from _call_message_4036
    call message("Shun", "Oh...") from _call_message_4037
    call message("Shun", "got it.") from _call_message_4038
    call screen phone_reply("I hope it all works out well... Please!", "cd1143", "You do have plan B, don't you?", "cd1144")
label cd1143:
    call phone_after_menu from _call_phone_after_menu_486
    call message_start("[name]", "I hope it all works out well... Please!") from _call_message_start_515
    call message("Yoshiro", "Good luck") from _call_message_4039
    call message("Yoshiro", "to all of us!") from _call_message_4040
    call message("Yoshiro", "I really hope this works T_T") from _call_message_4041
    jump am1122
label cd1144:
    call phone_after_menu from _call_phone_after_menu_487
    call message_start("[name]", "You do have plan B, don't you?") from _call_message_start_516
    call message("Sayaka", "We have plans A, B, and C just in case.") from _call_message_4042
    call message("Yoshiro", "Yes that's C&R!") from _call_message_4043
    jump am1122
label am1122:
    call message("Sora", "It will work.") from _call_message_4044
    call message("Yoshiro", "Wow, Sora!") from _call_message_4045
    call message("Yoshiro", "You're positive....") from _call_message_4046
    call message("Yoshiro", "I doubt you could get even an hour of sleep since yesterday.") from _call_message_4047
    call message("Sayaka", "Are you feeling alright?") from _call_message_4048
    call message("Sora", "Yes.") from _call_message_4049
    call message("Sora", "I've never felt better.") from _call_message_4050
    call message("Sora", "Because [name] is with me.") from _call_message_4051
    call screen phone_reply("I...have a good feeling too.", "cd1145", "It still feels like a dream that I'm here with you, Sora.", "cd1146")
label cd1145:
    call phone_after_menu from _call_phone_after_menu_488
    call message_start("[name]", "I...have a good feeling too.") from _call_message_start_517
    call message("Yoshiro", "Sora") from _call_message_4052
    call message("Yoshiro", "were you being affectionate just now?") from _call_message_4053
    call message("Shun", "Man...jealous...") from _call_message_4054
    call message("Shun", "This is a crisis") from _call_message_4055
    call message("Shun", "but my DNA for a relationship is screaming in jealousy T_T") from _call_message_4056
    call message("Yoshiro", "Shun...") from _call_message_4057
    call message("Yoshiro", "I'm not sure if my DNA for dating exists at all.") from _call_message_4058
    call message("Shun", ".....") from _call_message_4059
    jump am1123
label cd1146:
    call phone_after_menu from _call_phone_after_menu_489
    call message_start("[name]", "It still feels like a dream that I'm here with you, Sora.") from _call_message_start_518
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "Me too.") from _call_message_4060
    call message("Sora", "I wish we could stay together forever...") from _call_message_4061
    call message("Yoshiro", "Both of you...") from _call_message_4062
    call message("Yoshiro", "I really hope you'd be happy!") from _call_message_4063
    call message("Shun", "That's right...") from _call_message_4064
    call message("Shun", "You've finally made it across a valley! It's time for you to enjoy the flower bed!") from _call_message_4065
    jump am1123
label am1123:
    call message("Sayaka", "Mr. Sora,") from _call_message_4066
    call message("Sayaka", "The way he only speaks,") from _call_message_4067
    call message("Sayaka", "his speech resembles yours a lot, Mr. Han.") from _call_message_4068
    call message("Yoshiro", "Oh") from _call_message_4069
    call message("Yoshiro", "no wonder he seems so stiff in a cool way.") from _call_message_4070
    call message("Ryou", "And that made things easier for me as well.") from _call_message_4071
    call message("Sora", "Now we should all get on standby.") from _call_message_4072
    call screen phone_reply("Why don't we now dismiss?", "cd1147", "Is the filming starting soon?", "cd1148")
label cd1147:
    call phone_after_menu from _call_phone_after_menu_490
    call message_start("[name]", "Why don't we now dismiss?") from _call_message_start_519
    call message("Sayaka", "Yes. It seems we're ready.") from _call_message_4073
    jump am1124
label cd1148:
    call phone_after_menu from _call_phone_after_menu_491
    call message_start("[name]", "Is the filming starting soon?") from _call_message_start_520
    call message("Ryou", "Yes. I believe we're ready.") from _call_message_4074
    jump am1124
label am1124:
    call message("Sayaka", "Shun, I'll see you at the studio.") from _call_message_4075
    call message("Shun", "Sure thing.") from _call_message_4076
    call message("Shun", "Good luck, Sayaka!") from _call_message_4077
    call message("Yoshiro", "Make sure you get in the right van.") from _call_message_4078
    call message("Shun", "Of course I will...") from _call_message_4079
    call message("Ryou", "Apparently, they're almost there.") from _call_message_4080
    call message("Yoshiro", "Oh") from _call_message_4081
    call message("Yoshiro", "Shun!") from _call_message_4082
    call message("Yoshiro", "I think I see the van outside.") from _call_message_4083
    call message("Ryou", "Please check the number of the van.") from _call_message_4084
    call message("Shun", "Yeah, that's the one.") from _call_message_4085
    call message("Shun", "Here I come.") from _call_message_4086
    call screen phone_reply("Everything will be okay!", "cd1149", "I'll look forward to it!", "cd1150")
label cd1149:
    call phone_after_menu from _call_phone_after_menu_492
    call message_start("[name]", "Everything will be okay!") from _call_message_start_521
    call message("Shun", "Of course it will...!") from _call_message_4087
    call message("Shun", "No need to worry about me lmao") from _call_message_4088
    jump am1125
label cd1150:
    call phone_after_menu from _call_phone_after_menu_493
    call message_start("[name]", "I'll look forward to it!") from _call_message_start_522
    call message("Shun", "Sure thing!") from _call_message_4089
    call message("Shun", "Please take care of Sora.") from _call_message_4090
    jump am1125
label am1125:
    call message("Sayaka", "Shun...") from _call_message_4091
    call message("Sayaka", "have a safe trip.") from _call_message_4092
    call message("SYSTEM", "Shun has left the chatroom.") from _call_message_4093
    call message("Yoshiro", "But did you already fetch some bananas in that van?") from _call_message_4094
    call message("Sayaka", "You'll see when you get inside.") from _call_message_4095
    call message("Yoshiro", "Whoa....") from _call_message_4096
    call screen phone_reply("Yoshiro, you're not going with him?", "cd1151", "Bananas... Sound delicious...", "cd1152")
label cd1151:
    call phone_after_menu from _call_phone_after_menu_494
    call message_start("[name]", "Yoshiro, you're not going with him?") from _call_message_start_523
    call message("Yoshiro", "Well...") from _call_message_4097
    jump am1126
label cd1152:
    call phone_after_menu from _call_phone_after_menu_495
    call message_start("[name]", "Bananas... Sound delicious...") from _call_message_start_524
    call message("Yoshiro", "I already had my cereal, so I'm full lol") from _call_message_4098
    call message("Yoshiro", "But I am curious what will be waiting in the van.") from _call_message_4099
    jump am1126
label am1126:
    call message("Sayaka", "Yoshiro, you're coming too, aren't you?") from _call_message_4100
    call message_img("Yoshiro", "Hehe", "images/smile.png") from _call_message_img_251
    call message("Yoshiro", "I've been waiting for you to say that...") from _call_message_4101
    call message_img("Yoshiro", "You can't leave me out from such an important moment!", "images/expecting.png") from _call_message_img_252
    call message("Ryou", "Fetch dem cookies on your way back.") from _call_message_4102
    call screen phone_reply("And fetch dem crackers for me.", "cd1153", "....", "cd1154")
label cd1153:
    call phone_after_menu from _call_phone_after_menu_496
    call message_start("[name]", "And fetch dem crackers for me.") from _call_message_start_525
    call message("Sayaka", "...") from _call_message_4103
    call message_img("Yoshiro", "What's wrong with you two...?", 'images/well.png') from _call_message_img_253
    jump am1127
label cd1154:
    call phone_after_menu from _call_phone_after_menu_497
    call message_start("[name]", "....") from _call_message_start_526
    call message_img("Yoshiro", "Huh??", "images/shocked.png") from _call_message_img_254
    jump am1127
label am1127:
    call message("Ryou", "It was a jest.") from _call_message_4104
    call message("Sayaka", "Mr. Han,") from _call_message_4105
    call message("Sayaka", "why don't you read your dictionary on 21C terms after a new version is released....?") from _call_message_4106
    call message("Yoshiro", ".....I'll see you soon.") from _call_message_4107
    call message("Sayaka", "Alright, Yoshiro.") from _call_message_4108
    call screen phone_reply("Take care of Shun!", "cd1155", "Good luck!", "cd1156")
label cd1155:
    call phone_after_menu from _call_phone_after_menu_498
    call message_start("[name]", "Take care of Shun!") from _call_message_start_527
    call message_img("Yoshiro", "Leave it to me -", "images/expecting.png") from _call_message_img_255
    jump am1128
label cd1156:
    call phone_after_menu from _call_phone_after_menu_499
    call message_start("[name]", "Good luck!") from _call_message_start_528
    call message_img("Yoshiro", "And you too!", "images/smile.png") from _call_message_img_256
    jump am1128
label am1128:
    call message("SYSTEM", "Yoshiro has left the chatroom.") from _call_message_4109
    call message("Sayaka", "Oh. Mr. Sora is calling for me from the unit.") from _call_message_4110
    call screen phone_reply("Thank you for your hard work for so long.", "cd1157", "Sayaka... You're always so professional. And I find that admirable.", "cd1158")
label cd1157:
    call phone_after_menu from _call_phone_after_menu_500
    call message_start("[name]", "Thank you for your hard work for so long.") from _call_message_start_529
    call message("Sayaka", "I'm glad I could be of help.") from _call_message_4111
    call message("Sayaka", "I finally got to realize during the past couple days...") from _call_message_4112
    call message("Sayaka", "how important it is that everyone is safe and sound.") from _call_message_4113
    jump am1129
label cd1158:
    call phone_after_menu from _call_phone_after_menu_501
    call message_start("[name]", "Sayaka... You're always so professional. And I find that admirable.") from _call_message_start_530
    call message("Sayaka", "Oh my...") from _call_message_4114
    call message("Sayaka", "my heart skipped a beat for a second.") from _call_message_4115
    call message_img("Sayaka", "Thank you.", "images/smile.png") from _call_message_img_257
    call message("Sayaka", "I too remember how marvelous you are.") from _call_message_4116
    jump am1129
label am1129:
    call message("Sayaka", "Now I should go as well.") from _call_message_4117
    call message("Ryou", "Yes.") from _call_message_4118
    call message("SYSTEM", "Sayaka has left the chatroom.") from _call_message_4119
    call message("Ryou", "It'll be very busy until the end of the day.") from _call_message_4120
    call message("Ryou", "I haven't told my father about this yet.") from _call_message_4121
    call message("Ryou", "But it would be better to keep him uninformed forever....") from _call_message_4122
    call screen phone_reply("Even if you tell him later on, he'll forgive you.", "cd1159", "Shouldn't you go too?", 'cd1160')
label cd1159:
    call phone_after_menu from _call_phone_after_menu_502
    call message_start("[name]", "Even if you tell him later on, he'll forgive you.") from _call_message_start_531
    call message("Ryou", "He would've made a different call if he were in my seat...") from _call_message_4123
    call message("Ryou", "But I am different from him.") from _call_message_4124
    jump am1130
label cd1160:
    call phone_after_menu from _call_phone_after_menu_503
    call message_start("[name]", "Shouldn't you go too?") from _call_message_start_532
    call message("Ryou", "I should.") from _call_message_4125
    jump am1130
label am1130:
    call message("Ryou", "So I too shall take my leave.") from _call_message_4126
    call message("Ryou", "[name]...") from _call_message_4127
    call message("Ryou", "You're at the center of this incident.") from _call_message_4128
    call message("Ryou", "Today") from _call_message_4129
    call message("Ryou", "let us not lose our focus until the end") from _call_message_4130
    call message("Ryou", "so that everything works out as we planned.") from _call_message_4131
    call message("SYSTEM", "Ryou has left the chatroom.") from _call_message_4132
    call message("Sora", "..............") from _call_message_4133
    call message("Sora", ".............") from _call_message_4134
    call screen phone_reply3("Sora...?", "cd1161", "It will turn out well, right?", "cd1162", "Sora, I love you.", "cd1163")
label cd1161:
    call phone_after_menu from _call_phone_after_menu_504
    call message_start("[name]", "Sora...?") from _call_message_start_533
    call message("Sora", "Yes. I'm here.") from _call_message_4135
    jump am1131
label cd1162:
    call phone_after_menu from _call_phone_after_menu_505
    call message_start("[name]", "It will turn out well, right?") from _call_message_start_534
    call message("Sora", "It will.") from _call_message_4136
    call message("Sora", "But you know what...?") from _call_message_4137
    jump am1131
label cd1163:
    call phone_after_menu from _call_phone_after_menu_506
    call message_start("[name]", "Sora, I love you.") from _call_message_start_535
    call message("Sora", "I love you more.") from _call_message_4138
    call message("Sora", "I love you...") from _call_message_4139
    call message("Sora", "much more than...") from _call_message_4140
    call message("Sora", "...") from _call_message_4141
    call message("Sora", "No") from _call_message_4142
    call message("Sora", "I'll tell you this") from _call_message_4143
    call message("Sora", "later...") from _call_message_4144
    call message("Sora", "as I look into your eyes.") from _call_message_4145
    show gh at heart_pos with dissolve
    hide gh with dissolve
    jump am1131
label am1131:
    call message("Sora", "This messenger") from _call_message_4146
    call message("Sora", "feels different.") from _call_message_4147
    call message("Sora", "Probably because now I'm standing in the light.") from _call_message_4148
    call message("Sora", "[name]") from _call_message_4149
    call message("Sora", "I'm so happy I followed you and joined the RFA.") from _call_message_4150
    call screen phone_reply("And we'll be together forever, won't we?", "cd1164", "I'm also grateful that I got to meet you.", "cd1165")
label cd1164:
    call phone_after_menu from _call_phone_after_menu_507
    call message_start("[name]", "And we'll be together forever, won't we?") from _call_message_start_536
    call message("Sora", "Yes.") from _call_message_4151
    call message("Sora", "I...") from _call_message_4152
    jump am1132
label cd1165:
    call phone_after_menu from _call_phone_after_menu_508
    call message_start("[name]", "I'm also grateful that I got to meet you.") from _call_message_start_537
    call message("Sora", "Me too...") from _call_message_4153
    call message("Sora", "From now on") from _call_message_4154
    jump am1132
label am1132:
    call message("Sora", "I'll be with you") from _call_message_4155
    call message("Sora", "until the end of the world.") from _call_message_4156
    call message("Sora", "I should go standby.") from _call_message_4157
    call message("Sora", "I'll be right there with you.") from _call_message_4158
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_4159
    call phone_end from _call_phone_end_32
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "You've answered. Can you... can you come over to the window?"
    u "I'm outside right now."
    u "I'm so nervous... I came out to look up at the sky."
    u "Great cloudless day."
    menu:
        "I'm looking at it just now.":
            u "Then we both are looking at the same sky, aren't we?"
            jump u111
        "I'm looking at you, Sora.":
            u "No wonder I felt something behind my head. It was you watching me."
            u "Haha. I've looked up at the sky enough. Why don't you look up now?"
            jump u111
label u111:
    u "I don't know if I told you this."
    u "When I was little, I was caged in a room and wasn't permitted to go out. Mom wouldn't let us, afraid that father would send someone to harm us."
    u "I was always in our room but once in a while, my brother would take me out to see the outside world."
    u "One sweltering day, when I was depressed and sobbing for not being able to go out for weeks... like magic, my brother took me outside."
    u "I can still remember the sky I saw that day. With breezy winds and clouds slowly moving."
    u "I watched how the clouds moved, continuously changing and gathering together."
    u "...It was so fascinating that I thought I could watch the clouds all day."
    u "So, when if I grow up... I dreamt that I would be free and can look at the sky endlessly."
    u "And having enjoying ice cream that my brother bought me when it was hot."
    menu:
        "What do you dream of now?":
            u "Um... Want to guess.. what I dream of now?"
            u "I think you'll know what I'm dreaming of."
            menu:
                "Safely saving Zero?":
                    u "I hope that happens. Saving him safely and cutting off all connections to father."
                    u "Throwing away my past is also something that's important to me."
                    jump u112
                "Living on while being in love with me.":
                    u "That...just listening to that makes my heart full."
                    u "I want to spend all four seasons with you. If a year goes by, then another year with you... saying my love vows to you every year."
                    u "Like that, I'll always love you anew."
                    jump u112
        "Are you happy right now looking up at the sky?":
            u "Yes. I am."
            u "But it's not only because my childhood dream has come true."
            u "But being able to be with you, listening to your voice right now, and looking up at the sky... it's everything."
            u "Everything can become the reason. I'm glad that I'm here... happy that I was created."
            u "If you think about it... I understand the significance of freedom than anyone else."
            u "When I was free, I was always happy. I was miserable of the stability provided for not being free."
            jump u112
label u112:
    u "Thank you... For being under the same sky, for meeting me, and for being beside me. Thank you for telling me your true feelings to me."
    u "I'll treasure your heart... I treasure it as the most valuable thing to me."
    u "You are my paradise and angel. Thank you for saving me."
    u "I love you [name]."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "12:00" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene black with dissolve
    show J J11 at my_right with dissolve
    j "Initiating now!"
    show H H11 at my_left with dissolve
    j "And not a second of delay. Good."
    show U U314 with dissolve
    u "...Upload is complete."
    hide H with dissolve
    hide J with dissolve
    hide U with dissolve
    "(....)"
    scene black with dissolve
    play music "music/music24.ogg"
    g "Greetings. You're looking at a video that has been sent to every media company. And it will reveal something that every citizen deserves to know regarding a certain person's life."
    g "My name is Shun, a musical actor and a member of the RFA, a private fundraising organization."
    g "Previously, the RFA has hosted two charity parties to aid domestic and international charity works."
    g "However, today I am here to save and safeguard one of the members of the RFA, an innocent man who has been made victim to an outrageous kidnap."
    g "A member of the RFA, by the birth name Souta Choi, has been missing for the past two days."
    g "...He was kidnapped by his own father."
    g "...The reason why I stand here to announce this is because his father and the kidnapper is none other than Souga Choi, the current prime minister of Korea."
    g "Mr. Choi has twin sons he has kept hidden to this very day."
    g "Prime minister, sir, if you're watching this, we require you to release Souta Choi without any harm done."
    g "Otherwise...this would not be the end of our revelation, and we can guarantee you do not want to see it unfold."
    g "The RFA has already collected a vast amount of intelligence on you."
    g "If Souta Choi returns to us safe and sound, this will be our last and final broadcasting."
    g "Ladies and gentlemen, science has already proven their blood affiliation twice during the twins' childhood. Please refer to the image we have here."
    g "When there was no denying that the twins are his children, the prime minister attempted to eradicate all the evidence on a countless basis."
    g "The first attempt took place 22 years ago when he has just become a governmental official."
    scene bg19 with dissolve
    show Y Y17 at my_left with dissolve
    y "Shun is not nervous at all. He's doing great."
    show J J12 at my_right with dissolve
    j "The traffic is increasing at an incredible rate!"
    show U U32 with dissolve
    u "...I can see someone moving at the deserted house. Someone's coming out. I think he's calling someone urgently...."
    hide Y
    show H H15 at my_left
    h "Is there no sight of Levan?"
    show U U311
    u "No... Don't tell me he's actually dead..."
    show U U39
    menu:
        "That can't be...! Can't we send someone right now?":
            u "I'm sorry. No..."
            u "Not yet. It's too dangerous. We have to wait until the broadcasting's complete."
            jump u113
        "That's not possible... Please be safe...":
            u "That's right. He's stronger than me. He'll be safe..."
            jump u113
label u113:
    show J J12
    j "Sir, we've received a request for a phone conversation from the government."
    show H H13
    j "And several media companies we sent the files to are contacting as well!"
    show H H14
    h "...Put me through the governmental call first."
    j "Understood. But the amount of calls is increasing at an accelerated rate. So is the traffic."
    show U U32
    u "I'll watch over the server. No need to worry."
    hide H
    show Y Y14 at my_left
    y "Sayaka! I can help you."
    show J J14
    j "Oh, then I'll fetch you the manual."
    hide Y with dissolve
    hide J with dissolve
    show MC MC11 at my_right with dissolve
    "(...Everyone looks busy.)"
    show U U32
    u "...Everything is going as planned. Now if only he's still alive..."
    show U U310
    u "Thank you for staying here, [name]."
    hide MC with dissolve
    hide U with dissolve
    scene black with dissolve
    "……………"
    "……"
    "…"
    "(A week later)"
    news "...The entire nation is gripped in shock as everything was verified as bona fide history."
    news "As a result, the prime minister earned himself a warrant for arrest and lost all his privileges as a governmental official, a measurement taken at an unprecedented rate."
    news "Out of 96 foreign bank accounts, the former prime minister kept under apparently irrelevant person's name, according to files revealed by RFA, 42 have been confirmed to belong to him."
    news "The prosecution declared it will do everything in its power to track down the rest of the accounts."
    news "Choi has neither conceded nor denied the sources of his properties as reported by the RFA, claiming to instead disclose everything at the court."
    news "Souta Choi, Choi's kidnapped son, still remains missing. Choi is vetoing to provide any word on his whereabouts."
    news "Next up..."
    news "RFA, the heart of the recent storm of revelations, is now at the heart of the eyes of the entire nation."
    news "Ryou Han, the director of C&R currently serving as the temporary representative of this fundraising organization, announced that the company is undergoing reviews on recruiting members to work online."
    news "Some people upon the news started to criticize"
    news "that the company is utilizing such a national scandal as a mere viral news to raise popularity among the public."
    news "Notably, the twins revealed to be the former prime minister's illegitimate sons are growing more popular online, to the point no human imagination can fathom."
    news "Similarly, Shun the musical actor, also a member of the RFA, has gained explosive public popularity since his appearance on the video..."
    scene black with dissolve
    with Pause(1.0)
    "(.......)"
    scene bg8 with dissolve
    show U U32 at my_right with dissolve
    show MC MC11 with dissolve
    u "...Will he be able to return?"
    u "I'm sure he's still alive. Nobody found his body yet."
    show U U314
    u "We've survived like a piece of weed... He won't die so suddenly because of what happened."
    u "I've thought...that I'm so weak. But I was wrong. We're stronger than any person out there."
    u "When I was young, my brother told me that he can be stronger because I'm there for him. And he told me that's why I'm also strong like him..."
    show U U32
    u "Back then I didn't know what he meant. But now I think I do."
    menu:
        "Then are you going to make up with Levan...?":
            u "...I think at least I have to apologize for all those times of misunderstanding and hatred."
            jump u114
        "So are you strong because I'm here for you?":
            u "Exactly. You make me strong, like how I made him strong."
            u "I can be as strong as I can be, in order to protect you."
            jump u114
label u114:
    u "Remember what K said on the messenger? Souta became an informant to protect me from father."
    show U U33
    u "If that's true... Then my savior... I mean, Reina has lied to me."
    mc "Don't you think...we have to tell everyone that she's alive?"
    show U U32
    u "...I can't do that. Not for my sake only, that is."
    u "We'll have to tell them someday...but I'll need K's agreement."
    show U U314
    u "If he is okay with it, then I'll tell them everything."
    show U U32
    u "Because I think K is a victim as well..."
    play sound "music/dingdong.ogg"
    "(Ding-dong)"
    u "Oh...they're here."
    menu:
        "I'll get the door.":
            u "Oh, okay. Please."
            jump u115
        "Who is it?":
            y "It's us - !"
            jump u115
label u115:
    play sound "music/dooropen.ogg"
    "(Door opened.)"
    play music "music/music11.ogg"
    show Y Y17 at my_left with dissolve
    y "Okay, let's go."
    show Y Y13
    y "We're going to that house where Zero was locked up. We're going to look for evidence ourselves, remember?"
    show Y Y17
    y "Shun, Ryou, Sayaka... Everyone's waiting in the car."
    show Y Y19
    y "And I really need you to stop Shun and Ryou from having another war of bickering."
    show Y Y13
    y "We have to go now. We're supposed to be there when there are no people nearby."
    show Y Y14
    y "Oh, you do realize that house is now really hot among the public, don't you?"
    y "No one can trespass since that's the scene of a crime, but people just won't stop visiting. Now it's a popular tourist spot."
    show U U32
    u "...That's so weird. That house is just where we're from."
    show Y Y18
    y "The air is tasty, and so are the food in nearby restaurants. So says the SNS. Now hurry up! Let's go!"
    show U U38
    u "...Let's go, [name]."
    show Y Y19
    y "...You two won't let go of your hands, will you?"
    show U U310
    u "Nope."
    show Y Y11
    y "Ugh, lovebirds... Hmph. Hurry up!"
    hide Y with dissolve
    hide U with dissolve
    hide MC with dissolve
    scene bg4 with dissolve
    play sound "music/dooropen.ogg"
    "(...!)"
    show H H15 at my_left with dissolve
    h "I have a feeling we have a long journey ahead of us, Mr. Kim."
    show G G15 with dissolve
    g "Don't change the subject, CEO-in-line. Do you mind repeating yourself? What was that about the economy?"
    show H H13
    h "I've merely provided you a basic idea of economics."
    g "Are you saying I don't even have basic economic knowledge!? Huh?"
    show Y Y17 at my_right with dissolve
    y "Shun, here. Have a banana. It's organic."
    hide Y
    show J J14 at my_right
    j "Sora. [name]. Please have a seat. And Mr. Kim, could you please start the engine?!"
    hide H
    hide J
    hide G
    scene black with dissolve
    "(Car running)"
    "(........)"
    scene bg26 with dissolve
    show Y Y11 at my_right with dissolve
    y "...Is this really where Zero lived when he was young?"
    show H H12 at my_left with dissolve
    h "...Is this the drawing room?"
    show J J11 with dissolve
    j "No, this is the drawing room and the bedroom and the living room."
    show H H15
    h "I don't think I saw the kitchen..."
    j "You've just passed it. It's that corridor."
    hide H
    show G G15 at my_left
    g "Now this is an outrage! He had so much money hidden outside the country, and he made his own sons grow here?"
    g "He's not even human!"
    show J J15
    j "He must have wanted to deny that they're his own sons... That's such a tragedy."
    stop music
    hide J
    show U U36
    u "Huff... Ugh..."
    mc "Sora, are you okay?"
    u "Yeah..."
    u "Hah... It's just that I thought of my childhood... I think I'm choking a little."
    show U U311
    u "Could we....get out of here for a minute, [name]?"
    menu:
        "(Follow Sora.)":
            jump u116
        "What's wrong...?":
            u "I feel suffocated. And the air is suffocating, too...."
            jump u116
label u116:
    show Y Y14
    y "Feel free, Sora."
    show G G13
    g "Don't get too far. You don't want fans to catch sight of you."
    hide U with dissolve
    hide Y with dissolve
    hide G with dissolve
    scene bg16 with dissolve
    play music "music/music23.ogg"
    u "Huff... Huff... Hah..."
    u "Oh...the sky."
    u "When I was young, that tiny place was the only world I ever knew... Now I can walk out of there on my own."
    menu:
        "That's right. Now you're free, Sora.":
            u "Yeah... Maybe...if I had courage like Souta when I was young...maybe I could've been free"
            jump u117
        "Don't you think you should now put your tragic past behind?":
            u "...They're painful, and I want to forget about them all... But those memories are still part of me."
            u "I'm fine. I'm not afraid of anything if I'm with you... Nothing scares me."
            jump u117
label u117:
    u "Every single day was full of pain. I couldn't even dare make a breathing sound... But it turns out that tiny room was all there was for my childhood..."
    u "Now that I think about it...."
    menu:
        "Do you feel upset?":
            u "Kind of. But not anymore, I think. Because now I've met you."
            jump u118
        "Don't let your past haunt you.":
            u "Okay. I won't. Now I will enjoy my freedom as much as I can, right there waiting for me."
            u "Everything changed ever since I met you... It's all thanks to you."
            jump u118
label u118:
    mc "(Give him his bookmark.)"
    u "This is..."
    u "...You were keeping it for me."
    u "Yes... I will make sure my heart returns to where it was back then."
    scene white with dissolve
    u "I knew it. You're my angel, [name]. Now I remember."
    u "I wanted to see and feel everything in nature. That's what I really am."
    u "Souta will return. He will... He'll be worried about me."
    u "He'd want me to be happy...wherever he is."
    u "When I was researching about RFA for the first time, do you know what I thought when I read his baptismal name?"
    u "I thought he was making fun of me. Levan... I thought he got such name because he forgot about me."
    u "But now I know. I think he took such name in determination..."
    u "That he will never let darkness take over him... That he'll never be like father."
    u "...He won't let this be the end of him. I won't let this be the end of me, either."
    u "I'm sure we have much more time ahead of us, with much more things to enjoy...."
    u "[name]... What do you think will happen to us now?"
    menu:
        "Happiness will find us.":
            u "I want to make you happy....much happier than you can imagine."
            jump u119
        "I think we have challenges and pain ahead of us.":
            u "...Then let me take them on. I'll protect you, [name]."
            jump u119
        "I think you'll love me so much.":
            u "...My heart has already started at the very moment I met you, [name]."
            jump u119
label u119:
    u "[name], I can do whatever you want. I'll do anything to protect you..."
    u "I'm still amazed that I am free to think and act on my own..."
    u "Every single moment I spend with you is like magic to me."
    u "My angel, my miracle... [name]... I'll love you forever."
    scene white with dissolve
    with Pause(2.0)
    scene goodend with dissolve
    with Pause(3.0)
    "........."
    "......"
    scene black with dissolve
    "............"
    "(A year and a half ago)"
    scene bg17 with dissolve
    play music "music/music9.ogg"
    show R R13
    r "Ma'am... From now on, I'll keep Sora safe at the cathedral."
    show R R12
    r "I'll bring him back to you once you're fine."
    show R R13
    mother "What...? Who do you think you are to take my kid!? Where's Sora!?"
    r "I don't believe you'd noticed, but Sora's got talents. I will teach him to be an excellent person."
    mother "You... You lied to me, didn't you? You lied to me that he needs to go to the cathedral! Are you one of their father's sidekicks!?"
    r "It's no good to neglect children like you do. And..."
    show R R12
    r "I'm sure staying drunk all the time is disadvantageous to maintaining your custodial right."
    mother "Custodial right!? You!! You're here to kill Sora, aren't you!?"
    show R R17
    r "...Let me go! Ack!"
    mother "Souta's already gone, and now you're going to take Sora, too...!? Like that's gonna happen! Don't even think about it!"
    show R R19
    r "W-wait... Please, put that down. Let's talk, ma'am."
    r "A-alright. I'll bring Sora to you. So please, put that down... P-please, stay where you are, ma'am."
    mother "Did you already kill him? Huh? You made Souta a goner..."
    mother "That's right. Maybe making one of his gang a goner would teach that man a lesson!"
    mother "You'd never understand unless you get to suffer in our shoes!"
    hide R with dissolve
    scene bg27 with dissolve
    "(...!)"
    r "...Ugh... Uh..."
    r "Ugh..."
    r "No...no... I... It wasn't me. I didn't...do this..."
    r "No, no!!"
    r "...That's right. I must go protect Sora. He's been abandoned."
    r "I must take him in my bosom. He's been expelled... Ugh..."
    scene black with dissolve
    "......"
    "............"
    scene bg26 with dissolve
    show R R33 at my_right with dissolve
    r "......So now...this place is deserted."
    r "We must get rid of this place. This is the nest of those boy's agony."
    show V V113 at my_left with dissolve
    v "Is there a reason why you wanted to visit this place for the last time?"
    r "This is where those children suffered..."
    show V V114
    show R R32
    r "But this is also where my devil was begotten."
    show V V113
    show R R33
    v "In that case....I will incinerate this place myself, Reina."
    stop music
    scene black with dissolve
    with Pause(2.0)
    play music "music/music1-25.ogg"
    show text "THE END" with dissolve
    with Pause(2.0)
    show text "Resources used:" with dissolve
    with Pause(2.0)
    show text "Light Blue Rose Cursor by JEricaM" with dissolve
    with Pause(2.0)
    show text "SFC from http://soundbible.com" with dissolve
    with Pause(2.0)
    show text "Phone template by SolidKakadu" with dissolve
    with Pause(2.0)
    show text "Hand-painted lily of the valley by http://pngtree.com" with dissolve
    with Pause(2.0)
    show text "Background images by Pexels, Unsplash, Pixabay" with dissolve
    with Pause(2.0)
    show text "Music:" with dissolve
    with Pause(1.0)
    show text "Johhny Easton - Flow of Life" with dissolve
    with Pause(2.0)
    show text "Lion Free Music - Nail Biter" with dissolve
    with Pause(2.0)
    show text "VN Audio Pack composer/sound designer - Tim Reichert" with dissolve
    with Pause(2.0)
    show text "Helen Jane Long - Frog" with dissolve
    with Pause(2.0)
    show text "Mattia Cupelli - Piano Solo #1 RF" with dissolve
    with Pause(2.0)
    show text "Jens Kiilstofte - Damnation" with dissolve
    with Pause(2.0)
    show text "Whitesand - Infinite" with dissolve
    with Pause(2.0)
    show text "Nicolas Picard - Sad piano song/Dramatic piano" with dissolve
    with Pause(2.0)
    show text "Jonny Easton - Forever Memory" with dissolve
    with Pause(2.0)
    show text "Wayve - Searching" with dissolve
    with Pause(2.0)
    show text "95TurboSol - Above The Clouds" with dissolve
    with Pause(2.0)
    show text "Ross Bugden - Rapture" with dissolve
    with Pause(2.0)
    show text "Machninimasound - Ralyying The Defense" with dissolve
    with Pause(2.0)
    show text "Whitesand - Drops" with dissolve
    with Pause(2.0)
    show text "LyricsandInc - Romantic Music Instrumental" with dissolve
    with Pause(2.0)
    show text "Thank You for playing!" with dissolve
    with Pause(2.0)
    $ MainMenu(confirm=False)()
