label day9:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 9" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "01:29" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music19.ogg"
    call phone_start from _call_phone_start_6
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_76
    call message("Sora", "Hey") from _call_message_699
    call message("Sora", "little toy.") from _call_message_700
    call message("Sora", "You're not") from _call_message_701
    call message_img("Sora", "asleep yet.", "images/questioning.png") from _call_message_img_39
    call message("Sora", "Answer me.") from _call_message_702
    call message("Sora", "I know you're not sleeping.") from _call_message_703
    call message("Sora", "I can see you through the camera.") from _call_message_704
    call screen phone_reply("...The savior is the same person as 'Reina,' isn't she? The one who supposedly committed suicide?", "cd91", "I'm too hungry to sleep....", "cd92")
label cd91:
    call phone_after_menu from _call_phone_after_menu_72
    call message_start("[name]"," ...The savior is the same person as 'Reina,' isn't she? The one who supposedly committed suicide?") from _call_message_start_77
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "I thought I told you...") from _call_message_705
    call message("Sora", "I'm") from _call_message_706
    call message("Sora", "not here") from _call_message_707
    call message_img("Sora", "to answer your questions ^^", "images/expecting.png") from _call_message_img_40
    call message("Sora", "And") from _call_message_708
    call message("Sora", "don't you dare") from _call_message_709
    call message("Sora", "try to dig up something") from _call_message_710
    call message("Sora", "about my savior....") from _call_message_711
    call message("Sora", "Maybe") from _call_message_712
    call message_img("Sora", "you're not hungry enough.", "images/questioning.png") from _call_message_img_41
    jump am91
label cd92:
    call phone_after_menu from _call_phone_after_menu_73
    call message_start("[name]"," I'm too hungry to sleep....") from _call_message_start_78
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Ah") from _call_message_713
    call message("Sora", "I heard that's the case when people starve hahahahaha") from _call_message_714
    call message("Sora", "I thought you'd be hungry since you were tossing and turning for a while now lololololol") from _call_message_715
    call message("Sora", "Still it's weird that you can't sleep.") from _call_message_716
    call message("Sora", "I wonder what will happen") from _call_message_717
    call message("Sora", "if you starve tomorrow too.") from _call_message_718
    jump am91
label am91:
    call message_img("Sora", "Don’t you want to have this?", "images/cg40.png") from _call_message_img_42
    call screen phone_reply3("...You can't torment me. It won't work.", "cd93", "You should eat well.... You're so thin.", "cd94", "I want it....", "cd95")
label cd93:
    call phone_after_menu from _call_phone_after_menu_74
    call message_start("[name]", "...You can't torment me. It won't work.") from _call_message_start_79
    call message("Sora", "Tormenting you with food won't work?") from _call_message_719
    call message("Sora", "Oh so you decided") from _call_message_720
    call message("Sora", "to take this opportunity to go on a diet") from _call_message_721
    call message("Sora", "because you're gonna have to do it anyway? Hahaha") from _call_message_722
    call message("Sora", "Let's see") from _call_message_723
    call message_img("Sora", "how long you can take it hehe", "images/expecting.png") from _call_message_img_43
    jump am92
label cd94:
    call phone_after_menu from _call_phone_after_menu_75
    call message_start("[name]", "You should eat well.... You're so thin.") from _call_message_start_80
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Who do you think you are") from _call_message_724
    call message_img("Sora", "to care about me?", "images/questioning.png") from _call_message_img_44
    call message("Sora", "You think Ren is still in me?") from _call_message_725
    call message("Sora", "Stop caring.") from _call_message_726
    call message("Sora", "Now I feel so weird.") from _call_message_727
    jump am92
label cd95:
    call phone_after_menu from _call_phone_after_menu_76
    call message_start("[name]", "I want it....") from _call_message_start_81
    call message("Sora", "If you do") from _call_message_728
    call message("Sora", "you should behave.") from _call_message_729
    call message("Sora", "Do you mind trying...?") from _call_message_730
    call message("Sora", "I really don't feel like") from _call_message_731
    call message("Sora", "giving you anything to eat....") from _call_message_732
    jump am92
label am92:
    call message("Sora", "Seriously") from _call_message_733
    call message_img("Sora", "you're hopeless.", "images/well.png") from _call_message_img_45
    call message("Sora", "And I think") from _call_message_734
    call message("Sora", "my savior also found out") from _call_message_735
    call message("Sora", "after talking to you") from _call_message_736
    call message("Sora", "that you're hopeless") from _call_message_737
    call message("Sora", "with that stupid head of yours....") from _call_message_738
    call message("Sora", "She told me") from _call_message_739
    call message("Sora", "to get rid of you") from _call_message_740
    call message("Sora", "as soon as I get tired of you -") from _call_message_741
    call message("Sora", "How bad did you neglect them?") from _call_message_742
    call message("Sora", "They're not even interested in you") from _call_message_743
    call message("Sora", "even when you've lost contact with them.") from _call_message_744
    call message("Sora", "Are you sure") from _call_message_745
    call message_img("Sora", "you were one of them? ^^", "images/smile.png") from _call_message_img_46
    call message("Sora", "And") from _call_message_746
    call message("Sora", "one more thing,") from _call_message_747
    call message("Sora", "Ren sucks too.") from _call_message_748
    call message("Sora", "He chose someone like you lol") from _call_message_749
    call message("Sora", "I should change the entire system") from _call_message_750
    call message("Sora", "he came up with.") from _call_message_751
    call message("Sora", "Hey") from _call_message_752
    call message("Sora", "I think") from _call_message_753
    call message("Sora", "the RFA is going to surrender to me.") from _call_message_754
    call message("Sora", "What do you think? Hehehe.") from _call_message_755
    call screen phone_reply3("That's not true.", "cd96", "It's only because Levan is in trouble. They won't surrender.", "cd97", "Then you'd be able to show how strong you are.", "cd98")
label cd96:
    call phone_after_menu from _call_phone_after_menu_77
    call message_start("[name]", "That's not true.") from _call_message_start_82
    call message("Sora", "Why do you believe that?") from _call_message_756
    call message_img("Sora", "You don't have a proof that I'm wrong lol", "images/smile.png") from _call_message_img_47
    call message("Sora", "If it were Ryou Han") from _call_message_757
    call message("Sora", "he would've been so disappointed with you.") from _call_message_758
    jump am93
label cd97:
    call phone_after_menu from _call_phone_after_menu_78
    call message_start("[name]"," It's only because Levan is in trouble. They won't surrender.") from _call_message_start_83
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Trouble?") from _call_message_759
    call message_img("Sora", "You mean the fact that he did something the prosecution didn't like? Hehehe.", "images/questioning.png") from _call_message_img_48
    call message("Sora", "That traitor") from _call_message_760
    call message("Sora", "is walking on a path to destruction all by himself.") from _call_message_761
    call message("Sora", "I knew it.") from _call_message_762
    call message("Sora", "The savior was right....") from _call_message_763
    jump am93
label cd98:
    call phone_after_menu from _call_phone_after_menu_79
    call message_start("[name]", "Then you'd be able to show how strong you are.") from _call_message_start_84
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "You're saying that on purpose") from _call_message_764
    call message("Sora", "to make me feel better, aren't you?") from _call_message_765
    call message("Sora", "You're an idiot") from _call_message_766
    call message("Sora", "but that was kind of smart.") from _call_message_767
    jump am93
label am93:
    call message_img("Sora", "hehehe", "images/expecting.png") from _call_message_img_49
    call message("Sora", "Once I take over the RFA") from _call_message_768
    call message("Sora", "we'll make them one of us, won't we?") from _call_message_769
    call message("Sora", "And then") from _call_message_770
    call message("Sora", "they'll find out") from _call_message_771
    call message("Sora", "that you're nothing") from _call_message_772
    call message("Sora", "but a useless toy hahahaha") from _call_message_773
    call screen phone_reply("The RFA will never be part of the Ao Me.", "cd99", "Doesn't matter... As long as someone recognizes that I'm useful.", "cd910")
label cd99:
    call phone_after_menu from _call_phone_after_menu_80
    call message_start("[name]", "The RFA will never be part of the Ao Me.") from _call_message_start_85
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "We'll accept those who agree with us as believers") from _call_message_774
    call message("Sora", "and we'll cleanse those who don't.") from _call_message_775
    call message("Sora", "That's the simple rule of the Ao Me.") from _call_message_776
    call message("Sora", "Looks like you didn't realize it yet lol") from _call_message_777
    jump am94
label cd910:
    call phone_after_menu from _call_phone_after_menu_81
    call message_start("[name]", "Doesn't matter... As long as someone recognizes that I'm useful.") from _call_message_start_86
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Hahahaha") from _call_message_778
    call message("Sora", "you're desperate.") from _call_message_779
    call message("Sora", "I wonder what you'll look like") from _call_message_780
    call message("Sora", "when the members of the RFA you were once part of") from _call_message_781
    call message_img("Sora", "treat you like a useless toy.", "images/smile.png") from _call_message_img_50
    call message("Sora", "That will be fun haha") from _call_message_782
    jump am94
label am94:
    call message("Sora", "Oh") from _call_message_783
    call message("Sora", "that's right") from _call_message_784
    call message("Sora", "your face was so fun") from _call_message_785
    call message("Sora", "when you were flustered to see me last night -") from _call_message_786
    call message("Sora", "It was so fun") from _call_message_787
    call message("Sora", "since you couldn't do anything to me") from _call_message_788
    call message("Sora", "because my face") from _call_message_789
    call message("Sora", "looked exactly like") from _call_message_790
    call message("Sora", "your prince Ren.") from _call_message_791
    call screen phone_reply3("Though you look the same, I know that you're not him.", "cd911", "Now I don't miss Ren anymore.", "cd912", "I know that Ren is still in there.", "cd913")
label cd911:
    call phone_after_menu from _call_phone_after_menu_82
    call message_start("[name]", "Though you look the same, I know that you're not him.") from _call_message_start_87
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "If you know that") from _call_message_792
    call message("Sora", "why did you look like you didn't know what to do?") from _call_message_793
    call message("Sora", "Be honest.") from _call_message_794
    call message("Sora", "You still have") from _call_message_795
    call message("Sora", "that useless hope inside") from _call_message_796
    call message_img("Sora", "don't you...?", "images/expecting.png") from _call_message_img_51
    call message("Sora", "Don't you...?") from _call_message_797
    call message("Sora", "I'm going to see you again.") from _call_message_798
    jump am95
label cd912:
    call phone_after_menu from _call_phone_after_menu_83
    call message_start("[name]", "Now I don't miss Ren anymore.") from _call_message_start_88
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "?") from _call_message_799
    call message_img("Sora", "You already forgot about him? Lololololol", "images/questioning.png") from _call_message_img_52
    call message("Sora", "Agh") from _call_message_800
    call message("Sora", "but it won't be fun anymore if you forgot him....") from _call_message_801
    call message("Sora", "Should I find out if that's true?") from _call_message_802
    jump am95
label cd913:
    call phone_after_menu from _call_phone_after_menu_84
    call message_start("[name]", "I know that Ren is still in there.") from _call_message_start_89
    call message("Sora", "Hahahahaha") from _call_message_803
    call message("Sora", "fine") from _call_message_804
    call message("Sora", "if you wish lolololol") from _call_message_805
    call message("Sora", "You can't hate me") from _call_message_806
    call message_img("Sora", "no matter what I do to you.", "images/smile.png") from _call_message_img_53
    call message("Sora", "What an airhead hahahaha") from _call_message_807
    jump am95
label am95:
    call message("Sora", "I should make you make the same face again....") from _call_message_808
    call message("Sora", "Oh") from _call_message_809
    call message("Sora", "oh") from _call_message_810
    call message("Sora", "oh wait") from _call_message_811
    call message("Sora", "maybe I should try to play") from _call_message_812
    call message_img("Sora", "a different trick today.", "images/expecting.png") from _call_message_img_54
    call message("Sora", "Stay right there hehe") from _call_message_813
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_814
    call phone_end from _call_phone_end_7
    stop music
    scene black with dissolve
    scene bg7 with dissolve
    show MC MC14 at my_right with dissolve
    play music "music/music19.ogg"
    play sound "music/dooropen.ogg"
    "(Door Opened)"
    show U U21 at my_left with dissolve
    u "Princess - Ren's face is here."
    show U U24
    u "Rise up and welcome me, will you? I know you want to see your prince's face."
    u "Don't pretend to be asleep. I saw you flinching. Are you tired of Ren already?"
    show U U22
    u "Hmm - I thought your love for Ren was real. You betray him in mere days."
    show U U24
    menu:
        "Do not mock me....":
            u "Haha! That's right. You should keep loving him. It turns out losers won't let go of their feelings so easily."
            jump u91
        "You're not Ren.":
            u "But Ren used to occupy my body. Don't we give out similar vibe? At least we look the same."
            jump u91
label u91:
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    u "Look at me. Here's Ren's face. The face you love so much."
    u "Isn't there a list of things you want to do to him? Like a kiss..."
    u "What were you thinking?"
    u "Where did you let those stupid imaginations of yours take you?"
    u "What? Are you too shy to tell me?"
    u "You're a toy. You shouldn't be feeling shy.... You still don't know your place."
    u "Or is it because you're too stupid to remember?"
    u "You're too stupid for me to play with. You're no fun. Should I just throw you away?"
    u "I can throw you away after playing with you in the basement."
    u "You should entertain me if you don't want to end up there."
    u "Now that I've explained it to you nicely, don't tell me you still don't get it."
    u "I'll pretend to be Ren. So do that thing again."
    u "You know, what you did in the garden. Come on."
    u "If you waste more time, something no fun at all will take place...."
    u "I'm Ren, you stupid toy... Treat me like Ren, will you?"
    menu:
        "(Push Sora away.)":
            scene bg7 with dissolve
            "(...!)"
            "....."
            jump u92
        "(Do as he says.)":
            u "Heh..."
            scene white with dissolve
            u "....Ugh!"
            scene bg7 with dissolve
            "......"
            jump u92
label u92:
    stop music
    scene bg7 with dissolve
    show MC MC15 at my_right with dissolve
    show U U26 at my_left with dissolve
    u "....My head."
    show U U25
    menu:
        "Sora...?":
            u "...Huff... It hurts!"
            jump u93
        "Ren...!?":
            u "Don't you dare say that name!"
            jump u93
label u93:
    show U U26
    play music "music/music14.ogg"
    u "Don't...don't come any closer..."
    show U U212
    u "Huff... Huff... Ugh..."
    show U U213
    u_who "...Where...where...?"
    show U U212
    u_who "...Stop."
    show U U213
    u_who "Stop it. Please stop it..."
    menu:
        "What's wrong?":
            jump u94
        "Ren?":
            jump u94
label u94:
    show U U212
    ray "...Please leave me alone."
    ray "....I'll just stay silent like a grave. I'll do anything...."
    show U U213
    ray "So, please... Leave me alone. Stop it...."
    menu:
        "Ren, it's me..! [name]! What happened to you?":
            jump u95
        "Ren, why did you leave me alone?":
            jump u95
label u95:
    show MC MC17
    show U U212
    ray "...I want to be alone. I want to be alone!"
    ray "Please leave me alone.... I don't want this anymore...."
    show U U213
    menu:
        "Ren... You're back, aren't you?":
            ray "...Uh... You're..."
            jump u96
        "Don't run away! Save me! I'm the one in pain!":
            ray "Pain...?"
            jump u96
label u96:
    show U U212
    ray "You're...ugh... Ugh...."
    ray "My head...is aching...."
    show U U213
    ray "[name]... What have I done to you...?"
    hide U with dissolve
    hide MC with dissolve
    scene bg7 with dissolve
    ray "Oh, no... No! How could I ever do this to you? No...!"
    ray "[name]... I'm so sorry. This is all my fault!"
    ray "Don't forgive me. Just throw me away right now before he comes back!"
    ray "[name]. I'm sorry. I'm sorry. I'm so sorry. What do I do? What should I do?"
    ray "I hurt you. I made you shiver in pain... No...."
    ray "Are you alright? Didn't it hurt? Why are you so thin...?"
    ray "I can't believe this..... I'll never forgive myself...."
    menu:
        "It wasn't you, Ren. It was Sora... Are you back now?":
            ray "...I don't know. I might be dragged back into the abyss. So please, just... Ugh..."
            jump u97
        "This is too painful...":
            ray "This is all my fault... I'm sorry. I'm sorry...!"
            jump u97
label u97:
    ray "Ugh... My head is killing me.... I think I'll pass out any moment...."
    ray "Huff... [name]... Leave me here...and go."
    ray "Ren...wasn't supposed to last from the beginning."
    ray "I was in denial of my weaknesses... Ren...was created by bunching up those weaknesses..."
    menu:
        "You're not weak. You're the kindest person I have ever met...! Please don't go....":
            ray "Ever since I met you....I thought maybe I can stay.. But...."
            jump u98
        "Then is Sora the real you?":
            ray "Yes... The weak me must disappear...."
            jump u98
label u98:
    ray "Sora is my real name... Ren never existed in the first place...."
    ray "I'm...Sora. And...this devilish side...is also a part of me."
    ray "[name]......"
    ray "Both Ren and Sora...are scared."
    ray "[name]....the only time when I wasn't scared...was when I was with y..."
    ray "Ugh...! Huff...."
    scene bg7 with dissolve
    show MC MC15 at my_right with dissolve
    show U U212 at my_left with dissolve
    ray "Stay away from me. You have to stay away..!"
    show U U213
    ray "Huff...no. No... Huff...."
    show U U212
    ray "Ugh...!"
    hide U with dissolve
    "(Ren burst out of the room.)"
    stop music
    hide MC with dissolve
    scene black with dissolve
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "Hello..."
    ray "I know I don't have the right to talk with you but... I had to call before Sora returns..."
    ray "Since I can't meet you in person... I... won't be able to torment you..."
    menu:
        "Is it really you, Ren?":
            ray "I don't know myself too.... Feels like all my memories as mixed up..."
            ray "Memories of being harsh and saying bad things to you keep coming to me... it's horrible..."
            ray "Me hurting you.... I'm sorry, so sorry. I'm really sorry...!"
            jump u99
        "Ren, don't cry...":
            ray "Listening to your voice is going to make me cry... but this isn't the time for that."
            ray "I missed you... and... I'm sorry for being so weak."
            jump u99
        "I don't need you Ren, put Sora on the phone.":
            ray "I'm sorry for being weak and being a moron... sorry for not being strong... sorry..."
            ray "I'll try to disappear before you as fast as I can... I'm truly sorry..."
            jump u99
label u99:
    ray "...I learned when Sora awoke."
    ray "That you're out of my league for someone like me... that I should let go of you for your sake..."
    ray "but... I'm powerless. I'm so weak...!"
    ray "So....do whatever you can to get out of here right now... Leave me and Ao Me! Before Sora returns..."
    menu:
        "I'm going to leave this place with you, Ren.":
            ray "No, I'm already... an abandoned personality. No need to save someone like me. You should cherish yourself, [name]."
            ray "Don't get sick but stay happy. That's my last wish...!"
            jump u910
        "How can you guarantee that I'll be safe even if I leave now?":
            ray "I'll secure an escape route by hacking the lock code right now."
            ray "Get on moving! Run away as far as you can. Where I won't be able to reach you..."
            ray "... I'm sorry for not being strong enough to stay by your side."
            jump u910
label u910:
    ray "*Sobs* My... head..."
    ray "Agh... My head... is...."
    show MC MC15
    menu:
        "Ren, are you okay? Hang in there!":
            jump u911
        "Sora, are you back?":
            ray "Argh..."
            jump u911
label u911:
    ray "...[name], runaway from Sora..."
    ray "...ahh... survive... from here and escape..."
    ray "he's... again.... aargh...!"
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "03:37" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music20.ogg"
    call phone_start from _call_phone_start_7
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_90
    call screen phone_reply("Ren...!?", "cd914", "Sora...", "cd915")
label cd914:
    call phone_after_menu from _call_phone_after_menu_85
    call message_start("[name]", "Ren...!?") from _call_message_start_91
    call message("Sora", "Don't call that name!!!") from _call_message_815
    jump am96
label cd915:
    call phone_after_menu from _call_phone_after_menu_86
    call message_start("[name]", "Sora...") from _call_message_start_92
    jump am96
label am96:
    call message("Sora", "What have you done to me!?") from _call_message_816
    call message("Sora", "That airhead will never come back.") from _call_message_817
    call message("Sora", "That loser will never come back.") from _call_message_818
    call message("Sora", "That weakling will never come back!!") from _call_message_819
    call screen phone_reply("But you were Ren back then...!", "cd916", "You're right, Sora....", "cd917")
label cd916:
    call phone_after_menu from _call_phone_after_menu_87
    call message_start("[name]", "But you were Ren back then...!") from _call_message_start_93
    call message("Sora", "No") from _call_message_820
    call message("Sora", "that is not true!") from _call_message_821
    jump am97
label cd917:
    call phone_after_menu from _call_phone_after_menu_88
    call message_start("[name]", "You're right, Sora....") from _call_message_start_94
    call message("Sora", "Ren must never come back!!") from _call_message_822
    jump am97
label am97:
    call message("Sora", "This body cannot last without me.") from _call_message_823
    call message("Sora", "That weak skeleton") from _call_message_824
    call message("Sora", "will always let people hurt him.") from _call_message_825
    call message("Sora", "He'll always be scared and shiver.") from _call_message_826
    call message("Sora", "But I won't.") from _call_message_827
    call message("Sora", "I'm nothing like him!") from _call_message_828
    call screen phone_reply3("Sora, don't be scared... Everything will be alright.", "cd918", "I think you're right... Don't let Ren beat you.", "cd919", "Give me back Ren...! You're not real!", "cd920")
label cd918:
    call phone_after_menu from _call_phone_after_menu_89
    call message_start("[name]", "Sora, don't be scared... Everything will be alright.") from _call_message_start_95
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Do I look scared to you?") from _call_message_829
    call message("Sora", "No.") from _call_message_830
    call message("Sora", "I'm not scared") from _call_message_831
    call message("Sora", "of anything!!") from _call_message_832
    call message("Sora", "Nothing scares me!!") from _call_message_833
    call message("Sora", "You should be the one") from _call_message_834
    call message("Sora", "scared of me.") from _call_message_835
    jump am98
label cd919:
    call phone_after_menu from _call_phone_after_menu_90
    call message_start("[name]", "I think you're right... Don't let Ren beat you.") from _call_message_start_96
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Of course I'm right!") from _call_message_836
    jump am98
label cd920:
    call phone_after_menu from _call_phone_after_menu_91
    call message_start("[name]", "Give me back Ren...! You're not real!") from _call_message_start_97
    call message("Sora", "You....") from _call_message_837
    call message_img("Sora", "I'm going to get you for this.", "images/questioning.png") from _call_message_img_55
    call message("Sora", "Not real??") from _call_message_838
    call message("Sora", "If I'm not real") from _call_message_839
    call message("Sora", "Ren is not real too!!") from _call_message_840
    jump am98
label am98:
    call message("Sora", "My rage is my shield.") from _call_message_841
    call message("Sora", "If I don't get angry") from _call_message_842
    call message("Sora", "if I don't have my rage") from _call_message_843
    call message("Sora", "I cannot survive with this body.") from _call_message_844
    call message("Sora", "Do you get it?") from _call_message_845
    call message("Sora", "I'm right.") from _call_message_846
    call message("Sora", "I'm the right one.") from _call_message_847
    call message("Sora", "I'm the right one") from _call_message_848
    call message("Sora", "to stay!") from _call_message_849
    call screen phone_reply3("Sora, you're just trying to protect yourself. I know it...!", "cd921", "Let's just calm down. Where is Ren?", "cd922", "You're right. More than anyone else...", "cd923")
label cd921:
    call phone_after_menu from _call_phone_after_menu_92
    call message_start("[name]", "Sora, you're just trying to protect yourself. I know it...!") from _call_message_start_98
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Don't pretend that you understand me.") from _call_message_850
    call message("Sora", "Don't tell me...") from _call_message_851
    call message("Sora", "Are you") from _call_message_852
    call message_img("Sora", "pitying me?", "images/questioning.png") from _call_message_img_56
    jump am99
label cd922:
    call phone_after_menu from _call_phone_after_menu_93
    call message_start("[name]", "Let's just calm down. Where is Ren?") from _call_message_start_99
    call message("Sora", "He's dead!") from _call_message_853
    call message("Sora", "He'll never be back!!") from _call_message_854
    call message("Sora", "I killed him.") from _call_message_855
    call message("Sora", "I killed him....") from _call_message_856
    call message("Sora", "Do you get it?") from _call_message_857
    jump am99
label cd923:
    call phone_after_menu from _call_phone_after_menu_94
    call message_start("[name]", "You're right. More than anyone else...") from _call_message_start_100
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "You don't have to tell me that!") from _call_message_858
    call message("Sora", "But...") from _call_message_859
    jump am99
label am99:
    call message("Sora", "Hah") from _call_message_860
    call message("Sora", "I don't like this.") from _call_message_861
    call message("Sora", "Hey") from _call_message_862
    call message("Sora", "[name]") from _call_message_863
    call message("Sora", "I don't like you.") from _call_message_864
    call message("Sora", "You thought you were so great") from _call_message_865
    call message("Sora", "and you actually woke that skeleton") from _call_message_866
    call message("Sora", "and chatted with him.") from _call_message_867
    call message("Sora", "You're nothing but a useless toy!") from _call_message_868
    call message("Sora", "You think") from _call_message_869
    call message("Sora", "I was joking") from _call_message_870
    call message("Sora", "when I said I'll throw you out?") from _call_message_871
    call screen phone_reply3("Please don't throw me away!", "cd924", "Ren will be back again. You're scared, aren't you?", "cd925", "I'm not the reason why you're suffering right now... You're tormenting yourself.", "cd926")
label cd924:
    call phone_after_menu from _call_phone_after_menu_95
    call message_start("[name]", "Please don't throw me away!") from _call_message_start_101
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "It's already over...") from _call_message_872
    call message("Sora", "This is all your fault....") from _call_message_873
    jump am910
label cd925:
    call phone_after_menu from _call_phone_after_menu_96
    call message_start("[name]", "Ren will be back again. You're scared, aren't you?") from _call_message_start_102
    call message("Sora", "Be quiet.") from _call_message_874
    call message("Sora", "I have nothing more to talk to you.") from _call_message_875
    jump am910
label cd926:
    call phone_after_menu from _call_phone_after_menu_97
    call message_start("[name]", "I'm not the reason why you're suffering right now... You're tormenting yourself.") from _call_message_start_103
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "You're not my teacher.") from _call_message_876
    call message("Sora", "Don't you dare") from _call_message_877
    call message("Sora", "give that stupid idea") from _call_message_878
    call message("Sora", "to me.") from _call_message_879
    jump am910
label am910:
    call message("Sora", "I'll go tell savior") from _call_message_880
    call message("Sora", "to get rid of you right now....") from _call_message_881
    call message("Sora", "You're dead.") from _call_message_882
    call message("Sora", "The end.") from _call_message_883
    call message("Sora", "Get lost") from _call_message_884
    call message("Sora", "from this world forever") from _call_message_885
    call message("Sora", "with that pathetic prince of yours....!") from _call_message_886
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_887
    call phone_end from _call_phone_end_8
    stop music
    scene black with dissolve
    scene bg7 with dissolve
    show MC MC14 at my_right with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    menu:
        "...Who is it?":
            "...."
            mc "(Open the door.)"
            jump v91
        "(Open the door.)":
            jump v91
label v91:
    play sound "music/dooropen.ogg"
    "(Door opened)"
    whooo "...Shh. Quiet."
    play music "music/music21.ogg"
    show V V210 at my_left with dissolve
    show MC MC15
    menu:
        "K...!?":
            v "Yes, it's me... Quiet...."
            jump u9200
        "Who are you!?":
            v "It's me. K... Keep your voice down, please."
            jump u9200
label u9200:
    show V V29
    show MC MC14
    v "Oh, no.... You look terrible. Have you been eating properly?"
    v "Here... Have some of this. Please help yourself."
    show V V22
    v "I've been waiting for my opportunity all this time, and I was finally able to come."
    v "I was able to blend in among other believers to get here. I almost couldn't make it."
    show V V23
    v "However...I'm afraid it's impossible to take you out of this place."
    show V V219
    v "They have doubled the security to watch over your room."
    v "I'm so sorry.... I thought you were on good terms with Sora..."
    show V V23
    v "The rumors told me you are going through misfortunes you definitely do not deserve."
    show V V219
    v "And I see that the rumors were true...."
    show V V23
    v "I am sorry I am late."
    show V V24
    menu:
        "It's Sora. Ren's second persona. He's the one tormenting me.":
            v "...Second persona? I didn't know Sora would be suffering such..."
            v "Oh, so that's why his attack pattern has changed."
            v "You've suffered unspeakable agony, haven't you?"
            v "Everyone at the RFA is worried about you."
            v "You suddenly lost connection ever since I announced that the party will be on a hold...."
            jump u9300
        "Why have you come? You can't even rescue me.":
            v "I was worried about you."
            v "And I was concerned that the attack pattern on the messenger suddenly changed...."
            v "Most of all, you lost connection ever since I announced that the party will be on a hold."
            jump u9300
label u9300:
    show V V29
    v "Everyone's worried about you...."
    show V V23
    v "If I had known that you were being treated like this...."
    v "I would have never come alone... I'm sorry."
    menu:
        "I won't be able to survive....":
            v "Hah.... I will make sure that does not happen. No matter what."
            jump u9400
        "Do you know a way to bring back Ren?":
            v "I....I'm afraid I don't know exactly what Reina has done to him."
            jump u9400
label u9400:
    show V V218
    v "I'm starting to despise myself. Apparently, I can help you with nothing as of now...."
    show V V27
    v "It's a karma I deserve. I created a secret I couldn't control, and this is where it has brought me...."
    show V V29
    v "Since things have taken for the worst, I'll tell you everything."
    show V V23
    v "It's all my fault something like this has befallen you."
    show V V210
    v "Remember that Reina, my fiancee, has committed suicide?"
    v "That's what I told the members of the RFA."
    show V V23
    v "However...that's not true at all."
    show V V219
    menu:
        "What...?":
            jump u9500
        "Why did you keep that a secret?":
            v "I thought I should hide the truth in order to protect all."
            jump u9500
label u9500:
    show V V23
    v "Reina was brilliant on the surface, but...she contained dark complications within."
    show V V22
    v "I was the only one among the RFA who knew that, as I was her fiance."
    show V V23
    v "I thought Reina's darkness will melt away if I bathed her with my love."
    show V V21
    v "However...she did not give up on her darkness."
    show V V22
    v "Ultimately she left RFA and established an illegal cult called the Ao Me."
    show V V24
    menu:
        "So you mean Reina is the savior...?":
            v "That's right...."
            jump u9600
        "I already knew Reina is the savior.":
            v "...Is that true? Looks like...I left too many bread crumbs for you to pick up on."
            jump u9600
label u9600:
    show V V23
    v "I found out she's the creator of this place approximately a month after her disappearance."
    show V V22
    v "It took less than months for her to complete over half of this building. And she's got several authorities behind her back."
    v "I assume she stole part of the profile data on RFA's guests and utilized her gift of speech to turn them into her believers."
    show V V29
    v "That's why Levan and I deleted all the data in her apartment."
    show V V210
    v "I deleted all of her traces except for a couple of pictures...."
    show V V21
    v "I thought I would be able to bring her back."
    show V V22
    v "Unfortunately, as of now, the only thing occupying her sight is the Ao Me...."
    v "So I thought I should give her some time. And that thought remains unchanged."
    show V V23
    v "However, I couldn't tell the rest of the RFA about this."
    v "If I do, either RFA or Reina would have to stop existing."
    show V V29
    v "I didn't want anyone to get hurt."
    show V V27
    v "Reina is the founder of the RFA...the figure of reverence for all. But if I were to confess that she has been disfigured beyond imaginations...."
    show V V218
    v "I thought my confession would traumatize everyone at the RFA... And Reina would be far from safe, either."
    show V V23
    v "So that's why I lied that she has taken her own life... So that no one would ever search for her."
    show V V23
    menu:
        "But you shouldn't have lied about her death. Poor Yoshiro....":
            v "...I am aware of that."
            v "However, if Yoshiro finds out what truly lies beneath her countenance... His agony will compound."
            jump u9700
        "Was that really the best?":
            v "...It was, according to the logics of my love."
            jump u9700
label u9700:
    show V V27
    v "However...I had no idea...Sora has become such a cruel person because of her."
    show V V218
    v "It was my duty to protect him as well... How in the world am I going to ask for Souta's forgiveness...?"
    show V V23
    menu:
        "K, don't let your love cloud your judgment of justice!":
            v "...If the world's justice is to judge Reina...she will never be forgiven... I am well-aware of that."
            v "I had no idea love would render someone so pathetically dumb"
            v "...and lead someone else to destruction...."
            v "I had no idea you, Sora, and Souta would be plagued with pain...."
            jump u9800
        "Protecting Sora was your duty?":
            v "Yes..."
            jump u9800
label u9800:
    v "And those twins need my protection more than ever...."
    mc "How come? Is there something wrong?"
    show V V23
    v "It's a long story...but simply put...."
    show V V22
    v "Long ago Reina and I rescued Souta and Sora from an abusive household."
    v "And we assumed their guardians."
    v "To protect them from their birth father, that is."
    show V V210
    v "Their father...is an extremely powerful authority."
    v "And he's currently on the move to eliminate his own sons."
    show V V22
    menu:
        "Wait...are you talking about the prime minister?":
            v "...You're sharp."
            jump u9900
        "Don't tell me... You mean Ryou's father? The Chairman?":
            v "No. Ryou is the only child he has. And he's no evil man."
            v "The truth is...."
            jump u9900
label u9900:
    show V V210
    v "Souta and Sora are the illegitimate sons of the prime minister who sought to purchase my works."
    v "That's why Levan had to so desperately conceal his true identity throughout his life."
    show V V27
    v "But I never knew Reina would be using Sora like that...."
    show V V21
    show MC MC15
    menu:
        "What!?":
            v "I know it's shocking."
            v "And their father...wanted to silence his own sons in secret for political reasons."
            jump u91000
        "Illegitimate sons...?":
            v "That's right... They're his hidden sons. And right now both of them are in crisis."
            v "Because their existence would serve as a huge fault in the prime minister's future political career."
            jump u91000
label u91000:
    show V V210
    show MC MC14
    v "When you return to the RFA in days to come, please keep it a secret. Along with the truth about Reina."
    show V V22
    v "...You will walk out of here safe and sound. I'll also do my best."
    v "Actually, I'm here to check your well-being...but at the same time, I wanted to ask you whether the prime minister has ever approached Sora."
    show V V23
    v "However, now that I see how Sora has changed and you're in danger....I realized I can no longer neglect my faults."
    show V V218
    v "I'm terribly sorry.... It is all my fault you found yourself in a muddle of trouble like this."
    show V V29
    v "I will find a way...to protect you, Sora, and Souta from Reina. I will..."
    show V V23
    menu:
        "Do you really know nothing about how to bring back Ren? Any clue by any chance?":
            v "I...I'm sorry. I know nothing about his second persona..."
            v "The real Sora from my latest memory of him...was an innocent boy who loved flowers."
            v "It took me too long...to realize that Reina has been training Sora in secret."
            jump u91100
        "I will be able to get out of this place alive, won't I?":
            v "I will make sure you will. I cannot repeat my mistakes anymore."
            jump u91100
label u91100:
    play sound "music/knock.ogg"
    show V V24
    "(Knock knock)"
    v "...It appears someone wants to come in."
    show V V22
    v "Don't forget, [name]. I'll always be watching you in order to protect you."
    v "So...please be safe until you're rescued."
    stop music
    hide V with dissolve
    hide MC with dissolve
    scene black with dissolve
    show text "09:11" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music19.ogg"
    call phone_start from _call_phone_start_8
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_104
    call message("Sora", "Hey") from _call_message_888
    call message("Sora", "little toy.") from _call_message_889
    call screen phone_reply("...Do you feel better now?", "cd927", "Sora, please don't throw me away!", "cd928")
label cd927:
    call phone_after_menu from _call_phone_after_menu_98
    call message_start("[name]", "...Do you feel better now?") from _call_message_start_105
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Who do you think you are to care for me?") from _call_message_890
    jump am911
label cd928:
    call phone_after_menu from _call_phone_after_menu_99
    call message_start("[name]", "Sora, please don't throw me away!") from _call_message_start_106
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "My savior") from _call_message_891
    call message("Sora", "is already planning for the ceremony herself.") from _call_message_892
    call message("Sora", "This is already out of my hands haha.") from _call_message_893
    jump am911
label am911:
    call message("Sora", "But more importantly,") from _call_message_894
    call message("Sora", "who was the believer") from _call_message_895
    call message_img("Sora", "who brought you food?", "images/questioning.png") from _call_message_img_57
    call screen phone_reply3("How did you know?", "cd929", "Nobody.", "cd930", "It was K.", "cd931")
label cd929:
    call phone_after_menu from _call_phone_after_menu_100
    call message_start("[name]", "How did you know?") from _call_message_start_107
    call message("Sora", "You didn't think I wouldn't know, did you?") from _call_message_896
    call message("Sora", "I told you") from _call_message_897
    call message_img("Sora", "I put a camera in your room.", "images/expecting.png") from _call_message_img_58
    call message("Sora", "Did you forget because you're too stupid to remember?") from _call_message_898
    call message("Sora", "I'm going to find that believer.") from _call_message_899
    jump am912
label cd930:
    call phone_after_menu from _call_phone_after_menu_101
    call message_start("[name]", "Nobody.") from _call_message_start_108
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Liar.") from _call_message_900
    call message("Sora", "Are you rebelling against me") from _call_message_901
    call message("Sora", "because very soon") from _call_message_902
    call message_img("Sora", "you'll be gone?", "images/well.png") from _call_message_img_59
    call message("Sora", "Your room is off limits...") from _call_message_903
    call message_img("Sora", "so I'm sure you didn't have a chance to befriend other believers.", "images/questioning.png") from _call_message_img_60
    call message("Sora", "I should find out who it is.") from _call_message_904
    jump am912
label cd931:
    call phone_after_menu from _call_phone_after_menu_102
    call message_start("[name]", "It was K.") from _call_message_start_109
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "What?") from _call_message_905
    call message_img("Sora", "Are you serious?", "images/questioning.png") from _call_message_img_61
    call message("Sora", "I don’t believe this lolol") from _call_message_906
    call message("Sora", "He crawled back in like a rat....") from _call_message_907
    call message("Sora", "That traitor....") from _call_message_908
    call message("Sora", "I'm going to search through the entire building.") from _call_message_909
    jump am912
label am912:
    call message("Sora", "And I'm going to make him") from _call_message_910
    call message("Sora", "be part of your ceremony too.") from _call_message_911
    call message("Sora", "Tonight") from _call_message_912
    call message("Sora", "your cleansing ceremony will be held haha.") from _call_message_913
    call message("Sora", "This will be your debut stage and goodbye stage.") from _call_message_914
    call message_img("Sora", "Congrats hehe", "images/smile.png") from _call_message_img_62
    call message("Sora", "Oh") from _call_message_915
    call message("Sora", "but there is one thing that pisses me off.") from _call_message_916
    call message("Sora", "My savior told me") from _call_message_917
    call message("Sora", "that I") from _call_message_918
    call message("Sora", "can't take part in the ceremony....") from _call_message_919
    call message("Sora", "What a useless airhead.") from _call_message_920
    call message("Sora", "Can't you see?") from _call_message_921
    call message("Sora", "This world") from _call_message_922
    call message("Sora", "will only hurt people like you or Ren.") from _call_message_923
    call message("Sora", "Take a good look") from _call_message_924
    call message("Sora", "where your future is taking you.") from _call_message_925
    call message("Sora", "See for yourself") from _call_message_926
    call message("Sora", "what despair and pain are really like.") from _call_message_927
    call message("Sora", "Even if you admit at last") from _call_message_928
    call message("Sora", "that my way was right") from _call_message_929
    call message("Sora", "and nod in tears") from _call_message_930
    call message("Sora", "it'd be too late.") from _call_message_931
    call screen phone_reply3("...why would you tell me all of this now?", "cd932", "I think you're right!", "cd933", "You'll soon realize that you're the one who's wrong.", "cd934")
label cd932:
    call phone_after_menu from _call_phone_after_menu_103
    call message_start("[name]", "But if you won't get to see me anymore very soon...why would you tell me all of this now?") from _call_message_start_110
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "You're my toy.", "images/questioning.png") from _call_message_img_63
    call message("Sora", "And you'll soon be thrown away.") from _call_message_932
    call message("Sora", "So I deserve") from _call_message_933
    call message("Sora", "to say whatever I want regarding how I feel about that.") from _call_message_934
    jump am913
label cd933:
    call phone_after_menu from _call_phone_after_menu_104
    call message_start("[name]", "I think you're right! Please believe in me!") from _call_message_start_111
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Your face is so funny hehehe") from _call_message_935
    call message("Sora", "That desperate faces of yours hehehehe") from _call_message_936
    call message("Sora", "looks even more pathetic hehehe") from _call_message_937
    call message("Sora", "Try making a crying face.") from _call_message_938
    call message_img("Sora", "Huh??", "images/smile.png") from _call_message_img_64
    call message("Sora", "Then maybe") from _call_message_939
    call message("Sora", "I'll feel bad for you for itty bitty bit") from _call_message_940
    call message("Sora", "and help you lololol") from _call_message_941
    jump am913
label cd934:
    call phone_after_menu from _call_phone_after_menu_105
    call message_start("[name]", "You'll soon realize that you're the one who's wrong.") from _call_message_start_112
    call message("Sora", "In your dreams hahahaha") from _call_message_942
    call message("Sora", "But then again") from _call_message_943
    call message("Sora", "you'd never understand") from _call_message_944
    call message("Sora", "even if I keep telling you....") from _call_message_945
    call message("Sora", "You're an airhead.") from _call_message_946
    jump am913
label am913:
    call message_img("Sora", "Now", "images/expecting.png") from _call_message_img_65
    call message("Sora", "nobody cares for you") from _call_message_947
    call message("Sora", "whether you like Ren or not.") from _call_message_948
    call message_img("Sora", "Nobody!", "images/smile.png") from _call_message_img_66
    call message("Sora", "Now") from _call_message_949
    call message("Sora", "you're really") from _call_message_950
    call message("Sora", "a loner'") from _call_message_951
    call message("Sora", "in this world.") from _call_message_952
    call message("Sora", "Even if you") from _call_message_953
    call message("Sora", "cry during the ceremony") from _call_message_954
    call message("Sora", "that you love Ren") from _call_message_955
    call message("Sora", "it's too late....") from _call_message_956
    call message("Sora", "Now be gone") from _call_message_957
    call message("Sora", "from this world") from _call_message_958
    call message("Sora", "all alone") from _call_message_959
    call message("Sora", "and") from _call_message_960
    call message("Sora", "lonely...!") from _call_message_961
    call message("Sora", "Ugh") from _call_message_962
    call message("Sora", "this sucks....") from _call_message_963
    call message("Sora", "Why didn't I") from _call_message_964
    call message("Sora", "get invited") from _call_message_965
    call message_img("Sora", "to the ceremony?", "images/questioning.png") from _call_message_img_67
    call message("Sora", "I have no choice but to obey my savior") from _call_message_966
    call message("Sora", "but now I'm annoyed") from _call_message_967
    call message("Sora", "because of you.") from _call_message_968
    call message("Sora", "This is annoying.") from _call_message_969
    call message("Sora", "You're my toy. You're supposed to entertain me.") from _call_message_970
    call message("Sora", "But I can't even watch you") from _call_message_971
    call message("Sora", "crying in pain?") from _call_message_972
    call message("Sora", "You're useless until the end.") from _call_message_973
    call message("Sora", "You....") from _call_message_974
    call message("Sora", "you were annoying") from _call_message_975
    call message("Sora", "ever since I met you.") from _call_message_976
    call message("Sora", "Do you have any idea") from _call_message_977
    call message("Sora", "how annoyed I am") from _call_message_978
    call message("Sora", "every time I see you?") from _call_message_979
    call screen phone_reply4("Sora, please calm down....", "cd935", "Are you afraid that you'll lose me...?", "cd936", "Please come see me right now...", "cd937", "Is being angry all you can do?", "cd938")
label cd935:
    call phone_after_menu from _call_phone_after_menu_106
    call message_start("[name]", "Sora, please calm down....") from _call_message_start_113
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Do you have any idea") from _call_message_980
    call message("Sora", "how angry I am") from _call_message_981
    call message("Sora", "because of you!?") from _call_message_982
    jump am914
label cd936:
    call phone_after_menu from _call_phone_after_menu_107
    call message_start("[name]", "Are you afraid that you'll lose me...?") from _call_message_start_114
    call message("Sora", "I don't need you.") from _call_message_983
    call message_img("Sora", "I don't care if you're gone.", "images/well.png") from _call_message_img_68
    call message("Sora", "But") from _call_message_984
    call message("Sora", "before you're gone,") from _call_message_985
    jump am914
label cd937:
    call phone_after_menu from _call_phone_after_menu_108
    call message_start("[name]", "Please come see me right now...") from _call_message_start_115
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Don't tell me to come see you!") from _call_message_986
    call message("Sora", "I'll see you when I want to!") from _call_message_987
    call message("Sora", "And I...") from _call_message_988
    jump am914
label cd938:
    call phone_after_menu from _call_phone_after_menu_109
    call message_start("[name]", "Is being angry all you can do?") from _call_message_start_116
    call message("Sora", "No.") from _call_message_989
    call message("Sora", "You want to see") from _call_message_990
    call message("Sora", "what else I can do?") from _call_message_991
    call message("Sora", "I'll show you.") from _call_message_992
    call message("Sora", "I should go let off some steam") from _call_message_993
    call message("Sora", "on you") from _call_message_994
    call message_img("Sora", "right now.", "images/smile.png") from _call_message_img_69
    jump am914
label am914:
    call message("Sora", "I need to let off my steam") from _call_message_995
    call message("Sora", "And I will") from _call_message_996
    call message("Sora", "I will let off my steam on you...") from _call_message_997
    call message("Sora", "This is so frustrating.") from _call_message_998
    call message("Sora", "My mood is ruined") from _call_message_999
    call message("Sora", "because of you!") from _call_message_1000
    call message("Sora", "It's fun playing with you") from _call_message_1001
    call message("Sora", "but my fun goes away") from _call_message_1002
    call message("Sora", "in less than a minute...") from _call_message_1003
    call message("Sora", "After my playtime is over") from _call_message_1004
    call message("Sora", "I feel even more empty") from _call_message_1005
    call message("Sora", "What is this?") from _call_message_1006
    call screen phone_reply3("Maybe you're not torturing me... Maybe you're torturing yourself.", "cd939", "I'm sorry... I'll try to be more entertaining.", "cd940", "You're the one who's ruined....", "cd941")
label cd939:
    call phone_after_menu from _call_phone_after_menu_110
    call message_start("[name]", "Maybe you're not torturing me... Maybe you're torturing yourself.") from _call_message_start_117
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "You think so?") from _call_message_1007
    call message("Sora", "Fine.") from _call_message_1008
    call message("Sora", "If you know") from _call_message_1009
    call message("Sora", "so well") from _call_message_1010
    call message("Sora", "why don't you tell me more....?") from _call_message_1011
    call message("Sora", "I'll be there right now.") from _call_message_1012
    jump am915
label cd940:
    call phone_after_menu from _call_phone_after_menu_111
    call message_start("[name]", "I'm sorry... I'll try to be more entertaining.") from _call_message_start_118
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "You think a toy deserves to make me feel like this!?") from _call_message_1013
    call message("Sora", "You're responsible for this.") from _call_message_1014
    call message("Sora", "What are you going to do with my mood!?") from _call_message_1015
    call message("Sora"," I can't sit around anymore like this.") from _call_message_1016
    jump am915
label cd941:
    call phone_after_menu from _call_phone_after_menu_112
    call message_start("[name]", "You're the one who's ruined....") from _call_message_start_119
    call message("Sora", "Nobody in this place is not ruined.") from _call_message_1017
    call message("Sora", "Of course everyone's ruined....") from _call_message_1018
    jump am915
label am915:
    call message("Sora", "I'll see you") from _call_message_1019
    call message("Sora", "right now") from _call_message_1020
    call message("Sora", "and vent!!") from _call_message_1021
    call message("Sora", "So stay right there.") from _call_message_1022
    call message("Sora", "Stay right there for me...") from _call_message_1023
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_1024
    call phone_end from _call_phone_end_9
    stop music
    scene black with dissolve
    play music "music/music19.ogg"
    scene bg14 with dissolve
    show U U25 with dissolve
    u "Get out of my way! Why do you stop me?"
    u "I'm in one foul mood right now. How dare you stop me?"
    show U U26
    believer "Only the believers preparing for the ceremony can enter this room."
    show U U25
    u "Like hell. Like hell! My toy is in there!"
    hide U with dissolve
    play music "music/music16.ogg"
    scene bg19 with dissolve
    show J J11 at my_right with dissolve
    j "Mr. Han... There is an interview request as a reference witness."
    j "I believe it is the same one Shun received."
    show H H14 at my_left with dissolve
    h "As expected. But do not tell me there is already a summons for me."
    show J J12
    j "Of course not."
    show J J11
    j "Though there isn't one, I believe you would feel much better after you explain yourself during the interview."
    show H H13
    h "I agree. What about my lawyer?"
    j "He is already on stand-by."
    show H H14
    h "I will be back by evening at the latest, so proceed and complete the other tasks."
    h "And have the report prepared when I return."
    j "...Understood. But Mr. Han..."
    show J J13
    j "You don't think there's something wrong with Levan, do you?"
    h "Though he's rarely logging on to the messenger, isn't he still helping us with the security?"
    show J J11
    j "That's true, but...."
    show H H13
    h "He would not have helped the RFA at all if there was something troubling him."
    show H H14
    h "What I said about Levan is a fact."
    h "I do not wish to be concerned about something that has yet to unfold."
    show J J13
    j "Understood. I wish we could reach K at a time like this..."
    show H H13
    h "...I agree. His request just won't leave my mind."
    h "Just what kind of article was he seeking to submit...?"
    hide H with dissolve
    hide J with dissolve
    scene bg8 with dissolve
    show S S212 with dissolve
    s "K...."
    show S S210
    s "What are...these files? Receipts... Bank account details...Pictures...Articles...."
    show S S212
    s "Wait, this guy is the one who used to watch me at the cathedral... And...this is a tracking list of secret transactions...."
    s "These are...files on my father's past...the dark spots of his past now completely destroyed."
    s "....The oldest file dates back 10 years, and he has been collecting these for 3 years.... So K...has been investigating on him."
    show S S214
    s "Haaa... If I dig up just a little more based on these files,"
    s "I can prove that Sora and I are his sons and that he has been threatening our lives."
    show S S212
    s "So that is why K asked Ryou to help him with the news media.."
    s "K... I knew he would not stop helping me."
    show S S214
    s "Once all these files are unleashed, the entire world will learn our family history..."
    show S S212
    s "And the authorities related to this man will be also dragged under the spotlight..."
    show S S214
    s "So we will end up creating an army of enemies for us...."
    show S S212
    s "But that man is just too dangerous to leave untouched."
    show S S211
    s "I can just survive under hiding... But as for Sora...."
    s "...Haa... Either way will threaten his life...."
    hide S with dissolve
    stop music
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg5 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "Why are you picking up so late?"
    u "You useless moron, being such a lazy-poke!"
    u "Argh! So annoying... you're annoying!"
    u "What were you doing? Didn't you hear the large sound outside?"
    menu:
        "I thought I heard your voice... Is that what you're talking about?":
            u "Ha.. so, you heard everything..."
            u "You heard everything and didn't even bother to look outside?"
            jump u912
        "I was asleep... Did something happen?":
            u "Don't try to fool me. I'll know everything when I watch through the surveillance videos."
            jump u912
label u912:
    u "You were so relaxed because I wasn't here, weren't you?"
    u "Did you have fun as people didn't let me come in here? Did you enjoy it?"
    u "Ha... it won't last long. You watch, nobody can stop me."
    u "I'll get rid of everyone, break down the door. You should be prepared for that."
    menu:
        "Is there a meaning in meeting me?":
            u "Ha! Meaning? Meaning! Ha..."
            u "Aren't you full of yourself... You must be happy waiting for your disposition while shivering in fear, all lonely in that room."
            u "Who the hell are you to make me think! You're such a worthless dust!"
            jump u913
        "The reason why you can't come in here is because of savior's orders, isn't it?":
            u "... What do you know?"
            u "Savior will do what I ask."
            u "There's no reason to use the believers in probitting me from entering here when you're just a toy, not even worthwhile of torturing. It's a waste of manpower!"
            jump u913
label u913:
    u "The reason why you're so calm like this right now is because you don't know what the cleansing ceremony is like."
    u "Lets see if you can keep your calm when the ceremony starts... let's see."
    u "Do you know what happens when you intake more than a specific amount of the elixir?"
    u "It differs by people but the majority becomes miserable."
    u "However much one try to be strong, everyone eventually crawls the floor crying out like an animal."
    u "Savior said your ceremony will be proceeded very specially."
    u "That a powerful ceremony is being prepared, one that can't be compared to any."
    u "... Savior's words are always right but..."
    u "I'm not sure if such special preparations are necessary to cleanse someone like you."
    u "I shall tell the savior again that it's not worthwhile to waste the elixir to something like you."
    u "However much I think about it... you're most adequate to used as my toy like right now and get thrown away."
    u "You're a weak and useless moron who doesn't even deserve to be cleansed!"
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "12:16" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music13.ogg"
    call phone_start from _call_phone_start_9
    call message_start("SYSTEM", "Reina has entered the chatroom.") from _call_message_start_120
    call message("Reina", "[name]") from _call_message_1025
    call message("Reina", "I thought you'd be distraught with what will soon befall you.") from _call_message_1026
    call message("Reina", "But you're logged in?") from _call_message_1027
    call message("Reina", "I must admit you got courage.") from _call_message_1028
    call message("Reina", "I mean it.") from _call_message_1029
    call screen phone_reply("Why did you create the RFA?", "cd942", "What's going to happen tonight?", "cd949")
label cd942:
    call phone_after_menu from _call_phone_after_menu_113
    call message_start("[name]", "Why did you create the RFA?") from _call_message_start_121
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "You sound like") from _call_message_1030
    call message("Reina", "you are absolutely sure I am identical to that 'Reina.'") from _call_message_1031
    call message("Reina", "Did K sneak in again") from _call_message_1032
    call message("Reina", "like that last time") from _call_message_1033
    call message("Reina", "and tell you something outrageous?") from _call_message_1034
    call message("Reina", "But doesn't matter.") from _call_message_1035
    call message("Reina", "In the end") from _call_message_1036
    call message("Reina", "the Ao Me will be victorious.") from _call_message_1037
    jump am916
label cd949:
    call phone_after_menu from _call_phone_after_menu_114
    call message_start("[name]", "What's going to happen tonight?") from _call_message_start_122
    call message("Reina", "Are you running a simulation in your head?") from _call_message_1038
    call message("Reina", "I'm not sure either....") from _call_message_1039
    call message("Reina", "But I can guarantee I won't let you down.") from _call_message_1040
    jump am916
label am916:
    call message("Reina", "You don't have much time left") from _call_message_1041
    call message("Reina", "so why don't we look back at your days spent here?") from _call_message_1042
    call message("Reina", "Though it is undeniable") from _call_message_1043
    call message("Reina", "that Sora has scorned you,") from _call_message_1044
    call message("Reina", "my recognition for your potentials") from _call_message_1045
    call message("Reina", "was bigger than my acknowledgment for him....") from _call_message_1046
    call screen phone_reply3("Reina, I also think you're full of potentials. If only you have stayed on the right path...", "cd950", "Freedom is true salvation for Sora or Ren.", "cd951", "Please let me stay here...", "cd952")
label cd950:
    call phone_after_menu from _call_phone_after_menu_115
    call message_start("[name]", "Reina, I also think you're full of potentials. If only you have stayed on the right path...") from _call_message_start_123
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "So you did pick up something from who-knows-who.") from _call_message_1047
    call message("Reina", "I should take my time") from _call_message_1048
    call message("Reina", "and find out during the ceremony where you picked that up.") from _call_message_1049
    jump am917
label cd951:
    call phone_after_menu from _call_phone_after_menu_116
    call message_start("[name]", "Freedom is true salvation for Sora or Ren.") from _call_message_start_124
    call message("Reina", "I knew it.") from _call_message_1050
    call message("Reina", "That's exactly your problem.") from _call_message_1051
    call message("Reina", "You chose not to listen.") from _call_message_1052
    call message("Reina", "You forgot that you're a weakling") from _call_message_1053
    call message("Reina", "and let the delusion make you think that you are strong....") from _call_message_1054
    call message("Reina", "Too bad.") from _call_message_1055
    call message("Reina", "Which is why") from _call_message_1056
    call message("Reina", "you no longer deserve to stay in the Ao Me.") from _call_message_1057
    jump am917
label cd952:
    call phone_after_menu from _call_phone_after_menu_117
    call message_start("[name]", "Please let me stay here...") from _call_message_start_125
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "Sora told me") from _call_message_1058
    call message("Reina", "that a believer entered your room.") from _call_message_1059
    call message("Reina", "You dare to break rules") from _call_message_1060
    call message("Reina", "even in this situation.") from _call_message_1061
    call message("Reina", "So how can I trust you?") from _call_message_1062
    jump am917
label am917:
    call message("Reina", "Though it would be a nuisance") from _call_message_1063
    call message("Reina", "I can always find your replacement.") from _call_message_1064
    call message("Reina", "I would just have to cope with a bit of a disturbance until I do.") from _call_message_1065
    call message("Reina", "Oh") from _call_message_1066
    call message("Reina", "one more thing.") from _call_message_1067
    call message("Reina", "I'll tell you what - you might be happy to hear this.") from _call_message_1068
    call message("Reina", "Surprisingly...") from _call_message_1069
    call message("Reina", "Sora is perplexed, albeit little.") from _call_message_1070
    call message("Reina", "And it appears that's because of you.") from _call_message_1071
    call message("Reina", "See?") from _call_message_1072
    call message("Reina", "Your influence") from _call_message_1073
    call message("Reina", "is not light.") from _call_message_1074
    call message("Reina", "You should be proud of that.") from _call_message_1075
    call message("Reina", "It was actually beyond my expectations.") from _call_message_1076
    call screen phone_reply3("Then I might still be useful...!", "cd953","Can you help Sora...to be one day secure enough even without hurting other people?", "cd954", "You'll need me too.", "cd955")
label cd953:
    call phone_after_menu from _call_phone_after_menu_118
    call message_start("[name]", "Then I might still be useful...!") from _call_message_start_126
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "Maybe....") from _call_message_1077
    call message("Reina", "Nevertheless") from _call_message_1078
    call message("Reina", "once you're gone for good") from _call_message_1079
    call message("Reina", "he'll soon forget about you") from _call_message_1080
    call message("Reina", "and return to where he belongs.") from _call_message_1081
    jump am918
label cd954:
    call phone_after_menu from _call_phone_after_menu_119
    call message_start("[name]", "Can you help Sora...to be one day secure enough even without hurting other people?") from _call_message_start_127
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "That sounds beautiful.") from _call_message_1082
    call message("Reina", "But such words do not belong here.") from _call_message_1083
    call message("Reina", "I knew it. You don't belong here.") from _call_message_1084
    jump am918
label cd955:
    call phone_after_menu from _call_phone_after_menu_120
    call message_start("[name]", "You'll need me too.") from _call_message_start_128
    call message("Reina", "I might have wanted you to stay") from _call_message_1085
    call message("Reina", "if we met differently.") from _call_message_1086
    call message("Reina", "Nevertheless") from _call_message_1087
    call message("Reina", "this is how we met") from _call_message_1088
    call message("Reina", "and this is where your fate is sealed.") from _call_message_1089
    jump am918
label am918:
    call message_img("Reina", "This is a flower Ren gave me when he was alive", "images/cg41.png") from _call_message_img_70
    call message("Reina", "but now it's no longer flourishing....") from _call_message_1090
    call message("Reina", "I decided to leave it") from _call_message_1091
    call message("Reina", "since raven flowers seem to suit us better.") from _call_message_1092
    call message("Reina", "Really....") from _call_message_1093
    call message("Reina", "weaklings like this flower") from _call_message_1094
    call message("Reina", "just wouldn't last.") from _call_message_1095
    call message("Reina", "Do you know that") from _call_message_1096
    call message("Reina", "there's a similarity between you and K?") from _call_message_1097
    call message("Reina", "Both of you do not understand") from _call_message_1098
    call message("Reina", "what it truly means to be strong.") from _call_message_1099
    call message("Reina", "And both of you are delusional") from _call_message_1100
    call message("Reina", "that unconditional love") from _call_message_1101
    call message("Reina", "is the right shape of love.") from _call_message_1102
    call message("Reina", "But in truth") from _call_message_1103
    call message("Reina", "unconditional love") from _call_message_1104
    call message("Reina", "that asks nothing in return") from _call_message_1105
    call message("Reina", "is the main reason") from _call_message_1106
    call message("Reina", "why people are rendered weak.") from _call_message_1107
    call message("Reina", "There is no better pathway to doom") from _call_message_1108
    call message("Reina", "than for idiotic losers") from _call_message_1109
    call message("Reina", "who know nothing about love") from _call_message_1110
    call message("Reina", "to lunge blindly at each other") from _call_message_1111
    call message("Reina", "in unreasonable pursuit of love") from _call_message_1112
    call message("Reina", "and thereby destroy each other.") from _call_message_1113
    call screen phone_reply3("I agree.... Not all people deserve love.", "cd956", "Ren has become stronger in the warm embrace of love...", "cd957", "What about you? Have you succeeded in love?", "cd958")
label cd956:
    call phone_after_menu from _call_phone_after_menu_121
    call message_start("[name]", "I agree.... Not all people deserve love.") from _call_message_start_129
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "Look how strong Sora is!") from _call_message_1114
    call message("Reina", "I'm so proud....") from _call_message_1115
    call message("Reina", "I nurtured him much better") from _call_message_1116
    call message("Reina", "than how my own parents nurtured me!") from _call_message_1117
    call message("Reina", "I guided him with my true love") from _call_message_1118
    call message("Reina", "and I never abandoned him....") from _call_message_1119
    jump am919
label cd957:
    call phone_after_menu from _call_phone_after_menu_122
    call message_start("[name]", "Ren has become stronger in the warm embrace of love... You're the one who decided that is weak.") from _call_message_start_130
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "Become stronger in the warm embrace of love?") from _call_message_1120
    call message("Reina", "You think such a useless, pointless behavior") from _call_message_1121
    call message("Reina", "of throwing yourself for so-called sacrifice") from _call_message_1122
    call message("Reina", "is love?") from _call_message_1123
    call message("Reina", "...That's a common delusion.") from _call_message_1124
    jump am919
label cd958:
    call phone_after_menu from _call_phone_after_menu_123
    call message_start("[name]", "What about you? Have you succeeded in love?") from _call_message_start_131
    call message("Reina", "Look at my believers!") from _call_message_1125
    call message("Reina", "And look at Sora!") from _call_message_1126
    call message("Reina", "Look how strong they are...!") from _call_message_1127
    call message("Reina", "They make me so proud.") from _call_message_1128
    call message("Reina", "I'm doing much better") from _call_message_1129
    call message("Reina", "than my parents...") from _call_message_1130
    jump am919
label am919:
    call message("Reina", "I should say my farewell before you're gone.") from _call_message_1131
    call message("Reina", "Thank you...") from _call_message_1132
    call message("Reina", "for being trampled") from _call_message_1133
    call message("Reina", "by Sora") from _call_message_1134
    call screen phone_reply3("...This isn't the end of it.", "cd959", "I'm sure I'm still useful...!", "cd960", "Now it's time for Ren to come back.", "cd961")
label cd959:
    call phone_after_menu from _call_phone_after_menu_124
    call message_start("[name]", "...This isn't the end of it.") from _call_message_start_132
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "Fine...") from _call_message_1135
    call message("Reina", "have it your way with your imaginations.") from _call_message_1136
    jump am920
label cd960:
    call phone_after_menu from _call_phone_after_menu_125
    call message_start("[name]", "I'm sure I'm still useful...!") from _call_message_start_133
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "I'm sorry. I cannot save you.") from _call_message_1137
    jump am920
label cd961:
    call phone_after_menu from _call_phone_after_menu_126
    call message_start("[name]", "Now it's time for Ren to come back.") from _call_message_start_134
    call message("Reina", "I'll leave you with your imaginations.") from _call_message_1138
    call message("Reina", "Now farewell.") from _call_message_1139
    jump am920
label am920:
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_1140
    call phone_end from _call_phone_end_10
    stop music
    scene black with dissolve
    play music "music/music20.ogg"
    scene bg14 with dissolve
    believer "Mr. Sora, like I said, this place is off-limits.... Ugh!"
    show U U25 at my_left with dissolve
    u "Shut up before I kill you. I'm bored, so I'm here to take a look at her. Why are you stopping me?"
    believer "Mr. Sora! Please calm yourself!"
    show U U26
    u "This is annoying... You're annoying. You're annoying! You're all annoying...."
    u "Hah... You think you're stronger than me? You think you're better than me?"
    show U U25
    u "I can get rid of you all if I want to! I'm the strongest here!"
    u "No one can command me.... Don't you dare tell me what to do!"
    show U U26
    believer "Mr. Sora...!"
    u "No one can tell me what to do! I'm not afraid of anything."
    show U U25
    u "I'm strong. I'm really strong...!"
    show R R33 at my_right with dissolve
    r "Sora, let my believer go."
    show R R32
    believer "...My savior!"
    show R R33
    r "I know that you're strong. So you don't have to scream like that."
    show U U27
    u "Haha, that's right. I'm not afraid of anything. I can do anything for you, no matter how horrible it is."
    r "Yes, you're very strong. That's why I feel so safe with you."
    show U U24
    u "Hear that? Did you hear that?!"
    u "I'll be the one serving her. Because I'm the strongest one here!!"
    show U U25
    show R R32
    u "Cross me, and I won't let you get away with it."
    show R R33
    r "He's right. You should all obey him."
    show U U24
    u "Haha, you wanted me to drag the RFA here, didn't you? It's only a matter of time for that to happen!"
    r "I know, Sora. I'm so proud of you."
    r "So you don't have to scream like that..."
    show R R32
    r "You're already a brave warrior protecting us."
    show U U27
    u "Yes, that's right.... I...I can protect someone.... I'm now strong enough to do that."
    stop music
    hide U with dissolve
    hide R with dissolve
    scene black with dissolve
    play music 'music/music2chat-17.ogg'
    scene bg5 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Reina is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    r "Hello. Are you having a good time?"
    r "Just a while ago, Sora put on an interesting show."
    r "You must have heard it in your room."
    r "Thinking of which... It was Sora who chose you from all those mass number of people."
    r "I wasn't interested then but... thinking back, it's very interesting."
    r "Sora doesn't know it himself but... I believe there's a reason why that child has chosen you."
    r "Isn't it because... that child have felt instinctively the person he can step allover on?"
    r "He might have thought you as his prey since he was you the first time."
    r "But I think it's somewhat a shame."
    r "In my opinion, you showed enough potential to grow."
    r "It was confusing at first when you came here but, if you have chosen the right Ao Me path, you could have become stronger..."
    r "We could have dream of the avast far future if you've accepted our offer to be with us..."
    menu:
        "I won't become one of you.":
            r "No, I understand as you fit better in the outside world."
            r "... Something that doesn't curve will eventually break. The taller you stand, the more we'd want to break you."
            r "That's the reason why Sora act outrageously when he sees you."
            r "You're only left with breaking now. How unfortunate."
            jump r91
        "Wouldn't I be able to change from now on?":
            r "Are you now changing your mind?"
            r "It's great that you've changed your mind right now. But.. it's already too late."
            r "How do I believe you that you're not saying that to escape this situation right now?"
            r "If your words are true, endure the ceremony."
            r "Prove yourself that way. It's going to be an interesting sight."
            jump r91
label r91:
    r "What a poor little thing..."
    r "Why didn't you run away when you had the chance, when Ren was being so nice to you?"
    menu:
        "I don't run... I've always acted with my conscious. Always.":
            r "Yes... You may be right if you were outside of Magenuta."
            r "That set mind you have... keep it till the end. The more you do, the more the ceremony will be."
            jump r92
        "I have nowhere to go...":
            r "Then you should have adjusted here and thought about surviving."
            r "You should have held on the Ren's vulnerable state rather than sweet talking Ren."
            r "But it's already too late."
            jump r92
label r92:
    r "This will be enough fun to enjoy before the party."
    r "You're shaking... are you afraid of the destiny that will soon befall on you?"
    r "Wait for it. All the preparations shall be completed soon."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "14:31" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music20.ogg"
    call phone_start from _call_phone_start_10
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_135
    call message("Sora", "[name]") from _call_message_1141
    call message("Sora", "well?") from _call_message_1142
    call message("Sora", "How does it feel waiting for your end?") from _call_message_1143
    call message("Sora", "Even if those losers") from _call_message_1144
    call message("Sora", "keep scaring you") from _call_message_1145
    call message("Sora", "during your cleansing ceremony") from _call_message_1146
    call message("Sora", "they won't be able to torment you") from _call_message_1147
    call message("Sora", "as much as I did....") from _call_message_1148
    call message_img("Sora", "Don't you think?", "images/expecting.png") from _call_message_img_71
    call message("Sora", "I tried to get into your room") from _call_message_1149
    call message("Sora", "but the believers stopped me.") from _call_message_1150
    call message("Sora", "But still") from _call_message_1151
    call message("Sora", "doesn't matter.") from _call_message_1152
    call message("Sora", "Because I can look at you through the camera") from _call_message_1153
    call message_img("Sora", "just like now.", "images/expecting.png") from _call_message_img_72
    call message("Sora", "I'm already having the time of my life.") from _call_message_1154
    call message("Sora", "I'm putting my toy into good use...") from _call_message_1155
    call screen phone_reply("Why won't she let you see me...?", "cd962", "Sora, you sound a little different.", "cd963")
label cd962:
    call phone_after_menu from _call_phone_after_menu_127
    call message_start("[name]", "Why won't she let you see me...?") from _call_message_start_136
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "I think") from _call_message_1156
    call message("Sora", "the savior thinks") from _call_message_1157
    call message("Sora", "that you") from _call_message_1158
    call message("Sora", "give me") from _call_message_1159
    call message_img("Sora", "bad influence.", "images/well.png") from _call_message_img_73
    call message("Sora", "Oh!!") from _call_message_1160
    call message("Sora", "Listen.") from _call_message_1161
    jump am921
label cd963:
    call phone_after_menu from _call_phone_after_menu_128
    call message_start("[name]", "Sora, you sound a little different.") from _call_message_start_137
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "Do I?", "images/questioning.png") from _call_message_img_74
    call message("Sora", "I am ticked off.") from _call_message_1162
    call message("Sora", "My head hurts.") from _call_message_1163
    call message("Sora", "This is annoying....") from _call_message_1164
    call message("Sora", "But still,") from _call_message_1165
    jump am921
label am921:
    call message("Sora", "Just now") from _call_message_1166
    call message("Sora", "my savior said") from _call_message_1167
    call message("Sora", "that I'm the strongest believer") from _call_message_1168
    call message_img("Sora", "in front of everybody else!", "images/smile.png") from _call_message_img_75
    call message("Sora", "Though that's nothing surprising lol") from _call_message_1169
    call message("Sora", "You should be honored.") from _call_message_1170
    call message("Sora", "You were once a toy") from _call_message_1171
    call message_img("Sora", "of the strongest believer.", "images/expecting.png") from _call_message_img_76
    call message("Sora", "Ren that idiot") from _call_message_1172
    call message("Sora", "can't even dream of becoming like me....") from _call_message_1173
    call message("Sora", "That's why he'll be gone inside me") from _call_message_1174
    call message("Sora", "after struggling for nothing....") from _call_message_1175
    call message("Sora", "Oh") from _call_message_1176
    call message("Sora", "Once you're gone") from _call_message_1177
    call message("Sora", "I have a feeling") from _call_message_1178
    call message("Sora", "that loser") from _call_message_1179
    call message("Sora", "will be gone") from _call_message_1180
    call message("Sora", "forever from this world....") from _call_message_1181
    call screen phone_reply3("Ren is still in there, isn't he...?", "cd964", "Are you sure you hate Ren?", "cd965", "I want to stay with you and you only....", "cd966")
label cd964:
    call phone_after_menu from _call_phone_after_menu_129
    call message_start("[name]", "Ren is still in there, isn't he...?") from _call_message_start_138
    call message("Sora", "Don't be stupid.") from _call_message_1182
    call message("Sora", "He'll soon be gone!") from _call_message_1183
    call message("Sora", "And when he's gone....") from _call_message_1184
    jump am922
label cd965:
    call phone_after_menu from _call_phone_after_menu_130
    call message_start("[name]", "Are you sure you hate Ren?") from _call_message_start_139
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "That airhead?") from _call_message_1185
    call message("Sora", "If he sticks around") from _call_message_1186
    call message("Sora", "there will be nothing but trouble for me.") from _call_message_1187
    call message_img("Sora", "Why in the world would I like him?", "images/questioning.png") from _call_message_img_77
    call message("Sora", "I wish") from _call_message_1188
    call message("Sora", "you'd be gone soon") from _call_message_1189
    call message("Sora", "and") from _call_message_1190
    call message("Sora", "that loser would be completely gone.") from _call_message_1191
    call message("Sora", "And then....") from _call_message_1192
    jump am922
label cd966:
    call phone_after_menu from _call_phone_after_menu_131
    call message_start("[name]", "I want to stay with you and you only....") from _call_message_start_140
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Looks like...") from _call_message_1193
    call message("Sora", "you really like me.") from _call_message_1194
    call message_img("Sora", "You have such a weird preference hehehe", "images/smile.png") from _call_message_img_78
    call message("Sora", "Did you") from _call_message_1195
    call message("Sora", "also lose your mind") from _call_message_1196
    call message_img("Sora", "while staying here?", "images/expecting.png") from _call_message_img_79
    call message("Sora", "And very soon....") from _call_message_1197
    jump am922
label am922:
    call message_img("Sora", "Only the strongest one will stay - me!", "images/smile.png") from _call_message_img_80
    call message("Sora", "From now on") from _call_message_1198
    call message("Sora", "nobody") from _call_message_1199
    call message("Sora", "will dare") from _call_message_1200
    call message("Sora", "to torment me....") from _call_message_1201
    call message("Sora", "Oh") from _call_message_1202
    call message("Sora", "that's right.") from _call_message_1203
    call message("Sora", "Your life") from _call_message_1204
    call message("Sora", "is about to end") from _call_message_1205
    call message("Sora", "so why don't we talk about") from _call_message_1206
    call message("Sora", "that weakling you liked so much?") from _call_message_1207
    call message("Sora", "I'll teach you once again") from _call_message_1208
    call message("Sora", "how that airhead") from _call_message_1209
    call message("Sora", "was good for nothing....") from _call_message_1210
    call message("Sora", "He knew") from _call_message_1211
    call message("Sora", "that hacking") from _call_message_1212
    call message("Sora", "is the least thing he can do") from _call_message_1213
    call message("Sora", "to prove his worth.") from _call_message_1214
    call message("Sora", "Because other than hacking") from _call_message_1215
    call message("Sora", "he's really good for nothing haha") from _call_message_1216
    call message("Sora", "Seriously") from _call_message_1217
    call message("Sora", "he's such a good riddance") from _call_message_1218
    call message_img("Sora", "from this world.", "images/smile.png") from _call_message_img_81
    call message("Sora", "Can't he find interest in anything better?") from _call_message_1219
    call message("Sora", "He'll wear silly smiles") from _call_message_1220
    call message("Sora", "at something lame like flowers.") from _call_message_1221
    call message("Sora", "And when someone curses him") from _call_message_1222
    call message("Sora", "he'll just dig his own grave") from _call_message_1223
    call message("Sora", "blaming himself") from _call_message_1224
    call message("Sora", "and never dreaming of revenge.") from _call_message_1225
    call message("Sora", "Keep digging") from _call_message_1226
    call message("Sora", "and he would've lived forever as a loser haha") from _call_message_1227
    call screen phone_reply3("That's why he was no longer needed.", "cd967", "Things would've been different....if Ren realized how precious a person he is.", "cd968", "I prefer Ren.", "cd969")
label cd967:
    call phone_after_menu from _call_phone_after_menu_132
    call message_start("[name]", "That's why he was no longer needed.") from _call_message_start_141
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "That's right.") from _call_message_1228
    call message("Sora", "It's only natural") from _call_message_1229
    call message("Sora", "that he can't survive.") from _call_message_1230
    call message("Sora", "I'm strong! So that's why I stay!") from _call_message_1231
    call message("Sora", "That's the law of nature!") from _call_message_1232
    jump am923
label cd968:
    call phone_after_menu from _call_phone_after_menu_133
    call message_start("[name]", "Things would've been different....if Ren realized how precious a person he is.") from _call_message_start_142
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "Precious?", "images/questioning.png") from _call_message_img_82
    call message("Sora", "Ah") from _call_message_1233
    call message("Sora", "I remember very little what you said.") from _call_message_1234
    call message("Sora", "You said that to that lifeless boy") from _call_message_1235
    call message("Sora", "to have faith in himself hehehehe") from _call_message_1236
    call message("Sora", "That kind of idea") from _call_message_1237
    call message("Sora", "won't work in the real world.....") from _call_message_1238
    call message("Sora", "You should come back") from _call_message_1239
    call message("Sora", "to earth lolol") from _call_message_1240
    call message("Sora", "Try to get a grip") from _call_message_1241
    call message("Sora", "before you're gone, will you? Lol") from _call_message_1242
    jump am923
label cd969:
    call phone_after_menu from _call_phone_after_menu_134
    call message_start("[name]", "I prefer Ren.") from _call_message_start_143
    call message("Sora", "You prefer the weak because you're weak.") from _call_message_1243
    call message("Sora", "The weaklings stick together") from _call_message_1244
    call message("Sora", "and comfort each other") from _call_message_1245
    call message("Sora", "and do what weaklings are supposed to hehe") from _call_message_1246
    call message("Sora", "But I'm not interested") from _call_message_1247
    call message("Sora", "in something so lame.") from _call_message_1248
    call message_img("Sora", "I'm only interested in getting stronger lol", "images/expecting.png") from _call_message_img_83
    jump am923
label am923:
    call message("Sora", "I'm nothing like that idiot.") from _call_message_1249
    call message("Sora", "I") from _call_message_1250
    call message("Sora", "will never") from _call_message_1251
    call message("Sora", "be someone else's control.") from _call_message_1252
    call message("Sora", "No one can command me") from _call_message_1253
    call message("Sora", "except for my savior.") from _call_message_1254
    call message("Sora", "Because I'm strong") from _call_message_1255
    call message_img("Sora", "unlike Ren!", "images/smile.png") from _call_message_img_84
    call message("Sora", "I'll never be Ren.") from _call_message_1256
    call message("Sora", "I'm not Ren. Never.") from _call_message_1257
    call message("Sora", "Never, ever.") from _call_message_1258
    call message("Sora", "Ugh") from _call_message_1259
    call message("Sora", "I feel awful suddenly...") from _call_message_1260
    call message("Sora", "My head hurts....") from _call_message_1261
    call message("Sora", "What is this...??") from _call_message_1262
    call message("Sora", "I feel sick....") from _call_message_1263
    call message("Sora", "You expect") from _call_message_1264
    call message("Sora", "that Ren will be here, don't you?") from _call_message_1265
    call screen phone_reply3("Sora, you don't sound well... Are you alright?", "cd970", "No! Please stay here, Sora!", "cd971", "Of course.", "cd972")
label cd970:
    call phone_after_menu from _call_phone_after_menu_135
    call message_start("[name]", "Sora, you don't sound well... Are you alright?") from _call_message_start_144
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "My head.") from _call_message_1266
    call message("Sora", "It hurts....") from _call_message_1267
    call message("Sora", "It feels like it'll crack.") from _call_message_1268
    call message("Sora", "What") from _call_message_1269
    call message("Sora", "are you doing") from _call_message_1270
    call message("Sora", "to me?") from _call_message_1271
    jump am924
label cd971:
    call phone_after_menu from _call_phone_after_menu_136
    call message_start("[name]", "No! Please stay here, Sora!") from _call_message_start_145
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Don't you dare") from _call_message_1272
    call message("Sora", "think anything funny.") from _call_message_1273
    jump am924
label cd972:
    call phone_after_menu from _call_phone_after_menu_137
    call message_start("[name]", "Of course.") from _call_message_start_146
    call message("Sora", "That dream") from _call_message_1274
    call message("Sora", "will be shattered") from _call_message_1275
    call message("Sora", "tonight.") from _call_message_1276
    jump am924
label am924:
    call message("Sora", "Now Ren is gone...") from _call_message_1277
    call message("Sora", "Only the strong one is here!") from _call_message_1278
    call message("Sora", "Sora is a strong name!") from _call_message_1279
    call message("Sora", "It's a strong name") from _call_message_1280
    call message("Sora", "that no one can torment...!!") from _call_message_1281
    call screen phone_reply3("Please calm down, Sora....", "cd973", "I know that. So please come see me...", "cd974", "You say you're strong, but why do you sound so unstable?", "cd975")
label cd973:
    call phone_after_menu from _call_phone_after_menu_138
    call message_start("[name]", "Please calm down, Sora....") from _call_message_start_147
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "You're nothing but a toy.") from _call_message_1282
    call message("Sora", "Don't try to comfort me.") from _call_message_1283
    call message("Sora", "You're being arrogant!!") from _call_message_1284
    jump am925
label cd974:
    call phone_after_menu from _call_phone_after_menu_139
    call message_start("[name]", "I know that. So please come see me...") from _call_message_start_148
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "I think I'll throw up") from _call_message_1285
    call message("Sora", "if I see you.") from _call_message_1286
    call message("Sora", "This is annoying.") from _call_message_1287
    jump am925
label cd975:
    call phone_after_menu from _call_phone_after_menu_140
    call message_start("[name]", "You say you're strong, but why do you sound so unstable?") from _call_message_start_149
    call message("Sora", "Be quiet.") from _call_message_1288
    call message("Sora", "I talked to you for a little") from _call_message_1289
    call message("Sora", "and you've gotten arrogant....") from _call_message_1290
    call message("Sora", "Don't talk to me.") from _call_message_1291
    call message("Sora", "You're nothing but a toy!") from _call_message_1292
    jump am925
label am925:
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_1293
    call phone_end from _call_phone_end_11
    stop music
    scene black with dissolve
    if bad_end > good_end:
        call badend2 from _call_badend2
    else:
        jump u914
label u914:
    play music "music/music22.ogg"
    scene bg10 with dissolve
    show U U22 with dissolve
    u "Once my toy is gone...I won't need to look at the camera."
    u "Once she's gone...I won't be able to see that bewildered face of hers."
    show U U26
    u "No big deal. Her face didn't deserve my attention in the first place. "
    u "She was nothing but a toy useless after playtime's over."
    show U U24
    u "Her scent is a headache, and her face is awful...."
    u "She was the worst toy I could find. "
    u "She should be grateful I'm the one throwing her away."
    show U U22
    u "...."
    show U U26
    u "....This is annoying."
    stop music
    hide U with dissolve
    scene black with dissolve
    show text "16:56" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_11
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_150
    call message("Sora", "[name]") from _call_message_1294
    call message("Sora", "perfect timing.") from _call_message_1295
    call message("Sora", "You should talk to her too.") from _call_message_1296
    call message("Sora", "She should be here") from _call_message_1297
    call message("Sora", "any minute....") from _call_message_1298
    call message("SYSTEM", "Reina has entered the chatroom.") from _call_message_1299
    call message("Sora", "My savior!") from _call_message_1300
    call message("Reina", "Sora,") from _call_message_1301
    call message("Reina", "I happen to be busy preparing the ceremony.") from _call_message_1302
    call screen phone_reply("For eternal paradise!", "cd976", "Sora, could you tell me what's going on...?", "cd977")
label cd976:
    call phone_after_menu from _call_phone_after_menu_141
    call message_start("[name]", "For eternal paradise!") from _call_message_start_151
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "...For eternal paradise.") from _call_message_1303
    call message("Sora", "See?") from _call_message_1304
    call message("Sora", "See what a good believer she is?") from _call_message_1305
    call message("Reina", "Sora...") from _call_message_1306
    jump am926
label cd977:
    call phone_after_menu from _call_phone_after_menu_142
    call message_start("[name]", "Sora, could you tell me what's going on...?") from _call_message_start_152
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "Just do as I say") from _call_message_1307
    call message("Sora", "you idiot!") from _call_message_1308
    call message("Reina", "....") from _call_message_1309
    call message("Reina"," What is going on?") from _call_message_1310
    jump am926
label am926:
    call message("Reina", "What's the matter?") from _call_message_1311
    call message("Sora", "My savior,") from _call_message_1312
    call message("Sora", "the RFA's messenger is quiet.") from _call_message_1313
    call message("Sora", "I can get no response") from _call_message_1314
    call message("Sora", "from that whitehead's firewall.") from _call_message_1315
    call message("Sora", "My savior,") from _call_message_1316
    call message("Sora", "this is our chance") from _call_message_1317
    call message_img("Sora", "to get all the data from the RFA.", "images/smile.png") from _call_message_img_85
    call message("Reina", "But I thought his firewall was unresponsive since last night.") from _call_message_1318
    call message("Sora", "That's right.") from _call_message_1319
    call message("Sora", "So we should get on with it right now!") from _call_message_1320
    call message("Sora", "We can capture the RFA") from _call_message_1321
    call message("Sora", "as soon as we're done collecting data.") from _call_message_1322
    call screen phone_reply("He's right! We should get the RFA right now!", "cd978", "Did something happen at the RFA...?", "cd979")
label cd978:
    call phone_after_menu from _call_phone_after_menu_143
    call message_start("[name]", "He's right! We should get the RFA right now!") from _call_message_start_153
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "You're supporting Sora") from _call_message_1323
    call message("Reina", "only because you don't want to have your ceremony tonight.") from _call_message_1324
    call message("Sora", "My savior,") from _call_message_1325
    call message("Sora", "I think now is our time.") from _call_message_1326
    call message("Sora", "I mean it!") from _call_message_1327
    jump am927
label cd979:
    call phone_after_menu from _call_phone_after_menu_144
    call message_start("[name]", "Did something happen at the RFA...?") from _call_message_start_154
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "[name], all you need to do is to worry about yourself.") from _call_message_1328
    call message("Reina", "And Sora.") from _call_message_1329
    jump am927
label am927:
    call message("Reina", "Like I said") from _call_message_1330
    call message("Reina", "it isn't the time yet.") from _call_message_1331
    call message("Reina", "Until we get a full picture") from _call_message_1332
    call message("Reina", "of their hacker's actual whereabouts.") from _call_message_1333
    call message("Sora", "...") from _call_message_1334
    call message("Reina", "We should remain prudent") from _call_message_1335
    call message("Sora", "His communication is rather quiet") from _call_message_1336
    call message("Sora", "so maybe he's busy doing something else.") from _call_message_1337
    call message("Reina", "Not today. Not now.") from _call_message_1338
    call message("Reina", "We must have the cleansing ceremony.") from _call_message_1339
    call message("Sora", "That can wait, can't it?") from _call_message_1340
    call screen phone_reply("I'll greatly appreciate it if you could postpone the ceremony...!", "cd980", "Sora... Is it because you don't want to lose me?", "cd981")
label cd980:
    call phone_after_menu from _call_phone_after_menu_145
    call message_start("[name]", "I'll greatly appreciate it if you could postpone the ceremony...!") from _call_message_start_155
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina"," ....") from _call_message_1341
    call message("Reina", "I knew that was what you wanted.") from _call_message_1342
    jump am928
label cd981:
    call phone_after_menu from _call_phone_after_menu_146
    call message_start("[name]", "Sora... Is it because you don't want to lose me?") from _call_message_start_156
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "I never said I don't want to lose you.") from _call_message_1343
    call message("Reina", "But") from _call_message_1344
    call message("Reina", "you sound like you don't.") from _call_message_1345
    call message("Sora", "That's not true....") from _call_message_1346
    jump am928
label am928:
    call message("Sora", "But") from _call_message_1347
    call message("Sora", "I don't know why things have to be rushed like this.") from _call_message_1348
    call message("Sora", "She's just some useless toy.") from _call_message_1349
    call message("Sora", "Why bother?") from _call_message_1350
    call message("Reina", "It is not wise to be fickle.") from _call_message_1351
    call message("Reina", "Caprice molds people lazy and weak.") from _call_message_1352
    call screen phone_reply("I can be more faithful to the Ao Me!", "cd982", "I don't think she'll change her mind....", "cd983")
label cd982:
    call phone_after_menu from _call_phone_after_menu_147
    call message_start("[name]", "I can be more faithful to the Ao Me!") from _call_message_start_157
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "See?") from _call_message_1353
    call message("Sora", "She might be a useful toy.") from _call_message_1354
    call message("Reina", "You sound very unusual today.") from _call_message_1355
    jump am929
label cd983:
    call phone_after_menu from _call_phone_after_menu_148
    call message_start("[name]", "I don't think she'll change her mind....") from _call_message_start_158
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Looks like she can see things clearly.") from _call_message_1356
    call message("Reina", "I think you're the one who is acting unusual, Sora.") from _call_message_1357
    jump am929
label am929:
    call message("Sora", "It's just that my head hurts....") from _call_message_1358
    call message("Reina", "Then take the elixir.") from _call_message_1359
    call message("Sora", "Alright.") from _call_message_1360
    call message("Reina", "That's because you don't have enough faith and elixir.") from _call_message_1361
    call message("Reina", "That's the cause behind every trouble.") from _call_message_1362
    call screen phone_reply3("Could you please give me that elixir too...?", "cd984", "This is a sign that Ren will be back.", "cd985", "Sora, are you not feeling well?", "cd986")
label cd984:
    call phone_after_menu from _call_phone_after_menu_149
    call message_start("[name]", "Could you please give me that elixir too...?") from _call_message_start_159
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "You'll take it tonight anyways.") from _call_message_1363
    call message("Sora", "This is confusing....") from _call_message_1364
    call message("Sora", "But for now I'll obey you.") from _call_message_1365
    call message("Sora", "You make me strong.") from _call_message_1366
    call message("Reina", "Yes, and I hope you remember that.") from _call_message_1367
    jump am930
label cd985:
    call phone_after_menu from _call_phone_after_menu_150
    call message_start("[name]", "This is a sign that Ren will be back.") from _call_message_start_160
    call message("Sora", "....Do you think so?") from _call_message_1368
    call message("Reina", "Ren was back due to the side effect of the elixir.") from _call_message_1369
    call message("Reina"," Do not be a fool. Don't listen to her.") from _call_message_1370
    call message("Reina", "Very soon she'll be gone.") from _call_message_1371
    jump am930
label cd986:
    call phone_after_menu from _call_phone_after_menu_151
    call message_start("[name]", "Sora, are you not feeling well?") from _call_message_start_161
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "I feel sick... I've been feeling sick....") from _call_message_1372
    call message("Reina", "That's what happens as your body gets used to the elixir.") from _call_message_1373
    call message("Reina", "It's a common side effect.") from _call_message_1374
    call message("Sora", "...") from _call_message_1375
    jump am930
label am930:
    call message("Reina", "Like I said") from _call_message_1376
    call message("Reina", "I'm so proud of you. You're strong now.") from _call_message_1377
    call message("Sora", "Sorry. Please forget what I said.") from _call_message_1378
    call message("Sora", "I'll go take the elixir.") from _call_message_1379
    call message("Sora", "Well then...") from _call_message_1380
    call message("Sora", "For eternal paradise.") from _call_message_1381
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_1382
    call message("Reina", "...") from _call_message_1383
    call message("Reina", "He's breaking away.") from _call_message_1384
    call message("Reina", "It's all because of you...") from _call_message_1385
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_1386
    call phone_end from _call_phone_end_12
    stop music
    scene black with dissolve
    play music "music/music19.ogg"
    scene bg10 with dissolve
    show U U26 with dissolve
    u "Haaa... This is frustrating."
    show U U25
    u "This is so frustrating... Damn it."
    hide U with dissolve
    scene bg14 with dissolve
    show U U26 with dissolve
    believer "M-mr. Sora...."
    u "Out of my way. She would be a goner soon, wouldn't she? I'm just going to have some fun before she's gone."
    believer "But savior...."
    show U U25
    u "You heard the savior approve of me!"
    u "Get out of my way! Or do you want me to reserve you a seat for the cleansing?!"
    show U U26
    believer "....Please go ahead."
    hide U with dissolve
    scene bg5 with dissolve
    show U U22 at my_left with dissolve
    u "Come out, wherever you are, my toy."
    show U U26
    u "...."
    show U U25
    u "I said, come out!!"
    show U U24
    show MC MC14 at my_right with dissolve
    menu:
        "Alright... Calm down.":
            u "Who are you to give orders to me?"
            u "You don't deserve to say anything. I already told you."
            jump u915
        "Are you here to see me?":
            u "Of course not. I'm just here to make sure if your heart is still beating."
            u "The party would be a failure without the main dish, you know?"
            jump u915
label u915:
    show U U22
    u "Do you know why you're being dragged to the cleansing ceremony?"
    show U U26
    u "It's all your fault, not mine. It's because you did everything wrong!"
    u "You ended up like this because you did useless things. You're an airhead. You're good for nothing."
    show U U25
    u "Don't let me see that horrible face of yours!"
    show U U26
    u "But then again....this makes no sense. Why do they get to dispose of you? You're my toy!"
    show U U25
    u "Why can't I enjoy the finale? Why do I have to miss all the fun?!"
    menu:
        "Sora, please. That's enough! You'll just hurt yourself!":
            u "Shut up! What kind of logic is that?"
            u "You know nothing about me!"
            jump u916
        "You're right. They shouldn't be the one to do this.":
            u "That's what I mean. Now you're starting to get a little smart."
            u "But it's too late.... Your time is almost up."
            jump u916
label u916:
    show U U26
    u "I'm in a violent mood right now. If you do anything to tick me off, I'm going to throw you away right here."
    u "You just nod to whatever I tell you to do. I told you hundreds of times you don't deserve to say anything!"
    u "I'll throw you out if you rebel. You heard me...!"
    hide U
    hide MC
    scene black with dissolve
    mother "Shut up!"
    mother "You're not just an airhead. You're a good-for-nothing... You don't deserve to say anything for the day."
    mother "Don't you say anything other than 'yes.'"
    mother "Make any sound, and I'm locking you up in the closet."
    scene bg5 with dissolve
    show U U26 at my_left with dissolve
    show MC MC15 at my_right with dissolve
    u "...Ugh... What the? Huff..."
    mc "..."
    u "Don't laugh at me! I'll throw you away."
    u "I'm nothing like that woman.... I'm not her!"
    u "It's all your fault I'm thinking of her!"
    show U U25
    u "This is all because you forgot how useless you are and talked to me as if you're smart!"
    u "You're an airhead! You're a useless,stupid toy!"
    hide U
    hide MC
    scene black with dissolve
    mother "This is all your fault! You useless scum! You're much more stupid and weaker than your brother...."
    scene bg5 with dissolve
    u "Aaaaagh!"
    u "I'll kill you. I'll just go ahead and kill you."
    u "You can't torment me. I'll be the one to do that! I'll be fine as long as you're in pain."
    u "So show me how you writhe in pain! Cry out some more!"
    menu:
        "Please don't hurt me....":
            u "No, no. I'll get rid of you. So that I can get stronger!"
            jump u917
        "It's painful....":
            u "That's right. It's painful, isn't it? You should tremble some more! You should be more scared!"
            jump u917
        "...":
            u "Why aren't you saying anything?"
            u "Tell me! Tell me it's painful!"
            jump u917
label u917:
    u "No one can hurt or torment me. You're my proof."
    u "I'm not stupid."
    u "I'm not weak."
    u "I'm not useless...!"
    u "I'm smart. I'm strong. No one can look down on me!"
    u "I...! I can even kill you! That's how strong I am!"
    menu:
        "Even if I'm gone, you won't be saved....":
            u "Yes, I will be! Once you're gone, I'll do much better."
            u "I'll stand tall, and I'll have no one to fear! I'll protect this place with my power! And I'll be recognized!"
            u "I'll be much happier than I used to be...."
            jump u918
        "But to me, it seems you're falling apart from within....":
            u "Shut up! That's not true! What do you know about me? I'm strong within! I'll never fall apart!"
            u "I'll never be hurt by anyone... I'll never let anyone do that! I'll never let you hurt me!"
            jump u918
        "...That's right.You're strong.":
            u "Do you mean it? Tell me! Do you mean it?! Aren't you just pretending and mocking me inside?"
            u "You think Ren is better than me, don't you?!"
            u "You're the one who fooled around with that loser!"
            jump u918
        "....":
            u "Say something! Anything! Tell me I'm strong! You have to say it."
            u "You have to admit I'm strong! You will admit it, won't you?"
            u "Say that I'm strong! Or else I...I might throw you away!"
            jump u918
label u918:
    u "You need to tremble in pain! Cry in pain...!"
    u "When you're in pain, haha...This place feels so warm."
    u "It feels like I'm in a warm cozy place."
    u "Once I leave this room, it feels so pointless again... I can feel nothing but frustration."
    u "I... I... I can stay protected...only when you cry in pain."
    u "You have to keep crying in pain. You...you have to be tormented by me. You have to be my toy."
    u "But then if...if you're gone... If you're dead....this empty feeling I get once I leave your room will never stop...!"
    menu:
        "...You're strong, Sora. You don't have to torture someone to prove that.":
            jump u919
        "Don't kill me. Let me live. So that you can feel secure.":
            jump u919
        "Let's get out of here. Come with me.":
            u "No! You will leave me."
            u "Once you are out of this place, you will throw me away! You think I don't know that!?"
            jump u919
label u919:
    u "...Just who do you think you are? Who do you think you are?! How...how dare you...!"
    scene bg5 with dissolve
    show U U26 at my_left with dissolve
    show MC MC15 at my_right with dissolve
    u "You... You... Huff..."
    u "You...spin my world into chaos!"
    u "My head...It feels like it'll explode... It hurts... It hurts..!"
    u "God damn it...!"
    hide U with dissolve
    "(....)"
    "(Sora bolted out of the room.)"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "17:39" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music20.ogg"
    call phone_start from _call_phone_start_12
    call message_start("SYSTEM", "Reina has entered the chatroom.") from _call_message_start_162
    call message("Reina", "There you are.") from _call_message_1387
    call message("Reina", "I wonder") from _call_message_1388
    call message("Reina", "if you're smiling in victory.") from _call_message_1389
    call screen phone_reply3("Where is Sora?", "cd987", "I'll find him and send him back to you!", "cd988", "There is no winning and losing here...", "cd989")
label cd987:
    call phone_after_menu from _call_phone_after_menu_152
    call message_start("[name]", "Where is Sora?") from _call_message_start_163
    call message("Reina", "That's what I'm wondering.") from _call_message_1390
    call message("Reina", "It appears you don't know, either.") from _call_message_1391
    jump am931
label cd988:
    call phone_after_menu from _call_phone_after_menu_153
    call message_start("[name]", "I'll find him and send him back to you!") from _call_message_start_164
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "...Did you read my mind?") from _call_message_1392
    jump am931
label cd989:
    call phone_after_menu from _call_phone_after_menu_154
    call message_start("[name]", "There is no winning and losing here...") from _call_message_start_165
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Is that what you really think?") from _call_message_1393
    call message("Reina", "Did you think I wouldn't notice") from _call_message_1394
    call message("Reina", "how you pray that I'll fall in defeat") from _call_message_1395
    call message("Reina", "while you stand victorious?") from _call_message_1396
    call message("Reina", "Congratulations.") from _call_message_1397
    jump am931
label am931:
    call message("Reina", "Sora has gone missing.") from _call_message_1398
    call message("Reina", "Thanks to that") from _call_message_1399
    call message("Reina", "your cleansing ceremony") from _call_message_1400
    call message("Reina", "couldn't take place tonight.") from _call_message_1401
    call message("Reina", "All the preparation was close to completion...") from _call_message_1402
    call message("Reina", "I need you as a bait to get him back.") from _call_message_1403
    call message("Reina", "That was pretty intelligent of you.") from _call_message_1404
    call message("Reina", "I thought you're closer to a dunce") from _call_message_1405
    call message("Reina", "but I see there lurks cunning in you....") from _call_message_1406
    call message("Reina", "Let me make an offer -") from _call_message_1407
    call message("Reina", "Rather than hating each other") from _call_message_1408
    call message("Reina", "why don't we work together?") from _call_message_1409
    call screen phone_reply3("It'd be an honor.", "cd990", "I won't manipulate Sora or Ren.", "cd991", "No, thank you. I'll soon run away from here with Ren.", "cd992")
label cd990:
    call phone_after_menu from _call_phone_after_menu_155
    call message_start("[name]", "It'd be an honor.") from _call_message_start_166
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "You didn't hesitate for a moment.") from _call_message_1410
    call message("Reina", "So") from _call_message_1411
    call message("Reina", "you're starting to like this place.") from _call_message_1412
    call message("Reina", "This would be easier than I'd thought.") from _call_message_1413
    jump am932
label cd991:
    call phone_after_menu from _call_phone_after_menu_156
    call message_start("[name]", "I won't manipulate Sora or Ren.") from _call_message_start_167
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "I know that you don't agree") from _call_message_1414
    call message("Reina", "with the way I handle Sora.") from _call_message_1415
    call message("Reina", "However") from _call_message_1416
    call message("Reina", "once you come to understand") from _call_message_1417
    call message("Reina", "what this place is like") from _call_message_1418
    call message("Reina", "you'll realize") from _call_message_1419
    call message("Reina", "that I had no choice") from _call_message_1420
    call message("Reina", "but to treat every person like I do to him.") from _call_message_1421
    jump am932
label cd992:
    call phone_after_menu from _call_phone_after_menu_157
    call message_start("[name]", "No, thank you. I'll soon run away from here with Ren.") from _call_message_start_168
    call message("Reina", "If that's possible, then I'll admit that you're not completely useless.") from _call_message_1422
    call message("Reina", "However,") from _call_message_1423
    call message("Reina", "I hope you'd realize that your chance of escape is much less than faint.") from _call_message_1424
    call message("Reina", "In fact,") from _call_message_1425
    call message("Reina", "joining me and sharing my power at this place") from _call_message_1426
    call message("Reina", "would be the perfect method") from _call_message_1427
    call message("Reina", "to gain everything you want.") from _call_message_1428
    jump am932
label am932:
    call message("Reina", "Remember") from _call_message_1429
    call message("Reina", "that I told you") from _call_message_1430
    call message("Reina", "my love is the true love?") from _call_message_1431
    call screen phone_reply3("I agree with your method.", "cd993", "It's nothing but manipulating people with conditions as you'd like.", "cd994", "Love is impossible without courage to devote yourself.", "cd995")
label cd993:
    call phone_after_menu from _call_phone_after_menu_158
    call message_start("[name]", "You mean giving love only to strong and useful people? I agree with your method.") from _call_message_start_169
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "So you remember.") from _call_message_1432
    call message("Reina", "Some people") from _call_message_1433
    call message("Reina", "might find my method cruel.") from _call_message_1434
    call message("Reina", "However") from _call_message_1435
    jump am933
label cd994:
    call phone_after_menu from _call_phone_after_menu_159
    call message_start("[name]", "You say it's love, but it's nothing but manipulating people with conditions as you'd like.") from _call_message_start_170
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Do you think this is all") from _call_message_1436
    call message("Reina", "but my selfishness?") from _call_message_1437
    call message("Reina", "No...") from _call_message_1438
    call message("Reina", "I'm making a decision for everyone's good.") from _call_message_1439
    call message("Reina", "You should try to understand me.") from _call_message_1440
    jump am933
label cd995:
    call phone_after_menu from _call_phone_after_menu_160
    call message_start("[name]", "Love is impossible without courage to devote yourself like Ren.") from _call_message_start_171
    call message("Reina", "Devotional, unconditional love") from _call_message_1441
    call message("Reina", "makes the recipient weary.") from _call_message_1442
    call message("Reina", "...I should know.") from _call_message_1443
    jump am933
label am933:
    call message("Reina", "The greatest reason why my love is true love") from _call_message_1444
    call message("Reina", "is that no matter what mistake he makes") from _call_message_1445
    call message("Reina", "I'll never give up on him.") from _call_message_1446
    call message("Reina", "[name]....") from _call_message_1447
    call message("Reina", "as for you....") from _call_message_1448
    call message("Reina", "I did not pick you as my believer") from _call_message_1449
    call message("Reina", "so your case is different....") from _call_message_1450
    call message("Reina", "But such irregularity does not apply to Sora...") from _call_message_1451
    call message("Reina", "No matter what he does") from _call_message_1452
    call message("Reina", "I'll never give up on him.") from _call_message_1453
    call message("Reina", "At this place here") from _call_message_1454
    call message("Reina", "in Ao Me") from _call_message_1455
    call message("Reina", "there are hundreds of people") from _call_message_1456
    call message("Reina", "who were denied by the world and were") from _call_message_1457
    call message("Reina", "unable to stand on their own.") from _call_message_1458
    call message("Reina", "So if you bathe such people") from _call_message_1459
    call message("Reina", "with unconditional love") from _call_message_1460
    call message("Reina", "they won't be able") from _call_message_1461
    call message("Reina", "to cope with such love.") from _call_message_1462
    call message("Reina", "Such love is so alien to them") from _call_message_1463
    call message("Reina", "that they'll just question themselves.") from _call_message_1464
    call screen phone_reply3("It is reserved for people who had lots of possessions, lots of love...", "cd996", "We can provide a respite for them with our hearts.", "cd997", "I'll help him to defeat his demon of doubt.", "cd998")
label cd996:
    call phone_after_menu from _call_phone_after_menu_161
    call message_start("[name]", "Unconditional love is reserved for people who had lots of possessions, lots of love...") from _call_message_start_172
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "That's right....") from _call_message_1465
    call message("Reina", "for people like us") from _call_message_1466
    call message("Reina", "unconditional love") from _call_message_1467
    call message("Reina", "is nothing but poison....") from _call_message_1468
    call message("Reina", "that makes us constantly question our worth.") from _call_message_1469
    jump am934
label cd997:
    call phone_after_menu from _call_phone_after_menu_162
    call message_start("[name]", "If people question themselves, we can provide a respite for them with our hearts.") from _call_message_start_173
    call message("Reina", "I once basked in such love.") from _call_message_1470
    call message("Reina", "However....") from _call_message_1471
    call message("Reina", "the act of depending completely on someone") from _call_message_1472
    call message("Reina", "is no different from horror.") from _call_message_1473
    call message("Reina", "Because you'd feel like you'd lose a sense of who you are.") from _call_message_1474
    jump am934
label cd998:
    call phone_after_menu from _call_phone_after_menu_163
    call message_start("[name]", "I'll help him to defeat his demon of doubt. Deep down inside he truly longs for unconditional love.") from _call_message_start_174
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "[name]") from _call_message_1475
    call message("Reina", "people like you in the outside world") from _call_message_1476
    call message("Reina", "are the only ones who can handle such wonderful love.") from _call_message_1477
    call message("Reina", "But not the people here....") from _call_message_1478
    call message("Reina", "You cannot apply the same form of love") from _call_message_1479
    call message("Reina", "to every human being.") from _call_message_1480
    jump am934
label am934:
    call message("Reina", "So think again.") from _call_message_1481
    call message("Reina", "Think about what kind of love") from _call_message_1482
    call message("Reina", "Ren truly needed....") from _call_message_1483
    call message("Reina", "He might have been happy for a moment") from _call_message_1484
    call message("Reina", "due to your warm words.") from _call_message_1485
    call message("Reina", "But in return") from _call_message_1486
    call message("Reina", "he was slowly losing") from _call_message_1487
    call message("Reina", "the tools for his survival.") from _call_message_1488
    call message("Reina", "It's not too late.") from _call_message_1489
    call message("Reina", "Once you and I join forces") from _call_message_1490
    call message("Reina", "we can forge him") from _call_message_1491
    call message("Reina", "into the most powerful warrior") from _call_message_1492
    call message("Reina", "a living weapon") from _call_message_1493
    call message("Reina", "in the world of our own.") from _call_message_1494
    call screen phone_reply3("I'll stand with you...!", "cd999", "Stop manipulating him.", "cd9100", "I'll bring Ren back and be happy together... That's my dream.", "cd9101")
label cd999:
    call phone_after_menu from _call_phone_after_menu_164
    call message_start("[name]", "I'll stand with you...!") from _call_message_start_175
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "That's right....") from _call_message_1495
    call message("Reina", "I like it.") from _call_message_1496
    call message("Reina", "And then") from _call_message_1497
    call message("Reina", "he'll no longer have to suffer") from _call_message_1498
    call message("Reina", "any pain or woe...") from _call_message_1499
    call message("Reina", "That is what we should do for him.") from _call_message_1500
    call message("Reina", "That is right for him.") from _call_message_1501
    jump am935
label cd9100:
    call phone_after_menu from _call_phone_after_menu_165
    call message_start("[name]", "Stop manipulating him. You cannot possess and forge a human being.") from _call_message_start_176
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "He has no faith in himself.") from _call_message_1502
    call message("Reina", "How is he supposed to live and feel assured") from _call_message_1503
    call message("Reina", "unless I possess him and mold him with my teachings?") from _call_message_1504
    call message("Reina", "You can find no compliments") from _call_message_1505
    call message("Reina", "at this place") from _call_message_1506
    call message("Reina", "even if you make ethically right statements.") from _call_message_1507
    call message("Reina", "Are you sure you won't join me?") from _call_message_1508
    jump am935
label cd9101:
    call phone_after_menu from _call_phone_after_menu_166
    call message_start("[name]", "I'll bring Ren back and be happy together... That's my dream.") from _call_message_start_177
    call message("Reina", "You must be terribly fond of him....") from _call_message_1509
    call message("Reina", "If you cooperate well") from _call_message_1510
    call message("Reina", "I'll sometimes bring him out for you.") from _call_message_1511
    jump am935
label am935:
    call message("Reina", "And don't you forget") from _call_message_1512
    call message("Reina", "that helping me") from _call_message_1513
    call message("Reina", "would make your life more useful") from _call_message_1514
    call message("Reina", "as of now.") from _call_message_1515
    call message("Reina", "And now") from _call_message_1516
    call message("Reina", "I must tell my believers") from _call_message_1517
    call message("Reina", "to find Sora and bring him back to me.") from _call_message_1518
    call message("Reina", "My poor little lamb") from _call_message_1519
    call message("Reina", "where have you lost yourself...?") from _call_message_1520
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_1521
    call phone_end from _call_phone_end_13
    stop music
    scene black with dissolve
    play music "music/music19.ogg"
    scene bg13 with dissolve
    show U U26 at my_left with dissolve
    u "That's right... I should have just thrown her away.I'm not weak."
    u "I should get back there and get rid of her. I have to do this myself."
    show U U22
    u "Then I would never have to look at those stupid eyes."
    show U U26
    u "...Are they looking for me?"
    u "How dare they... I'll just eliminate them as soon as they confront me. Go ahead - let's see if you can find me!"
    show U U27
    u "I'll get rid of everything that crosses my path. And then it will be her turn."
    u "Then it would be my family... Then the RFA... And then...!"
    show U U26
    believer "Looks like someone's there!"
    believer "Be careful. Mr. Sora is rather violent. Let the three of us move together."
    u "...Slowpokes. Come closer, if you dare."
    believer "You there! Are you Mr. Sora?"
    whooo "...No. I'm here to tend to the flowers."
    show U U211
    u "...Who is that?"
    believer "You mean at this hour? Identify yourself!"
    whooo "I'm Believer K709, in charge of looking after the garden. Here's my ID."
    whooo "I check the garden twice per month, during night time as well...."
    whooo "since the savior often takes a walk through this place."
    believer "I see. Good job, man. Keep up the good work."
    whooo "...Understood."
    whooo "......."
    whooo "You can come out now. It's safe."
    show U U26
    u "This voice....!!"
    show V V219 at my_right with dissolve
    whooo "....It's me."
    show U U26
    u "K...! You dared to sneak in again, you termite. Good. Maybe I should take your head to my savior."
    show V V29
    v "...Sora, did you turn into a complete stranger...because of the side effects of the meds?"
    show U U25
    u "Who do you think you are to ask that!?"
    show U U26
    u "You betrayed my savior, destroyed my family... Don't even think about walking away from here alive."
    u "I'm not Ren. I'm not gonna lose you like some loser... Ugh..."
    show V V29
    v "Sora...!"
    u "Don't touch me! I have a gun. I'll kill you... Huff.. Ugh...."
    v "I know that I don't deserve to see you... I understand that you hate me...."
    show U U25
    u "Huff... Don't touch me. My head... It feels like it'll crack into pieces... It hurts... It hurts!"
    v "Let's escape together. This place will be nothing but a pit of poison for you."
    show V V23
    v "I had no idea Reina would turn you like this. Let's get out of here together...with [name]..."
    u "Are you mad...? You want me to escape? Don't be ridiculous. I... I..."
    show U U26
    u "If...if I leave this place... I...."
    hide U
    hide V
    scene black with dissolve
    scene bg20 with dissolve
    show R R32 with dissolve
    r "Everyone out there will only betray and torment you. This is the only place you can ever belong..."
    show R R35
    r "You will be much happier in this place."
    r "Here you are free to show them what you can do."
    r "Here you'll be the strong one. Then no one can even dare to betray you."
    show R R32
    r "Think about how lonely and painful you were out there...."
    hide R with dissolve
    scene bg13 with dissolve
    show U U25 at my_left with dissolve
    show V V23 at my_right with dissolve
    u "Don't try to trick me! I will never...leave this place."
    show U U26
    u "You're a liar. I knew it. Savior was right...."
    show V V29
    v "Do you really find this place happy, Sora...? Please tell me that's not true."
    v "You look nothing like what you wanted to become when you were a child."
    show U U26
    u "Don't talk about the past to me..."
    u "Back then you talked like you would do anything for me, just to betray me! And then you...."
    show V V27
    v "But Sora, you were the one who left me..."
    show U U25
    u "Liar!"
    show V V29
    v "It was you, Sora. Think about it. I'm not sure what Reina told you...but I wanted to teach you photogra..."
    show U U26
    u "Stop! Stop it! I'm strong! Now...you cannot fool me! You can't...!"
    u "That's right. I should kill you. I need to kill you now."
    v "...If you kill me, then your way out of this place will be forever shut. You won't ever see your brother."
    show U U25
    u "I'll kill you. I'll kill you, and I'll kill him! I'll kill everyone that betrayed me!"
    hide U with dissolve
    hide V with dissolve
    v "Sora...please."
    v "Sora, let's please turn back now...before you commit even greater crime like Reina."
    v "It would be now near impossible to change her back when she's turned you like this..."
    v "But Sora, please!"
    v "I beg you...please. Please tell me it's not too late to save you."
    u "Hah...! You're weak. If you remain the loser you are, it'd be too easy to kill you...."
    v "You can't kill people, Sora. You're a tender boy...who loved plants and the sky."
    u "What are you mumbling about...? That boy is gone. He was too weak to survive. I'm the only one left."
    u "I threw away those lame memories along with that useless weakling."
    u "I'm completely different now... Now I'm strong...! The boy you're looking for is not here anymore."
    u "I'm strong enough to kill you. I can show you right now!"
    v "...Have it your way."
    v "Souta will hate me, anyways, for making you like this...."
    u "Don't you dare speak that name!! Are you not scared of dying?"
    u "That scum doesn't care for me... You think I wouldn't know?"
    v "You're mistaken about Souta."
    v "Sora...please. Let's escape together.I'll explain everything as soon as we are out of here."
    u "Don't look at me as if you can save me! I'll kill you. I'll pull the trigger now!"
    v "Let's leave together with [name]. She's also worried about you."
    u "I don't care what happens to that toy!"
    u "That airhead doesn't deserve to think about me!"
    stop music
    u "That airhead... That loser...! If that idiot escapes...!"
    scene black with dissolve
    show U U22 with dissolve
    play music "music/music22.ogg"
    u "If that idiot...leaves this place..."
    show U U29
    u "...She'll be much happier, unlike me...."
    scene bg13 with dissolve
    show U U26 at my_left with dissolve
    show V V24 at my_right with dissolve
    u "Ugh... Huff... I will not leave... Never..."
    show U U25
    u "...Get lost with her if you want to leave!!"
    v "Sora, wait!"
    hide U with dissolve
    "......"
    show V V219
    v "Sora...."
    show V V29
    v "I can't fathom the magnitude of your pain. Was it devastating enough to make you forsake yourself like that....?"
    v "Hah... What should I do? I must get him out of here, before their father finds out about this place..."
    stop music
    hide V with dissolve
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "[name], this is K. You've answered right away."
    v "The thing is, I met Sora ... a while ago."
    menu:
        "How did it go?":
            v "I'm not sure about the details but he seemed to be cornered."
            v "I thought it would be a good opportunity to persuade him but... it wasn't as easy as I thought."
            jump v92
        "Did you tell him some other lies to him?":
            v "No. You seem to be misunderstanding me."
            v "I have never lied to that child. Just that... things didn't head the right way..."
            v "I tried persuading him right now to save him but... I've failed."
            jump v92
label v92:
    v "The child I saw in person, quite contrary from watching him afar.... it was unimaginably devastating."
    menu:
        "You made Sora like that, K.":
            v "...I've provided the sources. I know that fact to the bone as well."
            v "I.. should have protected that child... I trusted her too much..."
            v "While looking at Sora, the thought that I must save Sora came to me..."
            v "I know I don't have to right to ask for forgiveness but.... my only way to ask for it is by rescuing him...."
            v "But Sora, he's..."
            jump v93
        "Was Sora having a hard time?":
            v "He screamed at me but his eyes were shaking."
            v "Looking at that... it hurt me even me..."
            v "That child..."
            jump v93
label v93:
    v "However much I tried persuading him, he strongly refused."
    v "That rough and violent behavior... he has changed to a completely different person from the Sora I used to know."
    v "How many times he's been hurt... What make the warm hearted child change to this state...."
    v "Reina and I... what have we done to this child...I was in agony with all those thoughts running through my head ."
    v "But when I proposed that he should leave with you, he hesitated for the first time."
    v "It was for a short while but... I saw hope in that."
    v "[name], I...plan to bet everything in that hesitation. In order to do so... I desperately need your help."
    menu:
        "It seems it'll be better for me to adjust here and live here.":
            v "I don't know what Reina said to you but... don't trust her words"
            v "This place... is a place where they brainwash people with drugs that eat away your thoughts."
            v "All those people who say they're happy here, have been brainwashed."
            jump v94
        "Okay. I'll to what I can, so please K, make a move on it.":
            v "...Of course."
            v "Sora needs you."
            v "You should hold his hand. Save him from this place."
            v "I'll protect the two of you... so that both won't fall into the pit of darkness."
            jump v94
label v94:
    v "...Wait..."
    v "... I hear footsteps nearby. I'll get caught if I make a noise... I must hang up."
    v "Please, have your mindset and be prepared."
    v "It's for you and Sora's sake. Bye..."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "19:47" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music22.ogg"
    call phone_start from _call_phone_start_13
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_178
    call message("Sora", "...") from _call_message_1522
    call message("Sora", "......") from _call_message_1523
    call screen phone_reply3("Sora, are you alright?", "cd9102", "I should tell the savior.", "cd9103", "Ren...?", "cd9104")
label cd9102:
    call phone_after_menu from _call_phone_after_menu_167
    call message_start("[name]", "Sora, are you alright?") from _call_message_start_179
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "No...") from _call_message_1524
    jump am936
label cd9103:
    call phone_after_menu from _call_phone_after_menu_168
    call message_start("[name]", "I should tell the savior.") from _call_message_start_180
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "You must hate me...") from _call_message_1525
    jump am936
label cd9104:
    call phone_after_menu from _call_phone_after_menu_169
    call message_start("[name]", "Ren...?") from _call_message_start_181
    call message("Sora", "You miss him, don't you?") from _call_message_1526
    call message("Sora", "...") from _call_message_1527
    jump am936
label am936:
    call message("Sora", "I felt so weird") from _call_message_1528
    call message("Sora", "and I threw up for hours.") from _call_message_1529
    call message("Sora", "I could smell nothing") from _call_message_1530
    call message("Sora", "but this disgusting smell") from _call_message_1531
    call message("Sora", "of chemicals.") from _call_message_1532
    call screen phone_reply3("Are you feeling sick...? I'm so worried about you.", "cd9105", "I think you need the elixir again.....", "cd9106", "Ren must be coming back!", "cd9107")
label cd9105:
    call phone_after_menu from _call_phone_after_menu_170
    call message_start("[name]", "Are you feeling sick...? I'm so worried about you.") from _call_message_start_182
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "My mind and body") from _call_message_1533
    call message("Sora", "are in ruins....") from _call_message_1534
    call message("Sora", "I had no idea they're this bad....") from _call_message_1535
    jump am937
label cd9106:
    call phone_after_menu from _call_phone_after_menu_171
    call message_start("[name]", "I think you need the elixir again.....") from _call_message_start_183
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "This is painful....") from _call_message_1536
    call message("Sora", "Is there no one") from _call_message_1537
    call message("Sora", "who can save me...?") from _call_message_1538
    jump am937
label cd9107:
    call phone_after_menu from _call_phone_after_menu_172
    call message_start("[name]", "Ren must be coming back!") from _call_message_start_184
    call message("Sora", "I thought") from _call_message_1539
    call message("Sora", "he was gone for good...") from _call_message_1540
    call message("Sora", "Looks like he's not.") from _call_message_1541
    jump am937
label am937:
    call message("Sora", "I wish everything inside") from _call_message_1542
    call message("Sora", "would spill completely.") from _call_message_1543
    call message("Sora", "I wish") from _call_message_1544
    call message("Sora", "every particle") from _call_message_1545
    call message("Sora", "of this hellish fate of mine") from _call_message_1546
    call message("Sora", "so deeply rooted") from _call_message_1547
    call message("Sora", "in my bones") from _call_message_1548
    call message("Sora", "would complete leave") from _call_message_1549
    call message("Sora", "my entire body.") from _call_message_1550
    call screen phone_reply3("I see you've had such a hard time until now....", "cd9108", "This place will soon be a paradise!", "cd9109", "And...I wish my pure, innocent Ren were here...", "cd9110")
label cd9108:
    call phone_after_menu from _call_phone_after_menu_173
    call message_start("[name]", "I see you've had such a hard time until now....") from _call_message_start_185
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "I don't know.") from _call_message_1551
    call message("Sora", "Until now") from _call_message_1552
    call message("Sora", "I wasn't supposed to even think that I'm having a hard time.") from _call_message_1553
    call message("Sora", "If I think like that") from _call_message_1554
    call message("Sora", "that would mean I'm weak.") from _call_message_1555
    call message("Sora", "But") from _call_message_1556
    call message("Sora", "now") from _call_message_1557
    call message("Sora", "all the poison I swallowed to be stronger") from _call_message_1558
    call message("Sora", "feels so disgusting.") from _call_message_1559
    call message("Sora", "I want to throw them all up....") from _call_message_1560
    jump am938
label cd9109:
    call phone_after_menu from _call_phone_after_menu_174
    call message_start("[name]", "Why do you think this place is hell? This place will soon be a paradise!") from _call_message_start_186
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "Paradise....") from _call_message_1561
    call message("Sora", "That's what I heard.") from _call_message_1562
    call message("Sora", "And that's what I believed.") from _call_message_1563
    call message("Sora", "But....") from _call_message_1564
    jump am938
label cd9110:
    call phone_after_menu from _call_phone_after_menu_175
    call message_start("[name]", "And...I wish my pure, innocent Ren were here...") from _call_message_start_187
    call message("Sora", "So you don't need me, do you?") from _call_message_1565
    call message("Sora", "Perhaps") from _call_message_1566
    call message("Sora", "I'm going through the process") from _call_message_1567
    call message("Sora", "of his return....") from _call_message_1568
    jump am938
label am938:
    call message("Sora", "For some reason") from _call_message_1569
    call message("Sora", "the more I throw up") from _call_message_1570
    call message("Sora", "these disgusting memories") from _call_message_1571
    call message("Sora", "and endless suspicions") from _call_message_1572
    call message("Sora", "fill my head more and more...") from _call_message_1573
    call message("Sora", "I would rather be") from _call_message_1574
    call message("Sora", "nothing but an empty shell") from _call_message_1575
    call message("Sora", "incapable of remembering anything") from _call_message_1576
    call message("Sora", "or feeling anything.") from _call_message_1577
    call message("Sora", "My name...") from _call_message_1578
    call message("Sora", "my fate...") from _call_message_1579
    call message("Sora", "my life...") from _call_message_1580
    call message("Sora", "If I can throw up everything") from _call_message_1581
    call message("Sora", "labeled with my name...") from _call_message_1582
    call message("Sora", "If I can escape myself") from _call_message_1583
    call message("Sora", "and turn into an empty shell,") from _call_message_1584
    call message("Sora", "will I be able to step outside into the world out there?") from _call_message_1585
    call message("Sora", "I won't touch anything") from _call_message_1586
    call message("Sora", "or listen to anything") from _call_message_1587
    call message("Sora", "or talk to anyone.") from _call_message_1588
    call message("Sora", "Can I just") from _call_message_1589
    call message("Sora", "do nothing") from _call_message_1590
    call message("Sora", "but stare at the world?") from _call_message_1591
    call screen phone_reply3("Of course you can... You're free to choose that.", "cd9111", "Then you can't secure your future. You'll be scared.", "cd9112", "I want you to look at me.", "cd9113")
label cd9111:
    call phone_after_menu from _call_phone_after_menu_176
    call message_start("[name]", "Of course you can... You're free to choose that.") from _call_message_start_188
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "Free....?") from _call_message_1592
    call message("Sora", "I thought freedom is something you can find only in your dreams....") from _call_message_1593
    call message("Sora", "I....") from _call_message_1594
    call message("Sora", "I don't even dream of freedom. That's too much for me....") from _call_message_1595
    jump am939
label cd9112:
    call phone_after_menu from _call_phone_after_menu_177
    call message_start("[name]", "Then you can't secure your future. You'll be scared.") from _call_message_start_189
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "That's what I've thought.") from _call_message_1596
    call message("Sora", "I kept telling myself that") from _call_message_1597
    call message("Sora", "as if entering input in my head.") from _call_message_1598
    call message("Sora", "I've been afraid all the time...") from _call_message_1599
    call message("Sora", "ever since I was born.") from _call_message_1600
    call message("Sora", "I have never once") from _call_message_1601
    call message("Sora", "slept in peace.") from _call_message_1602
    call message("Sora", "But now I don't want this anymore...") from _call_message_1603
    call message("Sora", "I'm tired....") from _call_message_1604
    jump am939
label cd9113:
    call phone_after_menu from _call_phone_after_menu_178
    call message_start("[name]"," Don't let the world distract you. I want you to look at me.") from _call_message_start_190
    call message("Sora", "....You're not going to change like the savior, are you?") from _call_message_1605
    call message("Sora", "If you do") from _call_message_1606
    call message("Sora", "I think I'll be sad....") from _call_message_1607
    call message("Sora", "I would rather...") from _call_message_1608
    jump am939
label am939:
    call message("Sora", "If I were incapable of listening") from _call_message_1609
    call message("Sora", "or speaking") from _call_message_1610
    call message("Sora", "or thinking....") from _call_message_1611
    call message("Sora", "If I were incapable of understanding") from _call_message_1612
    call message("Sora", "what it means to be weak") from _call_message_1613
    call message("Sora", "what it means to be strong") from _call_message_1614
    call message("Sora", "or what it means to be useless...") from _call_message_1615
    call message("Sora", "If I were") from _call_message_1616
    call message("Sora", "completely alone") from _call_message_1617
    call message("Sora", "in this world...") from _call_message_1618
    call message("Sora", "Perhaps") from _call_message_1619
    call message("Sora", "that would be better for me.") from _call_message_1620
    call message("Sora", "If I were rather born") from _call_message_1621
    call message("Sora", "as a ball of weed") from _call_message_1622
    call message("Sora", "that nobody will ever care for") from _call_message_1623
    call message("Sora", "I think that") from _call_message_1624
    call message("Sora", "would have been better....") from _call_message_1625
    call message("Sora", "Even if people stomp on me") from _call_message_1626
    call message("Sora", "and even if I am bent into half") from _call_message_1627
    call message("Sora", "I'll still stand.") from _call_message_1628
    call message("Sora", "I'll survive") from _call_message_1629
    call message("Sora", "and carry on") from _call_message_1630
    call message("Sora", "and watch the sky") from _call_message_1631
    call message("Sora", "and feel the breeze") from _call_message_1632
    call message("Sora", "and see everything") from _call_message_1633
    call message("Sora", "I want to see") from _call_message_1634
    call message("Sora", "as much as I want,") from _call_message_1635
    call message("Sora", "until the day") from _call_message_1636
    call message("Sora", "I finally die....") from _call_message_1637
    call screen phone_reply3("Even a weed will be a beautiful flower for us.", "cd9114", "You don't have a choice.", "cd9115", "Let's forget about weeds. They're lame.", "cd9116")
label cd9114:
    call phone_after_menu from _call_phone_after_menu_179
    call message_start("[name]", "If we decide to think differently, even a weed will be a beautiful flower for us.") from _call_message_start_191
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "...") from _call_message_1638
    call message("Sora", "I wish") from _call_message_1639
    call message("Sora", "someone") from _call_message_1640
    call message("Sora", "would please wipe my head clean.") from _call_message_1641
    jump am940
label cd9115:
    call phone_after_menu from _call_phone_after_menu_180
    call message_start("[name]", "But you were born as a tragic human being. You don't have a choice.") from _call_message_start_192
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Sora", "Being a happy human being...") from _call_message_1642
    call message("Sora", "would be a dream I don't deserve...") from _call_message_1643
    jump am940
label cd9116:
    call phone_after_menu from _call_phone_after_menu_181
    call message_start("[name]", "Let's forget about weeds. They're lame.") from _call_message_start_193
    call message("Sora", "Lame...") from _call_message_1644
    call message("Sora", "That's something Ren liked....") from _call_message_1645
    call message("Sora", "Is he") from _call_message_1646
    call message("Sora"," coming back...?") from _call_message_1647
    jump am940
label am940:
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_1648
    call phone_end from _call_phone_end_14
    stop music
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "It's me."
    u "I called to ask you something."
    u "They say the weaker ones tend to dream absurd dreams."
    u "Like evading reality as reality is close to hell."
    u "So I thought you would have dreamt of something like that at least once."
    u "If... it was possible to leave this place... will you leave?"
    menu:
        "Definitely.":
            u "Yes... You would. You were not suitable here in the first place."
            u "You are a different person from me."
            jump u920
        "Why do you ask that?":
            u "Should there be a reason to ask you? I was curious."
            u "Or was I asking the obvious?"
            u "Yes... it was an obvious question."
            u "You're free to roam anywhere since there's nobody bullying you outside... of course you'd want to go out..."
            jump u920
        "Sora, you're not having bad thoughts, are you? Are you thinking of betraying savior?":
            u "Hearing that from you... sounds a bit weird."
            u "Up till now, you've been the outsider and I was the one following Ao Me's intentions.."
            u "But now... it's feels like we've changed roles."
            jump u920
label u920:
    u "Let me ask you one more thing. If... if you could get of here without any danger.... who would you go with?"
    u "You alone? Or with your dear lovey dovey Ren?"
    menu:
        "Me alone.":
            u "That's wasn't expected. Did you tender love for Ren disappear now?"
            u "Yes... you've came here alone, of course you'd go out alone."
            jump u921
        "I'm going out with Ren.":
            u "Yes... I knew you'd say that."
            u "The only reason you've stayed here is because of Ren"
            u E"ven when I bullied you, you never thought of escaping. All because of Ren, am I right?"
            jump u921
        "I'll leave with you, Sora.":
            u "With me? If you're joking, it's not funny."
            u "I can't leave here. You know that already."
            u "If I leave here, what do think will happen to me? Do you really think I can live an proper, ordinary life?"
            u "Someone like me can't enjoy life like you or the RFA people."
            u "... Don't try to make me hope the impossible dream."
            jump u921
        "I want to say here with savior.":
            u "That's odd..."
            u "From what I know of you, you haven't even taken the elixir but you talk like someone who did."
            jump u921
label u921:
    u "Anyway, I've said what I wanted to say to you."
    u "If you have nothing more to say, I'm hanging up."
    menu:
        "Sora, don't you want to get out of here?":
            u "I'm..."
            u "No. It's no use, useless now."
            jump u922
        "Is it really Sora? Aren't you Ren?":
            u "Why? Do you want that weaky squeaky Ren to come out again?"
            u "If you really want to see him, then do something like the last time."
            jump u922
        "Sora...is you're faith becoming weak?":
            u "You talk like savior."
            u "I don't know. I don't know what my faith is..."
            u "That's for savior to decide."
            u "As always..."
            jump u922
label u922:
    u "Is that all? Anyway, it was fun listening to your daydreams."
    u "Forget everything I said."
    u "I'm hanging up now."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "21:31" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music20.ogg"
    call phone_start from _call_phone_start_14
    call message_start("SYSTEM", "Reina has entered the chatroom.") from _call_message_start_194
    call message("Reina", "Oh no...") from _call_message_1649
    call message("Reina", "Sora actually wrote that") from _call_message_1650
    call message("Reina", "he wants to") from _call_message_1651
    call message("Reina", "leave this place....") from _call_message_1652
    call message("Reina", "He wants to directly violate Ao Me's most important rule.") from _call_message_1653
    call message("Reina", "Just what on earth") from _call_message_1654
    call message("Reina", "happened to him?") from _call_message_1655
    call screen phone_reply3("He's confused because of the elixir's side effect.", "cd9117", "....I think he's slowly breaking away from your boundaries.", "cd9118", "He's coming back as Ren.", "cd9119")
label cd9117:
    call phone_after_menu from _call_phone_after_menu_182
    call message_start("[name]", "He's confused because of the elixir's side effect.") from _call_message_start_195
    call message("Reina", "I think it's partially because of what you told him....") from _call_message_1656
    jump am941
label cd9118:
    call phone_after_menu from _call_phone_after_menu_183
    call message_start("[name]", "....I think he's slowly breaking away from your boundaries and opening his eyes to the truth.") from _call_message_start_196
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "[name], you spoke something outrageously unappreciated") from _call_message_1657
    call message("Reina", "into his ears again.") from _call_message_1658
    jump am941
label cd9119:
    call phone_after_menu from _call_phone_after_menu_184
    call message_start("[name]", "He's coming back as Ren.") from _call_message_start_197
    call message("Reina", "Did your chants calling for Ren") from _call_message_1659
    call message("Reina", "affect Sora?") from _call_message_1660
    jump am941
label am941:
    call message("Reina", "No...") from _call_message_1661
    call message("Reina", "No.") from _call_message_1662
    call message("Reina", "...That won't be enough to change him.") from _call_message_1663
    call message("Reina", "I wanted to watch over you more tightly.") from _call_message_1664
    call message("Reina", "But it turns out") from _call_message_1665
    call message("Reina", "your room is strictly off-limits") from _call_message_1666
    call message("Reina", "for the other believers.") from _call_message_1667
    call message("Reina", "I tried to watch you") from _call_message_1668
    call message("Reina", "through the camera he installed") from _call_message_1669
    call message("Reina", "but apparently, the camera was encrypted with a password") from _call_message_1670
    call message("Reina", "so no one can access it....") from _call_message_1671
    call screen phone_reply3("Oh dear. He must have liked me.", "cd9120", "I'll let you know right away once Sora comes back.", "cd9121", "...I see.", "cd9122")
label cd9120:
    call phone_after_menu from _call_phone_after_menu_185
    call message_start("[name]", "Oh dear. He must have liked me.") from _call_message_start_198
    call message("Reina", "I want you to know") from _call_message_1672
    call message("Reina", "that there's no way") from _call_message_1673
    call message("Reina", "he'd know how to properly love someone.") from _call_message_1674
    jump am942
label cd9121:
    call phone_after_menu from _call_phone_after_menu_186
    call message_start("[name]", "I'll let you know right away once Sora comes back.") from _call_message_start_199
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "Yes.") from _call_message_1675
    call message("Reina", "That's what I wanted to hear.") from _call_message_1676
    jump am942
label cd9122:
    call phone_after_menu from _call_phone_after_menu_187
    call message_start("[name]", "...I see.") from _call_message_start_200
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Why didn't he make them accessible to everybody else?") from _call_message_1677
    call message("Reina", "It appears his possessiveness over his toy") from _call_message_1678
    call message("Reina", "was stronger than I thought.") from _call_message_1679
    jump am942
label am942:
    call message("Reina", "He actually encrypted the camera....") from _call_message_1680
    call message("Reina", "I should have noticed sooner") from _call_message_1681
    call message("Reina", "how he's exhibiting strange behaviors....") from _call_message_1682
    call message("Reina", "This is partially my fault.") from _call_message_1683
    call message("Reina", "I trusted him too much.") from _call_message_1684
    call message("Reina", "Anyhow from now on") from _call_message_1685
    call message("Reina", "my believers will intermittently and randomly visit your room.") from _call_message_1686
    call message("Reina", "Since Sora might come to see you") from _call_message_1687
    call message("Reina", "alone in your room.") from _call_message_1688
    call screen phone_reply3("I'll cooperate, my savior.", "cd9123", "You're going to capture him by force.", "cd9124", "Ren will be back like a prince.", "cd9125")
label cd9123:
    call phone_after_menu from _call_phone_after_menu_188
    call message_start("[name]", "I'll cooperate, my savior.") from _call_message_start_201
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "That's a little reassuring.") from _call_message_1689
    jump am943
label cd9124:
    call phone_after_menu from _call_phone_after_menu_189
    call message_start("[name]", "You're going to capture him by force.") from _call_message_start_202
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "That's right.") from _call_message_1690
    jump am943
label cd9125:
    call phone_after_menu from _call_phone_after_menu_190
    call message_start("[name]", "Ren will be back like a prince.") from _call_message_start_203
    call message("Reina", "...I'd say Ren doesn't deserve to be a prince, but...") from _call_message_1691
    jump am943
label am943:
    call message("Reina", "If you see Sora") from _call_message_1692
    call message("Reina", "make sure you notify the other believers.") from _call_message_1693
    call message("Reina", "If you do...I'll acknowledge your feat") from _call_message_1694
    call message("Reina", "and cancel your cleansing ceremony") from _call_message_1695
    call message("Reina", "and promise your freedom at this place.") from _call_message_1696
    call message("Reina", "How about it?") from _call_message_1697
    call message("Reina", "Doesn't it sound attractive enough?") from _call_message_1698
    call screen phone_reply3("I can't do that. I know what he wants....", "cd9126", "I'll do my best, my savior!", "cd9127", "That's not good enough. I need a house to live with Ren.", "cd9128")
label cd9126:
    call phone_after_menu from _call_phone_after_menu_191
    call message_start("[name]", "I can't do that. I know what he wants....") from _call_message_start_204
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "I know the best") from _call_message_1699
    call message("Reina", "what he wants.") from _call_message_1700
    jump am944
label cd9127:
    call phone_after_menu from _call_phone_after_menu_192
    call message_start("[name]", "I'll do my best, my savior!") from _call_message_start_205
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "I'm counting on you.") from _call_message_1701
    jump am944
label cd9128:
    call phone_after_menu from _call_phone_after_menu_193
    call message_start("[name]", "That's not good enough. I need a house to live with Ren.") from _call_message_start_206
    call message("Reina", "If you're being serious") from _call_message_1702
    call message("Reina", "we can discuss that") from _call_message_1703
    call message("Reina", "after") from _call_message_1704
    call message("Reina", "that boy has fallen into my hands.") from _call_message_1705
    jump am944
label am944:
    call message("Reina", "But if you hide Sora") from _call_message_1706
    call message("Reina", "or say anything foolish to him...") from _call_message_1707
    call message("Reina", "you will get to see") from _call_message_1708
    call message("Reina", "Sora suffering...") from _call_message_1709
    call message("Reina", "suffering the most dreadful agony you can imagine.") from _call_message_1710
    call message("Reina", "And once he's cleansed after his agony") from _call_message_1711
    call message("Reina", "he'll return") from _call_message_1712
    call message("Reina", "all that pain he suffered") from _call_message_1713
    call message("Reina", "to you.") from _call_message_1714
    call screen phone_reply3("You seem to be used to exploiting pain.", "cd9129", "That won't happen. Please trust me.", "cd9130", "Reina, you don't happen to enjoy this, do you?", "cd9131")
label cd9129:
    call phone_after_menu from _call_phone_after_menu_194
    call message_start("[name]", "You seem to be used to exploiting pain.") from _call_message_start_207
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Pain is directly related to the remedy soon to come.") from _call_message_1715
    call message("Reina", "So pain is equivalent to remedy.") from _call_message_1716
    jump am945
label cd9130:
    call phone_after_menu from _call_phone_after_menu_195
    call message_start("[name]", "That won't happen. Please trust me.") from _call_message_start_208
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "I hope your loyalty actually brings me results....") from _call_message_1717
    call message("Reina", "We'll see.") from _call_message_1718
    call message("Reina", "So you plan to stay here as long as you can.") from _call_message_1719
    jump am945
label cd9131:
    call phone_after_menu from _call_phone_after_menu_196
    call message_start("[name]", "Reina, you don't happen to enjoy this, do you?") from _call_message_start_209
    call message("Reina", "I enjoy watching things") from _call_message_1720
    call message("Reina", "moving impeccably according to my designs.") from _call_message_1721
    call message("Reina", "In fact") from _call_message_1722
    call message("Reina", "remedy and agony") from _call_message_1723
    call message("Reina", "are part of a huge circle.") from _call_message_1724
    jump am945
label am945:
    call message("Reina", "And at this paradise") from _call_message_1725
    call message("Reina", "there is a circle forever spinning") from _call_message_1726
    call message("Reina", "with remedy as much as needed") from _call_message_1727
    call message("Reina", "as well as agony as much as needed...") from _call_message_1728
    call message("Reina", "You will soon get to witness") from _call_message_1729
    call message("Reina", "such a holy wonder.") from _call_message_1730
    call message("Reina", "I look forward") from _call_message_1731
    call message("Reina", "to your role") from _call_message_1732
    call message("Reina", "until he's back.") from _call_message_1733
    call message("Reina", "Now excuse me.") from _call_message_1734
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_1735
    call phone_end from _call_phone_end_15
    stop music
    scene black with dissolve
    with Pause(2.0)
    play music "music/music20.ogg"
    scene bg15 with dissolve
    show R R33 with dissolve
    r "...I must not leave Sora astray any longer. Now this is beyond dangerous."
    show R R32
    r "This is all because of that girl..."
    show R R33
    r "Once this is wrapped up, I should eradicate her, as if she's never existed."
    show R R37
    r "How dare that outsider play puppets with us, with my key believer as a bait? How dare that weed from a greenhouse...!"
    r "Have a good night's rest. Tonight will be the last night you would ever get to sleep comfortably..."
    hide R with dissolve
    stop music
    scene black with dissolve
    "... ..."
    "..."
    "........."
    "......"
    "...."
    whooo ".....[name]."
    scene bg7 with dissolve
    show MC MC14 at my_right with dissolve
    menu:
        "Mmm... I'm sleeping.":
            whooo "Sorry to wake you up."
            mc "...You're sorry? Hang on."
            jump u923
        "...K?":
            show U U22 at my_left with dissolve
            u "No. It's me."
            jump u923
        "Ren?":
            show U U22 at my_left with dissolve
            u "...He's also listening inside me."
            jump u923
        "Sora...?":
            show U U22 at my_left with dissolve
            u "You recognize me right away."
            u "...You weren't waiting for me, were you?"
            jump u923
label u923:
    show U U22
    u "No need to get out of bed."
    play music "music/music22.ogg"
    hide MC with dissolve
    hide U with dissolve
    scene black with dissolve
    u "Relax. I'm not here to torture you."
    u "...I will no longer torture you."
    menu:
        "How come?":
            u "...It's difficult to summarize in one word."
            jump u924
        "What happened?":
            u "Lots of things happened."
            jump u924
        "Is it because of Ren?":
            u "Haha... Yes...and no."
            u "I wouldn't have become so gentle if Ren stayed silent inside me."
            jump u924
        "I must tell the savior about this!":
            u "I'll let you do that...if you want to get revenge for torturing you."
            u "But before you do, please listen to me."
            jump u924
label u924:
    u "[name], you never gave up no matter how much I tormented you."
    u "You didn't let my torture and your wounds crush you."
    u "I cursed you dozens of times that your eyes are disgusting."
    u "Because you looked like you know a world completely different from mine."
    u "And when I kept looking into your face, it felt like I was losing. I wanted to get closer to you, but I also wanted to avoid you. I wanted to know you better, but at the same time, I wanted to neglect you."
    u "Even after I tormented you...it didn't feel like I beat you."
    u "That's why I felt empty. I felt so empty... So in the end, I got angrier, and I wanted to torment you even more."
    u "You're gentle, but you never fall down. You rebel, but you don't return your pain to me."
    u "You avoided me, but you never gave up on me completely... Even now you don't give up on me."
    u "My darkness is melting away, but you still remain the way you are..."
    u "How can you stay the same?"
    menu:
        "I thought that you were being mean to me because you were terribly wounded in the past.":
            u "You understood me. I didn't expect you to do so... I should thank you, shouldn't I?"
            u "Thank you...for understanding me."
            jump up925
        "I was waiting for Ren.":
            u "I see... He's inside me. He's alive, just like you."
            u "He didn't leave because you were waiting for him."
            jump u925
        "For now....I want to be loyal to the savior.":
            u "You didn't look like you wanted to be loyal this morning... What happened?"
            u "Or did you catch that from me...? Now I feel responsible."
            jump u925
label u925:
    u "About those things I said to you...that you're weak and useless... They're not true at all."
    u "But you already know that, don't you?"
    u "I'm the one who's really useless."
    u "I said all those cruel things to you because I didn't want you to realize that I'm useless...."
    menu:
        "Sora...you were cruel to me....but you did your job well. Sort of. You're not useless.":
            u "I tried to protect this place because I wanted somebody to recognize my worth."
            u "I wanted someone to tell me that I'm truly useful."
            u "But no matter how much recognition I got, it all felt pointless not long after."
            u "I'll just start feeling that I'm useless again."
            u "Like a computer programmed that way."
            u "I've never felt satisfied with something during my lifetime."
            u "No matter how much I bragged about getting compliments, in the end, I couldn't stop questioning myself whether I deserve them."
            jump u926
        "But even if you said those cruel words, you were the one to get hurt.":
            u "...Yes, maybe that's why it felt so empty. My actions were different from Ren's, but the results were the same."
            u "What I said to you weren't meant for you. They were meant for me."
            u "I was telling them to myself. Now I get it."
            jump u926
label u926:
    u "So no matter how much you tried to guide me to the right path, your words couldn't reach me. Even now...it's so difficult to accept myself the way I am. I'm ruined beyond hope."
    u "It's impossible for me to throw away my hatred against the world and start all over again in this little hell."
    u "But...maybe it is possible for Ren."
    u "He may be a chicken, but he doesn't know how to hate..."
    u "He'd rather hurt himself than to hate someone."
    u "Even if he was betrayed by his brother, his parents, and his trusted guardian K...."
    u "Even now...he's speaking inside me...that maybe...just maybe...."
    u "If what K said is true, he might meet again with his brother and stay together just like they used to...."
    u "And he's saying...that if he begs for your forgiveness,"
    u "and if by any chance you forgive him,"
    u "then he'll make you happy as best as he can..."
    u "He's a scaredy-cat, of course, so he's worried to death that his hope might turn out to be no use...."
    u "But his hope will never die. He's praying in the corner of my heart that everything will work out well and that he'll be happy."
    u "I...I know nothing but anger. I get angry because I'm scared my hope will turn into despair..."
    u "But as for Ren, no matter how many times he's betrayed and hurt and tortured, he'll keep hoping like a little weed."
    menu:
        "Is Ren really inside you, Sora?":
            u "Yes. And he's speaking even now."
            jump u927
        "Can't you be one with Ren?":
            u "...We already are one. It's just that my voice is stronger right now."
            jump u927
label u927:
    u "You miss Ren, don't you?"
    u "He was named Ren...in hopes that there will be no meaning to his name."
    u "But you look a little sad every time I say that name."
    u "Both Ren and I...are in this body."
    u "The monster that had to torture you to hide how pathetic he's become is me,"
    u "and the persistent idiot that doesn't lose hope no matter how much he's tormented is also me."
    u "...And it looks like you need Ren more than me."
    menu:
        "You're right. I need Ren.":
            u "...Yes, so which is why I should hide for a while. I won't be gone, but I'll continue to watch you quietly."
            jump u928
        "If both of them are you.....":
            u "Once I become good enough to keep you unharmed...we'll naturally become completely one."
            jump u928
        "Whoever you are...I hope you stay strong.":
            u "I wonder what your definition of strong is."
            u "Please tell Ren about it."
            jump u928
label u928:
    u "...I must go now."
    u "Ren will make you happy."
    u "Now I'll leave. So long..."
    menu:
        "Sora, wait!":
            u "I didn't want to tell you...that I'm sorry...."
            u "But I'm sorry. I'm sorry for hurting you."
            u "I hope...both you and Ren...will be happy."
            jump u929
        "Please come back as Ren.":
            u "I will... I hope you don't have to suffer anymore."
            jump u929
label u929:
    stop music
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Reina is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    r "You've picked up right away. Were you waiting for my call?"
    r "Or were you waiting for Sora?"
    r "Sora has lost his track."
    r "He's wandering in a thick fog, not being able to see nor hear."
    r "Poor child... if he stayed obediently under my darkness, he wouldn't have to wander like that..."
    r "The light you've showed to Ren has now seduced Sora."
    r "Even so, I won't let go of him. He is my child and I never abandon my child."
    r "You should have shown the proper road to Sora, who's been lightheadedly yearned the world and got lost. Numerous times!"
    r "So, did you think about my proposal?"
    r "The proposal to make Sora strong like a warrior by combining our powers."
    r "That child is still unstable. That's why someone must stay beside him and lead him. Someone like you."
    menu:
        "Sora can think on his own.":
            r "*sigh* Are you still saying that?"
            r "Then look at the results such thoughts have brought."
            r "You're still barred inside your room and Sora is wandering in a weak state."
            jump r93
        "Can I do as I like?":
            r "Sure. You can do whatever you want to do."
            r "As long as you don't break the rules of Ao Me, I can guarantee your freedom."
            jump r93
        "I'll make him into an outstanding warrior, beyond your imagination.":
            r "You're very confident. I like that attitude."
            r "I think highly of your capabilities."
            jump r93
label r93:
    r "So I hope you think it through."
    r "When you completely become one of Ao Me, you'll be a great help to us."
    r "I can give you everything. Everything you want."
    r "This place here applies a different set of rules of law and moral from the world."
    r "I look forward to your future, where you will spread your wings."
    r "I hope to hear a positive reply."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "23:13" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music16.ogg"
    scene bg8 with dissolve
    show S S212 with dissolve
    s "...Yeah, [name]'s logs are back. Are you sure she's safe? Haaa... Ok. I believe you."
    s "So could you tell the rest of the RFA that she's safe?"
    show S S214
    s "Everyone was so worried about her... Though I should be worrying about myself."
    show S S212
    s "K, I used those files you gave me...to complete my investigation. I feel like I've dug up every corner of the web of this planet."
    s "I had to put off the messenger security because of that, but now I feel like all pieces of the puzzle are in my hands."
    s "The secret about me and Sora is just the beginning."
    s "This guy's been breaking laws in every possible way throughout his life.... I had no idea it would be this bad."
    s "And people around him wrapped up what he did so meticulously to use them for his political benefits.... Which means they are all in this together."
    show S S214
    s "How can they make a man like him look so moral and noble...? He's terrifically smart. In a bad way, unfortunately."
    show S S212
    s "Once these files are unleashed...the entire world will know what he is really like. If these files manage to reach the public safely, that is."
    s "Once the investigations on him start, everyone related to his illegal activities will be summoned."
    show S S214
    s "Then the entire nation will be in uproar...and giant companies like C&R can't avoid investigations."
    show S S212
    s "To be honest, I have no idea how this scandal will end, K...."
    s "It would be only a matter of time for such a powerful authority to find me."
    s "So like I said, I think this is time for me to..."
    play sound "music/dingdong.ogg"
    "(Ding-dong)"
    show S S210
    s "Oh, Hiroki must be back. He's been using the doorbell once he learned there is one."
    show S S212
    s "Let's talk again, :."
    s "Yep. The agency will make a new alias for me only when I do a good job as an agent... So I might have to work undercover for a while."
    s "Sure. Watch yourself. I'll call again."
    play sound "music/beep.ogg"
    "(...)"
    show S S215
    stop music
    s "It's exactly about time he would be back. Now I should stop pretending and joking and get to work for real."
    play sound "music/dooropen.ogg"
    hide S with dissolve
    "(Door opened)"
    show S S212 at my_left with dissolve
    s "Welcome back, madam."
    show VW VW3 at my_right with dissolve
    vw "...."
    show S S210
    play music "music/music12.ogg"
    s "What's wrong? Why are you keeping your mouth shut? Huh...? That sign must be...."
    whooo "I told you not to do anything stupid!"
    show VW VW6
    vw "Ugh...!"
    show S S212
    s "Madam, could you please tell me this is a prank?"
    whooo "Don't move! Or else we'll shoot!"
    s "...I should've stayed armed. Who are you? Where are you all from?"
    whooo "Take them both away!"
    stop music
    hide VW with dissolve
    hide S with dissolve
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Sayaka is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    j "Hello? It's you. You haven't been sleeping yet."
    j "I've confirmed your profile and exchanged chats in the chatroom but... it didn't feel real that I had to call."
    j "At a state where the hacker issue hasn't been all cleaned up.... I thought there was a limit in talking only through chatrooms."
    menu:
        "Listening to your voice Sayaka, I can really feel that I'm back to the RFA.":
            j "Yes. It's good that you're back."
            j "The two days you've been gone were extremely very long two days."
            jump j91
        "You haven't been able to defend the hacker yet?":
            j "No... seems we're far from it."
            j "If it wasn't for Zero's help, we would have been even more lost."
            jump j91
label j91:
    j "Oh, thinking of which, I tried calling Levan to tell him that you're back but he didn't answer."
    j "I probably knows since he's faster in catching information than us."
    show MC MC14
    menu:
        "Did something happen...":
            j "That's what I thought a number of times but he helps out when we almost forget about him."
            j "As if he's notifying us to say he's alive."
            j "There's no one who cares about security as Levan so I don't think we need to worry."
            j "But...."
            jump j92
        "Maybe he's busy working....":
            j "Did Levan's agent work get busier?"
            j "Before, however his agent work was busy, he would drop by the chatroom but now he rarely comes in which is making me worry."
            j "And..."
            jump j92
label j92:
    j "It's a shame that everyone can't celebrate and share updates when we've all waited for your return."
    show MC MC11
    menu:
        "It would be great if everyone can gather and look at Shun's selfies...":
            j "That's...that's a very good idea."
            j "If it's to be done, I want to share everyone's thoughts about the aesthetics between Zekyll selfie and White selfie."
            j "... Though I'm not sure everyone will participate."
            jump j93
        "I think it's a bit early to be happy.":
            j "Yes... when I look at all the problems we have to solve... a sigh just comes out."
            j "Mr. Han's return and Levan can't be reached contact..."
            j "And most of all, the party on hold is just around the corner and we can't reach K well either"
            jump j93
label j93:
    j "*Yawn* ... Oh, I'm sorry. I should have a extra black coffee."
    j "Anyhow, since you've returned, it has solved a problem."
    menu:
        "Sayaka, you should rest.":
            j ".. Thank you for worrying about me."
            j "As expected... hearing to your voice, I can literally sense you're back... and I'm somewhat relieved."
            jump j94
        "Add an extra shot.":
            j "Why yes. I need to stay awake to finish the other tasks..."
            j "I think I felt a bit relieved for having one problem solved"
            j "Get a grip, Sayaka."
            jump j94
label j94:
    j "Anyhow, [name], welcome back to RFA."
    j "I.. wanted to say this to you directly."
    j "You did very well. Now, I shall return to work. Bye."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    call day10 from _call_day10
