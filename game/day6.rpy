label day6:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 6" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "00:55" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music4.ogg"
    show MC MC14 with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    "(Someone is knocking at the door.)"
    menu:
        "(Open the door.)":
            play sound "music/dooropen.ogg"
            jump r61
        "Who is it?":
            whooo "You're not asleep yet."
            whooo "There's something I must tell you regarding this place. May I come in?"
            "(I don't recognize this woman's voice.)"
            play sound "music/dooropen.ogg"
            mc "(Open the door.)"
label r61:
    show R R33mask at my_left with dissolve
    whooo "I'm sorry to intrude and introduce myself in the middle of a night."
    whooo "But I just couldn't afford to find another time."
    show R R31mask
    whooo "So...you must be [name]. Ren told me so much about you."
    show R R32mask
    whooo "I'm sure it was frustrating, being trapped here. I cannot thank you enough for waiting and keeping faith in us."
    show R R33mask
    whooo" Not to mention I heard you've been doing an excellent job in the RFA chat room in order to help us."
    show R R31mask
    whooo "Oh, where are my manners?"
    whooo "Did Ren already tell you about me?"
    show R R34mask
    whooo "I'm the founder of the Ao Me... Everyone at this place calls me savior."
    savior "You can call me that if you'd like."
    show R R33mask
    menu:
        "Where is Ren...?":
            savior "You want to know where he is?"
            savior "He usually works in the intelligence room not far from here."
            jump r62
        "So you're the one who cleanses him.":
            savior "Well, I can perform cleansing in completion on him...only after he has finished healing himself."
            savior "I plan to help him to the best of my abilities until then."
            savior "If you plan likewise, you'd be a great help to him."
            jump r62
label r62:
    show R R32mask
    savior "Ren is tasked with an essential role in this place."
    show R R33mask
    savior "He created the information system for this place. And he's in charge of the maintenance of that system."
    savior "His role is crucial for the proper operation of the Ao Me."
    savior "And he is also entrusted with the cleansing project for the RFA, which you are also a part of."
    savior "To give you more details, the main objective of this project is to collect preliminary information on the RFA through their chat rooms, to ultimately deliver them to us."
    savior "This is something Ren has been devoting all his time for the past 6 months..."
    savior "The Ao Me can never be free to establish its paradise unless we save the RFA from our enemies called K and Levan."
    savior "Which is why this project is directly related to our existence."
    show R R32mask
    savior "But thankfully, we were able to bring you here safe and sound to be part of this with Ren's effort."
    show R R33mask
    savior "I thought Ren's burden would have lessened with you..."
    savior "But now there is an unexpected problem."
    savior "I've noticed that since who-knows-when Ren has grown...unusually weak in heart."
    show R R31mask
    savior "Don't get me wrong. I'm not trying to blame you."
    show R R33mask
    savior "The problem does not lie within you. It lies within Ren."
    savior "Anyhow, my anxiety is growing. I am far from relieved that our work might not prove to be successful, with the finish line merely steps away."
    savior "Same could be said of Ren."
    savior "I'm sure he's frustrated as well..."
    savior "But it is also my duty to make sure his effort does pay off."
    savior "So this project must not fail. For his sake..."
    show R R31mask
    savior "Oh, that's right. I forgot the most important thing...."
    show R R34mask
    savior "I hereby give you my warmest and sincerest welcome, Believer [name], our special missionary."
    savior "I'm hearing a series of reports on the performances you're demonstrating in RFA chat rooms.... I look forward to your continued effort and success."
    show R R31mask
    savior "Here. This key card will prove your status in this place from here on."
    mc "(Take the key card.)"
    savior "With this card, you can access anywhere in this building."
    savior "You should avoid visiting the intelligence room, however, since that's where Ren is working. And the security guards will give you a rundown of the places that you should not access."
    show R R32mask
    savior "I give you my word that you'll find only salvation and cleansing in this place.... Believer [name]."
    show R R33mask
    savior "I wish I could stay longer, but I must go now. There is a report I'm waiting to hear."
    savior "I must check whether a certain promise has been kept."
    show R R34mask
    savior "Anyhow, I welcome you once again."
    show R R31mask
    savior "...For our paradise."
    savior "And now, I hope you find true happiness in you... I hope we meet again."
    hide R with dissolve
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "03:17" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music14.ogg"
    call phone_start from _call_phone_start_4
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_54
    call message("Ren", "[name]") from _call_message_408
    call message("Ren", "you're logged in") from _call_message_409
    call message_img("Ren", "at this hour...?", "images/questioning.png") from _call_message_img_24
    call message("Ren", "I heard about") from _call_message_410
    call message("Ren", "what happened.") from _call_message_411
    call message("Ren", "I heard that my savior paid you a visit") from _call_message_412
    call message("Ren", "and gave you the ID card to use at this place.....") from _call_message_413
    call message("Ren", "[name]...") from _call_message_414
    call message("Ren", "I was wondering whether she told you anything else...") from _call_message_415
    call screen phone_reply("I think she basically welcomed me. And it looked like she's worried about you too.", "cd61", "She said you need to do better than that.", "cd62")
label cd61:
    call phone_after_menu from _call_phone_after_menu_52
    call message_start("[name]", "I think she basically welcomed me. And it looked like she's worried about you too.") from _call_message_start_55
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Really...?") from _call_message_416
    call message_img("Ren", "I'm happy she welcomed you!", "images/smile.png") from _call_message_img_25
    jump am61
label cd62:
    call phone_after_menu from _call_phone_after_menu_53
    call message_start("[name]", "She said you need to do better than that.") from _call_message_start_56
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "....") from _call_message_417
    call message("Ren", "So I'm the problem after all.") from _call_message_418
    jump am61
label am61:
    call message("Ren", "I didn't think") from _call_message_419
    call message("Ren", "she'll give you the card in person.") from _call_message_420
    call message("Ren", "I'm the one who brought you here") from _call_message_421
    call message("Ren", "but I caused trouble...") from _call_message_422
    call message("Ren", "So I was thinking maybe she wanted to check for herself") from _call_message_423
    call message("Ren", "whether you really are a reliable person...") from _call_message_424
    call message("Ren", "But") from _call_message_425
    call message("Ren", "looks like I was worried for nothing.") from _call_message_426
    call message("Ren", "After seeing you in person") from _call_message_427
    call message("Ren", "she complimented me") from _call_message_428
    call message("Ren", "that I chose just the right person.") from _call_message_429
    call screen phone_reply("I have a feeling I'll get along with her well.", "cd63", "I'm happy...that I was welcomed and introduced to an interesting mission thanks to you.", "cd64")
label cd63:
    call phone_after_menu from _call_phone_after_menu_54
    call message_start("[name]", "I have a feeling I'll get along with her well.") from _call_message_start_57
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I'm so glad if that's the case.") from _call_message_430
    call message_img("Ren", "I knew you'd fit in perfectly!", "images/smile.png") from _call_message_img_26
    call message("Ren", "I....") from _call_message_431
    call message("Ren", "I'm closer to an outcast") from _call_message_432
    call message("Ren", "but [name]") from _call_message_433
    call message("Ren", "you're much smarter than me....") from _call_message_434
    jump am62
label cd64:
    call phone_after_menu from _call_phone_after_menu_55
    call message_start("[name]", "I'm happy...that I was welcomed and introduced to an interesting mission thanks to you.") from _call_message_start_58
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "I'm glad") from _call_message_435
    call message_img("Ren", "that's the case...", "images/smile.png") from _call_message_img_27
    call message("Ren", "I thought") from _call_message_436
    call message("Ren", "everything I do will end up being ruined") from _call_message_437
    call message("Ren", "but if there's one thing I really did well") from _call_message_438
    call message_img("Ren", "it's that I got to meet you.", "images/wink.png") from _call_message_img_28
    call message("Ren", "Oops...") from _call_message_439
    jump am62
label am62:
    call message("Ren", "My savior told me") from _call_message_440
    call message("Ren", "that I must control myself") from _call_message_441
    call message("Ren", "since you're too charming for me....") from _call_message_442
    call message("Ren", "....") from _call_message_443
    call message("Ren", "Anyways") from _call_message_444
    call message("Ren", "I'm...so glad") from _call_message_445
    call message("Ren", "that she didn't say anything about the elixir to you....") from _call_message_446
    call message("Ren", "It's so painful if you drink that.") from _call_message_447
    call message("Ren", "I'll have to take it regularly") from _call_message_448
    call message("Ren", "from now on") from _call_message_449
    call message("Ren", "but I hope you never get to take it!") from _call_message_450
    call screen phone_reply("You're protecting me, aren't you? Thank you...", "cd65", "I can drink it if I have to.", "cd66")
label cd65:
    call phone_after_menu from _call_phone_after_menu_56
    call message_start("[name]", "You're protecting me, aren't you? Thank you...") from _call_message_start_59
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "[name], you came all the way here only because you trusted me....") from _call_message_451
    call message("Ren", "I never ever wish") from _call_message_452
    call message("Ren", "to betray your faith....") from _call_message_453
    call message("Ren", "And [name]") from _call_message_454
    call message("Ren", "I will make sure") from _call_message_455
    call message("Ren", "you don't have to go through anything sad") from _call_message_456
    call message("Ren", "or demanding") from _call_message_457
    call message("Ren", "or painful!") from _call_message_458
    jump am63
label cd66:
    call phone_after_menu from _call_phone_after_menu_57
    call message_start("[name]", "I can drink it if I have to.") from _call_message_start_60
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "...Really?") from _call_message_459
    call message("Ren", "But....") from _call_message_460
    call message("Ren", "it's really painful if you drink it.") from _call_message_461
    call message("Ren", "I...") from _call_message_462
    call message_img("Ren", "I never want to see you in pain...", "images/sad.png") from _call_message_img_29
    jump am63
label am63:
    call message("Ren", "You don't have to drink it") from _call_message_463
    call message("Ren", "because") from _call_message_464
    call message("Ren", "you're much stronger and more wonderful than me....") from _call_message_465
    call message("Ren", "But I have to drink the elixir") from _call_message_466
    call message("Ren", "because my body just won't grow.") from _call_message_467
    call message("Ren", "That must be why my savior's worried, isn't it...?") from _call_message_468
    call message("Ren", "I'm worried that I might not live up to her expectations.") from _call_message_469
    call message("Ren", "When she visited earlier") from _call_message_470
    call message_img("Ren", "she looked like she wasn't happy with me....", "images/sad.png") from _call_message_img_30
    call message("Ren", "[name]") from _call_message_471
    call message("Ren", "Do you also think I'm not good enough...?") from _call_message_472
    call message("Ren", "I wish") from _call_message_473
    call message("Ren", "I were born") from _call_message_474
    call message("Ren", "stronger...") from _call_message_475
    call message("Ren", "Then") from _call_message_476
    call message("Ren", "I would have had more time") from _call_message_477
    call message("Ren", "to stay with you....") from _call_message_478
    call screen phone_reply("You're pathetic...", "cd67", "You might have a hard time right now, but things will get better soon. I hope you'd consider that I'm there with you.", "cd68")
label cd67:
    call phone_after_menu from _call_phone_after_menu_58
    call message_start("[name]", "You're pathetic...") from _call_message_start_61
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "...Sorry.") from _call_message_479
    call message("Ren", "I'm so sorry.") from _call_message_480
    call message("Ren", "What should I do to have your forgiveness?") from _call_message_481
    call message_img("Ren", "I'll do anything...", "images/sad.png") from _call_message_img_31
    call message("Ren", "Oh") from _call_message_482
    jump am64
label cd68:
    call phone_after_menu from _call_phone_after_menu_59
    call message_start("[name]", "You might have a hard time right now, but things will get better soon. I hope you'd consider that I'm there with you.") from _call_message_start_62
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "...[name]") from _call_message_483
    call message("Ren", "Your words") from _call_message_484
    call message("Ren", "are so sweet and happy.", "images/wink.png") from _call_message_485
    call message("Ren", "When I listen to you") from _call_message_486
    call message("Ren", "I think I can do anything.") from _call_message_487
    call message("Ren", "I want to stay with you...") from _call_message_488
    call message("Ren", "...") from _call_message_489
    call message("Ren", "......Ah!") from _call_message_490
    call message("Ren", "I lost control of my imaginations") from _call_message_491
    call message("Ren", "again.") from _call_message_492
    jump am64
label am64:
    call message("Ren", "I should log out") from _call_message_493
    call message("Ren", "before I start typing something weird") from _call_message_494
    call message("Ren", "like an airhead.") from _call_message_495
    call message("Ren", "Oh") from _call_message_496
    call message("Ren", "but before I go") from _call_message_497
    call message("Ren", "there's this picture I want to show you.") from _call_message_498
    call message_img("Ren", "Take a look at this.", "images/cg12.png") from _call_message_img_32
    call message("Ren", "This flower") from _call_message_499
    call message("Ren", "is called lily of the valley.") from _call_message_500
    call message("Ren", "And the language of the flower behind it") from _call_message_501
    call message("Ren", "is 'Promise of Happiness.'") from _call_message_502
    call message("Ren", "Oh") from _call_message_503
    call message("Ren", "the notification's in.") from _call_message_504
    call message("Ren", "I should really go now.") from _call_message_505
    call message("Ren", "I really wish") from _call_message_506
    call message("Ren", "you'll be happy in your dreams.") from _call_message_507
    call message("Ren", "I'll pray with all my heart...") from _call_message_508
    call screen phone_reply("I hope you get some rest too...", "cd69", "You should go. Now.", "cd610")
label cd69:
    call phone_after_menu from _call_phone_after_menu_60
    call message_start("[name]", "I hope you get some rest too...") from _call_message_start_63
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Yes") from _call_message_509
    call message("Ren", "I hope one day") from _call_message_510
    call message("Ren", "I'd get to hold your hand and take a nap together...") from _call_message_511
    jump am67
label cd610:
    call phone_after_menu from _call_phone_after_menu_61
    call message_start("[name]", "You should go. Now.") from _call_message_start_64
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Yeah!") from _call_message_512
    call message("Ren", "I'll get going.") from _call_message_513
    call message("Ren", "Sorry for annoying you...") from _call_message_514
    jump am67
label am67:
    call message("SYSTEM", "Ren has lef the chatroom.") from _call_message_515
    call phone_end from _call_phone_end_5
    stop music
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "...Hello? Oh, you're not asleep yet."
    ray "I was wondering whether you were awake. Sorry to call so late."
    menu:
        "That's alright. Is there something wrong?":
            ray "No, nothing's wrong."
            ray "I...I just wanted to talk to you."
            jump u61
        "I was waiting for your call.":
            ray "Really? So we're connected."
            ray "I was also waiting for your call but then decided to call first."
            ray "My savior would be disappointed to hear this...but I just couldn't help it."
            ray "I was dying to hear your voice."
            jump u61
label u61:
    ray "Actually, I called you because I'm scared of falling asleep."
    ray "I'm having bad dreams these days. And I'll end up screaming and bolting awake."
    mc "What kind of dream is it?"
    ray "...It's a dream where I go back to my childhood. You see..."
    ray "I'd dream about going back to my mom's house and getting locked up in a dark room."
    ray "I can see the Magenuta right there out the window, but no matter how hard I hit the window and wave my hand, people outside can't see me."
    ray "And when I keep hitting the window, my mom would scream that I'm making a fuss. And then she'd whisper to me..."
    ray "...You're being more and more pain in the back, you bug. Just die. Just go ahead and die. You're better off dead."
    ray "...Hah, now I feel like I made you uncomfortable. I'm sorry."
    ray "I tend to stare blankly on my bed for a while when I wake up from a scary dream."
    ray "When I sit alone in a room full of darkness with no sound..."
    ray "I feel like I'm left behind all alone. It feels like nobody ever wants me..."
    ray "But you don't know what it feels like, do you?"
    menu:
        "I do. It's so sad and lonely.":
            ray "But you're so brilliant and shining all the time. Do you actually feel lonely?"
            ray "...How strange. I'm sad when you're sad and lonely..."
            ray "But I feel a little reassured...when I think that you've felt the way I do."
            ray "I'm such a bad person... I'm having bad thoughts... Sorry..."
            ray "But still,"
            jump u62
        "Don't be scared. I'll stay with you.":
            ray "I want to believe you. I want to stay in your arms whenever you tell me that."
            ray "I'm trying to hold myself because I'm scared you'll hate me if I do that..."
            ray "But I can't help longing for your words."
            ray "Do you know that whenever I listen to you, I feel like I'm not a useless person?"
            ray "Whenever you comfort me...I sometimes think that maybe I'm a very little useful."
            ray "Because when you talk about something about me, that means you had found that part acceptable..."
            ray "So I think...perhaps whatever you're talking about me is fine...."
            jump u62
label u62:
    ray "I feel a little better talking to you like this."
    ray "I think I won't have any bad dreams tonight...if I fall asleep like this."
    ray "I should hang up now, right...? You should go to sleep."
    menu:
        "And so should you. Let's talk again tomorrow.":
            ray "Yeah. I don't want to hang up, but we can talk tomorrow... So I'll go now."
            jump u63
        "Don't go. Let's talk some more.":
            ray "I'd love to do that...but you'll get tired."
            ray "We can talk again tomorrow... Let's go to bed now."
            jump u63
label u63:
    ray "I'm going to take a snooze for a while and then work."
    ray "Sweet dream and I'll see you tomorrow."
    ray "Now I really have to hang up. Make sure you stay warm in your bed... Good night."
    play sound "music/beep.ogg"
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "07:40" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music12.ogg"
    scene bg8 with dissolve
    show S S210 with dissolve
    s "...Why would he....look for K's photos...?"
    s "Are my eyes deceiving me?"
    show S S211
    s "No...no. The chat log said that he actually wants to contact K."
    show S S212
    s "There's no way he'd know about us...."
    s "Did he find out that I'm part of the RFA? Is he looking for me?"
    show S S213
    s ".....Don't tell me....I hope nothing has happened to Sora."
    hide S with dissolve
    stop music
    scene black with dissolve
    show text "10:23" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2.ogg"
    scene bg19 with dissolve
    show H H11 with dissolve
    show V V12 at my_right with dissolve
    show PR at my_left with dissolve
    h "It is beyond my greatest pleasure that we could find time to meet. Welcome, prime minister, sir."
    h "This is my friend Jurou Kim, pseudonym K."
    v "...Greetings, sir."
    pr "I apologize for requesting to meet so suddenly. I'd like you to please send my regards to the Chairman. He so graciously granted my sudden request."
    show H H11
    h "Of course, sir. But there's no need for an apology. This is an honor and pleasure for us as well."
    pr "I might sound cliché, but as I stand in the presence of the two young men full of potential for the future...I am motivated to work even harder for this country."
    show H H11
    h "I have no doubt that you will."
    pr "Thank you. And now...Mr. Kim....it is an honor to meet you."
    pr "I am ashamed to admit it, but I requested to meet because I have a personal favor."
    pr" I don't particularly have a hobby. The only activity I've done that is closest to hobby is accompanying my wife whenever she visits exhibitions."
    pr "And then like destiny....I stumbled upon the collection of the sun... Yes, it was none other than your work, Mr. Kim."
    show H H11
    h "That collection is very prestigious. It's a pleasure that you found my friend's works appealing. K?"
    show V V110
    v "...Are you saying you are interested in those photos?"
    pr "I'd like to purchase every photo from that collection, if possible."
    show H H12
    h "All of them...?"
    pr "I'm sure you'd know that I've been through various tests and challenges throughout my life as a politician...."
    pr "But I realized upon gazing at the collection."
    pr "I realized that all the hardship I've gone through...is nothing but a human triviality under the sun...."
    show H H11
    h "That's one possible interpretation. In fact, a member of the RFA made a similar interpre..."
    show V V110
    v "...Prime minister, sir."
    pr "Yes, please go ahead."
    show V V12
    v "I am gravely sorry. For now, my works are not on sale."
    pr "Oh...is that true? May I ask you why?"
    show V V110
    v "...I'm afraid I cannot tell you why."
    v "..."
    show H H13
    h "I'm sure it's a personal reason."
    pr "I understand. However, I hope you consider it again."
    pr "There was something I wanted to do with your works."
    pr "Perhaps I shouldn't have invited you two in such a rush."
    pr "Of course, I'm not saying that I am unhappy that you declined my offer..."
    show H H14
    h "I'm sorry you could not get what you want, sir."
    pr "That's alright. I basically forced this appointment to take place."
    pr "If only I could know what you'd do after this, Mr. Kim.... Could you tell me what you can regarding your career plans for the future?"
    show V V12
    v "...Nothing has been scheduled or finalized so far."
    pr "That's a shame..."
    show V V13
    v "..."
    show H H12
    h "...?"
    show V V110
    v "If you'd like to hear about it, please contact my personal workplace."
    pr "Oh...if I can, that would be an honor. I will do that."
    show V V12
    v "Here's my contact information."
    pr "Thank you. I will contact you in the future."
    pr "I'd like to thank once again for sparing your precious time for me."
    show H H11
    h "...Not at all. I wish you the best of luck in your future, sir."
    hide H with dissolve
    hide PR with dissolve
    hide V with dissolve
    stop music
    scene black with dissolve
    scene bg5 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "[name], this is K."
    v "I was wondering how you're doing there... And I was concerned with a couple more things."
    v "Is there any particular difference you noticed? Or anything peculiar of some sort?"
    menu:
        "No, nothing":
            v "Then thank heavens."
            v "I was concerned for nothing."
            v "Nevertheless, I hope you do not let your guard down."
            jump v61
        "I got my ID card!":
            v "That would mean they trust you now."
            v "And that would mean you will be safe for a while. Thank heavens."
            v "But that does not mean you can be reassured."
            v "I hope you always regard your safety as your top priority."
            v "That place...is of no ordinary sort."
            jump v61
label v61:
    v "...Is Sora... I mean, is Ren doing alright?"
    menu:
        "He's kind of depressed, but he's okay, I guess.":
            v "I see."
            v "I know this would be a nuisance, but please keep watching over that boy."
            jump v62
        "So you were interested in Ren, weren't you? I should've known you wouldn't be interested in me at all... Sniffle...":
            v "Uh, no. That's not true at all."
            v "It's just that you're the one on our side who contacts him most often..."
            v "I apologize in case I offended you."
            jump v62
label v62:
    v "As you know, the management of RFA matters more than anything as of now."
    v "That is, it is a must that we safely hold the party and safeguard the purpose of the RFA with your help."
    v "Which is why we cannot afford any unexpected change in any case, especially when it comes to that child...."
    v "I hope you can understand in this regard."
    v "Anyhow, I'm glad there's nothing wrong with you."
    v "I'll call you again."
    play sound "music/beep.ogg"
    hide MC
    stop music
    scene black with dissolve
    show text "13:10" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music15.ogg"
    scene bg15 with dissolve
    show R R33 at my_right with dissolve
    savior "There you are."
    show U U111 at my_left with dissolve
    ray "My savior...."
    savior "...."
    show U U19
    ray "I'm here to report to you."
    savior "But before that, I would like to hear the full contents of our slogan."
    show U U112
    ray "...For Ao Me's eternal paradise...I solemnly swear....cough...."
    show U U19
    ray "Cough....excuse me. I solemnly swear upon all my wounds...that I will stay by your side until the end, my savior."
    ray "I collected a total of 6 pieces of information since last evening... Two of them happen to be of the last party's guests...."
    ray "As for the jobs at hand, I'm upgrading the access system's security and progressing the 3rd upgrade for the hacking system...."
    show U U112
    show R R32
    ray "I wrote the details on the report.... Just a moment, please... Cough..."
    show U U19
    show R R33
    savior "...Are you really staying faithful to the slogan?"
    show U U111
    ray "....Sorry?"
    savior "You said that you swear on every inch of your trauma. That you'll stay by my side until the end."
    show U U19
    ray "Yes. I'll stay by your side until..."
    savior "But then why do you look like that?"
    show U U111
    ray "Sorry?"
    savior "Don't you see? Don't you really see?"
    show U U19
    ray "My savior, please. Give me a moment to think....cough...."
    savior "...Just what happened to your face?"
    hide R
    hide U
    scene black with dissolve
    ray "...Huff..."
    savior "Your face is pale. You said you'll stay with me...but what happened to your face?"
    ray "Cough...my savior..."
    savior "I've been receiving dozens of reports on you and only you since last evening, Ren. I thought of you even when I closed my eyes to go to sleep."
    savior "Do I really have to make sure you take the elixir? You're such an inconvenience...."
    ray "I'm sorry... I tried my best, but I wasn't good enough. From now on..."
    savior "From now on?"
    ray "I won't skip taking the elixir. I won't be lazy in maintaining security. I'll never stop working until I get the results you'll like..."
    savior "I believe I already heard that last time in the basement. Now I'm starting to get tired of your words."
    savior "Don't you remember? It was only a few months ago."
    ray "..."
    savior "...Oh, dear. Your eyes are lifeless. Poor Ren."
    savior "Should I increase the amount of elixir?"
    ray "No, it's more than enough, my savior! I'm taking...more than enough."
    savior "You don’t know what's enough for you. I already told you hundred times."
    savior "I knew it. You're growing arrogant...."
    ray "No, that's not true.... I will never, ever betray you...my savior...!"
    savior "...Here you go again. I don't want to hear what you think, Ren."
    savior "You prove yourself through your actions, not words."
    ray "But...but I mean it. I really tried my best...."
    savior "Maybe you think that you mean it? But there's no way you can prove if you really mean it...."
    savior "If you're saying that there's no doubt you mean it, then you're being arrogant."
    ray "..."
    savior "For now, the only thing you can put your faith in...is salvation and the paradise soon to come."
    savior "You're too weak right now... You don't deserve to think."
    savior "If someone like you starts thinking, the only thing that will take place is a catastrophe."
    ray "..."
    savior "Even if you don't mean to ruin things, no matter how hard you cry out that you mean it, you'll ruin everything if you start to think and act on your own...."
    savior "So you don't deserve to think and act on your own. Don't you agree?"
    ray "You're right....my savior."
    savior "That's why you promised to leave all decision to be made by me, isn't it?"
    ray "Yes, my savior....."
    savior "Now you're admitting it. Ren, you don't deserve to think about anything."
    ray "No, I don't, my savior..."
    savior "You can't make any decision without me."
    ray "No, I can't, my savior....."
    savior "You've been naughty and arrogant for the past few days, thinking on your own...."
    savior "It's all your fault that you're sick."
    ray "Yes...yes, you're right, my savior."
    scene black with dissolve
    show R R35 at my_right with dissolve
    show U U19 at my_left with dissolve
    savior "Yes, that's the way your eyes should look. They should be looking only at me...."
    savior "You can remain on the right path only when you listen to me. Otherwise only sad things will unfold in your life..."
    ray "Yes...my savior."
    hide R with dissolve
    hide U with dissolve
    stop music
    scene black with dissolve
    show text "14:50" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music16.ogg"
    scene bg17 with dissolve
    "(15 years ago)"
    mother "Don't you dare step out of this door! If you do, your father will get you.... And then you're both dead...."
    mother "Stay here if you want to live."
    scene black with dissolve
    "(...)"
    scene bg8 with dissolve
    show S S12 with dissolve
    s "There's no way he was actually trying to buy K's photos...."
    show S S11
    s "I remember...that he sent someone to the cathedral...when we were young."
    s "He didn't find out that I used to attend the place, thank goodness...."
    show S S12
    s "But....does he remember K's face? I hope not...."
    play sound "music/answer.ogg"
    "(Phone vibrating...)"
    show S S16
    s "...Huh? It's K!"
    stop sound
    s "Hello? K! Where have you been?!"
    show S S11
    s "Oh...did you need time to set your phone...to avoid being bugged?"
    s "I'm sure you had a hard time reading that complicated manual..."
    show S S15
    s "Sorry. I've been on edge of my seat waiting for you to call...."
    show S S11
    s "So? What happened? What was his plan?"
    s "....Is that really all? Nothing related to Sora? Not even the slightest bit, no nothing?"
    show S S14
    s "...."
    show S S12
    s "....Never ever trust what he says."
    s "I'm sure he approached you to find me."
    s "You remember what he did to me and my brother."
    s "I've never slept in peace when I was young.... And because of him my brother..."
    show S S15
    s "...K."
    s "You don't think....he's done something to Sora, do you?"
    s "I can handle it if he finds me...but as for Sora...."
    show S S14
    s "I know I'm not supposed to ask this, but...is Sora safe...?"
    s "The last thing I remember of him is from that picture Reina gave me."
    show S S12
    s "Please tell me whether he's safe. Just this once..."
    s "...."
    show S S14
    s "...Okay."
    show S S15
    s "......Okay. Thanks. I won't ever ask again..."
    s "I know... I'm a shadow."
    show S S12
    s "But...if something happens to him...."
    s "K...I'll do anything you ask me to do, even if it's out of the scope of my understanding."
    s "I don't care if it puts my life on the line...."
    s "You would know if something bad happens to Sora."
    show S S14
    s "If so, tell me to do whatever..... I'll do anything you ask me without questioning your words."
    show S S12
    s "You're the only person who can do that...."
    stop music
    hide S with dissolve
    scene black with dissolve
    show text "20:49" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music14.ogg"
    call phone_start from _call_phone_start_5
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_65
    call message("Ren", "[name]...") from _call_message_516
    call message("Ren", "thank god") from _call_message_517
    call message("Ren", "you're here.") from _call_message_518
    call message("Ren", "Please answer me.") from _call_message_519
    call message("Ren", "[name]") from _call_message_520
    call message("Ren", "[name]") from _call_message_521
    call screen phone_reply("Yes, Ren... Are you alright?", "cd611", "Are you whining to me?", "cd612")
label cd611:
    call phone_after_menu from _call_phone_after_menu_62
    call message_start("[name]", "Yes, Ren... Are you alright?") from _call_message_start_66
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "I'm getting scared all sudden") from _call_message_522
    call message("Ren", "I'm scared you'll leave me.") from _call_message_523
    call message("Ren", "So I logged in") from _call_message_524
    call message("Ren", "I was scared....") from _call_message_525
    call message("Ren", ".....") from _call_message_526
    call message("Ren", "Listen") from _call_message_527
    call message("Ren", "[name]...") from _call_message_528
    call message_img("Ren", "you won't leave me, right?", "images/sad.png") from _call_message_img_33
    jump am68
label cd612:
    call phone_after_menu from _call_phone_after_menu_63
    call message_start("[name]", "Are you whining to me?") from _call_message_start_67
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Sorry") from _call_message_529
    call message("Ren", "sorry sorry sorry sorry") from _call_message_530
    call message("Ren", "I must be an airhead.") from _call_message_531
    call message("Ren", "I won't whine...") from _call_message_532
    call message("Ren", "So please don't leave me. Please?") from _call_message_533
    jump am68
label am68:
    call message("Ren", "I keep thinking about you...") from _call_message_534
    call message("Ren", "I wish you were here.") from _call_message_535
    call message("Ren", "I don't deserve to think like this, but I still do....") from _call_message_536
    call message("Ren", "And I keep thinking") from _call_message_537
    call message("Ren", "about myself....") from _call_message_538
    call message("Ren", "I hate myself...") from _call_message_539
    call message("Ren", "I hate myself") from _call_message_540
    call message("Ren", "for whining because of the elixir....") from _call_message_541
    call message("Ren", "After I met you") from _call_message_542
    call message("Ren", "I was determined to be a person who can protect you") from _call_message_543
    call message("Ren", "and I was excited....") from _call_message_544
    call message("Ren", "I wanted to show you") from _call_message_545
    call message("Ren", "only the good things...") from _call_message_546
    call message("Ren", "I wanted to make you happy....") from _call_message_547
    call message("Ren", "I wanted to give you") from _call_message_548
    call message("Ren", "only the best") from _call_message_549
    call message("Ren", "as best as I can....") from _call_message_550
    call message("Ren", "I'm....") from _call_message_551
    call message("Ren", "[name]") from _call_message_552
    call message("Ren", "I'm") from _call_message_553
    call message("Ren", "much more") from _call_message_554
    call message("Ren", "useless and pathetic") from _call_message_555
    call message("Ren", "than you think....") from _call_message_556
    call message("Ren", "So") from _call_message_557
    call message("Ren", "I'd rather see myself") from _call_message_558
    call message("Ren", "born again...") from _call_message_559
    call message("Ren", "As if I didn't exist in the first place...!") from _call_message_560
    call screen phone_reply("I like you just the way you are.... Please don't think like that.", "cd613", "I wish you'd be different. I think you're being a parrot here.", "cd614")
label cd613:
    call phone_after_menu from _call_phone_after_menu_64
    call message_start("[name]", "I like you just the way you are.... Please don't think like that.") from _call_message_start_68
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "[name]") from _call_message_561
    call message("Ren", "your words are so sweet") from _call_message_562
    call message("Ren", "I think I'm gonna cry....") from _call_message_563
    call message("Ren", "But") from _call_message_564
    call message("Ren", "I...") from _call_message_565
    call message("Ren", "I shouldn't make any decision on my own...") from _call_message_566
    call message("Ren", "Because I'm an airhead....") from _call_message_567
    jump am69
label cd614:
    call phone_after_menu from _call_phone_after_menu_65
    call message_start("[name]", "I wish you'd be different. I think you're being a parrot here.") from _call_message_start_69
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "...Ok.") from _call_message_568
    call message("Ren", "I'll change.") from _call_message_569
    call message("Ren", "I'll be the kind of person you'd like!") from _call_message_570
    call message("Ren", "I shouldn't forget to do that...") from _call_message_571
    call message("Ren", "I should write a memo....") from _call_message_572
    call message("Ren", "I wish I could at least be less stupid.") from _call_message_573
    jump am69
label am69:
    call message("Ren", "Why can't I still beat him?") from _call_message_574
    call message("Ren", "I'm working so hard") from _call_message_575
    call message_img("Ren", "but what am I missing?", "images/sad.png") from _call_message_img_34
    call message("Ren", "It must be because") from _call_message_576
    call message("Ren", "I'm so stupid, right?") from _call_message_577
    call message("Ren", "Idiot") from _call_message_578
    call message("Ren", "What an idiot.") from _call_message_579
    call message("Ren", "I'm an airhead.") from _call_message_580
    call message("Ren", "I wish I could curse myself!") from _call_message_581
    call message("Ren", "I'm full of faults.") from _call_message_582
    call message("Ren", "So it's only natural....") from _call_message_583
    call message("Ren", "that you don't like me.") from _call_message_584
    call message("Ren", "There's no way you'll like me so dearly") from _call_message_585
    call message("Ren", "when I'm so useless and stupid.") from _call_message_586
    call message("Ren", "Everyone who were around me") from _call_message_587
    call message("Ren", "all tortured me.") from _call_message_588
    call message("Ren", "The people who hurt me") from _call_message_589
    call message("Ren", "at least stayed with me when they did that....") from _call_message_590
    call message("Ren", "[name]") from _call_message_591
    call message("Ren", "so will you stay") from _call_message_592
    call message("Ren", "while you're torturing me?") from _call_message_593
    call message("Ren", "I'd rather have myself tortured by you.") from _call_message_594
    call message("Ren", "Please torture me!") from _call_message_595
    call message("Ren", "I'll take it all.") from _call_message_596
    call message("Ren", "So please") from _call_message_597
    call message("Ren", "just stay with me...") from _call_message_598
    call message("Ren", "I think for now") from _call_message_599
    call message("Ren", "the only way to have you stay with me") from _call_message_600
    call message("Ren", "is for me to suffer.") from _call_message_601
    call message("Ren", "I'm an airhead") from _call_message_602
    call message("Ren", "so this is the only thing I can think of.") from _call_message_603
    call message("Ren", "But my savior told me") from _call_message_604
    call message("Ren", "that if I work really hard") from _call_message_605
    call message("Ren", "things might be different....") from _call_message_606
    call message("Ren", "If I try really really hard") from _call_message_607
    call message("Ren", "like my savior tells me") from _call_message_608
    call message("Ren", "then just maybe....") from _call_message_609
    call message("Ren", "[name]") from _call_message_610
    call message("Ren", "maybe....") from _call_message_611
    call message("Ren", "I'll be the person you want to stay with.") from _call_message_612
    call message("Ren", "But") from _call_message_613
    call message("Ren", "I'll consider that...") from _call_message_614
    call message("Ren", "as something that will happen only after a long time.") from _call_message_615
    call message("Ren", "I won't be arrogant.") from _call_message_616
    call message("Ren", "I won't try to be recognized so soon....") from _call_message_617
    call message("Ren", "So please") from _call_message_618
    call message("Ren", "even if i") from _call_message_619
    call message("Ren", "have lots of faults") from _call_message_620
    call message("Ren", "please don't leave me.") from _call_message_621
    call message("Ren", "Instead") from _call_message_622
    call message("Ren", "you can do whatever you want with me...") from _call_message_623
    call message("Ren", "I'll....") from _call_message_624
    call message("Ren", "do anything...") from _call_message_625
    call message("Ren", "you ask me to do....") from _call_message_626
    call message("Ren", "Just don't leave me....") from _call_message_627
    call message("Ren", "You have no idea") from _call_message_628
    call message("Ren", "how happy I was") from _call_message_629
    call message_img("Ren", "after you decided to stay...!", "images/wink.png") from _call_message_img_35
    call message("Ren", "I can take whatever pain") from _call_message_630
    call message("Ren", "since now you're officially a part of the Ao Me....") from _call_message_631
    call screen phone_reply("But I think this place does no good to you, Ren.", "cd615", "So you'll do whatever I tell you to do?", "cd616")
label cd615:
    call phone_after_menu from _call_phone_after_menu_66
    call message_start("[name]", "But I think this place does no good to you, Ren.") from _call_message_start_70
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "...") from _call_message_632
    call message("Ren", "That's because") from _call_message_633
    call message("Ren", "I'm full of faults.") from _call_message_634
    call message("Ren", "That's why") from _call_message_635
    call message("Ren", "I cannot take all the good stuff") from _call_message_636
    call message("Ren", "this place gives me.") from _call_message_637
    call message("Ren", "And I belong nowhere") from _call_message_638
    call message("Ren", "outside this place.") from _call_message_639
    jump am610
label cd616:
    call phone_after_menu from _call_phone_after_menu_67
    call message_start("[name]", "So you'll do whatever I tell you to do?") from _call_message_start_71
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Yes") from _call_message_640
    call message_img("Ren", "I will!!", "images/smile.png") from _call_message_img_36
    jump am610
label am610:
    call message("Ren", "You can torture me.") from _call_message_641
    call message("Ren", "Though this isn't a good thing") from _call_message_642
    call message("Ren", "I'm already used to") from _call_message_643
    call message("Ren", "being in pain and suffering...") from _call_message_644
    call message("Ren", "So please....") from _call_message_645
    call screen phone_reply("Again with the please... I'm getting tired of this.", "cd617", "I think we should leave this place... Do you think we can do that?", "cd618")
label cd617:
    call phone_after_menu from _call_phone_after_menu_68
    call message_start("[name]", "Again with the please... I'm getting tired of this.") from _call_message_start_72
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "You're getting tired...?") from _call_message_646
    call message("Ren", "I'm sorry!") from _call_message_647
    call message("Ren", "No") from _call_message_648
    call message("Ren", "no") from _call_message_649
    call message("Ren", "if you're tired of this,") from _call_message_650
    call message("Ren", "please forget what I said.") from _call_message_651
    call message("Ren", "You don't have to tell me anything") from _call_message_652
    call message("Ren", "if you don't want to.") from _call_message_653
    call message("Ren", "You can just") from _call_message_654
    call message("Ren", "call me an airhead") from _call_message_655
    call message("Ren", "and just ignore me") from _call_message_656
    call message("Ren", "like I don't exist at all!") from _call_message_657
    jump am611
label cd618:
    call phone_after_menu from _call_phone_after_menu_69
    call message_start("[name]", "I think we should leave this place... Do you think we can do that?") from _call_message_start_73
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "No!") from _call_message_658
    call message("Ren", "That's....") from _call_message_659
    call message("Ren", "then my savior would be so mad.") from _call_message_660
    call message("Ren", "And") from _call_message_661
    call message("Ren", "I....") from _call_message_662
    call message("Ren", "I can't survive") from _call_message_663
    call message("Ren", "out of this place.") from _call_message_664
    call message("Ren", "[name]") from _call_message_665
    call message("Ren", "but if you'll stay with me...") from _call_message_666
    call message("Ren", "then...") from _call_message_667
    call message("Ren", "......") from _call_message_668
    call message("Ren", "will I be able to survive out there?") from _call_message_669
    call message("Ren", "Uh") from _call_message_670
    call message("Ren", "we're not supposed to talk about this!") from _call_message_671
    call message("Ren", "we'll be in trouble") from _call_message_672
    call message("Ren", "if she finds out about this!") from _call_message_673
    call message("Ren", "Let's never talk about") from _call_message_674
    call message("Ren", "getting out of here.") from _call_message_675
    call message("Ren", "Never....") from _call_message_676
    call message("Ren", "The rule says we shouldn't talk about that.") from _call_message_677
    call message("Ren", "I don't want you") from _call_message_678
    call message("Ren", "to have the cleansing ceremony.") from _call_message_679
    call message("Ren", "I don't want to see you...") from _call_message_680
    call message_img("Ren", "suffering from pain....", "images/sad.png") from _call_message_img_37
    jump am6111
label am611:
    call screen phone_reply("You're such an airhead. You're so stupid.", "cd619", "Let's talk face to face... Could you do that for me?", "cd620")
label cd619:
    call phone_after_menu from _call_phone_after_menu_70
    call message_start("[name]", "You're such an airhead. You're so stupid.") from _call_message_start_74
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I know....") from _call_message_681
    call message("Ren", "Do you want me to apologize?") from _call_message_682
    call message("Ren", "I'm so sorry I'm an airhead.") from _call_message_683
    call message("Ren", "I'm so sorry, m....") from _call_message_684
    jump am612
label cd620:
    call phone_after_menu from _call_phone_after_menu_71
    call message_start("[name]", "Let's talk face to face... Could you do that for me?") from _call_message_start_75
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "I knew it.") from _call_message_685
    call message("Ren", "You're tired of chatting through messenger") from _call_message_686
    call message("Ren", "because I haven't seen you long, aren't you?") from _call_message_687
    call message("Ren", "I'm sorry....") from _call_message_688
    call message("Ren", "Now") from _call_message_689
    call message("Ren", "I'll do my best") from _call_message_690
    call message("Ren", "and make sure I can see you again.") from _call_message_691
    call message("Ren", "And from now on...") from _call_message_692
    jump am612
label am612:
    call message("Ren", "I'll just be silent like a grave and work") from _call_message_693
    call message("Ren", "without bothering you.") from _call_message_694
    call message("Ren", "So please...") from _call_message_695
    call message("Ren", "[name]") from _call_message_696
    call message("Ren", "I hope...") from _call_message_697
    call message_img("Ren", "at least you'd be happy today as well.", "images/smile.png") from _call_message_img_38
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_698
    call phone_end from _call_phone_end_6
    stop music
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "Huh? Uh..it's me. Thanks for picking up."
    ray "...Actually, I have something to tell you."
    ray "I heard my savior talking about you..."
    ray "That you might be working on something else once the RFA is all cleansed."
    menu:
        "On what?":
            ray "I don't know. Maybe it's a glorious mission you'd get to share with my savior...?"
            ray "I didn't get much detail. My savior wanted me not to let that bug me and just focus on my work."
            ray "Sorry I couldn't be of more help."
            ray "My savior will tell you about it in person once this is wrapped up."
            jump u64
        "Again? I don't want to work....":
            ray "I sometimes don't want to work, either... But since my savior wants to give you another job, maybe that means you're useful..."
            ray "It's an honor to gain my savior's trust."
            ray "I...am working only on hacking business because this is the only thing I can do..."
            jump u64
label u64:
    ray "But the thing is...I might not be able to see you once you get a new job..."
    ray "It's not settled yet, but I keep thinking about it."
    ray "I have to stay here away from you as we speak, and we might be separated forever..."
    ray "Then maybe I won't get to hear you again... I guess I got a bit nervous."
    ray "Of course, you're a wonderful person, nothing like me.. I know you have to work on something important with someone who's much more wonderful than me."
    ray "I know that, but I feel upset... That's because I'm a moron, isn't it?"
    menu:
        "Focus on the present than the future.":
            ray "...Yeah, you're right. My savior told me something similar."
            ray "My savior will monitor and plan for the future..."
            ray "And I should work on what I have at hand."
            ray "And my savior will tell me what I have to do..."
            ray "I'm a moron, so I shouldn't plan for myself..."
            jump u65
        "I don't know. You should think about what you' do.":
            ray "...I'm a moron... I shouldn't think for myself..."
            ray "My savior told me I'll make the wrong choice every time and annoy people around me."
            ray "My savior told me I have no idea how weak I am, so I shouldn't be responsible for anything."
            ray "...But here's the thing. I should leave everything to my savior...but I don't want to leave you...to anyone."
            ray "I shouldn't be greedy, but I can't help it. Sorry for being greedy..."
            jump u65
label u65:
    ray "Still, I feel much reassured hearing you. Thanks."
    ray "...Oh, looks like someone's here. I should hang up."
    ray "I'll call again. You will pick up, won't you?"
    ray "Sorry, gotta go. Bye!"
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "21:21" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music14.ogg"
    scene bg7 with dissolve
    show MC MC14 at my_right with dissolve
    "(Sounds like someone is out the door.)"
    menu:
        "(Open the door.)":
            play sound "music/dooropen.ogg"
            "(Door Opened)"
            scene black with dissolve
            show U U13 at my_left with dissolve
            "...!"
            ray "S-sorry... I wasn't trying to listen in on you."
            jump u66
        "Who is it?":
            "(It's suddenly quiet.)"
            mc "(Open the door and check what's outside.)"
            play sound "music/dooropen.ogg"
            "(Door Opened)"
            scene black with dissolve
            show U U13 at my_left with dissolve
            ray "...I wasn't hiding!"
            ray "No, I was, but... I didn't mean anything bad!"
            jump u66
label u66:
    menu:
        "Come in, Ren!":
            show MC MC11 at my_right with dissolve
            ray "...Can I? Is it okay?"
            jump u67
        "This is creepy. I hate someone like you.":
            ray "Sorry... Sorry... I'll never do that again."
            ray "You're not leaving me, are you?"
            jump u67
label u67:
    scene bg7 with dissolve
    show MC MC11 at my_right with dissolve
    show U U13 at my_left with dissolve
    ray "Sorry to pop in so suddenly...."
    show U U19
    ray "I know you don't want to see me, so I tried not to make you see me..."
    show U U11
    ray "...But I wanted to give you this."
    ray "This tastes and smells and looks exactly the same as the elixir of salvation."
    ray "If someone tries to force you to take the elixir, drink this instead."
    show U U19
    ray "Uh...I dozed off for a second, and in my dream, you were in pain after taking the elixir."
    show U U11
    ray "So I couldn't just sit there."
    show U U110
    menu:
        "Thank you, Ren.":
            show MC MC11
            ray "Uh, no need to thank me. It's really nothing."
            jump u68
        "I don't need this. You should work if you have time to do this.":
            ray "Sorry.... I offended you."
            ray "But can't you please take it?"
            ray "There could be trouble.... So just in case."
            jump u68
label u68:
    show U U11
    ray "Here...take it."
    ray "And...could you please keep it a secret from the savior that I was here?"
    show U U19
    ray "But of course...you don't have to do that if you don't want to."
    show U U11
    ray "I'll do everything painful for you. You don't have to do any of that..."
    show U U19
    ray "No matter what...."
    ray "..."
    show U U112
    ray "...Ugh... Sob...."
    show U U113
    show MC MC14
    menu:
        "Ren?":
            ray "Uh...sorry."
            jump u69
        "Why are you crying?":
            ray "Sorry...please don't be mad. I won't cry."
            jump u69
label u69:
    ray "It's just that it's so great to see..."
    show U U112
    ray "Ugh..no. Nothing. Please forget that I cried."
    ray "Sorry. I'll...get going. Have a good night."
    hide U with dissolve
    show MC MC15
    menu:
        "Ren?":
            jump u610
        "Where are you going!?":
            jump u610
label u610:
    hide MC with dissolve
    stop music
    scene black with dissolve
    "(Ren ran away.)"
    scene white with dissolve
    "..."
    "......"
    "......"
    scene bg8 with dissolve
    play music "music/music16.ogg"
    show S S212 at my_left with dissolve
    s "Aaaaaaaaaah.... Aaaaah...."
    show VW VW1 at my_right with dissolve
    vw "What's with you?"
    show S S212
    s "I'm getting a depression attack...."
    s "You know my mood enjoys a ride on a roller coaster... Grab me a Ph.D. Pepper, will you?"
    show VW VW2
    vw "Like hell. Those are the only words I can think of when I look at you."
    show VW VW3
    vw "If it weren't for all those accomplishments you made last year.... Ugh."
    show VW VW2
    vw "Now I'm starting to question whether I'm here to monitor you or help with your personal activities...."
    show S S214
    s "Does that really matter? What matters is that you're here."
    show VW VW4
    vw "I wouldn't be here if you do your work correctly! Are you kidding me?"
    show S S212
    s "Madam."
    show VW VW2
    vw "I told you not to call me that... You'd better use better vocabulary if you want to chug that damned drink of yours."
    s "You think there's a bunch of better hackers out there than me?"
    show VW VW5
    vw "What's the sudden self-reflection for?"
    show S S215
    s "Answer me, please. And be serious."
    show VW VW2
    vw "What's gotten into you...?"
    show VW VW5
    vw "Hmm... I don't think I've ever seen someone who can both infiltrate a system within a minute and be such a slick actor at the same time."
    show S S212
    s "So I am a top hacker, right?"
    vw "...All of the members in the agency are at the top of their fields. And I'd say...you're at the top of the top."
    show VW VW3
    vw "Ugh...it feels creepy to say this myself. It feels like I'm complimenting you."
    show S S214
    s "So what would you do...if there's someone better than me?"
    show VW VW5
    vw "Then I'll bring that person over right away - ! But there's no such monster in our radar."
    show S S212
    s "....No, there isn't."
    show VW VW3
    vw "Did you actually meet one of a kind?"
    s "I d.."
    show S S214
    show VW VW1
    vw "You did?"
    show S S215
    s "Even if I did, there's nothing I can get by telling you."
    show VW VW2
    vw "That's right. Zip it. Truths can only hurt us."
    show VW VW5
    vw "Truths can only hold us back. Family, friends, lovers... They're nothing but weaknesses for us."
    show S S212
    s "Yeah...I know."
    show VW VW1
    vw "Hey, what's this?"
    show S S210
    s "Huh?"
    show VW VW5
    vw "...You even dug up these kinds of stuff? These are all political news."
    show S S214
    s "Oh...I did just the preliminary search. I didn't even start yet. I have tons of works to do...."
    show VW VW2
    vw "I'm going to believe that the boss's assignment is on the top of the list of your priorities..."
    show S S212
    s "I'll keep the deadline. I'll keep working on them...."
    show VW VW5
    vw "You should be grateful that informants these days don't have a lot of work. Your schedule would've been tighter if you had other works to do."
    show VW VW2
    vw "I happen to be here because I didn't have an assigned task. Why couldn't they give me a break if there was no assignment for me...?"
    show S S215
    s "Madam, where's my Ph.D. Pepper? I'm feeling so...so depressed right now...."
    show VW VW4
    vw "I told you not to call me that! And I don't want to even go near that fridge of yours. I don't even want to think about how it looks...."
    show S S216
    s "I'll deal with your problem, Hiroki! God7 algorithm online!!!"
    show S S211
    show VW VW2
    s "....Launch failed. I'm getting depressed.... Am I turning sentimental for the night?"
    vw "Want to grab a cigarette?"
    show S S214
    s "I don't smoke or drink."
    show VW VW3
    vw "How can you actually manage your life with just caffeine?"
    vw "Ugh. Okay, fine. I'll fetch you one, so could you at least pretend to be working?"
    hide VW with dissolve
    show S S215
    s "Thanks...Madam."
    show S S212
    s "....Ugh. I'm feeling so strange. What is this? Is it because of the hacker...?"
    stop music
    hide S with dissolve
    scene black with dissolve
    show text "23:19" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    if bad_end > good_end:
        call badend1 from _call_badend1
    else:
        jump goodend1
label goodend1:
    scene black with dissolve
    play music "music/music4.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    menu:
        "(Go outside.)":
            jump ge61
        "(Call Ren.)":
            play sound "music/call.ogg"
            "(... .... ...)"
            "(He does not pick up.)"
            mc "(Go outside.)"
            jump ge61
label ge61:
    hide MC with dissolve
    scene bg11 with dissolve
    show MC MC14 at my_left with dissolve
    believer "Step aside, please. The savior is coming through."
    show R R33mask at my_right with dissolve
    savior "No...She's fine."
    savior "For paradise, [name]. It's a beautiful night. Are you having trouble sleeping?"
    menu:
        "Savior...where are you going?":
            savior "I'm headed to the worship chamber. Tonight I'll be in the lead of the evening worship service."
            jump ge62
        "For paradise.":
            savior "You surely adapted fast and well, [name]."
            jump ge62
        "Have you seen Ren?":
            savior "He'll be working right now if he's following the plan."
            savior "I should send someone to check."
            jump ge62
label ge62:
    show R R31mask
    savior "Would you like to join me? It wouldn't be too bad to meet the new believers."
    show R R33mask
    savior "....No, wait. You're on a special mission, and since you're gifted with a special privilege, perhaps it'd be better for you not to meet the fellow believers yet."
    show R R32mask
    savior "You'd be lonely for a while, but please stay as mysterious as you are now, [name]."
    show R R33mask
    savior "Once you gain a thorough comprehension of our system and accomplish several feats in your current duty, I'll grant you an executive position right away."
    savior "I'm very pleased with your accomplishments in the messenger."
    show R R32mask
    savior "It's my greatest joy that Ren returned to his place."
    show R R33mask
    savior "He's never seen someone so attractive like you from the outside, so apparently, he got sidetracked."
    savior "I'll make sure there's no problem with Ren. I'll keep my eyes on him, so don't worry."
    savior "I hope...you would do your best for our paradise, [name]."
    savior "Now I must leave. For eternal paradise."
    stop music
    hide R with dissolve
    hide MC with dissolve
    scene bg13 with dissolve
    show MC MC11 at my_left with dissolve
    "(Seems there's nobody around.)"
    play music "music/music14.ogg"
    menu:
        "(Return to the room.)":
            "......"
            whooo "...Hold on."
            jump ge63
        "(Stay just a bit longer.)":
            "......"
            whooo "...Who's there?"
            jump ge63
label ge63:
    whooo "...Huh? Are you...?"
    show U U111 at my_right with dissolve
    menu:
        "Ren!":
            ray "[name], there you are..."
            jump ge64
        "What were you doing here?":
            ray "...Nothing."
            jump ge64
label ge64:
    show U U19
    ray "You don't want to see me, do you? I'll get back to my room now."
    show U U111
    menu:
        "That's not true, Ren! Stay with me for a bit.":
            ray "Can I...? You won't hate me even if I stay?"
            jump ge65
        "Why would you think that? You're frustrating me.":
            ray "...I'm sorry. For thinking and acting on my own."
            ray "Then tell me what to do...what to think."
            jump ge65
label ge65:
    show U U19
    ray "I thought you'd be despising me because I ran away like that."
    show U U13
    ray "But...your eyes seem to tell me that you don't."
    show U U111
    ray "Am I correct?"
    show U U110
    menu:
        "That's right. I don't despise you.":
            ray "I see... I knew it."
            jump ge66
        "I pity you.":
            ray "...You pity me?"
            ray "Then please keep doing that. You won't leave me if you pity me, will you?"
            jump ge66
label ge66:
    show U U19
    ray "Uh...I shouldn't tell you what I think... I'm not supposed to tell anyone what I think...."
    show U U112
    ray "I slipped again... I dared to tell you what I think...!"
    show U U113
    ray "I'm such an idiot. I'm such an airhead!"
    ray "Sob...sob..."
    show U U112
    ray "I'm so stupid... I keep thinking that I want you to embrace me even now."
    show U U113
    ray "I thought about that when I visited you earlier... I kept thinking that I want to tell you everything I'm having a hard time with...."
    ray "I don't deserve to do that... No matter how much elixir I take, I can't stop thinking about you."
    ray "My eyes hurt, and my head hurts.... If I find you, [name]...I keep having weak thoughts....that I'll get a little better..."
    show U U112
    ray "I don't deserve to do that..."
    show U U113
    ray "I'm useless...unless I work without stopping...!"
    show U U112
    ray "I keep thinking of the way you look at me... It hurts every time I think of it...."
    show U U112
    menu:
        "You're free to follow what your mind and heart desire.":
            ray "...No, no.... Nothing will work if I do as I think."
            ray "Only bad things will happen, and you'll leave me."
            ray "I shouldn't do as I think. I need to endure all this pain and difficulty."
            jump ge67
        "Why are you saying something like this right now?":
            ray "Are you angry? I knew you'd be angry if I say this...."
            ray "Sorry... Could you forget everything I said just now?"
            ray "If you leave, I'll be able to do nothing but crying. I won't be able to eat anything, and I'll be a good-for-nothing."
            ray "So I should just bear all this pain and difficulty...."
            ray "So forget everything I said. I'm so sorry."
            jump ge67
label ge67:
    show U U112
    ray "I can take it. I can take it all. I'm just going through a bit of a pain, that's all."
    show U U19
    ray "You shouldn't bear my whining. You can just turn and walk away...."
    ray "But just...just don't get too far... Don't leave me completely."
    ray "You can be mad at me if I'm frustrating you. You can throw stuff at me. I deserve that...."
    show U U112
    ray "No, you should be mad at me. If I can at least be your punching bag...!"
    show U U111
    menu:
        "Stop tormenting yourself, Ren. Look at me.":
            ray "[name]..."
            jump ge68
        "Don't say something like that.... I'll stop you from saying that.":
            ray "You'll stop me...?"
            jump ge68
label ge68:
    hide U
    hide MC
    scene cg17 with dissolve
    ray "...!?"
    ray ".....Huff..."
    ray "...Ugh..."
    scene bg13 with dissolve
    show MC MC16 at my_left with dissolve
    show U U110 at my_right with dissolve
    ray "...Huff...ugh..."
    show U U19
    ray "Wha...what...what was that?"
    show U U111
    ray "Ugh...was that some kind of a punishment? Or..."
    show U U19
    ray "I...I don't think I get what you meant by that. But..."
    show U U111
    ray "It's making me feel hot...and dizzy...."
    ray "I...need cold water... Sorry. Forgive me. I need... My body is acting weird..."
    hide U with dissolve
    stop music
    "(Ren sprinted away.)"
    "......"
    hide MC with dissolve
    scene black with dissolve
    call day7 from _call_day7
