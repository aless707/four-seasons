label day10:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 10" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "03:18" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2.ogg"
    scene bg23 with dissolve
    show V V17 with dissolve
    v "I cannot leave Sora and Reina here."
    show V V13
    v "I underestimated this place."
    v "The number of people and the size of the cult is compounding every time I check upon them..."
    show V V19
    v "Perhaps she's creating 2nd or 3rd Sora...."
    v "I must stop her...."
    show V V13
    v "However...is it impossible to protect her at the same time...?"
    show V V119
    v "...Haa. I must figure out where they have taken Levan... I cannot do everything alone."
    show V V11
    v "I must admit that now I need help..."
    show V V13
    v "Once those files are unleashed...all members of the RFA will find out Levan's story, anyway."
    show V V19
    v "Then I should rather..."
    stop music
    hide V with dissolve
    scene black with dissolve
    show text "06:21" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music2chat-17.ogg"
    call phone_start from _call_phone_start_26
    call message_start("...", "...") from _call_message_start_400
    call message("Sayaka", "Now I'll try to figure out with the unit") from _call_message_3249
    call message("Sayaka", "if the interrogative interview Shun and Mr. Han had") from _call_message_3250
    call message("Sayaka", "has nothing to do with our loss of contact with Zero....") from _call_message_3251
    call message("Sayaka", "But I feel frustrated.") from _call_message_3252
    call message("Sayaka", "Luckily, today") from _call_message_3253
    call message("Sayaka", "Mr. Han") from _call_message_3254
    call message("Sayaka", "an wi__##_") from _call_message_3255
    call reply_message("Sayaka?") from _call_reply_message_1
    call message("SYSTEM", "Sayaka has left the chatroom.") from _call_message_3256
    play music "music/music20.ogg"
    play sound "music/hack.ogg"
    call message("SYSTEM", "Reina has entered the chatroom.") from _call_message_3257
    stop sound
    call message("Reina", "I see that Sora") from _call_message_3258
    call message("Reina", "has recovered the chatroom last evening.") from _call_message_3259
    call message("Reina", "Even if he did manage") from _call_message_3260
    call message("Reina", "to recover the messenger,") from _call_message_3261
    call message("Reina", "there's no telling") from _call_message_3262
    call message("Reina", "when I'll find my way") from _call_message_3263
    call message("Reina", "inside this chatroom") from _call_message_3264
    call message("Reina", "like how Sora does.") from _call_message_3265
    call message("Reina", "So I suggest you choose your words") from _call_message_3266
    call message("Reina", "very carefully.") from _call_message_3267
    call screen phone_reply3("I'll keep that in mind, my savior.", "cd101", "...For now, I can't tell you anything. This is for Sora's sake.", "cd102", "I'm going to say whatever I want to. You can't stop me.", "cd103")
label cd101:
    call phone_after_menu from _call_phone_after_menu_377
    call message_start("[name]", "I'll keep that in mind, my savior.") from _call_message_start_401
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "Good.") from _call_message_3268
    jump am101
label cd102:
    call phone_after_menu from _call_phone_after_menu_378
    call message_start("[name]", "...For now, I can't tell you anything. This is for Sora's sake.") from _call_message_start_402
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "That's very bold of you.") from _call_message_3269
    call message("Reina", "You sound like you have a secret of your own.") from _call_message_3270
    call message("Reina", "And it appears you have no idea how much I know.") from _call_message_3271
    call message("Reina", "You'd better remember....") from _call_message_3272
    call message("Reina", "that I will no longer be so generous") from _call_message_3273
    call message("Reina", "with your arrogance.") from _call_message_3274
    jump am101
label cd103:
    call phone_after_menu from _call_phone_after_menu_379
    call message_start("[name]", "I'm going to say whatever I want to. You can't stop me.") from _call_message_start_403
    call message("Reina", "I can't stop your words.") from _call_message_3275
    call message("Reina", "But I can stop you.") from _call_message_3276
    call message("Reina", "You are welcome to try me if you're wondering whether that's true.") from _call_message_3277
    jump am101
label am101:
    call message("Reina", "I've been checking the RFA chat logs.") from _call_message_3278
    call message("Reina", "One of their members is missing") from _call_message_3279
    call message("Reina", "but they are having discussions casually as if they're on a picnic...") from _call_message_3280
    call message("Reina", "My sympathy for RFA grows by the minute.") from _call_message_3281
    call message("Reina", "If I were there,") from _call_message_3282
    call message("Reina", "I would have immediately sent someone to find him.") from _call_message_3283
    call screen phone_reply("They are so frustrating. You must save them now!", "cd104", "Unlike you, the RFA is being careful in order to minimize the damage.", "cd105")
label cd104:
    call phone_after_menu from _call_phone_after_menu_380
    call message_start("[name]", "They are so frustrating. You must save them now!") from _call_message_start_404
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "That's right....") from _call_message_3284
    call message("Reina", "They're no different from naive little lambs.") from _call_message_3285
    call message("Reina", "I should take action very soon.") from _call_message_3286
    jump am102
label cd105:
    call phone_after_menu from _call_phone_after_menu_381
    call message_start("[name]", "Unlike you, the RFA is being careful in order to minimize the damage.") from _call_message_start_405
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "You mean that our definitions of damage are different.") from _call_message_3287
    call message("Reina", "You must be ready to suffer some damage without hesitation") from _call_message_3288
    call message("Reina", "in order to contribute to the establishment of paradise to come.") from _call_message_3289
    call message("Reina", "And K is not strong enough to do that!") from _call_message_3290
    jump am102
label am102:
    call message("Reina", "A boy has gone missing,") from _call_message_3291
    call message("Reina", "and he's just neglecting him....") from _call_message_3292
    call message("Reina", "And that boy is very useful...") from _call_message_3293
    call message("Reina", "We cannot afford to lose him.") from _call_message_3294
    call message("Reina", "He is also what motivates Sora") from _call_message_3295
    call message("Reina", "to prove himself.") from _call_message_3296
    call message("Reina", "[name]") from _call_message_3297
    call message("Reina", "I have a question....") from _call_message_3298
    call message("Reina", "I wonder") from _call_message_3299
    call message("Reina", "if right now") from _call_message_3300
    call message("Reina", "you're scorning me and letting your imaginations fly in your room.") from _call_message_3301
    call message("Reina", "For example, that Ren will come back") from _call_message_3302
    call message("Reina", "and save you...") from _call_message_3303
    call message("Reina", "I was able to find him.") from _call_message_img_217
    call message("Reina", "He must be aware") from _call_message_3304
    call message("Reina", "that he has done something terribly wrong.") from _call_message_3305
    call message("Reina", "So he said nothing") from _call_message_3306
    call message("Reina", "and drank all the elixir I gave him...") from _call_message_3307
    call message("Reina", "I'm sure it was a lot to take in...") from _call_message_3308
    call message("Reina", "Has he finally come to understand") from _call_message_3309
    call message("Reina", "that there's no way for him to run") from _call_message_3310
    call message("Reina", "away from this place...") from _call_message_3311
    call message("Reina", "away from Magenuta?") from _call_message_3312
    call screen phone_reply3("I'm sure you made things very clear for him.", "cd106", "...Please, stop abusing him!", "cd107", "Even if you made him take the drug, he'll free himself from you once he gets back up.", "cd108")
label cd106:
    call phone_after_menu from _call_phone_after_menu_382
    call message_start("[name]", "I'm sure you made things very clear for him.") from _call_message_start_406
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "I hope he wouldn't be lost anymore...") from _call_message_3313
    call message("Reina", "Perhaps now is the time for you to help him.") from _call_message_3314
    jump am103
label cd107:
    call phone_after_menu from _call_phone_after_menu_383
    call message_start("[name]", "...Please, stop abusing him!") from _call_message_start_407
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Abuse him?") from _call_message_3315
    call message("Reina", "I'm saving this boy.") from _call_message_3316
    call message("Reina", "You can't save him.") from _call_message_3317
    call message("Reina", "You cannot.") from _call_message_3318
    call message("Reina", "But you can be my bait.") from _call_message_3319
    call message("Reina", "So") from _call_message_3320
    jump am103
label cd108:
    call phone_after_menu from _call_phone_after_menu_384
    call message_start("[name]", "Even if you made him take the drug, he'll free himself from you once he gets back up.") from _call_message_start_408
    call message("Reina", "You underestimate the side effect of the elixir.") from _call_message_3321
    call message("Reina", "However,") from _call_message_3322
    call message("Reina", "I must admit") from _call_message_3323
    call message("Reina", "that your influence on him is extraordinary.") from _call_message_3324
    jump am103
label am103:
    call message("Reina", "I shall keep you alive") from _call_message_3325
    call message("Reina", "just a little more...") from _call_message_3326
    call message("Reina", "Because you've proven yourself") from _call_message_3327
    call message("Reina", "useful to this boy on several occasions...") from _call_message_3328
    call message("Reina", "You will soon be grateful") from _call_message_3329
    call message("Reina", "that you can sacrifice yourself") from _call_message_3330
    call message("Reina", "for the establishment") from _call_message_3331
    call message("Reina", "of a mighty paradise....") from _call_message_3332
    call message("Reina", "Very soon") from _call_message_3333
    call message("Reina", "you will get to see the elixir of salvation.") from _call_message_3334
    call message("Reina", "And all members of the RFA") from _call_message_3335
    call message("Reina", "would get to taste the elixir") from _call_message_3336
    call message("Reina", "starting with you.") from _call_message_3337
    call screen phone_reply3("Do you think you can find peace through such a forceful method...?", "cd109", "I can't wait to be saved....", "cd1010", "Does the elixir taste good?", "cd1011")
label cd109:
    call phone_after_menu from _call_phone_after_menu_385
    call message_start("[name]", "Do you think you can find peace through such a forceful method...?") from _call_message_start_409
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "If you expect an answer to that") from _call_message_3338
    call message("Reina", "why don't you find a history book?") from _call_message_3339
    call message("Reina", "I couldn't even bother to answer you.") from _call_message_3340
    jump am104
label cd1010:
    call phone_after_menu from _call_phone_after_menu_386
    call message_start("[name]", "I can't wait to be saved....") from _call_message_start_410
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "I wish Sora would feel like you do.....") from _call_message_3341
    call message("Reina", "Since it appears he's completely fallen for you") from _call_message_3342
    call message("Reina", "you should educate him well.") from _call_message_3343
    jump am104
label cd1011:
    call phone_after_menu from _call_phone_after_menu_387
    call message_start("[name]", "Does the elixir taste good?") from _call_message_start_411
    call message("Reina", "I think you should care more about the effect than the taste.") from _call_message_3344
    call message("Reina", "Your mind is on something very insignificant.") from _call_message_3345
    jump am104
label am104:
    call message("Reina", "Oh") from _call_message_3346
    call message("Reina", "that's right.") from _call_message_3347
    call message("Reina", "Did you read what K said late at night?") from _call_message_3348
    call message("Reina", "He promised eternal love") from _call_message_3349
    call message("Reina", "as if he could give me") from _call_message_3350
    call message("Reina", "anything I want.") from _call_message_3351
    call message("Reina", "But he tried to drown me") from _call_message_3352
    call message("Reina", "in love that denies who I am!") from _call_message_3353
    call message("Reina", "That hypocrite...!") from _call_message_3354
    call message("Reina", "That hypocrite") from _call_message_3355
    call message("Reina", "will soon kneel before Sora.") from _call_message_3356
    call message("Reina", "Sora has grown stronger.") from _call_message_3357
    call message("Reina", "I hope Sora will wake up soon...") from _call_message_3358
    call message("Reina", "I believe there's nothing better") from _call_message_3359
    call message("Reina", "than watching Sora destroy K himself.") from _call_message_3360
    call message("Reina", "Hah...") from _call_message_3361
    call message("Reina", "now I must go see my child again.") from _call_message_3362
    call message("Reina", "As for you,") from _call_message_3363
    call message("Reina", "I want you to for the time being") from _call_message_3364
    call message("Reina", "play along with the RFA so that they'll continue talking.") from _call_message_3365
    call screen phone_reply("Alright, my savior.", "cd1012", "...Please don't hurt Sora.", "cd1013")
label cd1012:
    call phone_after_menu from _call_phone_after_menu_388
    call message_start("[name]", "Alright, my savior.") from _call_message_start_412
    show bh at heart_pos with dissolve
    hide bh with dissolve
    jump am105
label cd1013:
    call phone_after_menu from _call_phone_after_menu_389
    call message_start("[name]", "...Please don't hurt Sora.") from _call_message_start_413
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "I promise him a brilliant future.") from _call_message_3366
    call message("Reina", "Even if he suffers now...") from _call_message_3367
    call message("Reina", "he needs that suffering.") from _call_message_3368
    jump am105
label am105:
    call message("Reina", "Now I must go...") from _call_message_3369
    call message("Reina", "For eternal paradise.") from _call_message_3370
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_3371
    call phone_end from _call_phone_end_27
    stop music
    scene black with dissolve
    with Pause(2.0)
    scene black with dissolve
    "(8 years ago)"
    scene bg17 with dissolve
    ys "Sora... Mom's sleeping. Come on, this is our chance."
    yu "I'm scared, Souta..."
    ys "Look how much she drank. She'll be asleep for an hour or two. Trust me."
    yu "Okay..."
    scene white with dissolve
    play music "music/music22.ogg"
    scene bg16 with dissolve
    yu "..."
    ys "Hah... It feels much better without that smell of alcohol. Don't you think so?"
    yu "I don't know."
    ys "That's because you stay in the room all the time. Ugh - I'm never gonna drink in my life."
    ys "Here, Sora. Try this."
    scene white with dissolve
    yu "Wow, what is this? It's cold, but it's so sweet."
    ys "It's called ice cream... Isn't it good? But I couldn't bring it home because it melts so fast."
    yu "I'm so happy I can have this outside with you."
    yu "I hope we never have to go back home..."
    ys "I wish I could soon get out of there with you..."
    yu "But mom said if we run away, father will find us and kill us..."
    ys "I'll find a way. Until then, let's hang in there."
    yu "......"
    ys "What's wrong?"
    yu "You're smarter and stronger than me. So you'll be fine alone..."
    yu "But if I run from home, I'll be nothing but a burden."
    ys "What are you talking about...? Is it because of what mom told you?"
    yu "...She said I'm stupid and useless..."
    yu "She said I'll die if I live alone outside... She said I'm a burden, and I make her tired...."
    yu "So when we live together, I'll do just the same to you."
    ys "I want to run away safely and live safely because you're here with me."
    ys "If I were alone...I would have done dangerous jobs only."
    ys "I'm being a nice boy because you're here."
    ys "I'm so prone to being a bad guy -"
    yu "I don't get what you're saying. You're super-nice."
    yu "I think it would be awesome to live like you...."
    yu "Can't I be like you? I want to rebel against mom and run away like you..."
    yu "To be honest...I'm too much of a coward. I wish I could get angry when you rebel and help you..."
    yu "When I'm with mom, the only thing I can say is that I'm sorry..."
    yu "Don't you think I'm annoying...? Tell me if there's anything you want me to change."
    ys "...Change? I don't think there's anything you need to change in particular... But promise me one thing."
    yu "What is it?"
    ys "Let's stay the way we are..."
    ys "without thinking that we're wrong because of mom or dad."
    yu "But I'm a coward. I'm weak..."
    ys "That's what she said."
    ys "I...I think you're kind and tender."
    yu "...There's no way I'm kind and tender. I think that you're being too nice to me."
    yu "She said I'm a devil. Am I really a devil?"
    ys "No, no! Never!"
    ys "So don't trust what mom says."
    yu "..."
    ys "You're not a coward. You're not weak. It's just that you're kind and tender."
    ys "Promise you won't let mom change you. Okay?"
    yu "...Promise."
    ys "Here, let's do a pinky promise."
    scene black with dissolve
    ys "Let's make a promise on this delicious ice cream."
    yu "Do we have to promise on this ice cream?"
    ys "Yes. Sora, you promise me that you'll stay the way you are..."
    ys "and won't let mom change you."
    ys "Promise that you'll be generous Sora, not selfish Sora."
    ys "Promise that you'll be kind Sora who says sorry first, not angry Sora."
    yu "Okay. Promise, Souta..."
    yu "I promise...I'll never lose myself... I'll stay kind and tender."
    ys "No matter what?"
    yu "Yeah, no matter what."
    scene black with dissolve
    show U U32 with dissolve
    u "...Yes... We are not like our father."
    hide U with dissolve
    scene white with dissolve
    "........"
    "......"
    "...."
    ".."
    scene bg15 with dissolve
    show U U39 at my_left with dissolve
    whooo "...."
    show R R33 at my_right with dissolve
    r "You're awake..."
    r "Are you Sora? Or Ren...?"
    show R R32
    r "It appears you have to wait until evening to return to work."
    show R R33
    r "So take a break and get replenished."
    r "There are loads of works waiting for you to prove how strong you are."
    r "Now that we collected all data RFA had, it's time to plot a plan."
    show R R32
    r "We'll save them."
    show R R33
    r "Rejoice... The time has finally come to prove how useful you are."
    r "Show the world how strong and useful you are."
    stop music
    hide R with dissolve
    hide U with dissolve
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg5 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Reina is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    r "Hello. [name]. I've called, wondering how you're feeling."
    r "What are you thinking right now?"
    menu:
        "I'm worried about Sora.":
            r "Worrying over that child, I feel the same."
            r "If our prayers gather as one, it would be a big help to him..."
            r "We'll be able to create great synergy if you and I join hands."
            jump r101
        "What I feel is not important but... I want to remain in Ao Me.":
            r "I don't dislike those who cajole. I'm not influenced in any way by such since I can look through it."
            r "The proposal I mentioned before is still valid."
            r "If we are under understandings, don't forget that we can join hands anytime."
            jump r101
label r101:
    r "Still, you're a help to Sora."
    r "What's most miserable is when you can't do anything for someone."
    r "Even if you are capable to do something, situations where you're surrounded by adults to ignores and neglects such...."
    r "If that repeats, it's not weird for a demon to sprout inside you."
    menu:
        "You have experience?":
            r "....I might have."
            r "The reason I know exactly when the believers feel safe and secure through the way I handle them...."
            r "is maybe because I've experienced what they feel."
            r "I'll tell you one day... of the environment I grew up in."
            r "I get the feeling that you won't judge me based on whatever I tell you."
            jump r102
        "I'll do my best in my position even in the future.":
            r "I should have known you were smart."
            r "You might show a different type of leadership from mine."
            r "Thinking of Ao Me after joining hands with you.... makes my heart flutter with excitement."
            jump r102
label r102:
    r "I wonder what you're imagining of Sora?"
    r "Aren't you thinking of him smiling brightly without any traces of darkness someday?"
    r "...Him smiling. That does not suit Sora.... looks so awkward."
    r "He's most lively when he makes a desperate look. That's when you can feel his vibrant liveliness!"
    r "He can only live on relying on something. When he has that expression of agony and being lost, I'm filled with the thought that I want to protect him."
    r "....That face.... is because... he resembles..."
    menu:
        "of whom?":
            r "..."
            jump r103
        "Does he look like Yoshiro?":
            r "No."
            jump r103
label r103:
    r "... I think this is the end I invest my time to you. I've come to the end of the corridor."
    r "When I open this door, Sora would be there."
    r "Why don't you pray in that room while waiting?"
    r "Well then... for eternal paradise."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "08:36" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg5 with dissolve
    call phone_start from _call_phone_start_27
    call message_start("...", "...") from _call_message_start_414
    call message_img("Yoshiro", "So Zero is...", "images/sad.png") from _call_message_img_218
    call message("Yoshiro", "the illegitimate son of the prime minister...") from _call_message_3372
    call message("Yoshiro", "and he's missing.") from _call_message_3373
    call message("Yoshiro", "Zero..............") from _call_message_3374
    call message("Yoshiro", "this is.....") from _call_message_3375
    call message("Yoshiro", "really dangerous isn't it?") from _call_message_3376
    call screen phone_reply("His brother is in danger too...", "cd1014", "Yes. At least his brother's safe, and that's good.", "cd1015")
label cd1014:
    call phone_after_menu from _call_phone_after_menu_390
    call message_start("[name]", "His brother is in danger too...") from _call_message_start_415
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Yoshiro", "aaaaah.....") from _call_message_3377
    jump am106
label cd1015:
    call phone_after_menu from _call_phone_after_menu_391
    call message_start("[name]", "Yes. At least his brother's safe, and that's good.") from _call_message_start_416
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Yoshiro", "You're with him, so you should know...") from _call_message_3378
    jump am106
label am106:
    call message("Yoshiro", "Agh..................") from _call_message_3379
    call message("Yoshiro", "Did Reina") from _call_message_3380
    call message("Yoshiro", "know about this...") from _call_message_3381
    call message("Yoshiro", "when she was alive?") from _call_message_3382
    call screen phone_reply3("...Probably.", "cd1016", "Who knows if she's still alive? That could be part of the secrets.", "cd1017", "....", "cd1018")
label cd1016:
    call phone_after_menu from _call_phone_after_menu_392
    call message_start("[name]", "...Probably.") from _call_message_start_417
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Yoshiro", "If she did") from _call_message_3383
    call message("Yoshiro", "then this is no joke.") from _call_message_3384
    jump am107
label cd1017:
    call phone_after_menu from _call_phone_after_menu_393
    call message_start("[name]", "Who knows if she's still alive? That could be part of the secrets.") from _call_message_start_418
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Yoshiro", "Sorry???") from _call_message_3385
    jump am107
label cd1018:
    call phone_after_menu from _call_phone_after_menu_394
    call message_start("[name]", "....") from _call_message_start_419
    jump am107
label am107:
    call message("SYSTEM", "Yoshiro has left the chatroom.") from _call_message_3386
    play sound "music/hack.ogg"
    call message("SYSTEM", "Reina has entered the chatroom.") from _call_message_3387
    stop sound
    call message("Reina", "You were chatting with Yoshiro.") from _call_message_3388
    call message("Reina", "I must see whether you've said something you shouldn't.") from _call_message_3389
    call screen phone_reply("I only wish to bring the RFA to the Ao Me, my savior.", "cd1019", "Now both K and the RFA will confront the truth, Reina....", "cd1020")
label cd1019:
    call phone_after_menu from _call_phone_after_menu_395
    call message_start("[name]", "I only wish to bring the RFA to the Ao Me, my savior.") from _call_message_start_420
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "If that's what you intended, you deserve a compliment.") from _call_message_3390
    jump am108
label cd1020:
    call phone_after_menu from _call_phone_after_menu_396
    call message_start("[name]", "Now both K and the RFA will confront the truth, Reina....") from _call_message_start_421
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "And do you expect me to be scared?") from _call_message_3391
    call message("Reina", "Your threat is nothing but a ridiculous monologue to me.") from _call_message_3392
    jump am108
label am108:
    call message("Reina", "Ah...") from _call_message_3393
    call message("Reina", "I didn't know") from _call_message_3394
    call message("Reina", "their father has been looking for them....") from _call_message_3395
    call message("Reina", "That's new.") from _call_message_3396
    call screen phone_reply("This is a chance to bring both Zero and the prime minister to the Ao Me.", "cd1021", "Sora might be in danger too...!", "cd1022")
label cd1021:
    call phone_after_menu from _call_phone_after_menu_397
    call message_start("[name]", "This is a chance to bring both Zero and the prime minister to the Ao Me.") from _call_message_start_422
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "My thought exactly.") from _call_message_3397
    jump am109
label cd1022:
    call phone_after_menu from _call_phone_after_menu_398
    call message_start("[name]", "Sora might be in danger too...!") from _call_message_start_423
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "This place is safe.") from _call_message_3398
    jump am109
label am109:
    call message("Reina", "Hah...") from _call_message_3399
    call message("Reina", "if only Sora grows in a way I want him to") from _call_message_3400
    call message("Reina", "I would get to meet Yoshiro very soon.") from _call_message_3401
    call message("Reina", "I have a perfect place for him to be nurtured...") from _call_message_3402
    call message("Reina", "My heart ached so much to see him lost....") from _call_message_3403
    call screen phone_reply3("RFA would never find themselves at this place.", "cd1023", "Everything will go as you wish, my savior.", "cd1024", "You're delirious.", "cd1025")
label cd1023:
    call phone_after_menu from _call_phone_after_menu_399
    call message_start("[name]", "RFA would never find themselves at this place.") from _call_message_start_424
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "Are you playing prophet or something?") from _call_message_3404
    call message("Reina", "Unfortunately, you don't have the power to stop me.") from _call_message_3405
    jump am1010
label cd1024:
    call phone_after_menu from _call_phone_after_menu_400
    call message_start("[name]", "Everything will go as you wish, my savior.") from _call_message_start_425
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "I know.") from _call_message_3406
    jump am1010
label cd1025:
    call phone_after_menu from _call_phone_after_menu_401
    call message_start("[name]", "You're delirious.") from _call_message_start_426
    call message("Reina", "I'll show you") from _call_message_3407
    call message("Reina", "how my delusion becomes reality.") from _call_message_3408
    call message("Reina", "Then you'll come to trust me.") from _call_message_3409
    jump am1010
label am1010:
    call message("Reina", "I miss Yoshiro...") from _call_message_3410
    call message("Reina", "I wonder") from _call_message_3411
    call message("Reina", "when Sora will wake up...") from _call_message_3412
    call message("Reina", "I must return to nurse him.") from _call_message_3413
    call screen phone_reply("Is he suffering a lot...?", "cd1026", "With your love, he'll soon open his eyes.", "cd1027")
label cd1026:
    call phone_after_menu from _call_phone_after_menu_402
    call message_start("[name]", "Is he suffering a lot...?") from _call_message_start_427
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "He's under my care. He is none of your business.") from _call_message_3414
    jump am1011
label cd1027:
    call phone_after_menu from _call_phone_after_menu_403
    call message_start("[name]", "With your love, he'll soon open his eyes.") from _call_message_start_428
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "He should.") from _call_message_3415
    call message("Reina", "He only needs some time.") from _call_message_3416
    jump am1011
label am1011:
    call message("Reina", "Now I'm leaving.") from _call_message_3417
    call message("Reina", "For eternal paradise.") from _call_message_3418
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_3419
    call phone_end from _call_phone_end_28
    stop music
    scene black with dissolve
    show text "11:49" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg5 with dissolve
    call phone_start from _call_phone_start_28
    call message_start("...", "...") from _call_message_start_429
    call message("Yoshiro", "first") from _call_message_3420
    call message("Yoshiro", "the truth about") from _call_message_3421
    call message("Yoshiro", "the prime minister and his illegitimate sons is a must.") from _call_message_3422
    call message("Yoshiro", "And if there's any evidence") from _call_message_3423
    call message("Yoshiro", "on the group of people") from _call_message_3424
    call message("Yoshiro", "that used the hacker and you to their benefits") from _call_message_3425
    call message("Yoshiro", "we should first") from _call_message_3426
    call message("Yoshiro", "ge t t h em-------------------22222") from _call_message_3427
    call message("SYSTEM", "Yoshiro has left the chatroom.") from _call_message_3428
    play music "music/music20.ogg"
    play sound "music/hack.ogg"
    call message("SYSTEM", "Reina has entered the chatroom.") from _call_message_3429
    stop sound
    call message("Reina", "You want everyone in the world") from _call_message_3430
    call message("Reina", "to learn about those twins") from _call_message_3431
    call message("Reina", "and the Ao Me?") from _call_message_3432
    call message("Reina", "I'm afraid that cannot happen.") from _call_message_3433
    call message("Reina", "Because Magenuta") from _call_message_3434
    call message("Reina", "should never be revealed to the world.") from _call_message_3435
    call message("Reina", "And it is better") from _call_message_3436
    call message("Reina", "for the likes to get together.") from _call_message_3437
    call message("Reina", "If that man") from _call_message_3438
    call message("Reina", "is powerful enough") from _call_message_3439
    call message("Reina", "to kidnap Souta so easily....") from _call_message_3440
    call message("Reina", "he'd be wonderfully useful to us.") from _call_message_3441
    call message("Reina", "I wish") from _call_message_3442
    call message("Reina", "there's a chance to meet him in the future.") from _call_message_3443
    call screen phone_reply3("Are you saying it doesn't matter how evil he is?", "cd1028", "I think he'll be of great help for the Ao Me.", "cd1029", "It'd be a miracle if you could meet the prime minister.", "cd1030")
label cd1028:
    call phone_after_menu from _call_phone_after_menu_404
    call message_start("[name]", "Are you saying it doesn't matter how evil he is, as long as he's powerful?") from _call_message_start_430
    show gh at heart_pos with dissolve
    hide gh with dissolve
    jump am1012
label cd1029:
    call phone_after_menu from _call_phone_after_menu_405
    call message_start("[name]", "I think he'll be of great help for the Ao Me.") from _call_message_start_431
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "If he and I work together") from _call_message_3444
    call message("Reina", "I'd be able to create a paradise") from _call_message_3445
    call message("Reina", "much greater and mightier than I planned!") from _call_message_3446
    jump am1012
label cd1030:
    call phone_after_menu from _call_phone_after_menu_406
    call message_start("[name]", "It'd be a miracle if someone like you could meet the prime minister.") from _call_message_start_432
    call message("Reina", "You'd better not underestimate me.") from _call_message_3447
    call message("Reina", "RFA had nothing during its primitive stage.") from _call_message_3448
    call message("Reina", "I'm the one who rendered it a famous fundraising organization.") from _call_message_3449
    call message("Reina", "And that's all because") from _call_message_3450
    call message("Reina", "I had an uncanny judgment of people.") from _call_message_3451
    call message("Reina", "As for the prime minister...") from _call_message_3452
    jump am1012
label am1012:
    call message("Reina", "Of course") from _call_message_3453
    call message("Reina", "he's a useless failure as a parent") from _call_message_3454
    call message("Reina", "but I'd say he's useful in another aspect.") from _call_message_3455
    call screen phone_reply("Is Sora safe...?", "cd1031", "It's a great relief that Sora is under your perfect protection.", "cd1032")
label cd1031:
    call phone_after_menu from _call_phone_after_menu_407
    call message_start("[name]", "Is Sora safe...?") from _call_message_start_433
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "You've been asking that for quite some time now.") from _call_message_3456
    call message("Reina", "He'll eventually be safe") from _call_message_3457
    call message("Reina", "and he'll be strong.") from _call_message_3458
    call message("Reina", "Once he awakens...") from _call_message_3459
    jump am1013
label cd1032:
    call phone_after_menu from _call_phone_after_menu_408
    call message_start("[name]", "It's a great relief that Sora is under your perfect protection.") from _call_message_start_434
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "You're right...") from _call_message_3460
    call message("Reina", "Once this boy wakes up") from _call_message_3461
    jump am1013
label am1013:
    call message("Reina", "the inert 'Ren'") from _call_message_3462
    call message("Reina", "who's finally back") from _call_message_3463
    call message("Reina", "will eventually obey me,") from _call_message_3464
    call message("Reina", "though he wouldn't want to at first.....") from _call_message_3465
    call message("Reina", "K would never have lost Levan") from _call_message_3466
    call message("Reina", "if he kept him locked up right next to him") from _call_message_3467
    call message("Reina", "and protected him like I do.") from _call_message_3468
    call message("Reina", "I'm so upset") from _call_message_3469
    call message("Reina", "that K actually lost Levan.") from _call_message_3470
    call message("Reina", "That boy was so useful.") from _call_message_3471
    call screen phone_reply("He made such a poor vow that he'll be a parent. He shouldn't have done that.", "cd1033", "Reina, you're not Sora's guardian.", "cd1034")
label cd1033:
    call phone_after_menu from _call_phone_after_menu_409
    call message_start("[name]", "He made such a poor vow that he'll be a parent. He shouldn't have done that.") from _call_message_start_435
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "[name]") from _call_message_3472
    call message("Reina", "I see that you see") from _call_message_3473
    call message("Reina", "exactly what the problem is.") from _call_message_3474
    jump am1014
label cd1034:
    call phone_after_menu from _call_phone_after_menu_410
    call message_start("[name]", "Reina, you're not Sora's guardian.") from _call_message_start_436
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "In that case,") from _call_message_3475
    call message("Reina", "who would protect this boy") from _call_message_3476
    call message("Reina", "if I don't?") from _call_message_3477
    call message("Reina", "You? You think you can protect him when you're saying nothing but what weaklings say?") from _call_message_3478
    call message("Reina", "You're no different from K.") from _call_message_3479
    jump am1014
label am1014:
    call message("Reina", "That's the kind of person he is.") from _call_message_3480
    call message("Reina", "K wanted to 'protect'") from _call_message_3481
    call message("Reina", "such a useful and smart boy") from _call_message_3482
    call message("Reina", "and sent him somewhere dangerous") from _call_message_3483
    call message("Reina", "and thus forced him to stand on his own.") from _call_message_3484
    call message("Reina", "If he hadn't done that") from _call_message_3485
    call message("Reina", "by now") from _call_message_3486
    call message("Reina", "there would've been two useful hackers with me...") from _call_message_3487
    call message("Reina", "I am so glad") from _call_message_3488
    call message("Reina", "I didn't let K") from _call_message_3489
    call message("Reina", "take control over Sora.") from _call_message_3490
    call screen phone_reply("Please, you have to realize how bad you made Sora tremble in fear...!", "cd1035", "Zero will eventually join us.", "cd1036")
label cd1035:
    call phone_after_menu from _call_phone_after_menu_411
    call message_start("[name]", "Please, you have to realize how bad you made Sora tremble in fear...! If you don't stop, Sora will be permanently traumatized!") from _call_message_start_437
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "I'd rather have him traumatized.") from _call_message_3491
    call message("Reina", "I'd rather have him bent and broken and bankrupt of feelings.") from _call_message_3492
    call message("Reina", "In that case") from _call_message_3493
    call message("Reina", "it'll be so much easier") from _call_message_3494
    call message("Reina", "for the both of us...") from _call_message_3495
    jump am1015
label cd1036:
    call phone_after_menu from _call_phone_after_menu_412
    call message_start("[name]", "Zero will eventually join us. I can tell him better than anyone else that this place is the safest ground on Earth.") from _call_message_start_438
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "You're right...") from _call_message_3496
    call message("Reina", "Look at Sora.") from _call_message_3497
    call message("Reina", "Look how secure he is") from _call_message_3498
    call message("Reina", "ever since he came under my wings.") from _call_message_3499
    jump am1015
label am1015:
    call message("Reina", "Really, K...") from _call_message_3500
    call message("Reina", "you don't know what the forsaken children") from _call_message_3501
    call message("Reina", "really need.") from _call_message_3502
    call message("Reina", "Children brought in misery") from _call_message_3503
    call message("Reina", "can never stand alone") from _call_message_3504
    call message("Reina", "in the usual way.") from _call_message_3505
    call message("Reina", "They must have someone") from _call_message_3506
    call message("Reina", "to keep them entrapped in bosom") from _call_message_3507
    call message("Reina", "under protection") from _call_message_3508
    call message("Reina", "throughout their lifetime of education.") from _call_message_3509
    call message("Reina", "Protection within a sturdy fence") from _call_message_3510
    call message("Reina", "is the best gift") from _call_message_3511
    call message("Reina", "those children can have.") from _call_message_3512
    call screen phone_reply3("That's what you think.", "cd1037", "Everyone at this place can stay secure because you're here.", "cd1038", "I'm so sick and tired with your philosophy.", "cd1039")
label cd1037:
    call phone_after_menu from _call_phone_after_menu_413
    call message_start("[name]", "That's what you think. People who cannot stand by themselves might as well live with insecurity instead of security.") from _call_message_start_439
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Reina", "That insecurity") from _call_message_3513
    call message("Reina", "arises from security.") from _call_message_3514
    call message("Reina", "Such people would feel insecure even if they stand alone.") from _call_message_3515
    call message("Reina", "No...") from _call_message_3516
    call message("Reina", "the very act of standing alone") from _call_message_3517
    call message("Reina", "is the epitome of insecurity!") from _call_message_3518
    jump am1016
label cd1038:
    call phone_after_menu from _call_phone_after_menu_414
    call message_start("[name]", "Everyone at this place can stay secure because you're here.") from _call_message_start_440
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Reina", "That's right...") from _call_message_3519
    call message("Reina", "I'm growing more important in need.") from _call_message_3520
    jump am1016
label cd1039:
    call phone_after_menu from _call_phone_after_menu_415
    call message_start("[name]", "I'm so sick and tired with your philosophy.") from _call_message_start_441
    call message("Reina", "Both you") from _call_message_3521
    call message("Reina", "and K...") from _call_message_3522
    call message("Reina", "are from a world completely different from mine.") from _call_message_3523
    call message("Reina", "You'd never know") from _call_message_3524
    call message("Reina", "what the definition of happiness is for us.") from _call_message_3525
    jump am1016
label am1016:
    call message("Reina", "I'm looking at Ren right now") from _call_message_3526
    call message("Reina", "resting like an angel in my presence....") from _call_message_3527
    call message("Reina", "And I can see how much he needs me.") from _call_message_3528
    call message("Reina", "I should get a new towel for him") from _call_message_3529
    call message("Reina", "before he starts sweating more.") from _call_message_3530
    call message("Reina", "Now I must take my leave...") from _call_message_3531
    call message("Reina", "For eternal paradise.") from _call_message_3532
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_3533
    call phone_end from _call_phone_end_29
    stop music
    scene black with dissolve
    play music "music/music13.ogg"
    scene bg15 with dissolve
    u "...."
    show R R33 at my_right with dissolve
    r "Ren...? The doctor said you can get up now."
    show R R32
    r "I knew you're not Sora. Are you temporarily back to Ren?"
    show R R33
    r "But you already took all those bottles of elixir... Maybe the elixir isn't working."
    r "Or maybe something was wrong in the production of that batch."
    show U U32 at my_left with dissolve
    u "..."
    show R R32
    r "But don't worry. I'll never leave you... We can start all over again when the new elixir is complete."
    show U U32
    u "...Where is [name]?"
    show R R33
    r "I knew you would look for her. She is in her room. I know you want to see her, but there's something you must do before that."
    r "Right now Levan is absent from RFA."
    show R R35
    r "So this is our chance to work on our plan to bring each member of the RFA, one by one, based on the data we collected so far."
    show R R33
    r "We must complete our analyses on at least two of them before the night is over..."
    r "So we must hurry. We don't know when Levan will be back."
    u "...But what if I say no?"
    r "...Looks like the elixir is a complete failure. But your face tells me the elixir is definitely in effect... How strange."
    r "You know the rules of this place. If you don't obey, I have no choice but to make you obey."
    show R R32
    r "And that would mean you'll suffer."
    u "...Being a hacker is also a suffering."
    show R R33
    r "You cannot survive doing only what you want to do."
    r "Even if you don't assume your role, someone else will."
    u "If this place is really the paradise...then nobody would have to do that job. Isn't that right?"
    show R R32
    r "...I wish you wouldn't talk about the paradise like that, Ren."
    r "You have no idea what paradise is really like."
    u "This isn't paradise."
    show R R33
    r "What was that?"
    u "This isn't paradise. Paradise isn't with you... Paradise is with the person who makes me feel I'm alive."
    show U U33
    u "I felt alive when she hugged me. I could feel my heart beating."
    u "Even the air I breathed in and out tasted so happy... If that's not paradise, what else is?"
    show U U32
    r "You've felt that you've become someone else because of those flurries of emotions."
    show R R32
    r "I, too, experienced something similar when I was still with K."
    show R R33
    r "Those emotions will one day betray and deny you. And they'll make you lose yourself."
    r "But that won't happen in this place. That's why I founded this place."
    show R R32
    r "Darkness is not a sin in this place... We can stay the way we were born. That's who you are...Ren."
    r "You're a failure from birth."
    show R R35
    r "You're dark, cowardly.... You are helpless, incapable of anything without someone's command."
    r "I know that. Even if you say these silly things, I'll never dispose of you. I love you."
    r "This place is under progress for security and happiness of people like us."
    show R R32
    r "I'm devoting myself for that. But if you don't obey me, you'll never reach the true paradise."
    show U U36
    show R R33
    u "You're not the leader of the paradise. You...you only want a machine that will do everything as you say."
    show R R36
    r "Ren, you're working at this place in return for a secure life!"
    show R R37
    r "Aren't you happy? Aren't you happy that somebody needs people like us?!"
    r "Step outside, if you dare. You think the world wants such dark-oriented failures like us?"
    show U U35
    u "...Now I can see. I can see that's what you're scared of."
    show R R36
    r "What...?"
    u "You're scared to step outside, aren't you? You're scared that people will leave you."
    u "Like you said...you don't want to be abandoned by someone, like how you were abandoned by your parents."
    r "You're speaking gibberish because the elixir isn't working."
    show U U36
    u "That's why you're not leaving the people who came here. You're trying to force everyone to do something."
    show R R37
    r "Who told you that? Is it [name]?"
    show U U33
    u "It was Sora. He told me this from within."
    show U U32
    u "He said you'll always be heartless to the believers..."
    u "He said you'll do the same heartless thing that someone else did to you..."
    r "No! I love them. I'm doing this all for their sake! Just why would you think I'm doing something they did to me?"
    show R R36
    r "Who do you think you are to tell me that!?"
    show U U33
    u "Sora told me he did something heartless, too, because he didn't want to suffer such heartless things himself."
    show U U32
    u "And you're hurting people because you're scared you'll get hurt, aren't you?"
    show R R37
    r "I'm nothing like my parents. I'll never abandon my children. I'm saving them!"
    u "That's just an excuse. You're just being cruel to people at this place, just like how your parents were cruel to you..."
    r "No, my parents... They... Huff..."
    hide R
    hide U
    scene black with dissolve
    teacher "That's what happened, but she wouldn't say anything about it."
    teacher "Ma'am, shouldn't you schedule a counseling for her? I'm not sure if she can perform well enough to meet the school's demands."
    mother "...Counseling? My family does not need counseling."
    teacher "As you know, this is a private school. It is not reserved for all students."
    teacher "If this continues, other parents will start to file complaints."
    mother "Well, excuse me... But are you aware who my husband is?"
    teacher "Of course. We wouldn't have chosen to go this far if she weren't his daughter."
    scene white with dissolve
    mother "...You disgraced your father so much today."
    mother "Once you're expelled from school, you're expelled from this household as well."
    r "But they told me not to say anything... They said they'll be mean to me if I tell teachers..."
    mother "Why do you think they torment you? Why do you think they torment someone so dark and stupid like you...?"
    mother "...It's because of the devil inside you. The devils will keep haunting you because you're such a wretched child."
    mother "I should call a preacher and have you cleansed!"
    r "No, no...!"
    mother "I can see the devil... It's provoking me again inside you."
    mother "You'll be useless if we don't have you cleansed regularly."
    mother "A child with a devil can belong nowhere!"
    mother "We should never have brought someone like you..."
    mother "Hah... What did I ever do wrong in my life?"
    mother "We should have brought that kid who was right beside you...! That one looked like an angel...!"
    mother "It's my fault. God gave me a sign, but I decided to ignore it..."
    scene bg15 with dissolve
    show R R37 at my_right with dissolve
    r "Who...who are you? You're not Ren!!!"
    r "You're wrong! No! No!! You're my child! You have to obey me!"
    r "Or else I'll expel you. Do you want to be expelled from this place?!"
    r "You belong nowhere out of this place!"
    r "You disobedient child... I should never have brought...someone like you..."
    r "I should have brought Souta...! He's smarter than you! He's better than you...!"
    r "Huff... Huff.."
    show U U32 at my_left with dissolve
    u "......"
    show R R38
    r "No, no..."
    show R R39
    r "Sora, I lost myself for a little...."
    r "I'll never leave you. I was just trying to scare you."
    show R R38
    r "So please listen to me. I'll never treat you like my mother treated me."
    r "I'm your real mother. I'm the one who loves you..."
    u "...Good-bye."
    hide U with dissolve
    show R R39
    r "...Don't go, Sora... Where are you going?"
    r "If you leave..."
    hide R with dissolve
    scene black with dissolve
    r "...No one will ever accept us."
    stop music
    scene black with dissolve
    show text "16:13" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene black with dissolve
    play sound "music/dooropen.ogg"
    play music "music/music12.ogg"
    "(...!)"
    scene bg5 with dissolve
    show MC MC15 at my_right with dissolve
    believer "Special missionary [name], the savior has ordered you to reside in the basement until your cleansing ceremony."
    believer "So I need you to..."
    scene black with dissolve
    show MC MC15 at my_right
    "(...!)"
    believer "Why did the lights go off so suddenly? What is this?!"
    whooo "...[name], grab onto my arm."
    "(Somebody embraced me.)"
    stop music
    menu:
        "(Struggle.)":
            show U U32 at my_left with dissolve
            u "It's me. Sorry I couldn't come back sooner..."
            jump u101
        "Ren...?":
            show U U32 at my_left with dissolve
            u "Yes..I'm back. Sora is also with me..."
            jump u101
        "Sora...?":
            show U U32 at my_left with dissolve
            u "That's right. And I'm also Ren. So don't worry."
            jump u101
label u101:
    show U U32 at my_left
    u "We're getting out of here."
    u "Don't be afraid. Trust me."
    show U U314
    show MC MC14
    u "Just grab onto my arm, close your eyes, and count to three."
    u "One, two..."
    u "Three...!"
    hide U with dissolve
    hide MC with dissolve
    scene white with dissolve
    with Pause(1.0)
    scene bg24 with dissolve
    play music "music/music23.ogg"
    show U U310 at my_left with dissolve
    show MC MC14 at my_right with dissolve
    u "They'll be gaining on us soon. I already secured a way out of here."
    u "[name]... I'm so glad you're safe."
    show U U314
    menu:
        "Ren... Are you really Ren?":
            u "Yes... But Sora's also inside."
            jump u102
        "Is it okay to escape like this?":
            u "Savior won't be okay..."
            u "But now that I know what the truth is, I cannot stay here any longer."
            jump u102
label u102:
    show U U32
    u "No need to worry. You won't get to see cruel Sora anymore."
    show U U31
    u "Both Ren and Sora inside me...want you to be happy."
    u "My name is now both Ren and Sora. You can call me as you like."
    show U U311
    u "[name]... Though I've come back for you...I know it'd be difficult to forgive what I've done to you."
    u "I had no idea I had thousands of wounds within."
    u "I thought I cannot fall for you any deeper..."
    show U U39
    u "But I didn't know my wounds were so deep, deep enough to cloud my feelings for you and leave such scars on you..."
    show U U38
    menu:
        "I don't think I can forgive you for what you did...":
            u "Yes, I know."
            u "That was also part of me. I won't deny what I did."
            u "I know I can't be saved...but right now I want to save you."
            u "I'll ask for your forgiveness....after I get you out of here safely."
            jump u103
        "Why are you sorry? I even liked being tormented by you.":
            u "...What?"
            u "...You're joking because you don't want me to be sad, aren't you? I tortured you beyond imagination."
            u "I think I'm gonna cry thinking about it again...."
            jump u103
        "I already forgave you. I understand what you've suffered...":
            u "Thank you...[name]."
            u "You're my greatest salvation of life. I want you to know that."
            jump u103
label u103:
    show U U311
    u "Before I met you...I was always hiding. I felt like my fear, my monster inside would take over me."
    u "But that monster was a chain someone forced upon me."
    show U U39
    u "People always used to tell me..."
    u "That I'm stupid and weak... That's why I'm useless..."
    show U U32
    u "Both my mother and my savior told me that...in order to control me."
    u "And they treated me like a tool."
    show U U33
    u "They made me think that I'm useless if I don't at least serve as a tool."
    u "That's why I had to be strong. I had to desperately fight for my life in order to prove that I'm useful."
    show U U32
    u "If people didn't tell me I was useful, all that was left for me was loneliness, despair, and this suffocating self-hatred...."
    menu:
        "Ren...":
            jump u104
        "Sora...":
            jump u104
label u104:
    show U U310
    u "But when you chose me...I felt something change inside me."
    show U U38
    show MC MC16
    u "Because whenever you smiled, I felt proud of myself for making you smile."
    show U U33
    u "At that moment I could be me, not someone's tool."
    show U U310
    u "I've never felt anything like that before."
    u "It was as if...I could feel my emotions breathing and taking flight by my will."
    show U U33
    u "When you were happy, your happiness chased away my monster, my despair...my self-hatred."
    show U U310
    u "And I came to wish I could make you happy wherever you are... And I want to be happy with you."
    show U U33
    u "That was something my heart naturally came to wish. Nobody ever forced it on me."
    show U U39
    u "Sometimes I got scared that I won't be able to make you happy enough."
    show U U310
    u "But that was a sweet fear."
    u "It was much more exciting and heart-beating than being used and constantly having to prove my usefulness."
    u "If it is okay for a coward like me to like you... If it is okay for me to stay with you and protect you...."
    u "Please let me be the person I wanted to be, for the first time in my life..."
    u "Please choose me as the person who'll make you happy."
    show U U311
    u "...My parents cannot control my life. My savior cannot control my life anymore."
    show U U32
    u "Because now...I'm nobody's tool."
    show U U38
    u "I'm a person who wishes to make you happy."
    show MC MC11
    menu:
        "Will you make me happy, then?":
            u "Yes. Yes, I will..! You can count on me. I'm so happy..."
            jump u105
        "Congratulations on discovering the real you. Now Ren... Sora... You're no longer a tool.":
            u "No, I'm not. I don't want to be their tool anymore."
            jump u105
label u105:
    show U U311
    u "I knew it."
    show U U32
    u "...This is why the elixir didn't work on me anymore."
    show U U33
    u "Now I don't need the elixir anymore."
    show U U32
    u "Because the real salvation of my life is you."
    hide U with dissolve
    hide MC with dissolve
    scene white with dissolve
    u "[name]... You're my angel."
    u "Now my dark past doesn't matter when I'm with you."
    u "Now I'm even thinking...."
    u "Perhaps the reason why my past has been so dark...was to see how your happy smile lights up the world so bright."
    u "I really hope that's it."
    u "[name]... Please keep smiling. Please be happy.... I'll do the same with you."
    scene white with dissolve
    show U U310 at my_left with dissolve
    show MC MC11 at my_right with dissolve
    u "...I wish we could stay like this for a while, but they'll start looking for us now."
    show U U314
    u "First, let's get out of here."
    show MC MC14
    mc "Umm... You do know that Levan is missing, don't you?"
    show U U32
    u "What...?"
    show U U311
    u "Uh..."
    show U U32
    u "Let's talk about that after we make it out of here."
    stop music
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    show text "18:37" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music24.ogg"
    scene bg25 with dissolve
    show U U36 at my_left with dissolve
    show MC MC11 at my_right with dissolve
    u "Huff... Huff... I think we've lost them."
    show U U32
    menu:
        "Sora, could you please take a look at the messenger?":
            u "The messenger...?"
            jump u106
        "Don't you feel nervous out here?":
            u "No, I feel so free. I don't care where I am as long as I'm with you."
            u H"uh? You're still gripping onto your phone...."
            jump u106
label u106:
    show U U35
    u "Wait, that's right! They might track us if the messenger app is still connected to the Ao Me. We must get rid of it!"
    show U U36
    u "Though it'll take a while for them to actually start tracking us. I locked the app with a bunch of protection walls before finding you."
    show MC MC14
    mc "What will you do about Levan?"
    show U U32
    u "..."
    u "My father kidnapped him, didn't he?"
    show U U311
    u "To be honest, I'm not sure yet...."
    show U U33
    u "I know that everything the savior told me was intended to take advantage of me."
    show U U311
    u "I don't want to live in the paradise she forces on people..."
    show U U39
    u "But when it comes to my brother...."
    menu:
        "You hate him, don't you?":
            u "Yeah, kind of. But..."
            jump u107
        "What do you wish would happen to him?":
            jump u107
label u107:
    show U U311
    u "I used to say that I wish he'd be gone from this world...but I don't think that's what I really wanted."
    u "I don't want to meet him right now."
    show U U39
    u "I doubt he ever thought about me ever since he joined the agency..."
    show U U33
    u "But...that doesn't mean I want him killed by my father."
    show U U32
    u "....He must stay alive. Just like how I stayed alive."
    show U U311
    u "[name], what do you think?"
    show U U311
    show MC MC11
    menu:
        "I think both of you have to live your own separate lives.":
            u "You're right. I should live my life, and he should live his."
            u "However...if he's in danger because of my father...then it could affect my life as well."
            jump u108
        "I think you're right.":
            u "...Yeah. At least he was worried about me... That's the last thing I remember about him."
            u "I should believe that he actually meant... Cough..."
            u "Sorry... This is because of the brainwashing."
            u "I was trained that I shouldn't talk in favor of him. Huff... Just a sec. Let me take some breath."
            u "But since we're not in Magenuta anymore..."
            jump u108
label u108:
    show U U32
    u "...Let's see what's happening in the messenger right now."
    stop music
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    show text "20:22" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg25 with dissolve
    call phone_start from _call_phone_start_29
    call message_start("...", "...") from _call_message_start_442
    call message_img("Yoshiro", "Zero", "images/sad.png") from _call_message_img_219
    call message("Yoshiro", "what do we do?") from _call_message_3534
    call message("Yoshiro", "Is he") from _call_message_3535
    call message("Yoshiro", "being tortured or something?") from _call_message_3536
    call message("Yoshiro", "T_T") from _call_message_3537
    call message("Yoshiro", "I wish we can reach Sora soon.") from _call_message_3538
    call message("Yoshiro", "And I wish we'd be friends.") from _call_message_3539
    call screen phone_reply("I'll try calling him.", "cd1040", "He might answer you if you call for him out loud.", "cd1041")
label cd1040:
    call phone_after_menu from _call_phone_after_menu_416
    call message_start("[name]", "I'll try calling him.") from _call_message_start_443
    call message_img("Yoshiro", "Really?", "images/shocked.png") from _call_message_img_220
    call message("Yoshiro", "Let me join you.") from _call_message_3540
    jump am1017
label cd1041:
    call phone_after_menu from _call_phone_after_menu_417
    call message_start("[name]", "He might answer you if you call for him out loud.") from _call_message_start_444
    jump am1017
label am1017:
    call message("Yoshiro", "Sora - !!") from _call_message_3541
    call message("Yoshiro", "Help!!!") from _call_message_3542
    call message_img("Shun", "Hey", "images/well.png") from _call_message_img_221
    call message("Shun", "you're calling out to a guy") from _call_message_3543
    call message("Shun", "you haven't even said hi to?") from _call_message_3544
    call message("Yoshiro", "Come on") from _call_message_3545
    call message("Yoshiro", "I think we already said hi.") from _call_message_3546
    call message("Yoshiro", "Though it was through hacking...") from _call_message_3547
    call message("Yoshiro", "Sora - !!!! T_T") from _call_message_3548
    call message_img("Shun", "Hey", "images/well.png") from _call_message_img_222
    call message("SYSTEM", "Sora has entered the chatroom.") from _call_message_3549
    call message("Shun", "stop it") from _call_message_3550
    call message("Yoshiro", "whaaa") from _call_message_3551
    call message("Yoshiro", "aaaa") from _call_message_3552
    call message("Yoshiro", "aaaaaaaaaa") from _call_message_3553
    call message_img("Shun", "!?!?!?!", "images/shocked.png") from _call_message_img_223
    call screen phone_reply("Welcome, Sora.", "cd1042", "Sora... Thank you for doing me a favor.", "cd1043")
label cd1042:
    call phone_after_menu from _call_phone_after_menu_418
    call message_start("[name]", "Welcome, Sora.") from _call_message_start_445
    call message("Sora", "Yeah. I am here to answer you.") from _call_message_3554
    jump am1018
label cd1043:
    call phone_after_menu from _call_phone_after_menu_419
    call message_start("[name]", "Sora... Thank you for doing me a favor.") from _call_message_start_446
    call message("Sora", "You will always") from _call_message_3555
    call message("Sora", "light up my way to the truth.") from _call_message_3556
    jump am1018
label am1018:
    call message_img("Yoshiro", "So", "images/shocked.png") from _call_message_img_224
    call message("Yoshiro", "sora...?") from _call_message_3557
    call message("Sora", "Yoshiro Kim") from _call_message_3558
    call message("Sora", "and Shun") from _call_message_3559
    call message("Sora", "Am I right?") from _call_message_3560
    call message("Shun", "!?!?!") from _call_message_3561
    call message("Shun", "Are you the hacker?") from _call_message_3562
    call message("Shun", "You're the hacker, right?") from _call_message_3563
    call screen phone_reply("That's him.", "cd1044", "I think you're greeting him a little fervently ^^...", "cd1045")
label cd1044:
    call phone_after_menu from _call_phone_after_menu_420
    call message_start("[name]", "That's him.") from _call_message_start_447
    call message("Shun", "Uh") from _call_message_3564
    call message("Shun", "ok...") from _call_message_3565
    jump am1019
label cd1045:
    call phone_after_menu from _call_phone_after_menu_421
    call message_start("[name]", "I think you're greeting him a little fervently ^^...") from _call_message_start_448
    call message("Yoshiro", "What's gotten into you Shun..?") from _call_message_3566
    call message("Shun", "Uh") from _call_message_3567
    jump am1019
label am1019:
    call message("Shun", "I mean") from _call_message_3568
    call message("Shun", "you're Zero's brother...") from _call_message_3569
    call message("Shun", "Mr. Sora?") from _call_message_3570
    call message("Shun", "Sora?") from _call_message_3571
    call message("Shun", "What should I call you?") from _call_message_3572
    call message("Shun", "Ah") from _call_message_3573
    call message("Shun", "wait this isn't time for this") from _call_message_3574
    call message("Shun", "anyways if you're really in this chatroom") from _call_message_3575
    call message("Shun", "hear us out.") from _call_message_3576
    call message("Yoshiro", "Your brother has been kidnapped!!!") from _call_message_3577
    call message_img("Yoshiro", "By your father!!!", "images/sad.png") from _call_message_img_225
    call screen phone_reply("I already told him about it...", "cd1046", "That was a perfect summary...", "cd1047")
label cd1046:
    call phone_after_menu from _call_phone_after_menu_422
    call message_start("[name]", "I already told him about it...") from _call_message_start_449
    call message("Shun", "Uh") from _call_message_3578
    call message("Shun", "you did?") from _call_message_3579
    call message("Yoshiro", "Ugh...") from _call_message_3580
    call message("Yoshiro", "then what should we tell him?") from _call_message_3581
    jump am1020
label cd1047:
    call phone_after_menu from _call_phone_after_menu_423
    call message_start("[name]", "That was a perfect summary...") from _call_message_start_450
    call message("Shun", "Hey") from _call_message_3582
    call message("Shun", "you shouldn't phrase it like that.") from _call_message_3583
    jump am1020
label am1020:
    call message("Shun", "So") from _call_message_3584
    call message("Shun", "uhm") from _call_message_3585
    call message("Shun", "Uh") from _call_message_3586
    call message("Shun", "where should I start?") from _call_message_3587
    call message("Shun", "My hands are shaking") from _call_message_3588
    call screen phone_reply("Relax.... Sora is not a scary person.", "cd1048", "Sora has suddenly become silent.", "cd1049")
label cd1048:
    call phone_after_menu from _call_phone_after_menu_424
    call message_start("[name]", "Relax.... Sora is not a scary person.") from _call_message_start_451
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Shun", "Yeah") from _call_message_3589
    call message("Shun", "we know.") from _call_message_3590
    call message("Shun", "But") from _call_message_3591
    call message("Shun", "it's just so awkward....") from _call_message_3592
    jump am1021
label cd1049:
    call phone_after_menu from _call_phone_after_menu_425
    call message_start("[name]", "Sora has suddenly become silent.") from _call_message_start_452
    call message("Shun", "What?") from _call_message_3593
    call message("Shun", "Is it because") from _call_message_3594
    call message("Shun", "I didn't think about this beforehand?") from _call_message_3595
    call message("Shun", "Ugh") from _call_message_3596
    call message("Shun", "I'm so nervous.") from _call_message_3597
    jump am1021
label am1021:
    call message("Yoshiro", "Relax Shun") from _call_message_3598
    call message("Yoshiro", "first") from _call_message_3599
    call message("Yoshiro", "what do you want to ask him?") from _call_message_3600
    call message("Shun", "Rather than asking") from _call_message_3601
    call message("Shun", "first") from _call_message_3602
    call message("Shun", "we should ask him one by one") from _call_message_3603
    call message("Shun", "so that he won't be shocked.") from _call_message_3604
    call message("Yoshiro", "ok...") from _call_message_3605
    call message("Shun", "Sora...") from _call_message_3606
    call message("Shun", "hi...") from _call_message_3607
    call message("Shun", "listen...") from _call_message_3608
    call message("Shun", "do you know...that your father is the prime minister?") from _call_message_3609
    call message("Sora", "......") from _call_message_3610
    call message("Sora", "I'll contact the C&R intelligence unit.") from _call_message_3611
    call message("Sora", "Yes.") from _call_message_3612
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_3613
    call message_img("Shun", "He logged in all so sudden.....", "images/shocked.png") from _call_message_img_226
    call message("Shun", "I think I really freaked out back there....") from _call_message_3614
    call message("Shun", "Agh...man") from _call_message_3615
    call message("Shun", "I wanted to help") from _call_message_3616
    call message_img("Shun", "but it's not working out how I want it to T_T", "images/sad.png") from _call_message_img_227
    call message("Shun", "You talked to him for this opportunity, [name]") from _call_message_3617
    call message("Shun", "but I blew it T_T") from _call_message_3618
    call message("...", "...") from _call_message_3619
    call phone_end from _call_phone_end_30
    stop music
    show MC MC11 at my_right with dissolve
    show U U311 at my_left with dissolve
    play music "music/music23.ogg"
    u "[name].... I'm nervous."
    show U U33
    u "I spent more than 6 months preparing this app, to bring you to the Magenuta."
    show U U32
    u "I analyzed my brother's codes and imitated them. That's basically the only thing I've done for those 6 months, along with hating the RFA."
    show U U314
    u "...But now...I'm going back to who I really am."
    show U U310
    menu:
        "(Hold his hand.)":
            u "Ah..."
            u "...Yes. I feel brave."
            jump u109
        "You can do it, Sora.":
            u "You're calling me sometimes Ren and sometimes Sora. But I like either of them when you say them."
            jump u109
label u109:
    show U U32
    u "So now...let's return C&R unit's contact."
    hide U with dissolve
    hide MC with dissolve
    scene bg19 with dissolve
    iu "Ma'am, take a look at this!"
    show J J12 at my_right with dissolve
    j "Oh! At last! Mr. Han!! We finally have a reply!"
    show H H12 at my_left with dissolve
    h "...So it seems."
    show H H13
    elly "Meow."
    iu "What should we tell him? He wants to talk in real-audio."
    show J J11
    j "Mr. Han will talk to him in person."
    j "Sir....please use this."
    show H H14
    h "Alright."
    hide H with dissolve
    hide J with dissolve
    scene black with dissolve
    h "Can you hear me?"
    u "...Yes."
    h "It's a pleasure to meet you. It's Sora Choi, isn't it? You're Levan's twin brother.I am..."
    u "Ryou Han, the director of C&R. I already know."
    h "Then I believe this conversation would be easy."
    j "...You look very much like your brother, except for your bleached hair."
    h "Am I mistaken if I consider your reply as a favorable sign to us?"
    u "No, you're not mistaken. [name] helped me to see the truth."
    j "Her role was crucial..."
    h "In that case, for now, let's pretend your assault on our messenger never happened. And let's discuss what you need."
    h "I believe you already picked up details from our contact, but your brother, Levan, is missing."
    h "I do not wish to talk about your father, the prime minister. I assume you already know what you need to know about him."
    h "We require two things from you."
    h "First, finding Levan in collaboration with C&R's intelligence unit."
    h "Second, saving Levan without inducing the prime minister's retaliation."
    h "...I know both of them are a challenge."
    u "I need the case number for the prosecution investigations and the email the prime minister sent you."
    u "They were both on my other computer, but right now I don't have them. I'll get to work as soon as I have them."
    h "Very well... And one more thing."
    h "I'm not sure if these can help you to reach our joint goal, but I have something to give you."
    u "What are these?"
    h "They are from your brother and K. Take a look and use them well. We already reviewed what they're about."
    h "In summary, they contain atrocities that could finish your father's invisible tyranny once and for all."
    u "...Do they contain details about us?"
    h "The secret about his illegitimate sons are the core contents."
    u "What do you want me to do with these?"
    h "The tide might turn in our favor...if we spill some of those details and announce that he's kidnapped Levan."
    u "It's too dangerous. He'll definitely try to bite back if we kick his back. And once the world knows about us..."
    h "Which is why I would like you to think thoroughly before you decide to unleash them and notify the C&R. And discussion with me should not be optional."
    h "Because we happen to be analyzing every high authority related to each of the secrets."
    u "Then tell me what you have analyzed so far..."
    h "...Assistant Kang, relay to him what you have on a regular basis."
    j "Understood."
    h "The reason why we believe you are not just because you're Levan's brother. It's also because we trust both K and the RFA."
    h "Additionally, it's because of what my judgment told about [name], whom I believe is currently with you... After 10 days of observations, I determined she's a trustworthy person."
    h "So I would like you to promise me one thing."
    h "Promise on her, on K, and on your brother...that you won't betray us."
    u "...I might not be perfect, but I won't betray anybody. I promise."
    h "Very well. Then I'll count on you, and let the joint project begin."
    h "From now on, I want all staff members of the unit to follow his orders."
    iu "Understood!"
    stop music
    scene black with dissolve
    with Pause(1.0)
    play music "music/music2chat-17.ogg"
    scene bg25 with dissolve
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "It's me. I called in case you were looking for me."
    menu:
        "Sora, are you outside right now?":
            u "Yes. I'm outside. So you were looking for me."
            u "I was planning to go straight to you but it a while... I thought you'd be worried and called."
            jump u1010
        "I'm holding your girlfriend as hostage!!!":
            u "Whoa!...That took me by surprise. My heart almost dropped."
            u "I knew it was your voice but still, I got startled and almost tripped...*sigh*... Don't startle me..."
            jump u1010
label u1010:
    u "The thing is, I saw a lily of the valley in the forest on the way here."
    u "I'm looking around here to find that lily of the valley."
    menu:
        "Lily of the valley? Do you mean the flower in the photo you sent me before":
            u "Yes... I don't like picking flowers but... I really wanted to give you flowers as a gift."
            jump u1011
        "For me to become happy, I need you next to me than the flower.":
            u "A promise of happiness.... you remembered the flower language I mentioned before."
            u "Yes, if we want to become happy, we need to be next to each other."
            u "Still... however busy I am, I want to give you a gift. Though, I can only give you flower gifts for now."
            jump u1011
label u1011:
    u "When... when I was young, I was given a bibliography book of flowers... In that book, lily of the valley was my favorite."
    u "It's white, round and looks like as if it'll make small chime sounds when touched. That image... just somehow took over my heart."
    u "At first, the shape caught my eye but... after I looked up its flower language, I liked it even more."
    u "A promise of happiness...' I prayed that I wanted that to come true..."
    u "I dream of becoming happy when looking at that flower even when I was a grown up."
    menu:
        "You're going to be happy just like the flower words. It's not that far.":
            u "It came true the minute you came to me. I'm happy."
            u "Since the day I was beside you as Ren... though my body was in agony, my mind was extremely happy."
            jump u1012
        "Will you make me happy?":
            u "Without a doubt."
            u "I... I'm not modest like other people but... I don't lose a battle when it comes to how much and truthfully I love you."
            jump u1012
label u1012:
    u "Oh.. found the flower. I should get this and go."
    menu:
        "Don't pick the flower, rather let's go see it together when the day is bright.":
            u "Should we...? It's hunching down under the tree but it's shining brightly even in the dark."
            jump u1013
        "How many are there?":
            u "I think there's about five. I'll take it as is, pretty and all. I hope you like it."
            jump u1013
label u1013:
    u "[name]... let's only make choices that will lead to happiness."
    u "I finally know what a happy choice means."
    u "It's a choice that takes place under my pure freedom.. without any stigma or bias implanted by others and without anyone's demand."
    u "I'm definite that I'm happy right now."
    u "Even if I don't have any memories of being loved in my painful past... even if my past were filled with days where I believed myself to be miserable and dogeared...."
    u "Still, I'll believe in myself, become free and choose...to love you. To get away from this coziness of pain. To open my eyes to the truth and refuse illusions and fantasies."
    u "You told the afraid me numerous times that it's okay to do so and that I can do as I wished. So... I'm going to spread my wings and fly now."
    u "...Since I know I have the power to take action and make choices with freedom... I'm happy. I'm... definitely happy right now."
    u "I think it'll be a good idea to make this lily of the valley our new faith... Talking about this, I really miss you so much. Even though I saw you just a while ago..."
    u "...I'll go back now. Wait for me."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "21:39" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg25 with dissolve
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "Are... you with Sora right now?"
    menu:
        "Yes! We are together!":
            v "It's a relief that you're very energetic."
            v "I think Sora is working energetically, cooperating with the intelligence unit."
            jump v101
        "Sora is working hard with the intelligence unit.":
            v "Oh... I see. That's great."
            v "Looking at how cooperative he is... it's all thanks to you."
            jump v101
label v101:
    v "It feels like a dream... that he's trying his best to save Levan, whom he hated for so long."
    v "I hope the two can unite safely."
    menu:
        "Yes. That's what I hope for as well.":
            v "It would be great if the world was a place where the two brothers can live comfortably..."
            v "Without the need to hide or be in pain... that kind of world."
            v "... Reina might have wanted to create such world. Oh. I don't think talking about Reina would be a good idea right now."
            jump v102
        "I hope Sora doesn't like Zero more than me, would he?":
            v "Sorry...? Oh... even if he likes him, it would be a different line of emotion since they're brothers."
            v "I don't think you need to worry about that."
            jump v102
label v102:
    v "When I first started to gather information.... I only thought of protecting the two from their evil father."
    v "It wasn't something that I was told to do but... I thought I need the info as insurance."
    v "I stopped gathering info out of relief when Levan entered the agency but... still, I didn't think that this old evidence would be used as the smoking gun."
    v "Uh.. [name]. There's something I want to ask you."
    v "When these are released to the public.... wouldn't Sora or Levan get hurt?"
    menu:
        "...Sora is strong enough.":
            v "I see. You must know better of the now Sora, who's an adult."
            jump v103
        "There's no other way but to reveal everything.":
            v Y"es. I was hoping there would be another way but..."
            jump v103
label v103:
    v "When it's revealed that they're illegitimate sons, lots of people will talk about them. I... I think that will happen most likely."
    v "Thank you for telling me."
    v "...Oh, it's nothing. I'll be there soon."
    v "[name], I'm sorry but I must hang up now."
    v "When tonight passes by, there will a lot of things that have changed... I trust that you'll hold your ground well under the catastrophe."
    v "Regardless of what's right or wrong, I believe true love is making the other acknowledge the power of freedom. I've learned a lot from you."
    v "Please take good care of Sora. Thank you."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    with Pause(1.0)
    play music "music/music9.ogg"
    scene bg13 with dissolve
    show R R32 at my_right with dissolve
    r "...I've done nothing wrong. I'm a good person. I tried to save that child."
    r "He's the one who ran away. One day he'll be crying and crawling back to me. Poor Sora..."
    show R R37
    r "I'm not wrong. I did not torture anybody. What I did was love!"
    show R R32
    r "Yes, this is all because of the RFA. That's why I'm confused as well."
    show R R39
    r "Huff.. Huff... I'm...not wrong. Or..was I?"
    show R R38
    r "Why doesn't Sora love me?"
    show R R36
    r "Every believer here loves me! They say I'm right...."
    show V V113 at my_left with dissolve
    v "...Reina."
    show R R37
    r "You...? How did you...?!"
    show R R36
    r "Hah! Are you here to mock me again? Because I've been deserted again? How dare you let me see that shameless face of yours...?"
    r "Go ahead! Try to condemn me!Try to deny everything I've accomplished!"
    show V V114
    r "Try that rubbish that one day I'll give up everything and return for your love! I am not moving an inch from this spot."
    show V V113
    v "Reina...I think you're amazing. You managed to accomplish this much in such a short time."
    show R R37
    r "What?"
    show V V122
    v "...I admit it. I'll admit...that I've been trying to force my ideas upon you so far."
    r "You're trying to trick me, aren't you...?"
    show V V121
    v "No. I finally know how I should love you, Reina...."
    show V V122
    v "I thought I had to devote myself to you eternally. I tried to take responsibility for everything you've done."
    show V V121
    v "However...now I've realized that was not the appropriate way to love you."
    show V V122
    show R R39
    v "Reina...I accept you for who you are. I love your way more than anyone else. This place is beautiful, as much as you..."
    r "...This...this isn't a dream, is it...?"
    v "No, it isn't. I'm here to love you again, Reina...."
    show V V121
    show R R31
    v "But this time I won't try to kill you. I won't force responsibilities upon you...."
    r "Haaa... K...!"
    scene white with dissolve
    r "I missed you, my love... My sun!"
    show V V122 at my_left with dissolve
    show R R32 at my_right with dissolve
    v "...But now your sun might perish into a pile of ashes, Reina."
    show R R33
    r "That's exactly what I want."
    show R R35
    r "No...you had better turn yourself into ashes. Otherwise, I will incinerate you, K...."
    stop music
    hide V with dissolve
    hide R with dissolve
    scene black with dissolve
    show text "23:48" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg25 with dissolve
    play music "music/music23.ogg"
    call phone_start from _call_phone_start_30
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_453
    call message("Sora", "[name]") from _call_message_3620
    call message("Sora", "where are you?") from _call_message_3621
    call screen phone_reply("At the kitchen. It looked like you didn't eat anything for a while...", "cd1050", "I'm taking a walk.", "cd1051")
label cd1050:
    call phone_after_menu from _call_phone_after_menu_426
    call message_start("[name]", "At the kitchen. It looked like you didn't eat anything for a while...") from _call_message_start_454
    call message("Sora", "Then") from _call_message_3622
    call message("Sora", "can I get something sweet...?") from _call_message_3623
    call screen phone_reply("Okay.", "cd1052", "I was going to make something warm for you..", "cd1053")
label cd1052:
    call phone_after_menu from _call_phone_after_menu_427
    call message_start("[name]", "Okay.") from _call_message_start_455
    jump am1022
label cd1053:
    call phone_after_menu from _call_phone_after_menu_428
    call message_start("[name]", "I was going to make something warm for you..") from _call_message_start_456
    call message("Sora", "Then I'll go with that.") from _call_message_3624
    jump am1022
label cd1051:
    call phone_after_menu from _call_phone_after_menu_429
    call message_start("[name]", "I'm taking a walk.") from _call_message_start_457
    call message("Sora", "You didn't get far, did you?") from _call_message_3625
    call message("Sora", "Don't take too long..") from _call_message_3626
    call message("Sora", "You don't know what might happen.") from _call_message_3627
    call message("Sora", "I should make you a switch or something") from _call_message_3628
    call message("Sora", "that you can press when you're in danger.") from _call_message_3629
    jump am1022
label am1022:
    call message("Sora", "[name]") from _call_message_3630
    call message("Sora", "I...") from _call_message_3631
    call message("Sora", "found out where my brother could be.") from _call_message_3632
    call message("Sora", "It's...") from _call_message_3633
    call message("Sora", "the place where we used to live as children.") from _call_message_3634
    call screen phone_reply("I knew it...", "cd1054", "Isn't that place...where your trauma came from?", "cd1055")
label cd1054:
    call phone_after_menu from _call_phone_after_menu_430
    call message_start("[name]", "I knew it...") from _call_message_start_458
    jump am1023
label cd1055:
    call phone_after_menu from _call_phone_after_menu_431
    call message_start("[name]"," Isn't that place...where your trauma came from?") from _call_message_start_459
    call message("Sora", "That's the place") from _call_message_3635
    call message("Sora", "where tons of things I'd hate to remember took place...") from _call_message_3636
    call message("Sora", "But that's also the place") from _call_message_3637
    call message("Sora", "where most of my memories") from _call_message_3638
    call message("Sora", "with my brother are from.") from _call_message_3639
    jump am1023
label am1023:
    call message("Sora", "The C&R told me") from _call_message_3640
    call message("Sora", "that there are vicious-looking men") from _call_message_3641
    call message("Sora", "standing guard outside the house.") from _call_message_3642
    call screen phone_reply("Then maybe Zero's in there!", "cd1056", "You should take a break in between...! Are you feeling okay?", "cd1057")
label cd1056:
    call phone_after_menu from _call_phone_after_menu_432
    call message_start("[name]", "Then maybe Zero's in there!") from _call_message_start_460
    call message("Sora", "Maybe.") from _call_message_3643
    jump am1024
label cd1057:
    call phone_after_menu from _call_phone_after_menu_433
    call message_start("[name]", "You should take a break in between...! Are you feeling okay?") from _call_message_start_461
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "Yes.") from _call_message_3644
    call message("Sora", "I can do this") from _call_message_3645
    call message("Sora", "because you're with me.") from _call_message_3646
    jump am1024
label am1024:
    call message("SYSTEM", "Yoshiro has entered the chatroom.") from _call_message_3647
    call message("Sora", "Oh") from _call_message_3648
    call message("Yoshiro", "Sora!") from _call_message_3649
    call message("Yoshiro", "[name]...") from _call_message_3650
    call message("Sora", "Yoshiro Kim...") from _call_message_3651
    call message("Yoshiro", "You can just call me Yoshiro...") from _call_message_3652
    call message_img("Yoshiro", "I'm so glad you found him!", "images/smile.png") from _call_message_img_228
    call message("Yoshiro", "Is Zero safe?") from _call_message_3653
    call message("Sora", "We don't know...") from _call_message_3654
    call message("Sora", "not yet.") from _call_message_3655
    call message("Sora", "But") from _call_message_3656
    call message("Sora", "he might be in danger.") from _call_message_3657
    call message("Sora", "There are security guards at that place...") from _call_message_3658
    call message_img("Yoshiro", "Then what should we do!?", "images/shocked.png") from _call_message_img_229
    call message("Yoshiro", "If we call the police...") from _call_message_3659
    call message("Sora", "That won't work.") from _call_message_3660
    call message("Sora", "The police have been bribed and is already on the prime minister's side.") from _call_message_3661
    call message("Yoshiro", "Waaah") from _call_message_3662
    call message("Yoshiro", "if the justice isn't on our side") from _call_message_3663
    call message_img("Yoshiro", "how do we win? T_T", "images/sad.png") from _call_message_img_230
    call message("Sora", "There's only one way") from _call_message_3664
    call message("Sora", "That Shun person") from _call_message_3665
    call message("Sora", "is with you right now, isn't he?") from _call_message_3666
    call message("Yoshiro", "Yep") from _call_message_3667
    call message_img("Yoshiro", "want me to call for him?", "images/questioning.png") from _call_message_img_231
    call message("Sora", "Yes") from _call_message_3668
    call message("Yoshiro", "Just a sec.") from _call_message_3669
    call message("SYSTEM", "Shun has entered the chatroom.") from _call_message_3670
    call message("Shun", "Hey") from _call_message_3671
    call message("Shun", "Sora") from _call_message_3672
    call message("Shun", "And hey, [name].") from _call_message_3673
    call message("Sora", "I need a favor.") from _call_message_3674
    call message("Shun", "Oh") from _call_message_3675
    call message("Shun", "what is it?") from _call_message_3676
    call message_img("Shun", "Just tell me and I'll do it.", "images/ohyeah.png") from _call_message_img_232
    call message("Yoshiro", "Wow") from _call_message_3677
    call message("Yoshiro", "it's so cool to see you two talking.") from _call_message_3678
    call message("Sora", "I'm going to publish an article as the main news article on a web portal.") from _call_message_3679
    call message_img("Yoshiro", "What?!??!", "images/shocked.png") from _call_message_img_233
    call message_img("Shun", "Seriously?!", "images/shocked.png") from _call_message_img_234
    call message("Sora", "I need you to be in the video that will be on the news") from _call_message_3680
    call message("Sora", "and read some scripts.") from _call_message_3681
    call message("Shun", "What????") from _call_message_3682
    call message("Shun", "You want me to be the announcer?") from _call_message_3683
    call message("Sora", "If you put it that way...") from _call_message_3684
    call message("Sora", "The C&R was against revealing every detail.") from _call_message_3685
    call message("Sora", "I'm going to reveal me and my brother's childhood") from _call_message_3686
    call message("Sora", "and where he seems to be right now.") from _call_message_3687
    call message("Shun", "Ah") from _call_message_3688
    call screen phone_reply("The public power would have to step in once this becomes an issue!", "cd1058", "Are you sure you're okay with this?", "cd1059")
label cd1058:
    call phone_after_menu from _call_phone_after_menu_434
    call message_start("[name]", "The public power would have to step in once this becomes an issue!") from _call_message_start_462
    call message("Yoshiro", "It will!") from _call_message_3689
    call message("Shun", "Now I get it.") from _call_message_3690
    jump am1025
label cd1059:
    call phone_after_menu from _call_phone_after_menu_435
    call message_start("[name]", "Are you sure you're okay with this?") from _call_message_start_463
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "This is the only way...") from _call_message_3691
    call message("Yoshiro", "Sora... Zero...") from _call_message_3692
    call message("Yoshiro", "Both of you will be on millions of lips") from _call_message_3693
    call message("Yoshiro", "in a mere day....") from _call_message_3694
    call message("Shun", "And don't forget the prime minister too...") from _call_message_3695
    jump am1025
label am1025:
    call message("Shun", "But...by publishing this article") from _call_message_3696
    call message("Shun", "don't you mean that you'll publish it by hacking?") from _call_message_3697
    call message("Sora", "Yes.") from _call_message_3698
    call screen phone_reply("Will people trust us...?", "cd1060", "Is there really no other way to do this in the usual way...?", "cd1061")
label cd1060:
    call phone_after_menu from _call_phone_after_menu_436
    call message_start("[name]", "Will people trust us...?") from _call_message_start_464
    jump am1026
label cd1061:
    call phone_after_menu from _call_phone_after_menu_437
    call message_start("[name]", "Is there really no other way to do this in the usual way...?") from _call_message_start_465
    call message("Sora", "There is...but the side effect will be bigger.") from _call_message_3699
    call message("Sora", "Our enemies will figure out what happened right away.") from _call_message_3700
    call message("Shun", "Once the article is up by hacking,") from _call_message_3701
    call message("Shun", "won't people be suspicious whether the article is true?") from _call_message_3702
    jump am1026
label am1026:
    call message("Sora", "Don't worry.") from _call_message_3703
    call message("Sora", "I'll make it look like a technical problem.") from _call_message_3704
    call message("Shun", "Aha...") from _call_message_3705
    call message("Yoshiro", "Whoa - Sora!") from _call_message_3706
    call message_img("Yoshiro", "You're really smart.", "images/expecting.png") from _call_message_img_235
    call message("Shun", "Man") from _call_message_3707
    call message("Shun", "your family got some brains.") from _call_message_3708
    call message("Yoshiro", "His father is the prime minister, after all.") from _call_message_3709
    call message("Sora", "...Though he'll soon be a criminal.") from _call_message_3710
    call message_img("Yoshiro", "Sorry Sora.", "images/shocked.png") from _call_message_img_236
    call message("Sora", "You didn't say anything wrong.") from _call_message_3711
    call screen phone_reply("Shun, do you think you can do this to reveal the twins' secret...?", "cd1062", "I'm worried about the plan backfiring....", "cd1063")
label cd1062:
    call phone_after_menu from _call_phone_after_menu_438
    call message_start("[name]", "Shun, do you think you can do this to reveal the twins' secret...?") from _call_message_start_466
    call message("Shun", "I must.") from _call_message_3712
    call message("Shun", "This is a right thing to do!") from _call_message_3713
    jump am1027
label cd1063:
    call phone_after_menu from _call_phone_after_menu_439
    call message_start("[name]", "I'm worried about the plan backfiring....") from _call_message_start_467
    call message("Sora", "I'll make sure there's no mistake.") from _call_message_3714
    call message("Sora", "This is the only way....") from _call_message_3715
    call message("Shun", "You can leave the announcement to me!") from _call_message_3716
    jump am1027
label am1027:
    call message("Shun", "I got the best vocal ability in the RFA.") from _call_message_3717
    call message("Shun", "I'll make the best announcer.") from _call_message_3718
    call message("Shun", "I'll gladly show my face...") from _call_message_3719
    call message("Shun", "for my friend.") from _call_message_3720
    call message("Sora", "Okay.") from _call_message_3721
    call message("Yoshiro"," But Shun won't be in danger after the news, will he?") from _call_message_3722
    call message("Sora", "There will be minor threats") from _call_message_3723
    call message("Shun", "What?") from _call_message_3724
    call message("Sora", "but they'll be more people trying to shield him.") from _call_message_3725
    call message("Sora", "According to the analysis by the C&R advertisement department.") from _call_message_3726
    call message("Yoshiro", "I feel like") from _call_message_3727
    call message("Yoshiro", "we're invincible") from _call_message_3728
    call message("Yoshiro", "with Ryou's power") from _call_message_3729
    call message("Yoshiro", "and your power together.") from _call_message_3730
    call message("Shun", "Agreed...!") from _call_message_3731
    call message("Shun", "I would hate to have both of you as my enemies.") from _call_message_3732
    call screen phone_reply("And I'll make sure there will be peace at the RFA - !", "cd1064", "So please be good to Sora lolol", "cd1065")
label cd1064:
    call phone_after_menu from _call_phone_after_menu_440
    call message_start("[name]", "And I'll make sure there will be peace at the RFA - !") from _call_message_start_468
    call message("Shun", "Looks like you have an important role here, [name].") from _call_message_3733
    jump am1028
label cd1065:
    call phone_after_menu from _call_phone_after_menu_441
    call message_start("[name]", "So please be good to Sora lolol") from _call_message_start_469
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Shun", "I think he's a good person.") from _call_message_3734
    call message("Yoshiro", "Yeah...") from _call_message_3735
    call message("Yoshiro", "feels totally different from Zero lolol") from _call_message_3736
    call message("Shun", "Which in another words mean that...") from _call_message_3737
    call message("Yoshiro", "Ooops") from _call_message_3738
    call message("Shun", ".....Let's just leave it at that. We're busy here.") from _call_message_3739
    jump am1028
label am1028:
    call message("Shun", "In that case") from _call_message_3740
    call message("Shun", "when's the date?") from _call_message_3741
    call message("Sora", "The article will be released tomorrow at noon.") from _call_message_3742
    call message("Sora", "So we'll start filming around 8 tomorrow.") from _call_message_3743
    call message_img("Shun", "What?", "images/shocked.png") from _call_message_img_237
    call message("Shun", "There's not much time.") from _call_message_3744
    call message("Sora", "Chief assistant Sayaka Kang will give you the scripts before sunrise.") from _call_message_3745
    call message("Shun", "Ok....") from _call_message_3746
    call message("Yoshiro", "What a title for her lol") from _call_message_3747
    call message("Shun", "I should rehearse early in the morning.") from _call_message_3748
    call message("Sora", "Now") from _call_message_3749
    call message("Sora", "[name]") from _call_message_3750
    call message("Sora", "I should return to hacking.") from _call_message_3751
    call screen phone_reply("Okay. I'll be there soon.", "cd1066", "Aren't your eyes sore?", "cd1067")
label cd1066:
    call phone_after_menu from _call_phone_after_menu_442
    call message_start("[name]", "Okay. I'll be there soon.") from _call_message_start_470
    call message("Sora", "Okay....") from _call_message_3752
    call message("Sora", "don't take too long.") from _call_message_3753
    jump am1029
label cd1067:
    call phone_after_menu from _call_phone_after_menu_443
    call message_start("[name]", "Aren't your eyes sore?") from _call_message_start_471
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Sora", "This is") from _call_message_3754
    call message("Sora", "nothing.") from _call_message_3755
    jump am1029
label am1029:
    call message("Sora", "I'll be working") from _call_message_3756
    call message("Sora", "and I'll be waiting for you.") from _call_message_3757
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_3758
    call message("Shun", "....") from _call_message_3759
    call message("Shun", "He's so coolheaded.") from _call_message_3760
    call message("Yoshiro", "He's as smart as Zero.") from _call_message_3761
    call message("Yoshiro", "But his personality is so different.") from _call_message_3762
    call message("...", "...") from _call_message_3763
    call phone_end from _call_phone_end_31
    stop music
    with Pause(1.0)
    play music "music/music24.ogg"
    show MC MC11 at my_right with dissolve
    show U U311 at my_left with dissolve
    u "Haaaa.... There's nothing that can tell me whether he's alive. But I guess this is the best I can do for now..."
    show U U32
    menu:
        "Are you worried about him?":
            u "....Yeah. I don't want him to die by my father's hands."
            u "Both of us were so desperate to stay alive since childhood."
            jump u1014
        "Thank you for your hard work, Sora.":
            u "I was lucky. It feels like God is on my side, helping me..."
            u "Or did my brother leave trails for me on purpose?"
            jump u1014
label u1014:
    show U U311
    u "I had no idea I'll succeed this fast."
    show U U310
    u "[name], ever since you came to me, ever since I decided to protect you, I feel like fate has finally shown me my path."
    show U U33
    u "I've been so tense with fear throughout my life to stay safe from father,"
    u "but now I'm in the lead of the battle to capture him. It's still incredible."
    show U U314
    u "...My brother tried to protect me from father when we were young. From mother, too."
    u "I can save him once everything works out. I've been stupid and weak and helpless, but now I can save him...."
    show U U33
    menu:
        "You can do anything, Sora.":
            u "...Yes, I can do anything."
            jump u1015
        "You can do anything, Ren.":
            u "Yes, I can do anything...with you."
            jump u1015
label u1015:
    show U U310
    u "We got a grand operation tomorrow, but I've never felt so free..."
    show U U38
    u "[name], you're my angel."
    show U U33
    u "Tomorrow, I'll finally overcome my limits. I'll push my past behind me."
    show U U38
    u "And we will be happy. We'll be happy in days to come... I can feel it."
    stop music
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg25 with dissolve
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "Hello? It's me. I called as I couldn't find you."
    menu:
        "I'm in the kitchen!":
            u "I see. The minute you answered I heard your voice."
            u "Haha. Cute. Am I not cute calling you like this?"
            jump u1016
        "It's a secret!":
            u "Um... I hear something in the kitchen... should I pretend I didn't hear anything?"
            u "Sorry, you said it's a secret but I got the hint already."
            jump u1016
label u1016:
    u "What are you doing in the kitchen?"
    u "Oh, are you hungry?"
    menu:
        "I thought of showing you what I got. Just for you, Sora.":
            u "Really? You're going to cook for me?"
            u "Oh my god... my heart's beating faster..."
            u "It's the first time someone ever cooked for me."
            jump u1017
        "Yeah. I was a bit hungry.... Aren't you hungry, Sora?":
            u "I was too focused that I didn't know."
            u "I rarely ate when I work so..."
            u "Oh... listening to you, I can hear my tummy growl."
            jump u1017
label u1017:
    u "Thinking of which, I don't think I've cooked for even though I always said I will."
    u "I always had that in my mind and... I didn't even think about cooking when we came here."
    u "Oh, I mean, we've had a meal together since we were busy and all...!"
    u "This is not right..."
    menu:
        "Let make something and enjoy it together!":
            u "I was waiting for you to say that. I was successful, wasn't I? Hehe."
            jump u1018
        "Sora, it's your fault.":
            u "... I'm sorry..."
            u "...is not what I was planning to say! I'll go there right now, let's cook together!"
            jump u1018
label u1018:
    u "...Since I like cooking, I thought of cooking for every day if it's possible."
    u "It would be enough for me to watch you eating what I've made."
    u "I'll go to the kitchen as soon as I wrap up here."
    u "Let's decide what we want to eat together and make it."
    u "I'm so happy just even thinking of it... Talking to you about something like this..."
    u "There would be a lot of things to talk about like this, right?"
    u "Would there be a day where we would fight?"
    u "...Since I really love you, I think I'll be happy even fighting you."
    u "Oh. I almost sidetracked again. I'll head your way. See you soon... my love."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    call day11 from _call_day11
