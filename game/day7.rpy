label day7:
    $good_end = 0
    $bad_end = 0
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 7" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "00:58" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music14.ogg"
    scene bg10 with dissolve
    show U U110 with dissolve
    ray "...Our lips...touched..."
    ray "Haah... That felt good..."
    ray "...I miss you, [name]."
    stop music
    hide U with dissolve
    scene black with dissolve
    play music "music/music3.ogg"
    scene bg8 with dissolve
    show S S11 at my_left with dissolve
    show VW VW2 at my_right with dissolve
    vw "Hey...are you into politics these days? Why do you keep collecting intel on politicians?"
    vw "This is related to your informant duty, right? I'm going to believe that there is a huge mission behind this."
    show S S16
    s "No!"
    show VW VW3
    vw "What?"
    show S S13
    s "Palm oil was used for these cookies..."
    show VW VW2
    vw "...Say what?"
    show S S12
    s "Palm oil destroys the orangutan habitat for production! Ugh, I'm not having these cookies again."
    show VW VW4
    vw "Hey! Don't you know what your trash can is for? Why would you fling them on the floor?!"
    show S S18
    s "The orangutans are endangered species! You know nothing, Hiroki - !"
    show S S16
    show VW VW1
    play sound "music/dinggong.ogg"
    "(Ding-dong)"
    s "Huh? Someone's at the door."
    show VW VW3
    vw "...You actually have a bell?"
    show S S19
    s "Yep. I put one out there when I moved in."
    show VW VW2
    vw "Your place is more ordinary than I thought... And I thought you'd have something like an auto-reply system."
    show S S12
    s "Who could it be? I don't have any package to come..."
    hide S with dissolve
    hide VW with dissolve
    scene black with dissolve
    stop music
    scene bg8 with dissolve
    play music "music/music2.ogg"
    show S S212 at my_left with dissolve
    s "...Hiroki, I think you should leave. Use the back door."
    show VW VW3 at my_right with dissolve
    vw "Huh? Who is it?"
    s "...I'll fill you in later."
    show VW VW2
    vw "You're hiding again... Is this your personal acquaintance again?"
    s "Yep. The government official is here to collect delayed taxes."
    show VW VW6
    vw "...Seriously?"
    show VW VW3
    vw "Hey, I know that changing alias is piece of cake for you, but do you mind paying your taxes on time?"
    vw "Is this concerning your cars?"
    show S S215
    s "Out through the back door you go, Madam. My guest is not very patient."
    vw "...I'm going out for a drive. Get some work done after your guest is gone."
    show VW VW6
    vw "Seriously, if there's even the least amount of conscience in you, you gotta make at least a particle of the job done. Alright?"
    show VW VW2
    vw "I don't care if you type with your toes. Just get it done!"
    s "I wanna go for a drive, too...."
    show VW VW3
    vw "Why would you envy and stare at a person who's stepping out to give you some room...?"
    hide VW with dissolve
    show S S212
    s "...."
    play sound "music/dooropen.ogg"
    "(Door opened)"
    show V V18 at my_right with dissolve
    v "Levan... Huff."
    show S S210
    s "K...what's going on? Did you run...? Is there something wrong?"
    show V V17
    v "I was worried about you. The prime minister wanted to see me the other day...so I was wondering how you were doing."
    show S S214
    s "No need to worry about it. I've been searching all kinds of stuff because of that."
    show V V111
    v "It must be an ordeal for you...to research about your own father."
    show S S210
    s "Ordeal?"
    show V V19
    v "Considering what you have gone through..."
    show S S215
    s "Oh.... Well, it's no biggie..."
    show S S212
    s "But what am I going to do if he tries to approach Sora? That's the only thing that concerns me... That would be worse of an ordeal for me..."
    v "Levan..."
    show S S214
    s "...I know. I shouldn't care about him anymore."
    show S S212
    s "I chose to part with him and become an informant..."
    s "I know I should be ashamed of myself for saying this... But...I really wish he'd be safe."
    show S S211
    s "No, he must be safe! He must be safer than me. He must be happier than me!"
    show S S212
    s "...Just like any other person out there..."
    show V V17
    v "......."
    s "About the prime minister, I found nothing about his past last evening. Looks like he purged everything."
    s "I could collect almost nothing..."
    show S S214
    s "My guess is that...he's trying to destroy everything related to that filthy past of his before the presidential election."
    show S S212
    s "And are we included on his hit list?"
    show V V19
    v "...Levan."
    s "I think I need to do more research...but I'm going to need some time. But I will protect Sora no matter what it takes."
    show S S211
    s "K, you will keep protecting us...even if the prime minister contacts you, right?"
    v "..."
    show S S216
    s "Maybe that was too obvious."
    show S S215
    s "K, your face is heavy with concern. But we don't know why he contacted you, so let's not get hopeless just yet."
    s "Perhaps he's really interested in your works and nothing more. I hope that's the case. Then things will be easier."
    show S S212
    s "...But if things go wrong, you or the rest of the RFA can get in trouble because of me. He might be a threat to everyone if he finds out that his illegitimate son is in the RFA."
    s "I know better than anyone else what that man is like..."
    v "Levan..."
    s "So I've been thinking what would be the best option for me...what would be the best for everyone..."
    show S S214
    s "....If he's really looking for me to bury his past...."
    show S S212
    s "Then I'll kick myself out of the RFA and begone from this place."
    s "I'll make sure to destroy everything that can lead to me, of course."
    s "...I know that will make me no different from my father...but it's for the sake of the RFA."
    s "You suggested me to be an informant in the first place so that I can hide from my father's reach. I didn't think I would actually get to make use of that."
    show V V13
    v "Let's...give more thought about that. Once this dilemma with your father is settled, there's no need for you to live as an informant any longer..."
    show S S215
    s "I would have done that if he were an easy opponent. Just imagine how happy I would be if that's the case. I might get to meet Sora again."
    show S S212
    s "But...that man...is a demon, K."
    s "There was this reporter that wrote about his illegitimate sons...and now the said reporter is missing. That man is probably doing the purge behind everyone's back."
    s "I'm sure he has an army of cleaners to do the job for him. I can't fight him...by myself."
    s "It's best to avoid him until I'm ready. If he's really after me, then I should disappear from the RFA."
    show S S214
    s "And...if things get serious, I'll end my ties with you. That way I can protect you."
    show V V14
    v "No, don't do that, Levan."
    show S S215
    s "You gave me a new life...but I'm always a burden. Sorry about that. But still..."
    show S S212
    s "In case that's what happens... Please take care of Sora for me, K. Please."
    show V V118
    v "Levan, don't say that. Really... I..."
    show S S210
    s "Huh...? K, are you alright?"
    show V V17
    v "....I might be more hopeless than I thought."
    show S S211
    s "K..why would you say that?"
    show V V19
    v "I'm the one who should be sorry. I'm helpless... I couldn't give you enough light..."
    s "What are you talking about, K? What do you mean, you couldn't give me enough light...? You're here because you were worried about me."
    s "I get it now that you're having such a hard time."
    show S S212
    s "To be honest...I didn't get everything you did, and I've been plotting several theories these days."
    s "I didn't get that investigation you asked me to do months ago...and I didn't get why I couldn't contact you sometimes."
    show S S215
    s "But now that I see you here...I get how much you care for me. I get that you care for everyone in the RFA, not just me..."
    show S S212
    s "The daffodil might have withered already like Ryou said. But just keep in mind that I'm here for you."
    show S S215
    s "You were there for me when I was having the most difficult time of my life. I'll...do the same."
    show V V119
    v "You don't have to do that for me...Levan."
    show S S211
    s "Why do you keep saying that...?"
    show V V13
    v "...Take this."
    show S S210
    s "Huh? What's this?"
    show V V110
    v "It's a collection of the data I've collected in the past."
    v "If you base your search on that...you might pick up something new. You'll see what's inside...once you access it."
    show S S212
    s "...Got it."
    show S S211
    s "K...I know you're having a difficult time...but I hope you remember what a fabulous person you are."
    show S S215
    s "You gave me a life...and I'm living one...enjoying everything there is for me to enjoy."
    show S S212
    s "It doesn't change the fact that you saved me...even when my friends or environment changes."
    show S S215
    s "I'm always grateful to you... Thanks for the data, though I don't know what this is."
    show V V119
    v "..."
    show V V19
    v "...I'll leave you in peace now."
    hide V with dissolve
    hide S with dissolve
    stop music
    scene black with dissolve
    show text "03:01" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "Oh, no... She picked up..."
    ray "...Wha...what do I do?"
    ray "Uh...hi? Wait, no. Wrong word... Hello...?"
    ray "Sorry...I started thinking about you suddenly, and I called you by accident while playing with my phone... Sorry."
    ray "I couldn't sleep...so I was taking pictures with my phone."
    ray "I like taking pictures..."
    menu:
        "Wow! Could you show them to me?":
            ray "Would you like to see? But I don't think my pictures are good enough to show you...."
            ray "I collect pictures of flowers and trees. So I can get a sense of time around me."
            ray "And with my pictures, it feels like the passing time has become mine..."
            ray "I don't want to lose even a second of the time I spend with you."
            jump u71
        "Ren, you used to take pictures?":
            ray "Yeah. I learned a little...when I was young. So whenever I..."
            ray "Uh...no. I'm not supposed to talk about this. Let's talk about something else. I'm sorry."
            jump u71
label u71:
    ray "Actually...I want to take pictures of you instead of these."
    ray "What do you think?"
    ray "Pictures are the only way for me to memorize our precious time together... So I want to fill my album with your pictures...."
    menu:
        "Let's take a selfie together!":
            ray "Huh...? Selfie? With me? I've never taken a picture of myself with someone..."
            ray "But I'm ugly. I'm so thin... I don't think the picture will turn out good with me in it."
            ray "...But I'll take it anytime...if you want to."
            jump u72
        "I want to take a picture of you!":
            ray "...Me? You will?"
            ray "I...never thought about doing that."
            ray "...But...I want to have a picture of myself...from you."
            ray "S-so then...could you tell me beforehand? I want to do my hair again and wear nice clothes."
            ray "After all, it's going to be the very first picture you'll take for me."
            jump u72
label u72:
    ray "Even the thought is making my heart warm..."
    ray "Where should we take it?"
    menu:
        "At the garden!":
            ray "Oh, that's right. That'll be perfect. We can take one with the flowers."
            ray "It would be even better...if we can take it with the lily of the valley."
            ray "I'll work much harder. Once I wrap up this task well...I'll be able to stay with you as long as I can."
            jump u73
        "What about your intelligence room? It'll make a really natural and realistic picture!":
            ray "Uh....but don't you think the picture would turn out too dark? This place is tiny and dark... The picture will look scary...."
            ray "The background isn't pretty, either... There's nothing but a bunch of computers and cords here. It'll be no fun."
            ray "But we can take it here...if you want to."
            jump u73
label u73:
    ray "Now that we're talking to each other...I miss you even more...."
    ray "I've never felt this way before. You will stay with me forever...won't you?"
    ray "...Oh...my savior's calling for me. I gotta go now."
    ray "Sleep tight...and have a sweet dream."
    ray "I wish you only happy dreams."
    ray "Bye..."
    play sound "music/beep.ogg"
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "06:17" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2.ogg"
    scene bg9 with dissolve
    show V V11 with dissolve
    v "I would more than like to tell you all the truth...."
    show V V19
    v "But then I would never be able to save Reina...."
    show V V17
    v "Reina, why did you have to make Sora part of this...?"
    v "Why did you have to make him like that...?"
    show V V19
    v "I can never forget what his eyes looked like.... I'm sure they can't be more cornered than they already are..."
    v "Is it me who made him like that...? Or is it her?"
    show V V17
    v "Perhaps I'm no different from her."
    v "Then am I also an accomplice to this?"
    show V V19
    v "We promised to protect the twins... But now that promise is...."
    show V V13
    v "Reina....I still hold you close to my heart. However...."
    show V V19
    v "Will they be safe...by the time you raise your head again towards the light?"
    play sound "music/answer.ogg"
    "(Phone vibrating...)"
    show V V14
    v "...This number is a stranger. Who could it be?"
    show V V110
    play sound "music/beep.ogg"
    v "...Jurou Kim speaking. Yes, yes. Oh..."
    show V V11
    v "It's you, prime minister."
    show V V110
    v "...Yes, my promise is still valid. Why don't you visit my workplace this evening if the time allows it?"
    v "Yes, I'll see you there...."
    play sound "music/beep.ogg"
    "(...)"
    show V V17
    v "....Just as I thought, he's a bit persistent. I'm sure this is no coincidence."
    show V V13
    v "...Is the worst-case scenario coming true?"
    show V V12
    v "I must see him and find out what he wants."
    stop music
    hide V with dissolve
    scene black with dissolve
    show text "08:43" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music18.ogg"
    call phone_start from _call_phone_start_25
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_389
    call message("Ren", "[name]") from _call_message_3150
    call message("Ren", "I'm back!") from _call_message_3151
    call message("Ren", "And I wanted to log in earlier") from _call_message_3152
    call message("Ren", "but I thought maybe you'd be tired of seeing me") from _call_message_3153
    call message("Ren", "since we met last evening...") from _call_message_3154
    call message("Ren", "So I've been waiting....") from _call_message_3155
    call message("Ren", "But now...") from _call_message_3156
    call message_img("Ren", "I don't think I can wait longer....", "images/cg18.png") from _call_message_img_205
    call message("Ren", "The clouds were so pretty") from _call_message_3157
    call message("Ren", "so I took a picture.") from _call_message_3158
    call screen phone_reply("Remember that we kissed the other day?", "cd71", "I don't want to talk to you", "cd72")
label cd71:
    call phone_after_menu from _call_phone_after_menu_366
    call message_start("[name]", "Remember that we kissed the other day?") from _call_message_start_390
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Of course...") from _call_message_3159
    call message("Ren", "how can I forget?") from _call_message_3160
    call message("Ren", "Actually") from _call_message_3161
    call message("Ren", "I wanted to talk to you about that....") from _call_message_3162
    call message("Ren", "Listen...") from _call_message_3163
    jump am71
label cd72:
    call phone_after_menu from _call_phone_after_menu_367
    call message_start("[name]", "I don't want to talk to you.") from _call_message_start_391
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I'm sorry...") from _call_message_3164
    call message("Ren", "I made a bunch of mistakes, didn't I?") from _call_message_3165
    call message("Ren", "In that case") from _call_message_3166
    call message("Ren", "I'll be really quick and log out.") from _call_message_3167
    call message("Ren", "Uh.....") from _call_message_3168
    call message("Ren", "[name]") from _call_message_3169
    call message("Ren", "About the other day....") from _call_message_3170
    call message("Ren", "is it okay") from _call_message_3171
    call message("Ren", "to talk about.....") from _call_message_3172
    call message("Ren", "what you did to me?") from _call_message_3173
    call message("Ren", "....") from _call_message_3174
    jump am71
label am71:
    call message("Ren", "I've been thinking about that....") from _call_message_3175
    call message("Ren", "Can I...") from _call_message_3176
    call message("Ren", "[name]") from _call_message_3177
    call message("Ren", "Can I think of it") from _call_message_3178
    call message("Ren", "as a token that you don't hate me...?") from _call_message_3179
    call screen phone_reply("Of course I don't hate you! I really like you!", "cd73", "No, it means nothing....", "cd74")
label cd73:
    call phone_after_menu from _call_phone_after_menu_368
    call message_start("[name]", "Of course I don't hate you! I really like you!") from _call_message_start_392
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Ah...!") from _call_message_3180
    call message_img("Ren", "I'm so so so happy!", "images/ohyeah.png") from _call_message_img_206
    call message("Ren", "Me too.... I also like you.") from _call_message_3181
    call message_img("Ren", "I like you so much... I can't describe how much I like you.", "images/wink.png") from _call_message_img_207
    jump am72
label cd74:
    call phone_after_menu from _call_phone_after_menu_369
    call message_start("[name]", "No, it means nothing....") from _call_message_start_393
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "....") from _call_message_3182
    call message("Ren", "Ah....") from _call_message_3183
    call message("Ren", "I knew it.....") from _call_message_3184
    call message("Ren", "That was close. I almost misunderstood you.") from _call_message_3185
    call message("Ren", "...") from _call_message_3186
    call message("Ren", "...You don't have to like me.") from _call_message_3187
    call message("Ren", "As long as you don't hate me....") from _call_message_3188
    call message("Ren", "And about what we did....") from _call_message_3189
    jump am72
label am72:
    call message("Ren", "I heard somewhere") from _call_message_3190
    call message("Ren", "that you're not supposed to do that") from _call_message_3191
    call message("Ren", "unless you're doing it to a person you really like.") from _call_message_3192
    call message("Ren", "Oh") from _call_message_3193
    call message("Ren", "Wait, no.") from _call_message_3194
    call message_img("Ren", "I'm not supposed to talk about it anymore", "images/sad.png") from _call_message_img_208
    call message("Ren", "Please pretend you didn't read that.") from _call_message_3195
    call screen phone_reply3("I want to kiss you again....", "cd75", "I miss you. Are you busy?", "cd76", "There's no future for us if we like each other in this circumstance.", "cd77")
label cd75:
    call phone_after_menu from _call_phone_after_menu_370
    call message_start("[name]", "I want to kiss you again....") from _call_message_start_394
    call message("Ren", "...") from _call_message_3196
    call message("Ren", "...") from _call_message_3197
    call message("Ren", "......") from _call_message_3198
    call message("Ren", "............") from _call_message_3199
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Actually.... I also want to") from _call_message_3200
    call message("Ren", "do it again with you...") from _call_message_3201
    jump am73
label cd76:
    call phone_after_menu from _call_phone_after_menu_371
    call message_start("[name]", "I miss you. Are you busy?") from _call_message_start_395
    call message("Ren", "I miss you too.....") from _call_message_3202
    call message("Ren", "I wish my work is done quickly.") from _call_message_3203
    call message("Ren", "I want to stay...") from _call_message_3204
    call message("Ren", "with you.") from _call_message_3205
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    jump am73
label cd77:
    call phone_after_menu from _call_phone_after_menu_372
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "....Do you think there's no future for me") from _call_message_3206
    call message("Ren", "no matter how much I struggle...?") from _call_message_3207
    call message("Ren", "Do you think...") from _call_message_3208
    call message("Ren", "I have to live like this") from _call_message_3209
    call message("Ren", "no matter how much I struggle...?") from _call_message_3210
    call message("Ren", "I feel") from _call_message_3211
    call message("Ren", "little depressed....") from _call_message_3212
    call message("Ren", "But") from _call_message_3213
    call message("Ren", "[name]") from _call_message_3214
    call message("Ren", "As long as you don't hate me") from _call_message_3215
    call message("Ren", "...") from _call_message_3216
    call message_img("Ren", "that's more than enough for me to keep living.", "images/smile.png") from _call_message_img_209
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    jump am73
label am73:
    call screen phone_reply("First let's try to spend more time together.", "cd78", "....Look where we are right now. We're both trapped in here.", "cd79")
label cd78:
    call phone_after_menu from _call_phone_after_menu_373
    call message_start("[name]", "First let's try to spend more time together.") from _call_message_start_396
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Yeah!!!!") from _call_message_3217
    call message("Ren", "Let's do that!!") from _call_message_3218
    call message_img("Ren", "I'll make sure to do that.", "images/ohyeah.png") from _call_message_img_210
    call message("Ren", "When I'm with you") from _call_message_3219
    call message_img("Ren", "everything makes me happy.", "images/wink.png") from _call_message_img_211
    call message("Ren", "When I'm with you") from _call_message_3220
    call message("Ren", "I feel like") from _call_message_3221
    call message("Ren", "I'm in a warm paradise...") from _call_message_3222
    jump am74
label cd79:
    call phone_after_menu from _call_phone_after_menu_374
    call message_start("[name]", "....Look where we are right now. We're both trapped in here.") from _call_message_start_397
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "...We're not trapped.") from _call_message_3223
    call message("Ren", "We will be happy in this place.") from _call_message_3224
    call message("Ren", "....") from _call_message_3225
    call message("Ren", "But") from _call_message_3226
    call message("Ren", "[name]") from _call_message_3227
    call message("Ren", "if you think that we're trapped here") from _call_message_3228
    call message_img("Ren", "...I feel sad.", "images/sad.png") from _call_message_img_212
    call message("Ren", "I want to make you happier in this place....") from _call_message_3229
    call message("Ren", "But I'm not good enough, right? That must be why you think like that....") from _call_message_3230
    call message("Ren", "I'm so mad at myself...") from _call_message_3231
    jump am74
label am74:
    call message("Ren", "I wish") from _call_message_3232
    call message("Ren", "I could finish my job fast") from _call_message_3233
    call message_img("Ren", "and go see you...", "images/smile.png") from _call_message_img_213
    call screen phone_reply("Please stay positive, even if you're having a hard time!", "cd710", "Do you think our feelings matter...at a place like this?", "cd711")
label cd710:
    call phone_after_menu from _call_phone_after_menu_375
    call message_start("[name]", "Please stay positive, even if you're having a hard time!") from _call_message_start_398
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Ok.") from _call_message_3234
    call message("Ren", "[name]...I think with you") from _call_message_3235
    call message("Ren", "there will be a lot of happy days for me.") from _call_message_3236
    call message("Ren", "I'm having such happy thoughts.") from _call_message_3237
    call message_img("Ren", "Hehe.", "images/wink.png") from _call_message_img_214
    jump am75
label cd711:
    call phone_after_menu from _call_phone_after_menu_376
    call message_start("[name]", "Do you think our feelings matter...at a place like this?") from _call_message_start_399
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "They might not matter to other people") from _call_message_3238
    call message("Ren", "but to me") from _call_message_3239
    call message("Ren", "what matters the most") from _call_message_3240
    call message("Ren", "is what you think") from _call_message_3241
    call message_img("Ren", "and how you feel....", "images/questioning.png") from _call_message_img_215
    jump am75
label am75:
    call message("Ren", "Uh") from _call_message_3242
    call message("Ren", "the notification's just in") from _call_message_3243
    call message("Ren", "so I need to go....") from _call_message_3244
    call message("Ren", "I'm sorry...") from _call_message_3245
    call message("Ren", "Now...") from _call_message_3246
    call message_img("Ren", "I hope you spend the happiest afternoon in the world.", "images/wink.png") from _call_message_img_216
    call message("Ren", "Bye!") from _call_message_3247
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_3248
    call phone_end from _call_phone_end_26
    stop music
    scene black with dissolve
    play music "music/music15.ogg"
    scene bg10 with dissolve
    show U U110 at my_left with dissolve
    ray "....She's not going to hate me again because of what I typed, is she?"
    ray "What if I typed something that might offend her...? I'm not a good writer... I should check again."
    show U U111
    ray "No, no. I should concentrate."
    show U U13
    ray "Ren...Get it together, Ren.... The savior will be so mad if you don't, Ren..."
    show R R33 at my_right with dissolve
    savior "...Ren."
    show U U15
    ray "Ack!"
    ray "My savior...!"
    show R R33
    savior "Why are you so surprised? You look like you're hiding something."
    show U U19
    ray "I, uh....."
    savior "You're not meeting my eyes."
    savior "I was planning to commend you if you manage to finish this job flawlessly... But why would you make me sad? I adore you so much, but you...."
    show R R32
    show U U111
    savior "I prayed for you last night in the worship chamber."
    show R R33
    savior "And...I've seen the light. The light has given me the answer."
    savior "I was told that I must stop approving you, not even a little... I was told that you will come back after you fall even deeper."
    show R R32
    savior "The only thing that can save you is your own downfall to the bottom of the abyss."
    show R R35
    savior "...But you can still survive, can't you?"
    show U U19
    ray "Yes...."
    scene black with dissolve
    show R R33
    savior "You're shivering.... Poor Ren. But I'm only trying to save you."
    show R R35
    savior "If you turn more useless than you already are...who will ever save you?"
    stop music
    hide R with dissolve
    hide U with dissolve
    scene black with dissolve
    show text "18:31" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music4.ogg"
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    stop sound
    menu:
        "Who is it?":
            whooo "I'm from the lost and found department."
            jump u74
        "(Open the door.)":
            play sound "music/dooropen.ogg"
            jump u74
label u74:
    stop sound
    believer "For eternal paradise. I'm from the lost and found department."
    believer "Is this bookmark yours...?"
    believer "It looks like handmade. So we are looking for its owner."
    hide MC
    scene black with dissolve
    "(It's the one K dropped back then. The name Souta is written on it.)"
    menu:
        "Yes, it's mine.":
            believer "I'm glad the item has returned to its owner."
            jump u75
        "I'm not sure.":
            believer "Then could you ask Mr. Ren for me?"
            believer "Right now it is forbidden for us to see him.... Since you two see each other often, please ask him. Please contact us again if this does not belong to him."
            jump u75
label u75:
    believer "The flower, the name... It doesn't look like an average bookmark."
    believer "I hope it isn't lost anymore."
    believer "Now excuse me...."
    menu:
        "Wait a minute! Have you seen Ren?":
            believer "Uh...did you not hear?"
            jump u76
        "Have you heard nothing about Ren yet?":
            believer "Have I heard nothing...? You haven't heard yet."
            jump u76
label u76:
    scene bg7 with dissolve
    show MC MC15 with dissolve
    believer "Mr. Ren is currently undergoing his cleansing."
    believer "You must be shocked... After all, he's your comrade in the special mission."
    believer "You will get to see him as soon as his cleansing is complete."
    believer "I hope this time he can completely adapt to the Ao Me's rules..."
    believer "Since the savior is quite fond of him, I'm sure he'll be back as a good believer."
    believer "And now I must leave... For eternal paradise."
    "...."
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "20:06" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2.ogg"
    scene bg9 with dissolve
    show PR at my_left with dissolve
    pr "Are you sure I can see all these undisclosed collections? Now this must be my lucky day."
    show V V12 at my_right with dissolve
    v "Please, take your time."
    pr "I thought you were a cold person when we first met. But now you feel different."
    pr "Is it because this is taking place in your comfort zone?"
    pr "I'm aware that there is this characteristic among artists regarding their comfort zones."
    show V V13
    v "I don't believe I'm accomplished enough to be dubbed an artist... I appreciate it, though."
    pr "I would say your works are a masterpiece. And I'm sure you'd improve much more in the future."
    pr "Most of all...you've got talents. Perhaps they're from your mother."
    show V V110
    v "Do you know my mother?"
    pr "I do know that she was a world-renowned violinist. I am terribly sorry that she lost her hearings and departed this world a little too early...."
    show V V12
    v "...I appreciate it. I've earned a life as a photographer after her passing, so I think it is fate."
    pr "I've felt such a strong life within your photographs. Is it because your reverence for your mother is embedded in them?"
    show V V11
    v "....I'm flattered that you view my works in such way. However, I have yet to discover myself through my works."
    show V V12
    v "People would often tell me that my works are glamorous and beautiful... But I find myself very far from those terms."
    pr "I hope you'd carry on with your career with excellence. By the way...."
    pr "Did you say this is your atelier? It looks like this is also your residence."
    v "It is..."
    pr "Do you live alone?"
    show V V110
    v "...I used to live with my fiancée. Not anymore."
    pr "Is that woman 'Reina' by any chance?"
    show V V12
    v "..."
    pr "I also know about the death of the founder of the RFA."
    pr "If I dare say, death seems to be a common visitor in your life."
    show V V110
    v "How much do you know about me? It appears you've done a considerable amount of research."
    v "I'm sure the amount of information you can collect is vast...since you're working as a governmental official."
    pr "Haha, did I scare you?"
    pr "I hope I'm not offending you. I personally happen to be your fan, so I've done my personal research."
    pr "But borrow the power of the governmental body? I cannot do that for my personal interest."
    show V V12
    v "...I'm sure you cannot."
    pr "Of course I can't. But to be honest, I'm interested in all members of the RFA."
    pr "They hold fundraising parties and sell your works in auction and donate the money to various places that need help in our society."
    pr "It's such a wonderful association, but I just don't understand why you would keep its members secret."
    pr "May I ask you why?"
    show V V110
    v "They will inevitably be burdened with hundreds of duties if the association becomes public enough."
    show V V12
    v "I decided at least its members must remain a secret so that they can all carry on with their personal lives."
    show V V110
    v "...Did you personally investigate the rest of the members as well?"
    pr "A little. Actually, the government awards groups of people who do good for the society. And RFA happens to be on the list of to-reward for commendation."
    pr "I believe we would make note if you tell me about the rest of the members...."
    pr "But most of the details on the RFA were classified."
    v "I'm sorry, but to what extent do you know about us?"
    pr "Not much. In your case...it was easy for me to learn about your father as well."
    show V V17
    v "..."
    pr "Haha, there's no need to be so tense. I've read a single article. That is all."
    pr "I've seen a picture your father has taken with his new wife."
    pr "There is no way I'll be unaware of Ryou Han, the director of the C&R. Same could be said of his chief assistant, Sayaka Kang."
    pr "As for the rest...I'm afraid I don't know much. There is this black-haired young man and exceptionally attractive-looking musical actor.... I don't remember their names, though."
    pr "Are they all? I'm not sure if I remember all members of the RFA. Haha..."
    pr "Is there anyone missing?"
    show V V110
    v "...There is a new member, but this person works online. I don't have much details, either."
    pr "A new member...? Hmm... Is there no one else? I thought there might be one more. Or two...."
    pr "...I sincerely wish to mark everyone's name on the commendation trophy. I would love to hear their names on everyone's lips. So please..."
    v "There is no other."
    pr "Are you sure?"
    v "Yes. I'll ask everyone if they are interested in commendation and get back to you."
    pr "Could you tell me if there's anyone who has left the RFA? It doesn't matter if they do not work in the association anymore."
    show V V12
    v "...."
    pr "This is a golden opportunity to become the pride of the nation. Please think about it."
    show V V110
    v "...Very well, sir."
    stop music
    hide PR with dissolve
    hide V with dissolve
    scene black with dissolve
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "...Hello? Is that you? Is that really you?"
    ray "I'm not hallucinating, am I? ...Can you hear me? Please don't hang up!"
    ray "....Don't hang up. Please... please don't...!"
    show MC MC15
    menu:
        "Ren...?! What's going on?!":
            ray "Huff... My head... It hurts...so much..."
            ray "It's hot. I think it's going to burst. Please, leave me alone. I'm too scared..."
            ray "...I'm too weak...! I should rather be gone..."
            ray "There's no place for me in paradise... Please, just let me stay in the corner..."
            jump u77
        "Ren, where are you? I'm coming for you right now!":
            ray "No, no...! Don't...!"
            ray "You'll be in danger... I can't even protect you... I shouldn't bring you into this..."
            ray "Sniffle... I'm sorry... But please, listen to me..."
            ray "I might...lose myself. I'm feeling faint...."
            jump u77
label u77:
    ray "This is too painful... I wish everything would just go away. My past, my weakness..."
    ray "No one would torture me if I am strong... So I should make myself gone in that tiny corner... I’m too weak..."
    menu:
        "Ren, please! Snap out of it!":
            jump u78
        "Ren, you've done nothing wrong...!":
            jump u78
label u78:
    ray "Huff... I'm too scared..."
    ray "...I should rather be gone...."
    ray "That's right... Perhaps I shouldn't have breathed alive in the first place..."
    ray "I must be gone for our salvation..."
    ray "No...no! I want to stay...! I want to see you so much... I want to stay with you..."
    ray "...Paradise... Paradise...! I'll do anything for our paradise. So please...please let me stay alone in the corner..."
    ray "[name]... I miss you...."
    ray "But...I need to be gone for you to stay in the paradise..."
    ray "Please, leave me here... I'm too weak... And you...should be happy..."
    play sound "music/beep.ogg"
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "29:59" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "Hello?"
    v "[name]? Is everything okay? Tell me you're okay."
    v "I'm near that building you're locked in...but something doesn't feel right."
    menu:
        "What's the matter?":
            v "That's...something I wanted to ask you."
            v "According to Levan, the style of hacking upon our chat rooms has changed as well."
            v "I think things are taking the turn for the worse, in more than one way."
            v "Maybe...you and that boy might be in danger."
            jump u79
        "You're nearby? Are you planning to sneak in?":
            v "Yes, once given the opportunity... But right now..."
            v "The security outside the building has multiplied... And the atmosphere is quite vicious."
            v "There's no doubt something has happened in that building."
            jump u79
label u79:
    v "[name], I will get in there no matter what it takes."
    v "Please stay where you are, if possible. You shouldn't compromise your location with unnecessary movement."
    v "At least I know where you are, so please leave the rest to me."
    v "I will protect you in whatever situation."
    v "So I hope you would trust me as well."
    v "You're the only person I trust the most and maintain contact in secret. You're also the only person who knows about Levan and Sora."
    v "And the prime minister is after the both of them."
    v "In fact, he contacted me in order to figure out where they are."
    menu:
        "Don't you think you're imagining things?":
            v "No, I'm positive. The prime minister has been waiting for an opportunity to get rid of both of them since they were children."
            jump u710
        "So is that the reason why you refused his proposal for commendation?":
            v "Yes, it is."
            v "The prime minister already mentioned commendation when I met him in my studio."
            v "And he told me that he needs a list of all RFA personnel for that."
            v H"e obviously wanted to gain something about Levan."
            jump u710
        "I figured.":
            v "I see. I figured that you'd figure, though our chat rooms."
            jump u710
label u710:
    v "When I first met them, Levan was risking his all in order to protect his brother, though Sora would be unaware of this."
    v "And that has never changed."
    v "He would often tell me that he doesn't care what happens to him, as long as he can protect Sora."
    v "I wish to protect him and the brother he is so desperately trying to protect."
    v "I'm sure you have a lot of questions. I wish I could tell you everything, but..."
    v "Nonetheless, I thank you so much for keeping secrets about them."
    play sound "music/break.ogg"
    v "I believe it is a must that I see you as quickly as possible and talk about... Huh...?"
    show MC MC15
    v "[name]...?"
    v "Hello? [name], can you hear me? Hello?"
    v "...What...the matter...this...? Hello?"
    whooo "Hehehehe... How very fun."
    v "...llo?"
    stop sound
    stop music
    whooo "...I've been listening to everything here."
    whooo "You should better choose your words wisely, if you want to buy more time to stay breathing, even for a sec."
    whooo "I'm sure that's enough for you to get what's going on. Or maybe not, since you're a moron."
    whooo "Stupid toy... I'll be there soon, so you just wait and let your imaginations entertain you. Hahaha!"
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    play music "music/music12.ogg"
    scene bg9 with dissolve
    show V V17 with dissolve
    v "Levan...."
    show V V118
    v "I'm sure he's feeling complicated with the dilemma with the prime minister... And now the hacker's back...?"
    show V V17
    v "Something must have happened to Sora."
    v "...I must go back. I must find out what happened."
    stop music
    hide V with dissolve
    scene black with dissolve
    "...."
    ".........."
    play sound "music/knock.ogg"
    "(Knock knock)"
    scene bg7 with dissolve
    show MC MC14 at my_right with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    menu:
        "(Open the door.)":
            stop sound
            play sound "music/dooropen.ogg"
            "(Door opened)"
            scene black with dissolve
            show U U24 at my_left with dissolve
            whooo "There you are..."
            jump u711
        "Who is it?":
            play sound "music/knock.ogg"
            "(Knock knock)"
            play sound "music/knock.ogg"
            "(Knock knock)"
            play sound "music/knock.ogg"
            stop sound
            mc "(Open the door.)"
            play sound "music/dooropen.ogg"
            "(Door opened)"
            hide MC with dissolve
            scene black with dissolve
            whooo "What took you so long?"
            jump u711
label u711:
    scene bg7 with dissolve
    play music "music/music19.ogg"
    show MC MC12 at my_right with dissolve
    show U U21 at my_left with dissolve
    menu:
        "Ren...! I missed you so much!":
            ray_who "...What a passionate welcome."
            ray_who "I was really curious about you. But your face is so not my type."
            jump u712
        "Where have you been?":
            ray_who "I don't think I have to answer that."
            jump u712
label u712:
    show U U23
    show MC MC14
    ray_who "Hah... This room smells strange."
    show U U22
    ray_who "Is this your smell?"
    show U U26
    ray_who "It's been only a week since you've been here, and this room is already full of your smell."
    show U U24
    ray_who "He definitely had his way with the decorations. He even brought you flowers like a good boy."
    ray_who "Why can't he see that he'll end up becoming a puppet when he acts like a nice idiot?"
    menu:
        "Ren...? This isn't like you.":
            ray_who "What an airhead. I just told you if you behave like him, you'll end up a puppet like him."
            ray_who "I don't like saying things twice. Don't get on my nerves."
            jump u713
        "Ren, you don't sound like you.":
            ray_who "You're looking for that pathetic idiot. I figured."
            jump u713
label u713:
    show U U21
    ray_who "I wonder... What you were doing here?"
    show U U24
    ray_who "I suppose you've been dreaming yourself happy with that idiot."
    ray_who "Or maybe you were planning to use his naive feelings to rip off something big time."
    show U U22
    ray_who "Why do you look at me like that...?"
    show U U24
    ray_who "...Is that what you expected me to say?"
    ray_who "You look like you have so many questions you want to ask. As if I'm going to tell you."
    ray_who "Tough luck. I'm not some teacher who tells you everything about your dumb questions."
    show U U27
    ray_who "And I'm definitely not an idiot who would so desperately devote himself to you."
    show MC MC15
    mc "You have the same body and face as Ren... But you mean you're not Ren?"
    show U U22
    whooo "Didn't I tell you I'm not a teacher?..."
    show U U25
    whooo "I told you not to ask me questions!"
    show U U24
    whooo "Ugh - what an airhead. Hah... Are you intentionally trying to piss me off?"
    show U U21
    whooo "Since savior told me to treat you 'specially' nice, I'll 'specially' tell you."
    show U U22
    whooo "I'm only going to say this once, so you should listen up."
    show U U24
    whooo "Ren is my creation. Only the most pathetic and lamest part of me balled up to be him."
    u "I'm the real deal. Me, Sora. Ren is a fraud. So you will never get to see him again."
    u "Though my savior told me to do so.... Great. I ended up explaining myself so nicely to you..."
    show U U27
    u "...This is sort of annoying."
    u "You don't think you're a princess or something, do you? Haha."
    hide MC
    hide U
    scene black with dissolve
    "(...!)"
    scene black with dissolve
    u "This smell of yours has been getting on my nerves for a while now."
    u "This room, your smell, I don't like any of them. But Ren told you that he likes it, didn't he?"
    u "To be frank, I think I'm going to be sick."
    u "I don't like it at all. This room, you, everything. Everything is getting on my nerves."
    u "Don't you dare stare at me so blankly like some idiot."
    u "Now you look even dumber.... Can't you show me anything interesting with that face?"
    u "And those eyes of yours in particular really piss me off. Mind if you close them for me? Oh, maybe I should blindfold you."
    u "[name], I know it was you. You tried to get rid of me by saying all that weird stuff to Ren."
    u "Did you think you can use him like some puppet? Hahaha! What a combination of idiots."
    u "Too bad. Your time is up, princess."
    u "Can you show me a sad face? Huh? Come on, try it."
    u "How about an angry face? Hmm?? Come on, do something to entertain me."
    menu:
        "...This is not fair. Let go of me.":
            u "Hahaha! This isn't fair....?"
            u "I see you're from a happy world of fairy tales, princess..."
            jump u714
        "Okay...":
            u "Your voice...it's hideous. Don't talk to me without lowering your voice from now on."
            jump u714
label u714:
    u "Why would that bug-like someone disastrous like you? I don't get it. No, wait. I get it."
    u "You two must have comforted each other. Because you're both idiots. Geez... Now I'm really getting sick."
    u "Just what good are you? You're stupid, you're slow... You're good for nothing."
    u "What? You've got something to say? Did you just realize what a useless human being you are? No way...."
    u "You should know your place from now on... And be silent like a grave."
    u "And don't make me notice that you're breathing. You'll get on my nerves if you shiver like that!"
    u "Stop shivering. Ugh- this totally sucks. Why won't you just listen to what I say?"
    scene black with dissolve
    u "...Should I bite your head off or something?"
    scene bg7 with dissolve
    "(...!)"
    u "Don't move. Haha."
    u "Hey, that tickles..."
    u "Are you trying to push me away?"
    u "Hahaha..... Did I scare you, princess?"
    u "Too bad. I've barely begun... You shouldn't freak out with this."
    u "I wonder if the marks will stay until tomorrow. I wonder.... I should check tomorrow morning."
    u "I'm not sure if you would understand what I'm saying, but let me tell you this. You'd better start using your head and think of a way to make me not hate you."
    u "You should be an interesting toy if you want me to keep you company a lot. Right now you are no fun. Seriously, you're useless."
    u "Oh - that's it. Your face changed. Haha... Hahaha!"
    u "Show me an angry face. Huh? Come on."
    u "Try and provoke me. Make me mad. Mess with me. Come on, try it... Hmm? I'll play with you."
    u "....Ugh, this is no fun. Looks like this is the end of your usefulness."
    scene bg7 with dissolve
    show U U26 at my_left with dissolve
    show MC MC17 at my_right with dissolve
    u "I'm getting tired of you. You're no fun at all. Hah... This is so annoying. Seriously. I knew you're a nuisance."
    show U U24
    u "I'm sure I'll have some fun if you make some trouble here..."
    u "For example...beg the RFA for help. Yes, something like that... Do you mind trying?"
    show U U21
    u "Then the savior will say that you'll need cleansing, too."
    u "I'll be the one to do that for you. It's going to be so fun. Hahaha!"
    show U U27
    u "I know how to draw out the maximum pain in the process. I've done it a lot... Curious, right?"
    u "It's going to be so good. Should I tell the savior that you're a naughty girl?"
    show U U24
    u "...You're quiet. I was going to toy with you again if you start whining like an idiot."
    show U U22
    u "You're no fun - and I'm not playing with a boring toy."
    u "I'm going back. I'll be back once I get bored."
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    u "...Hey! Don't put anything in this room. This is a toy room that will keep my toy stashed up."
    u "Oh, and...make sure my toy stays put. Keep your eyes on her."
    scene black with dissolve
    stop music
    call day8 from _call_day8
