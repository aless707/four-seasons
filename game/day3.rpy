label day3:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 3" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "2 years ago..." with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene black with dissolve
    play music "music/music2.ogg"
    show V V112 at my_left with dissolve
    v "Oh, Reina. Were you waiting for me?"
    show R R11 at my_right with dissolve
    r "No, I just finished."
    show V V113
    v "How was your counseling session for today?"
    show R R12
    r "My counselor said that I've improved, thank goodness."
    show V V114
    show R R13
    v "Whew...that's good."
    r "So my therapist decided I should take less medication from now on."
    show V V113
    v "Reina...that's all thanks to your hard work."
    show V V115
    v "Oh,right.I have a good news, too."
    show R R11
    r "What is it?"
    show V V114
    v "Mr. Ryu, I mean Shun, that musical actor you like,"
    show V V115
    v "he has decided to become a member of our association."
    show R R14
    r "Wow, you mean it? That's great!"
    show R R12
    r "He had turned down our offer before, so I've given up on it..."
    show R R11
    r "Do you think he decided to join because he felt sorry for us?"
    v "I can't completely deny that, but he seems to think that joining us will influence him in a good way. It's a relief, isn't it?"
    show R R14
    r "K, your good deed led to a good result... I'm so glad!"
    show V V114
    v "Me,too."
    show V V113
    show R R11
    r "You've seen what he's like when he's acting, right?"
    show V V115
    v "Yes, I did. And I've also seen how charming his face is..."
    show R R14
    r "He's the most wanted person on our to-recruit list. He's the type of person with the image of the rising star!"
    v "Though he's not that famous yet, you have a bright future in mind for him, don't you?"
    r "Yes, of course!"
    show V V114
    show R R12
    v "Yes... He was a dazzling subject in my eyes as welll... So he'll no doubt become a superstar."
    show V V113
    show R R11
    r "I'm feeling great! I'm sure RFA will keep growing to its completion like this!"
    show V V115
    v "It must be the sign that your wish will come true, that you'll be able to help even more people."
    show R R14
    r "Yes! I'm so elated. Shun..."
    r "I think he'll make a great friend with Ryou in particular."
    hide R with dissolve
    hide V with dissolve
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "2:46" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_1
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_21
    call message_img("Ren","[name]! You're not sleeping yet!", "images/ohyeah.png") from _call_message_img_12
    call message("Ren","Aren't you sleepy? It's quite late.") from _call_message_97
    call message("Ren","What were you doing?") from _call_message_98
    call screen phone_reply("I was testing the game you gave me. ", "cd31", "Ren, why aren't you sleeping?", "cd32")
label cd31:
    call phone_after_menu from _call_phone_after_menu_20
    call message_start("[name]","I was testing the game you gave me.") from _call_message_start_22
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","You were playing till so late....?") from _call_message_99
    call message("Ren","Thanks for playing so hard.") from _call_message_100
    call message("Ren","You're playing so enthusiastically.") from _call_message_101
    call message("Ren","Does that mean you like the game I made?") from _call_message_102
    call message("Ren","I'm really glad.") from _call_message_103
    call message("Ren","I'll put my best efforts as well!") from _call_message_104
    jump am31
label cd32:
    call phone_after_menu from _call_phone_after_menu_21
    call message_start("[name]","Ren, why aren't you sleeping?") from _call_message_start_23
    call message("Ren","I thought of you while I was working...") from _call_message_105
    call message("Ren","and I was hoping that you'd be in the chatroom") from _call_message_106
    call message("Ren","I'm happy that you're really here.") from _call_message_107
    jump am31
label am31:
    call message("Ren","There was something I wanted to tell you...mind hearing me out?") from _call_message_108
    call message("Ren","You know, [name]...") from _call_message_109
    call message("Ren","There's a plant I received some time ago as a gift.") from _call_message_110
    call message("Ren","I'm normally not into the habit of growing plants and didn't really look after it.") from _call_message_111
    call message("Ren","But the plant seeds survived and sprouted.") from _call_message_112
    call message("Ren","Wanna see?") from _call_message_113
    call message_img("Ren","It's supposed to grow with a single stalk", "images/cg6.png") from _call_message_img_13
    call message("Ren","but it grew two.") from _call_message_114
    call message("Ren","I wanted to show you this as it seemed interesting.") from _call_message_115
    call message("Ren","It got on my nerves that it was growing in a small pot") from _call_message_116
    call message("Ren","then I eventually ended up looking after it at some point.") from _call_message_117
    call message("Ren","One of the stalk started to grow stronger") from _call_message_118
    call message("Ren","and the other weaker.") from _call_message_119
    call message("Ren","Even with the same amount of sunlight and water") from _call_message_120
    call message("Ren","this is how these two turned out") from _call_message_121
    call message("Ren","as if it's already destined...") from _call_message_122
    call message("Ren","which one would be weak.") from _call_message_123
    call screen phone_reply3("Wouldn't it be because only one had sunlight and the other had none?", "cd33", "It could be that one of it is like an extra giveaway, since it grew into two from one stalk.", "cd34", "I feel sad for the weaker stalk...", "cd35")
label cd33:
    call phone_after_menu from _call_phone_after_menu_22
    call message_start("[name]","Wouldn't it be because only one had sunlight and the other had none?") from _call_message_start_24
    call message("Ren","No.") from _call_message_124
    call message("Ren","The growing environment was the same.") from _call_message_125
    call message("Ren","But sadly....") from _call_message_126
    jump am32
label cd34:
    call phone_after_menu from _call_phone_after_menu_23
    call message_start("[name]","It could be that one of it is like an extra giveaway, since it grew into two from one stalk.") from _call_message_start_25
    show bh at heart_pos with dissolve
    hide bh with dissolve
    call message("Ren","No.") from _call_message_127
    call message("Ren","There's no way for that.") from _call_message_128
    call message("Ren","It's nothing like an extra giveaway.") from _call_message_129
    call message("Ren","That's sad.") from _call_message_130
    call message("Ren","Don't say such things [name]....") from _call_message_131
    jump am32
label cd35:
    call phone_after_menu from _call_phone_after_menu_24
    call message_start("[name]","I feel sad for the weaker stalk...") from _call_message_start_26
    call message("Ren","You think so too?") from _call_message_132
    call message("Ren","You're thinking the way I am.") from _call_message_133
    call message("Ren","Right.") from _call_message_134
    call message("Ren","It's sad....") from _call_message_135
    call message("Ren","The weaker stalk didn't do anything wrong.") from _call_message_136
    jump am32
label am32:
    call message("Ren","The pests keep targeting the weaker stalk...") from _call_message_137
    call message("Ren","No matter how many times I get rid of the bugs,") from _call_message_138
    call message("Ren","only the weaker stalk would wither.") from _call_message_139
    call message("Ren","Somebody told me") from _call_message_140
    call message("Ren","that I should cut off the weaker stalk to save this pot.") from _call_message_141
    call message("Ren","But you know what?") from _call_message_142
    call message("Ren","I think...") from _call_message_143
    call message("Ren","that's wrong.") from _call_message_144
    call message("Ren","I'm sure both of them grew from the same seed") from _call_message_145
    call message("Ren","what did the weaker stalk ever do wrong to suffer this damnation?") from _call_message_146
    call screen phone_reply("There's nothing you can do, it's nature's way of survival of the fittest.", "cd36", "I want to save the weaker stalk somehow.", "cd37")
label cd36:
    call phone_after_menu from _call_phone_after_menu_25
    call message_start("[name]","There's nothing you can do, it's nature's way of survival of the fittest.") from _call_message_start_27
    call message("Ren","True.") from _call_message_147
    call message("Ren","But I think it's sad to be sacrificed just because you're weak.") from _call_message_148
    call message("Ren","All kinds of living have a right to be happy.") from _call_message_149
    jump am33
label cd37:
    call phone_after_menu from _call_phone_after_menu_26
    call message_start("[name]","I want to save the weaker stalk somehow.") from _call_message_start_28
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yea. I also desperately wanted to do something.") from _call_message_150
    call message("Ren","Because it's heartbreaking to keep watching it.") from _call_message_151
    call message("Ren","I thought it was too wretched...") from _call_message_152
    call message("Ren","that the weaker always gets everything stolen and lost.") from _call_message_153
    call message("Ren","Don't you think the one that should survive is the weaker one?") from _call_message_154
    jump am33
label am33:
    call message("Ren","Just because the stronger stalk is more healthy,") from _call_message_155
    call message("Ren","sacrificing the weaker one to let the stronger one keep living") from _call_message_156
    call message("Ren","didn't make sense to me.") from _call_message_157
    call screen phone_reply4("But for flowers to blossom, the weaker one should be stripped.", "cd38", "You're right. The weaker stalk was the one that sacrificed!", "cd39", "Rescue both.", "cd310", "Throw it out and buy a new pot.", "cd311")
label cd38:
    call phone_after_menu from _call_phone_after_menu_27
    call message("[name]","But for flowers to blossom, the weaker one should be stripped.") from _call_message_158
    call message("Ren","[name]...") from _call_message_159
    call message("Ren","I'm really sad that you said that...") from _call_message_160
    call message("Ren","Are you by chance infected by RFA to think that way?") from _call_message_161
    call message("Ren","I can't let go of the weaker stalk.") from _call_message_162
    jump am34
label cd39:
    call phone_after_menu from _call_phone_after_menu_28
    call message_start("[name]","You're right. The weaker stalk was the one that sacrificed!") from _call_message_start_29
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","...Right.") from _call_message_163
    call message("Ren","I'm glad that we think the same.") from _call_message_164
    call message("Ren","I don't want to create a world") from _call_message_165
    call message("Ren","where only the takers gain from the weak.") from _call_message_166
    jump am34
label cd310:
    call phone_after_menu from _call_phone_after_menu_29
    call message_start("[name]","Rescue both.") from _call_message_start_30
    call message("Ren","Maybe the two were born") from _call_message_167
    call message("Ren","to despise each other from the start.") from _call_message_168
    call message("Ren","It might be that") from _call_message_169
    call message("Ren","they were separated") from _call_message_170
    call message("Ren","into two to steal and") from _call_message_171
    call message("Ren","attack each other") from _call_message_172
    call message("Ren","endlessly.") from _call_message_173
    call message("Ren","So that one side's nastiness") from _call_message_174
    call message("Ren","gets larger than the other and") from _call_message_175
    call message("Ren","eventually consume the other...") from _call_message_176
    jump am34
label cd311:
    call phone_after_menu from _call_phone_after_menu_30
    call message_start("[name]","Throw it out and buy a new pot.") from _call_message_start_31
    call message("Ren","Or maybe...") from _call_message_177
    call message("Ren","I thought of breaking both stalks together and bury them in the ground.") from _call_message_178
    call message("Ren"",It's not bad to start over from the start!") from _call_message_179
    call message("Ren","But I decided to grow it and see the end of it.") from _call_message_180
    jump am34
label am34:
    call message("Ren","Therefore") from _call_message_181
    call message("Ren","What I did") from _call_message_182
    call message_img("Ren","eventually", "images/happy.png") from _call_message_img_14
    call message("Ren","was to break the stronger stalk") from _call_message_183
    call message("Ren","and used it as fertilizer to the weaker one.") from _call_message_184
    call message("Ren","They were both born from the same root") from _call_message_185
    call message("Ren","but the stronger one") from _call_message_186
    call message("Ren","has been feeding off the weaker one...") from _call_message_187
    call message("Ren","I thought the villain was the stronger stalk.") from _call_message_188
    call message("Ren","The weaker one has finally gotten back") from _call_message_189
    call message("Ren","and got back everything that was stolen...^^") from _call_message_190
    call message("Ren","I hope the weaker one grows strong and fast") from _call_message_191
    call message("Ren","I hope it grows stronger and blossom pretty flowers.") from _call_message_192
    call message_img("Ren","I'll show the flowers to you once they're in full bloom ^^", "images/happy.png") from _call_message_img_15
    call message("Ren","Ah...") from _call_message_193
    call message("Ren","my tale got too long.") from _call_message_194
    call message("Ren","I feel like time flies when I'm talking to you.") from _call_message_195
    call message("Ren","I hope I didn't bore you.") from _call_message_196
    call screen phone_reply("It was fun. With hidden morals and all....", "cd312", "Yawn. I think the game you made is more interesting.", "cd313")
label cd312:
    call phone_after_menu from _call_phone_after_menu_31
    call message_start("[name]","It was fun. With hidden morals and all....") from _call_message_start_32
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Good to know that you liked it.") from _call_message_197
    call message("Ren","If you see a stronger one taking away something from the weaker...") from _call_message_198
    call message_img("Ren","think of this plant.", "images/smile.png") from _call_message_img_16
    jump am35
label cd313:
    call phone_after_menu from _call_phone_after_menu_32
    call message_start("[name]","Yawn. I think the game you made is more interesting.") from _call_message_start_33
    call message("Ren","Ah...I guess I got you really bored.") from _call_message_199
    call message("Ren","To the point that you typed the word for yawing....") from _call_message_200
    call message("Ren","You think the AIs are more fun than me?") from _call_message_201
    call message("Ren","Really...?") from _call_message_202
    call message("Ren","Really......?") from _call_message_203
    call message("Ren","....................") from _call_message_204
    call message("Ren","Sorry") from _call_message_205
    call message("Ren","I really thought it was an interesting story...") from _call_message_206
    call message("Ren","Next time I'll tell you something that you'll like....") from _call_message_207
    jump am35
label am35:
    call message_img("Ren","It's time for me to get going.", "images/smile.png") from _call_message_img_17
    call message("Ren","Good night, my princess. [name].") from _call_message_208
    call message("SYSTEM", "Ren has left the chatroom") from _call_message_209
    stop music
    call phone_end from _call_phone_end_2
    play music "music/music2chat-17.ogg"
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "You weren't sleeping yet."
    ray "I hope you weren't trying to sleep, were you?"
    menu:
        "I was just lying on the bed, staring blankly.":
            ray "Seems you can't sleep."
            ray "I called as I saw your light was turned on. Guess I did good calling you."
            jump u31
        "Ren, didn't you say you were going to work?":
            ray "Yes. You're right. I was working, and I thought of you again."
            ray "I don't know what to do. I can't get you out of my head..."
            jump u31
        "I was going to go to sleep just now.":
            ray "Oh, I see. Did I wake you with my call? If so, I'm really sorry."
            ray "I should have kept to myself and called you tomorrow."
            jump u31
label u31:
    ray "I couldn't stop myself as I wanted to listen to your voice."
    ray "It wasn't enough...by talking in the chat room with you."
    ray "I'm curious... Do you...sometimes think of me like I think of you?"
    menu:
        "I think of you almost every day.":
            ray "Really...? I'm so...happy right now."
            ray "You know, when I look at my phone, I think of you enjoying the game I gave you."
            ray "Is there something that makes you think of me when you see it? If so, my heart will feel so ticklish..."
            jump u32
        "I just think of hosting the party.":
            ray "Okay. Having the party is important. I'm glad that you're enjoying the game I gave you."
            ray "Why don't you invite a lot of people and have a grand party? It'll be fun if more guests come."
            ray "Of course, I'm leaving everything to you about the party."
            jump u32
        "I'm too busy that I don't have time to think.":
            ray "What are you so busy about...? Is it because of the game?"
            ray "Don't push yourself too hard, since the game was only made to let you have fun."
            ray "But I'm still glad that you're enjoying the game to the point that you can't think of other things..."
            jump u32
label u32:
    ray "Haa... Talking to you like this is really great."
    ray "I have headaches when I work since I think too much, but when I listen to your voice, I feel at ease."
    ray "The doctor told me to relax, and I think I relax when I talk to you."
    ray "I now have to go to work, but I want to listen to your voice nonstop."
    ray "It's a shame to end this call like this."
    ray "Um.. Can I...ask you a favor?"
    ray "Can you...tell me to cheer up? I think I'll be able to work hard if you tell me that."
    menu:
        "Think of me and cheer up, Ren!":
            ray "I will! I'll think of you and do my best."
            ray "If you have a tough time, call me... We can meet in the chat room... And I'll make some to time to visit you. I promise..."
            ray "But in order to do so, I must work hard."
            ray "Thanks a lot. I think I'm gaining all the energy I need."
            jump u33
        "Stop whining and go to work.":
            ray "Sorry for whining...."
            ray "But thanks for telling me to go to work. It helps a lot..."
            jump u33
        "I want to give you hug.":
            ray "Thanks for saying that."
            ray "I feel heart-warmed. How warm and cozy it'll be I get hugged by you..?."
            ray "Sounds so good that I can't express it... I... My heart is racing..."
            jump u33
label u33:
    ray "Oh... I have to go now as I said."
    ray "Thanks to you, I've gained enough energy to work. Thanks. Bye."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "12:22" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_2
    call message_start("SYSTEM", "Ren has entered the chatroom") from _call_message_start_34
    call message("Ren","[name]!") from _call_message_210
    call message("Ren","I logged in as soon as I caught your log-in log.") from _call_message_211
    call message_img("Ren","I'm glad we can talk like this", "images/smile.png") from _call_message_img_18
    call message("Ren","I was about to ask you") from _call_message_212
    call message("Ren","if you had lunch.") from _call_message_213
    call message("Ren","AIs talked about friendship.") from _call_message_214
    call message("Ren","The friendship between K and Ryou...") from _call_message_215
    call message("Ren","[name], do you believe in friendship?") from _call_message_216
    call message("Ren","I never had friends.") from _call_message_217
    call message("Ren","I wasn't allowed to play outside when I was young.") from _call_message_218
    call message("Ren","That's why I try to go out often these days.") from _call_message_219
    call message("Ren","I really enjoy going out to see flowers in particular.") from _call_message_220
    call screen phone_reply("You're friends with flowers ^^ ", "cd314", "Aren't you working indoors all the time?", "cd315")
label cd314:
    call phone_after_menu from _call_phone_after_menu_33
    call message("[name]","You're friends with flowers ^^") from _call_message_221
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yes! I am!") from _call_message_222
    call message("Ren","If you can call flowers your friends") from _call_message_223
    call message("Ren","I have quite many ^^") from _call_message_224
    jump am36
label cd315:
    call phone_after_menu from _call_phone_after_menu_34
    call message_start("[name]","Aren't you working indoors all the time?") from _call_message_start_35
    call message("Ren","You're right. I've been busy working these days.") from _call_message_225
    call message("Ren","I'm going to find time to go to the garden.") from _call_message_226
    call message("Ren","I want to go with you...") from _call_message_227
    call message("Ren","If it's alright late at night, I want to go with you.") from _call_message_228
    call message("Ren","We can look at the flowers") from _call_message_229
    call message("Ren","with lights lit in the garden after sunset.") from _call_message_230
    jump am36
label am36:
    call message("Ren","Oh right") from _call_message_231
    call message("Ren","I'm memorizing flower languages too.") from _call_message_232
    call message("Ren","Let me know if there's any meaning you'd like to know!") from _call_message_233
    call message("Ren","Hey, [name].") from _call_message_234
    call message("Ren","I wonder if I can be your best friend.") from _call_message_235
    call message("Ren","A friend who can share a deep wide world....") from _call_message_236
    call message_img("Ren","even deeper than K and Ryou's world!", "images/wink.png") from _call_message_img_19
    call screen phone_reply("You can! Though more time would be needed.", "cd316", "Um.. I think I feel more close with the friends inside the game.", "cd317")
label cd316:
    call phone_after_menu from _call_phone_after_menu_35
    call message_start("[name]","You can! Though more time would be needed.") from _call_message_start_36
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Of course we'll need time") from _call_message_237
    call message("Ren","but what's important is for you to wish to be here.") from _call_message_238
    call message("Ren","because I have to stay here") from _call_message_239
    jump am37
label cd317:
    call phone_after_menu from _call_phone_after_menu_36
    call message_start("[name]","Um.. I think I feel more close with the friends inside the game.") from _call_message_start_37
    call message("Ren","Ah...") from _call_message_240
    call message("Ren","it's because I don't log in as often, isn't it?") from _call_message_241
    call message("Ren","I can feel my spirit getting small...") from _call_message_242
    call message("Ren","It's sad.") from _call_message_243
    call message("Ren","Still, [name], do not forget") from _call_message_244
    call message("Ren","The AIs are all fake.") from _call_message_245
    call message("Ren","The only thing that's real and alive...") from _call_message_246
    call message("Ren","is me.") from _call_message_247
    call message("Ren","You shouldn't forget that.") from _call_message_248
    jump am37
label am37:
    call message("Ren","I just finished my meal with that important guest I told you the day before.") from _call_message_249
    call message("Ren","It would have been better if I ate with you....") from _call_message_250
    call message("Ren","Actually, that person is the one who") from _call_message_251
    call message("Ren","supervises everything about this place.") from _call_message_252
    call message("Ren","We sometimes") from _call_message_253
    call message("Ren","have meals or tea together") from _call_message_254
    call message("Ren","but it's been uncommon these days...") from _call_message_255
    call screen phone_reply("Who is it?", "cd318", "I'm jealous... ", "cd319")
label cd318:
    call phone_after_menu from _call_phone_after_menu_37
    call message_start("[name]","Who is it?") from _call_message_start_38
    call message("Ren","I can't reveal the name...") from _call_message_256
    call message("Ren","but it's someone important.") from _call_message_257
    call message("Ren","Though you're curious, could you wait a little more?") from _call_message_258
    call message("Ren","I'll tell you when the time is right.") from _call_message_259
    jump am38
label cd319:
    call phone_after_menu from _call_phone_after_menu_38
    call message_start("[name]","I'm jealous...") from _call_message_start_39
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Ah.. really?") from _call_message_260
    call message("Ren","I'll eat with you next time!") from _call_message_261
    call message("Ren","Ah, and the three of us together!") from _call_message_262
    call message("Ren","You, and the person I mentioned....") from _call_message_263
    call message("Ren","enjoying food and talking with") from _call_message_264
    call message("Ren","only the important people to me...") from _call_message_265
    call message("Ren","Yeah that's how it will be.") from _call_message_266
    call message("Ren","I hope that day comes someday...!") from _call_message_267
    jump am38
label am38:
    call message("Ren","While we were eating, this person said") from _call_message_268
    call message("Ren","that all my senses are focused on you") from _call_message_269
    call message("Ren","ever since you came here") from _call_message_270
    call message("Ren","and that it was important for me to not get obsessed with something") from _call_message_271
    call message("Ren","if I wanted to take care of big things.") from _call_message_272
    call message("Ren","I...") from _call_message_273
    call message("Ren","didn't think I was being obsessed with you") from _call_message_274
    call message("Ren","What do you think?") from _call_message_275
    call screen phone_reply3("I don't think you're obsessed with me. I'm just glad that you seem to care for me a lot!", "cd320", "I don't feel it at all...I wish you'd be obsessed with me a bit more....", "cd321", "Um, I don't know. I haven't thought about it....", "cd322")
label cd320:
    call phone_after_menu from _call_phone_after_menu_39
    call message_start("[name]","I don't think you're obsessed with me. I'm just glad that you seem to care for me a lot!") from _call_message_start_40
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Right. You're right, [name].") from _call_message_276
    call message("Ren","I think of you a lot.") from _call_message_277
    call message("Ren","But I can't express all of it.") from _call_message_278
    call message("Ren","Because if I express too much") from _call_message_279
    call message("Ren","I'll be filled with your thoughts...") from _call_message_280
    call message("Ren","I haven't learned") from _call_message_281
    call message("Ren","how to fully control my emotions.") from _call_message_282
    call message("Ren","So, I don't know what will happen when I reveal all this.") from _call_message_283
    call message("Ren","I'm a bit...scared") from _call_message_284
    jump am39
label cd321:
    call phone_after_menu from _call_phone_after_menu_40
    call message_start("[name]","I don't feel it at all...I wish you'd be obsessed with me a bit more....") from _call_message_start_41
    call message("Ren","You want me to be obsessed with you?") from _call_message_285
    call message("Ren","Is that true?") from _call_message_286
    call message("Ren","Then...") from _call_message_287
    call message("Ren","can I reveal more of myself?") from _call_message_288
    call message("Ren","I can bother you and constantly contact you?") from _call_message_289
    call message("Ren","But if I do that...") from _call_message_290
    call message("Ren","I don't think I'll be able to the other things") from _call_message_291
    call message("Ren","Someday") from _call_message_292
    call message("Ren","when I don't have to do anything") from _call_message_293
    call message("Ren","I want to do it") from _call_message_294
    call message("Ren","I truly will...") from _call_message_295
    call message("Ren","Since it's something you want...") from _call_message_296
    call message("Ren","it's okay to do so, right?") from _call_message_297
    jump am39
label cd322:
    call phone_after_menu from _call_phone_after_menu_41
    call message_start("[name]","Um, I don't know. I haven't thought about it....") from _call_message_start_42
    call message("Ren","Really?") from _call_message_298
    call message("Ren","Then it's probably okay.") from _call_message_299
    jump am39
label am39:
    call message("Ren","I'm not sure about myself either...") from _call_message_300
    call message("Ren","Honestly") from _call_message_301
    call message("Ren","I've never given a deep thought about my emotions ^^;") from _call_message_302
    call message("Ren","Ah") from _call_message_303
    call message("Ren","that important person") from _call_message_304
    call message("Ren","was greatly interested in you.") from _call_message_305
    call message("Ren","Actually") from _call_message_306
    call message("Ren","the overall development plan for that app you're testing") from _call_message_307
    call message("Ren","was all that person's idea!") from _call_message_308
    call message("Ren","If things go well...") from _call_message_309
    call message("Ren","you'll be able to meet this person as well.") from _call_message_310
    call message("Ren","I thoroughly admire this person") from _call_message_311
    call message("Ren","so I'm sure you'll be the same ^^") from _call_message_312
    call message("Ren","When I think about you being here") from _call_message_313
    call message("Ren","I still feel so thrilled...") from _call_message_314
    call message("Ren","The fact that you came here with your own will") from _call_message_315
    call message("Ren","still makes me happy...") from _call_message_316
    call message("Ren","I'd be really happy") from _call_message_317
    call message("Ren","if you stayed here continuously.") from _call_message_318
    call message("Ren","If possible...") from _call_message_319
    call message_img("Ren","even after everything's done!", "images/smile.png") from _call_message_img_20
    call screen phone_reply("I want to be with you...! My mind will not change.", "cd323", "When the party is held...I think I would want to leave here.", "cd324")
label cd323:
    call phone_after_menu from _call_phone_after_menu_42
    call message_start("[name]","I want to be with you...! My mind will not change.") from _call_message_start_43
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Really? You're sure?") from _call_message_320
    call message("Ren","Oh") from _call_message_321
    call message("Ren","what am I to do?") from _call_message_322
    call message("Ren","It's my first time hearing such words.") from _call_message_323
    call message("Ren","I'm really glad.") from _call_message_324
    call message("Ren","I'm really really really happy!") from _call_message_325
    call message("Ren","My heart's beating really fast....") from _call_message_326
    call message("Ren","What do I do?") from _call_message_327
    call message("Ren","Oh") from _call_message_328
    call message("Ren","omg...") from _call_message_329
    call message("Ren","My heart's about to burst.") from _call_message_330
    call message("Ren","I don't know how to describe this.") from _call_message_331
    call message("Ren","Ah...!") from _call_message_332
    call message("Ren","Ah...") from _call_message_333
    call message("Ren","I think I have to go.") from _call_message_334
    call message("Ren","Though I want to talk to you a bit more") from _call_message_335
    jump am310
label cd324:
    call phone_after_menu from _call_phone_after_menu_43
    call message_start("[name]","When the party is held...I think I would want to leave here.") from _call_message_start_44
    call message("Ren","Ahm...") from _call_message_336
    call message("Ren","really?") from _call_message_337
    call message("Ren","...You might") from _call_message_338
    call message("Ren","change your mind by then") from _call_message_339
    call message("Ren","so for now I'm satisfied.") from _call_message_340
    call message("Ren","When the party starts...") from _call_message_341
    call message("Ren","you will definitely think differently.") from _call_message_342
    call message("Ren","I'll trust that you will hold on to that for now.") from _call_message_343
    call message("Ren","Sadly") from _call_message_344
    call message("Ren","it's time for me to go.") from _call_message_345
    jump am310
label am310:
    call message_img("Ren","A log that I need to check just came up...", "images/sad.png") from _call_message_img_21
    call message_img("Ren","Enjoy the game.", "images/smile.png") from _call_message_img_22
    call screen phone_reply("I want to see you today, Ren!", "cd325", "Bye!", "cd326")
label cd325:
    call phone_after_menu from _call_phone_after_menu_44
    call message_start("[name]","I want to see you today, Ren!") from _call_message_start_45
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Okay") from _call_message_346
    call message("Ren","I'll try my best to find time for that!") from _call_message_347
    call message("Ren","Thanks for telling me that...[name].") from _call_message_348
    jump am311
label cd326:
    call phone_after_menu from _call_phone_after_menu_45
    call message_start("[name]","Bye!") from _call_message_start_46
    jump am311
label am311:
    call message("Ren","I'll drop by again.") from _call_message_349
    call message("Ren","Then I'll see you, [name]!") from _call_message_350
    call message("SYSTEM", "Ren has left the chatroom") from _call_message_351
    call phone_end from _call_phone_end_3
    stop music
    scene black with dissolve
    play music "music/music16.ogg"
    scene bg12 with dissolve
    show Y Y11 with dissolve
    play sound "music/answer.ogg"
    "(Phone vibrating…)"
    y "Ugh, mom...? Not again..."
    play sound "music/beep.ogg"
    show Y Y12
    y "Hello? Mom, I told you I'm not going. Come on, you know why!"
    show Y Y11
    y "Aunt and uncle are gonna be there, aren't they? You know I hate them..."
    show Y Y12
    y "Ugh - don't try to reason with me. I'm not going. I'm so so so not going!"
    show Y Y14
    y "...I'm disappointing you? Mom... Why did you get so close to aunt and uncle all of a sudden? You told me not to even visit them when I was a child!"
    y "You know how close I was to Reina."
    show Y Y12
    y "But her parents didn't even attend the funeral for their own daughter. You call that a family?"
    show Y Y14
    y "Mom... Did you borrow some money from them by any chance? They're rich, you know?"
    y "Yikes! What's the yelling for?! You're making me even more suspicious."
    show Y Y12
    y "Ugh, forget it! I don't care if it's a family gathering... Yes, call me a bad son. I don't care!"
    y "I'm not going to see them! They didn't even say farewell to Reina at her last moment! I'm hanging up!!"
    play sound "music/beep.ogg"
    show Y Y15
    y "......."
    show Y Y11
    y "This isn't fair... This is not fair, mom."
    show Y Y16
    y "That goes for the rest of the families... This is so not fair."
    y "How can they forget her so easily...? They're acting as if she didn't even exist in the first place...!"
    show Y Y15
    y "That's the same for the RFA..."
    show Y Y16
    y "Six months have passed, and everything feels back to normal..."
    show Y Y15
    y "Poor Reina...."
    show Y Y16
    y "Everyone... This isn't fair."
    hide Y with dissolve
    stop music
    scene black with dissolve
    show text "20:11" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_3
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_47
    call message("Ren","Oh") from _call_message_352
    call message("Ren","you're here!") from _call_message_353
    call message("Ren","I think I'm really lucky^^") from _call_message_354
    call message("Ren","because I was on my way to your room..") from _call_message_355
    call screen phone_reply("You're on your way here? Wow…", "cd327", "Why does the messenger act weird whenever you log on?", "cd328")
label cd327:
    call phone_after_menu from _call_phone_after_menu_46
    call message_start("[name]","You're on your way here? Wow…") from _call_message_start_48
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yes.") from _call_message_356
    call message_img("Ren","I really miss you.", "images/ohyeah.png") from _call_message_img_23
    jump am312
label cd328:
    call phone_after_menu from _call_phone_after_menu_47
    call message_start("[name]","Why does the messenger act weird whenever you log on?") from _call_message_start_49
    call message("Ren","Oh...") from _call_message_357
    call message("Ren","I have to forcefully open the port") from _call_message_358
    call message("Ren","so the codes at the back become visible to you.") from _call_message_359
    call message("Ren","You don't have to worry about that ^^") from _call_message_360
    jump am312
label am312:
    call message("Ren","[name], what were you up to?") from _call_message_361
    call message("Ren","I think I've thought of you the whole day") from _call_message_362
    call message("Ren","You know what...?") from _call_message_363
    call message("Ren","I've never liked somebody, so I'm not sure about this, but...") from _call_message_364
    call message("Ren","I keep thinking about you.") from _call_message_365
    call message("Ren","That must be because I like you, isn't it?") from _call_message_366
    call screen phone_reply("How much do you like me? ", "cd329", "Um....don't we need time to learn about each other?", "cd330")
label cd329:
    call phone_after_menu from _call_phone_after_menu_48
    call message_start("[name]","How much do you like me?") from _call_message_start_50
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Whatever I do, you pop into my head....") from _call_message_367
    call message("Ren","I've only thought of doing my best to achieve my goal") from _call_message_368
    call message("Ren","before you came here...") from _call_message_369
    call message("Ren","but it has changed now") from _call_message_370
    call message("Ren","Whether you have had your meal") from _call_message_371
    call message("Ren","whether you're enjoying the game I made....") from _call_message_372
    call message("Ren","I keep thinking of these kinds of stuff.") from _call_message_373
    jump am313
label cd330:
    call phone_after_menu from _call_phone_after_menu_49
    call message_start("[name]","Um....don't we need time to learn about each other?") from _call_message_start_51
    call message("Ren","Ok, if you say so....") from _call_message_374
    call message("Ren","I'm...") from _call_message_375
    call message("Ren","I might not be an interesting person") from _call_message_376
    call message("Ren","I hope you won't be disappointed in me....") from _call_message_377
    call message("Ren","because") from _call_message_378
    call message("Ren","I already can't get you out of my head....") from _call_message_379
    jump am313
label am313:
    call message("Ren","I keep thinking of you...even when I'm talking to you") from _call_message_380
    call message("Ren","And when I'm not talking to you....") from _call_message_381
    call message("Ren","You keep popping in to my head.") from _call_message_382
    call message("Ren","The funny thing is....") from _call_message_383
    call message("Ren","I want you to think of me too.") from _call_message_384
    call message("Ren","What do you think about the desire to control someone else's actions?") from _call_message_385
    call message("Ren","Is it good? Or is it bad?") from _call_message_386
    call screen phone_reply("I actually feel good.", "cd331", "I feel...a tad suffocated.", "cd332")
label cd331:
    call phone_after_menu from _call_phone_after_menu_50
    call message_start("[name]","I actually feel good.") from _call_message_start_52
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Really?") from _call_message_387
    call message("Ren","Ah...") from _call_message_388
    call message("Ren","My heart's racing....") from _call_message_389
    call message("Ren","I think this symptom will worsen.") from _call_message_390
    call message("Ren","I get that idea as you said you like it.") from _call_message_391
    call message("Ren","You're thinking of me") from _call_message_392
    call message("Ren","while talking to me, right?") from _call_message_393
    call message("Ren","I'm so happy.") from _call_message_394
    jump am314
label cd332:
    call phone_after_menu from _call_phone_after_menu_51
    call message_start("[name]","I feel...a tad suffocated.") from _call_message_start_53
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Ah...I see.") from _call_message_395
    call message("Ren","I'm sorry if I made you feel cramped up...") from _call_message_396
    call message("Ren","Don't be too burdened by it.") from _call_message_397
    jump am314
label am314:
    call message("Ren","How was the game today?") from _call_message_398
    call message("Ren","Up to which part") from _call_message_399
    call message("Ren","have my AIs revealed their secrets to you?") from _call_message_400
    call message("Ren","How much have you hung out with the AIs?") from _call_message_401
    call message("Ren","I want to see you right now.....") from _call_message_402
    call message("Ren","Oh.") from _call_message_403
    call message("Ren","Almost there.") from _call_message_404
    call message("Ren","[name],") from _call_message_405
    call message("Ren","Could you open the door?") from _call_message_406
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_407
    call phone_end from _call_phone_end_4
    stop music
    scene black with dissolve
    play music "music/music4.ogg"
    scene bg7 with dissolve
    show MC MC11 at my_right with dissolve
    show U U11 at my_left with dissolve
    ray "Finally...I've missed you so much."
    show U U13
    ray "I'm sorry I couldn't visit more often."
    show U U12
    menu:
        "I've been waiting for you! It's great to see you.":
            ray "I want to see you a lot more, but things just wouldn't go as planned."
            ray "But I did try to drop by the chatrooms frequently..."
            ray "You were waiting for me, weren't you? I'm so glad."
            jump u34
        "Aren't you busy?":
            ray "I am, but I wanted to see you... I hope I'm not bothering you."
            jump u34
label u34:
    show U U13
    ray "The air is good tonight."
    show U U12
    ray "It's a perfect weather to take a walk!"
    show U U11
    ray "Remember what I told you yesterday?"
    menu:
        "You said you needed caffeine.":
            ray "I don't remember saying that..."
            ray "You must have forgotten that there's this garden I want to take you to."
            ray "You didn't mistake what I said with what the other AI said, did you?"
            ray "If you did, I'll be so disappointed..."
            ray "I told you that I want to take you to this garden."
            ray "I even sent you a picture..."
            jump u35
        "You talked about a garden.":
            ray "Yes, and I'm going to take you there!"
            jump u35
label u35:
    show U U12
    ray "Don't you feel bored stuck in this floor?"
    ray "I appreciate playing my game so earnestly, but you might forget the reality if you're too focused about the game."
    show U U13
    menu:
        "I'm not an idiot -.":
            ray "I know...!You're not offended, are you?"
            jump u36
        "I want to go to that garden fast.":
            ray "Sure! Me,too. I was so excited on my way here."
            ray "I hope you like it."
            ray "And even if it's different from what you were expecting, I hope you're not too disappointed..."
            jump u36
label u36:
    show U U11
    ray "Let's go out there and take a walk. You've never really been outside, have you?"
    show U U12
    ray "I found some free time for myself to be with you."
    show U U11
    ray "I want to make a 'memory' with you...a lot of memories!"
    ray "So let's go see the flowers...at my favorite garden."
    show U U14
    ray "You'll see them glowing softly in the moonlight."
    hide MC with dissolve
    hide U with dissolve
    scene black with dissolve
    scene bg13 with dissolve
    show MC MC11 at my_right with dissolve
    show U U11 at my_left with dissolve
    ray "Ahaha, it's so great to be here with you!"
    menu:
        "They're beautiful...!":
            ray "You think so,too?"
            ray "...I feel great. I feel great about you, this garden, and that moon."
            jump u37
        "It's a little chilly.":
            ray "Ah...must be because it's night. What should I do?"
            ray "Umm, I'll lend you my jacket. If that's okay with you."
            jump u37
label u37:
    show U U12
    ray "It's been three days since you came."
    ray "Whenever I wake up from my short nap, I keep checking the log. Because it still feels like a dream."
    show U U13
    ray "You're enjoying your chat with the RFA...a lot more than you do with me..."
    show U U12
    ray "I'm grateful to you for testing the game in my place...but I'm often scared that you might disappear into the game."
    ray "And today...I thought that I might lose you to the game, so I couldn't help coming for you!"
    menu:
        "When will this game end?":
            ray "I'm not sure. ...Would you like it to be over soon?"
            jump u38
        "But...just what is this place?":
            ray "Oh...this is where I make my game, and... Let's just say it's a sort of 'paradise.'"
            ray "...The goal of this place is to create dreams and happiness!"
            ray "I guess you can say...this is...a non-commercial entertainments company. Yes, something like that."
            jump u38
        "Just what do you do here, Ren? Why are you so busy?":
            ray "What do I do...? There are a lot of things I do, but they're all basically the same!"
            ray "It's showing somebody something entertaining."
            ray "Everything in this place is administered by my 'supervisor.'"
            ray "I can't help being busy. There are still people out there who need our help..."
            jump u38
label u38:
    show U U15
    ray "Oh, watch your feet."
    show U U12
    ray" Were you scared? There are construction materials left behind..."
    ray "I told them to clean them up. It seems they didn't finish their job."
    show U U13
    ray "This building hasn't been completed yet."
    show U U12
    ray "It's complete on the outside, but the basement inside is still under construction."
    show U U11
    ray "You'll soon be able to roam around the building as you please...though a few more contracts would be necessary."
    show U U12
    ray "Oh, let's not talk about this now."
    ray "If you want to be one of us forever, I'll do everything in my power to help you."
    show U U13
    ray "So please, I ask you to let me in..."
    show U U17
    ray "So that we... So that I can get inside."
    show U U12
    ray "Oh...the wind's gone icy. You shouldn't catch cold, [name]..."
    ray "Should we head in now?"
    menu:
        "Let's walk for few more minutes. I want to talk to you some more.":
            ray "I'd love to talk to you longer...."
            ray "I feel like I'm in a dream. But it feels so good...so it feels like it's unreal. I'm actually a bit anxious."
            ray "I'm sure there will be a plenty of chances to talk."
            ray "As long as you stay beside me like this..."
            jump u39
        "I want to go back and rest!":
            ray "Sure... Are you tired?"
            ray "I must have been too excited and kept you here too long."
            jump u39
        "Already? I don't want to go back. I feel trapped in there...":
            ray "You're right. You must feel like you're trapped, stuck in that room with no chance to be anywhere else..."
            ray "If you promise to be one of us, you could be free...."
            ray "Could you give me some more time?"
            ray "I'll log in and try to visit you more often. I won't keep you alone and trapped."
            jump u39
label u39:
    show U U11
    ray "Now let's go. I'll walk you to your room."
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    stop music
    scene bg13 with dissolve
    show V V210 with dissolve
    v "......Could that be..?"
    hide V with dissolve
    scene black with dissolve
    stop music
    show text "21:49" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Take call.)"
    v "Hello, it's me."
    v "I did say I'll call you, didn't I?"
    v "It's nothing, but...there was something I wanted to ask you..."
    v "Are you...staying alone by any chance?"
    v "Is there someone frequently visiting you?"
    menu:
        "I was and am alone.":
            v "I see... If I'm not mistaken, it's..."
            v "No, nothing. Don't mind what I'm saying."
            jump v31
        "Why do you ask?":
            v "If you're alone, I was wondering if you were in a safe place."
            v "I was asking because I was worried about you, and I apologize if I have offended you."
            jump v31
label v31:
    v "If there's nothing going on, that's a relief..."
    v "Can you promise me one thing?"
    v "If something happens or if there's something you want to tell me, feel free to tell us."
    menu:
        "Didn't you ask me to promise one thing? That's two.":
            v "Oh, is it?"
            v "I thought it would be great if you could tell us even the smallest things to the RFA members."
            jump v32
        "I'll tell you if something's up!":
            v "Thank you for taking it the good way."
            v "Can you tell us in prior if you feel like something is going to happen?"
            v "Maybe...it might be very helpful to us."
            v "I'm really worried about you."
            v "I hope nothing happens to you."
            jump v32
        "I'm not a child.":
            v "Oh... I didn't mean it that way. I was worried since you became a member in a unique way."
            v "I hope...my concerns don't feel like an interference."
            jump v32
label v32:
    v "Anyhow, you should always take care."
    v "I'll look after you."
    menu:
        "Look after me?":
            v "Oh, yes. As I'm the head of the RFA, I am always in the position to look after all our members... Haha..."
            v "I was trying to say that I'll be looking after your well-being through the messenger."
            jump v33
        "I'll look after you, too, K.":
            v "Sorry...? Me....?"
            v "Oh, that's what I said in the messenger... You mean the same thing, right? Yes, I get it."
            jump v33
        "You should take care as well, K.":
            v "I appreciate your concerns. I keep making everyone worried...including you and the rest of the members..."
            v "To be honest, I'm more worried about you. I'm worried since you're a nice person who tries to look after me even in this kind of situation."
            jump v33
label v33:
    v "I have some things to attend do, so I should go."
    v "You should...always...look after yourself."
    v "Please excuse me first."
    play sound "music/beep.ogg"
    hide MC with dissolve
    scene black with dissolve
    call day4 from _call_day4
