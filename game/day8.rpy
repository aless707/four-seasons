label day8:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 8" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "01:37" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    call phone_start from _call_phone_start_20
    call message_start("SYSTEM", "Sayaka has entered the chatroom.") from _call_message_start_317
    call message("Sayaka", "The moon is bright.") from _call_message_2505
    call message("Sayaka", "I see you're not asleep yet, [name].") from _call_message_2506
    call screen phone_reply("....There's something I'd like to discuss.", "cd81", "I can't sleep.", "cd82")
label cd81:
    call phone_after_menu from _call_phone_after_menu_299
    call message_start("[name]", "....There's something I'd like to discuss.") from _call_message_start_318
    call message("Sayaka", "There's nothing wrong") from _call_message_2507
    call message("Sayaka", "with your safety, is there?") from _call_message_2508
    call screen phone_reply("I might need some help now....", "cd83", "Please save me!", "cd84")
label cd83:
    call phone_after_menu from _call_phone_after_menu_300
    call message_start("[name]", "I might need some help now....") from _call_message_start_319
    jump am81
label cd84:
    call phone_after_menu from _call_phone_after_menu_301
    call message_start("[name]", "Please save me!") from _call_message_start_320
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end += 1
    jump am81
label cd82:
    call phone_after_menu from _call_phone_after_menu_302
    call message_start("[name]", "I can't sleep.") from _call_message_start_321
    call message("Sayaka", "Neither can I.") from _call_message_2509
    call message("Sayaka", "I'm concerned whether the unit's duty tomorrow") from _call_message_2510
    call message("Sayaka", "will proceed fluently.") from _call_message_2511
    call message("Sayaka", "My duties with the intelligence unit") from _call_message_2512
    call message("Sayaka", "could not be neglected for a second") from _call_message_2513
    call message("Sayaka", "so it is my greatest joy") from _call_message_2514
    call message("Sayaka", "that the C&R's utopia team") from _call_message_2515
    call message("Sayaka", "will be on a temporary hold.") from _call_message_2516
    call screen phone_reply("Uhm...could you please help me?", "cd85", "...Sayaka, I have something important to tell you.", "cd8666")
label cd85:
    call phone_after_menu from _call_phone_after_menu_303
    call message_start("[name]", "Uhm...could you please help me?") from _call_message_start_322
    jump am81
label cd8666:
    call phone_after_menu from _call_phone_after_menu_304
    call message_start("[name]", "...Sayaka, I have something important to tell you.") from _call_message_start_323
    jump am81
label am81:
    play sound "music/hack.ogg"
    play music "music/music19.ogg"
    call message("SYSTEM", "Sora has entered the chatroom.") from _call_message_2517
    call screen phone_reply3("???", "cd86", "I didn't say anything I shouldn't!", "cd87", "There's something wrong with the chatroom.", "cd88")
label cd86:
    call phone_after_menu from _call_phone_after_menu_305
    call message_start("[name]", "???") from _call_message_start_324
    call message("Sora", "???") from _call_message_2518
    call message_img("Sora", "?????", "images/questioning.png") from _call_message_img_131
    call message("Sora", "What were you planning to tell her?") from _call_message_2519
    call message("Sora", "Idiot.") from _call_message_2520
    jump am82
label cd87:
    call phone_after_menu from _call_phone_after_menu_306
    call message_start("[name]", "I didn't say anything I shouldn't!") from _call_message_start_325
    call message("Sora", "I've been watching") from _call_message_2521
    call message("Sora", "everything in the first place") from _call_message_2522
    call message_img("Sora", "No need to make excuses for every single thing lol", "images/smile.png") from _call_message_img_132
    jump am82
label cd88:
    call phone_after_menu from _call_phone_after_menu_307
    call message_start("[name]", "There's something wrong with the chatroom.") from _call_message_start_326
    call message("Sora", "It's me.") from _call_message_2523
    call message("Sora", "Where's your welcome?") from _call_message_2524
    call message_img("Sora", "You're so slow hehehe", "images/wxpecting.pnh") from _call_message_img_133
    jump am82
label am82:
    call message("Sora", "Aren't you such a hard worker") from _call_message_2525
    call message("Sora", "until late at night? Hehe") from _call_message_2526
    call message("Sora", "That's right.") from _call_message_2527
    call message("Sora", "You should at least do your work right...") from _call_message_2528
    call message("Sora", "Then maybe you can prove even for little") from _call_message_2529
    call message_img("Sora", "that you're more useful alive.", "images/smile.png") from _call_message_img_134
    call message("Sora", "And I might find your room any moment.") from _call_message_2530
    call message("Sora", "Did I scare you? Hehehe") from _call_message_2531
    call screen phone_reply("I am scared... But I know Ren is in you. So I feel complicated.", "cd89", "I'm scared...", "cd810")
label cd89:
    call phone_after_menu from _call_phone_after_menu_308
    call message_start("[name]", "I am scared... But I know Ren is in you. So I feel complicated.") from _call_message_start_327
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Sora", "You idiot.") from _call_message_2532
    call message("Sora", "I told you") from _call_message_2533
    call message_img("Sora", "that airhead is already gone!", "images/questioning.png") from _call_message_img_135
    call message("Sora", "I guess you can't cope with the world without multiple lessons.") from _call_message_2534
    call message("Sora", "Or are you phrasing your hopes") from _call_message_2535
    call message("Sora", "as truth?") from _call_message_2536
    call message("Sora", "But then again") from _call_message_2537
    call message_img("Sora", "that's something perfect for an airhead hehe", "images/smile.png") from _call_message_img_136
    call message("Sora", "If you're not scared") from _call_message_2538
    call message("Sora", "you shouldn't shiver like you did back then lolol") from _call_message_2539
    jump am83
label cd810:
    call phone_after_menu from _call_phone_after_menu_309
    call message_start("[name]", "I'm scared...") from _call_message_start_328
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end += 1
    call message_img("Sora", "Really?", "images/questioning.png") from _call_message_img_137
    call message("Sora", "Already?") from _call_message_2540
    call message("Sora", "But") from _call_message_2541
    call message("Sora", "I'm") from _call_message_2542
    call message_img("Sora", "just getting started!", "images/smile.png") from _call_message_img_138
    jump am83
label am83:
    call message("Sora", "If") from _call_message_2543
    call message("Sora", "you're so") from _call_message_2544
    call message("Sora", "so") from _call_message_2545
    call message("Sora", "so") from _call_message_2546
    call message("Sora", "so scared of me") from _call_message_2547
    call message("Sora", "why not ask the RFA") from _call_message_2548
    call message("Sora", "for help?") from _call_message_2549
    call screen phone_reply("That can put everyone in danger.", "cd811", "....Then what will happen to me?", "cd812")
label cd811:
    call phone_after_menu from _call_phone_after_menu_310
    call message_start("[name]", "That can put everyone in danger.") from _call_message_start_329
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Sora", "Everyone?") from _call_message_2550
    call message("Sora", "You mean the RFA?") from _call_message_2551
    call message("Sora", "Or....") from _call_message_2552
    call message_img("Sora", "their representative? ^^", "images/expecting.png") from _call_message_img_139
    jump am84
label cd812:
    call phone_after_menu from _call_phone_after_menu_311
    call message_start("[name]", "....Then what will happen to me?") from _call_message_start_330
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end += 1
    call message("Sora", "Why don't you try and find out? ^^") from _call_message_2553
    call message("Sora", "I think") from _call_message_2554
    call message_img("Sora", "it's going to be so fun hahaha", "images/expecting.png") from _call_message_img_140
    call message("Sora", "Hmm") from _call_message_2555
    call message("Sora", "to give you a small spoiler") from _call_message_2556
    call message("Sora", "you might...") from _call_message_2557
    call message("Sora", "end up with me") from _call_message_2558
    call message("Sora", "for the next 24 hours! Hahaha") from _call_message_2559
    call message("Sora", "Well?") from _call_message_2560
    call message("Sora", "Sounds fun, right?") from _call_message_2561
    call message("Sora", "So go ahead and give it a try ^^") from _call_message_2562
    jump am84
label am84:
    call message_img("Sora", "Just remember", "images/smile.png") from _call_message_img_141
    call message("Sora", "that I'm watching ^^") from _call_message_2563
    call message("Sora", "[name]...") from _call_message_2564
    play sound "music/hack.ogg"
    play music "music/music2chat-17.ogg"
    call message_img("Sayaka", "?", "images/questioning.png") from _call_message_img_142
    call message("Sayaka", "Oh") from _call_message_2565
    call message("Sayaka", "now I can type again.") from _call_message_2566
    call message("Sayaka", "The chatroom was weird for a moment.") from _call_message_2567
    call phone_end from _call_phone_end_21
    stop music
    scene black with dissolve
    play music "music/music14.ogg"
    scene black with dissolve
    "(A year ago)"
    show R R31 with dissolve
    savior "You are so tender, kind...."
    savior "So was I when I was a child."
    show R R32
    savior "When my eyes were still clouded, I thought everything was my fault."
    show R R33
    savior "But we've been victims to agony more than enough, haven't we?"
    savior "Now it's time to embrace what all that agony has seared into us."
    show R R35
    savior "You should embrace that you shouldn't stay so nice and innocent."
    show R R32
    savior "We were begotten from darkness. Embrace that, and things will be so much easier."
    savior "If you keep that angelic mask on, you'll be met with nothing but suffering and sneering."
    show R R33
    savior "You must be a devil in order to survive. There's no other choice. And that's not a bad thing."
    show R R32
    savior "Don't let the world trick you."
    show R R35
    savior "Becoming a devil is not evil."
    play music "music/music19.ogg"
    scene bg20 with dissolve
    show R R35
    savior "So unleash the evil within. You can do it."
    savior "You were begotten from darkness."
    u "...Reina...?"
    show R R33
    savior "I can see it. Just a bit more...just a little more, and you'll be fine."
    show R R36
    savior "What are you all waiting for? Continue the cleansing."
    believer "Yes, ma'am!"
    show R R33
    u "Cough... Cough..."
    show R R35
    savior "Once you get used to hatred, you'll be happy. I can't imagine what you've been through so far...."
    show R R32
    savior "I can't imagine how much you struggled to bridle all your hatred... You poor child..."
    u "Cough... Cough... I would rather..."
    show R R33
    savior "What was that?"
    u "...I would rather...be gone. Cough... This is a dream..."
    show R R35
    savior "You can feel the elixir taking effect, can't you? The happiness is dawning upon you."
    savior "I can feel it, Sora. I feel happy just imagining your happiness soon to unfold."
    u "Souta... Help..."
    show R R32
    savior "Souta abandoned you. He so heartlessly left you all alone...in that lonesome place."
    show R R35
    savior "And there lies vengeance towards him in your heart."
    savior "There is a pure essence of vengeance that just began to bloom... Now show it to me, Sora."
    u "That's...not who I am. I cannot hate my own brother..."
    show R R32
    u "Kgh... Huff... NO, NO!!!"
    show R R33
    savior "Keep going. We must save this boy..."
    scene black with dissolve
    show R R33
    savior "Your brother is not coming."
    u "......I would rather be gone."
    show R R32
    savior "He left you. K left me. Our parents left us in the abyss."
    u "....I would rather make myself gone."
    show R R33
    savior "Now you see? Sora...."
    show R R32
    savior "We are victims. You and I are nothing different.... We were fools left behind."
    show R R35
    savior "But not anymore. Now we are born again."
    hide R with dissolve
    scene black with dissolve
    u "....I would rather make them all gone."
    savior "So do I."
    u "I will make them all gone. So nothing and nobody can ever hurt me."
    savior "Haha, now your cleansing is complete."
    savior "You have been cleansed. Now, this is the real you. You are the real Sora."
    savior" You are strong... You are not useless. You will never be useless."
    u "I am not useless. I am strong...."
    u "I will become stronger. So that nobody can hurt me..."
    savior "If you regularly ingest the elixir..."
    savior "you can stay strong. Remember, Sora...."
    savior "You are not weak. That is not the real you. The weak one's name is 'Ren.'"
    savior "You are the real Sora...."
    savior "Only children like you can survive in this world."
    savior "Children like Ren...will always lose. Keep that in mind."
    stop music
    scene black with dissolve
    show text "03:46" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music19.ogg"
    call phone_start from _call_phone_start_21
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_331
    call message("Sora", "Hey") from _call_message_2568
    call message("Sora", "hey") from _call_message_2569
    call message("Sora", "answer me.") from _call_message_2570
    call screen phone_reply("Is there something wrong?", "cd813", "Yes...?", "cd814")
label cd813:
    call phone_after_menu from _call_phone_after_menu_312
    call message_start("[name]", "Is there something wrong?") from _call_message_start_332
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "You think I can talk to you only when there's something wrong?", "images/questioning.png") from _call_message_img_143
    call message("Sora", "I decide what to do.") from _call_message_2571
    call message_img("Sora", "You don't ask me questions.", "images/well.png") from _call_message_img_144
    call message("Sora", "Oh I forgot hehe") from _call_message_2572
    call message("Sora", "you think you're a princess....") from _call_message_2573
    jump am85
label cd814:
    call phone_after_menu from _call_phone_after_menu_313
    call message_start("[name]", "Yes...?") from _call_message_start_333
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "You were sluggish when I saw you") from _call_message_2574
    call message("Sora", "and your typing") from _call_message_2575
    call message("Sora", "is just as sluggish hahahaha") from _call_message_2576
    jump am85
label am85:
    call message_img("Sora", "But you logged in this late to see me.", "images/smile.png") from _call_message_img_145
    call message("Sora", "Should I give you a kudos? Haha.") from _call_message_2577
    call message("Sora", "Hey") from _call_message_2578
    call message("Sora", "answer me.") from _call_message_2579
    call message("Sora", "You like playing with me") from _call_message_2580
    call message("Sora", "don't you?") from _call_message_2581
    call screen phone_reply3("Yes.", "cd815", "No.", "cd816", "Please don't play with me....", "cd817")
label cd815:
    call phone_after_menu from _call_phone_after_menu_314
    call message_start("[name]", "Yes.") from _call_message_start_334
    call message("Sora", "Too bad") from _call_message_2582
    call message("Sora", "because") from _call_message_2583
    call message("Sora", "for me") from _call_message_2584
    call message("Sora", "it's so so") from _call_message_2585
    call message_img("Sora", "so not fun.", "images/expecting.png") from _call_message_img_146
    call message("Sora", "Come up with a new trick or two, will you?") from _call_message_2586
    call message("Sora", "Is there anything you can do...?") from _call_message_2587
    jump am86
label cd816:
    call phone_after_menu from _call_phone_after_menu_315
    call message_start("[name]", "No.") from _call_message_start_335
    call message("Sora", "Of course it shouldn't be.") from _call_message_2588
    call message("Sora", "I'm supposed to be having fun.") from _call_message_2589
    call message_img("Sora", "Who do you think you are to have fun? Hehehe", "images/well.png") from _call_message_img_147
    call message("Sora", "Hah....") from _call_message_2590
    call message("Sora", "now I'm thinking again...") from _call_message_2591
    call message("Sora", "about that lame face of yours....") from _call_message_2592
    call message("Sora", "I'm starting to feel weird.") from _call_message_2593
    call message("Sora", "Next time I see you") from _call_message_2594
    call message("Sora", "turn around so I can't see you.") from _call_message_2595
    call message("Sora", "I made myself") from _call_message_2596
    call message("Sora", "very") from _call_message_2597
    call message_img("Sora", "clear...", "images/expecting.png") from _call_message_img_148
    jump am86
label cd817:
    call phone_after_menu from _call_phone_after_menu_316
    call message_start("[name]", "Please don't play with me....") from _call_message_start_336
    call message("Sora", "If you keep pretending princess") from _call_message_2598
    call message("Sora", "I'll make you stay away from typing.") from _call_message_2599
    call message_img("Sora", "^^", "images/expecting.png") from _call_message_img_149
    call message("Sora", "Ren must have planted some weird imaginations in you....") from _call_message_2600
    call message("Sora", "This is serious") from _call_message_2601
    call message("Sora", "really serious.") from _call_message_2602
    jump am86
label am86:
    call message("Sora", "Oh") from _call_message_2603
    call message("Sora", "I just told the believers") from _call_message_2604
    call message("Sora", "from now on") from _call_message_2605
    call message("Sora", "to not let anyone in your room.") from _call_message_2606
    call message("Sora", "Then I'll be the only person") from _call_message_2607
    call message_img("Sora", "you can ever see.", "images/smile.png") from _call_message_img_150
    call message("Sora", "After a month") from _call_message_2608
    call message("Sora", "I'll be") from _call_message_2609
    call message("Sora", "the living hell for you") from _call_message_2610
    call message("Sora", "But then again...") from _call_message_2611
    call message("Sora", "are you going to start missing me? ^^") from _call_message_2612
    call message("Sora", "Oh") from _call_message_2613
    call message("Sora", "did I mention") from _call_message_2614
    call message("Sora", "that from now on") from _call_message_2615
    call message("Sora", "I told them not to feed you?") from _call_message_2616
    call message("Sora", "Do we really") from _call_message_2617
    call message("Sora", "have to waste time") from _call_message_2618
    call message("Sora", "trying to feed an airhead like you....?") from _call_message_2619
    call message_img("Sora", "And talk about the waste of food...", "images/well.png") from _call_message_img_151
    call message("Sora", "But then") from _call_message_2620
    call message("Sora", "if you starve to death") from _call_message_2621
    call message("Sora", "my savior will be so mad -") from _call_message_2622
    call message("Sora", "She told me") from _call_message_2623
    call message("Sora", "not to be too mean to you") from _call_message_2624
    call message("Sora", "until the party.") from _call_message_2625
    call message("Sora", "Oh") from _call_message_2626
    call message("Sora", "that's it.") from _call_message_2627
    call message("Sora", "If I find some hideous dish") from _call_message_2628
    call message("Sora", "I'll take it to you") from _call_message_2629
    call message("Sora", "and make you eat it haha.") from _call_message_2630
    call message("Sora", "If I do that") from _call_message_2631
    call message_img("Sora", "will you be a little more fun?", "images/smile.png") from _call_message_img_152
    call screen phone_reply("How can I make you happier?", "cd818", "You seem to be constantly seeking something to entertain you.", "cd819")
label cd818:
    call phone_after_menu from _call_phone_after_menu_317
    call message_start("[name]", "How can I make you happier?") from _call_message_start_337
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Hahahahahaha.") from _call_message_2632
    call message("Sora", "Isn't that something") from _call_message_2633
    call message("Sora", "Ren used to tell you?") from _call_message_2634
    call message_img("Sora", "Did you catch that manner ofspeech of his?", "images/smile.png") from _call_message_img_153
    call message("Sora", "Seriously, likes attract.") from _call_message_2635
    call message("Sora", "You'll be treated just as bad") from _call_message_2636
    call message("Sora", "just as same as Ren hahaha.") from _call_message_2637
    call message("Sora", "And once you're no longer useful") from _call_message_2638
    call message("Sora", "you'll be thrown away") from _call_message_2639
    call message("Sora", "so sadly...") from _call_message_2640
    jump am87
label cd819:
    call phone_after_menu from _call_phone_after_menu_318
    call message_start("[name]", "You seem to be constantly seeking something to entertain you.") from _call_message_start_338
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "That's because", "images/questioning.png") from _call_message_img_154
    call message("Sora", "you'd make me feel") from _call_message_2641
    call message("Sora", "so empty....") from _call_message_2642
    call message("Sora", "You should keep me entertained all the time!") from _call_message_2643
    call message("Sora", "Don't pretend to be so noble") from _call_message_2644
    call message("Sora", "as if I'm weird") from _call_message_2645
    call message("Sora", "and you're right....") from _call_message_2646
    call message("Sora", "At least in this place") from _call_message_2647
    call message("Sora", "you're a useless airhead.") from _call_message_2648
    jump am87
label am87:
    call message_img("Sora", "It's going to take some time for you", "images/smile.png") from _call_message_img_155
    call message("Sora", "to accept the fact") from _call_message_2649
    call message("Sora", "that you're a stupid toy") from _call_message_2650
    call message("Sora", "good for nothing haha.") from _call_message_2651
    call message("Sora", "You've been living like a princess so far -") from _call_message_2652
    call screen phone_reply("I'll get used to it fast....", "cd820", "Don't you think you want to look down on me so that you can stand more superior?", "cd821")
label cd820:
    call phone_after_menu from _call_phone_after_menu_319
    call message_start("[name]", "I'll get used to it fast....") from _call_message_start_339
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Oh") from _call_message_2653
    call message("Sora", "at least you understood right away") from _call_message_2654
    call message("Sora", "that if you don't get used to it fast") from _call_message_2655
    call message_img("Sora", "you can't survive ^^", "images/questioning.png") from _call_message_img_156
    call message("Sora", "Try to come up with a charm haha") from _call_message_2656
    call message("Sora", "But to be honest") from _call_message_2657
    call message("Sora", "I have no idea") from _call_message_2658
    call message("Sora", "whether you actually have") from _call_message_2659
    call message("Sora", "a charm.") from _call_message_2660
    jump am88
label cd821:
    call phone_after_menu from _call_phone_after_menu_320
    call message_start("[name]", "Don't you think you want to look down on me so that you can stand more superior?") from _call_message_start_340
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "Hehehehe", "images/smile.png") from _call_message_img_157
    call message("Sora", "hehehehehehe") from _call_message_2661
    call message("Sora", "hehehehehe") from _call_message_2662
    call message("Sora", "hehe....") from _call_message_2663
    call message("Sora", "Hey") from _call_message_2664
    call message("Sora", "I'm going to remember that.") from _call_message_2665
    call message("Sora", "You.......") from _call_message_2666
    call message("Sora", "...............") from _call_message_2667
    call message_img("Sora", "...................Just you wait.", "images/expecting.png") from _call_message_img_158
    jump am88
label am88:
    call message("Sora", "Oh") from _call_message_2668
    call message("Sora", "right right right") from _call_message_2669
    call message("Sora", "one more thing -") from _call_message_2670
    call message("Sora", "Open wide") from _call_message_2671
    call message("Sora", "and read this carefully -") from _call_message_2672
    call message("Sora", "From now on") from _call_message_2673
    call message_img("Sora", "you better not look me in the eyes.", "images/well.png") from _call_message_img_159
    call message("Sora", "If you do") from _call_message_2674
    call message("Sora", "I'll completely lose my appetite haha.") from _call_message_2675
    call message("Sora", "Did my savior have to assign") from _call_message_2676
    call message("Sora", "this important mission of") from _call_message_2677
    call message("Sora", "making the RFA hold the party") from _call_message_2678
    call message("Sora", "to some airhead like you?") from _call_message_2679
    call message("Sora", "My savior....") from _call_message_2680
    call message("Sora", "She's just too kind.") from _call_message_2681
    call message("Sora", "I need to keep you alive") from _call_message_2682
    call message("Sora", "but you're good for nothing....") from _call_message_2683
    call message_img("Sora", "Just what can you do?", "images/questioning.png") from _call_message_img_160
    call screen phone_reply("I'll do anything you tell me... Please don't throw me away.", "cd822", "Those words can't hurt me.", "cd823")
label cd822:
    call phone_after_menu from _call_phone_after_menu_321
    call message_start("[name]", "I'll do anything you tell me... Please don't throw me away.") from _call_message_start_341
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Of course you should do") from _call_message_2684
    call message("Sora", "everything I tell you haha.") from _call_message_2685
    call message("Sora", "I'll throw you away") from _call_message_2686
    call message("Sora", "once you're no longer useful.") from _call_message_2687
    call message_img("Sora", "Hahaha.", "images/smile.png") from _call_message_img_161
    call message("Sora", "I'll be there soon") from _call_message_2688
    call message("Sora", "so try to entertain me then.") from _call_message_2689
    call message("Sora", "Try crying if you have to -") from _call_message_2690
    call message("Sora", "Will it be fun") from _call_message_2691
    call message("Sora", "if I see you crying?") from _call_message_2692
    jump am89
label cd823:
    call phone_after_menu from _call_phone_after_menu_322
    call message_start("[name]", "Those words can't hurt me.") from _call_message_start_342
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Oh") from _call_message_2693
    call message("Sora", "they can't?") from _call_message_2694
    call message_img("Sora", "Aren't you great or what?", "images/expecting.png") from _call_message_img_162
    call message("Sora", "Oh my my -") from _call_message_2695
    call message("Sora", "what should I do?") from _call_message_2696
    call message("Sora", "Now I wanna see even more...") from _call_message_2697
    call message("Sora", "I must see") from _call_message_2698
    call message("Sora", "your face in tears.", "images/smile.png") from _call_message_2699
    call message("Sora", "I must see your face....") from _call_message_2700
    jump am89
label am89:
    call message("Sora", "Even if it's no fun") from _call_message_2701
    call message("Sora", "maybe I should let off some steam haha.") from _call_message_2702
    call message("Sora", "It was kind of fun") from _call_message_2703
    call message("Sora", "when I saw you scared hahaha.") from _call_message_2704
    call message("Sora", "Oh") from _call_message_2705
    call message("Sora", "speaking of which") from _call_message_2706
    call message_img("Sora", "Maybe I should pay you a visit now ^^", "images/expecting.png") from _call_message_img_163
    call message("Sora", "Just wait there") from _call_message_2707
    call message("Sora", "nice and quiet") from _call_message_2708
    call message("Sora", "in your room") from _call_message_2709
    call message("Sora", "like a toy lol") from _call_message_2710
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_2711
    call phone_end from _call_phone_end_22
    stop music
    show MC MC14 with dissolve
    play music "music/music4.ogg"
    play sound "music/knock.ogg"
    "(Knock knock)"
    stop sound
    play sound "music/dooropen.ogg"
    "(Door opened)"
    hide MC with dissolve
    scene black with dissolve
    show R R33mask at my_left with dissolve
    savior "...So this is what Ren likes."
    scene bg7 with dissolve
    show MC MC15 at my_right with dissolve
    show R R34mask at my_left with dissolve
    savior "I thought I stepped into some princess's room. But then again, I expect nothing else from him. He loved flowers."
    show R R31mask
    savior "Ever since I entrusted him with everything related to the RFA messenger, I didn't even think of looking around this place."
    show R R33mask
    menu:
        "Ren has become different.... I was so alarmed.":
            savior "And did you like it? You seem calmer than I thought."
            savior "You are very intriguing."
            jump u81
        "What have you done to Ren!?":
            savior "Oh, dear. It appears you don't like the way he changed."
            savior "I understand. Everyone has different preferences."
            jump u81
label u81:
    show R R33mask
    savior "You might miss Ren, but I suggest you forget him. Because he is no longer here."
    show R R34mask
    menu:
        "No longer here...?":
            savior "That's right. He permanently silenced Ren's persona in order to survive."
            jump u82
        "I believe he will be back.":
            savior "...That's what you hope. How very innocent."
            jump u82
label u82:
    show R R33mask
    savior "It was you, wasn't it...? You are the one who told him the world might be more beautiful than he would have thought."
    show R R32mask
    savior "He became strange ever since he met you."
    show R R33mask
    savior "It looked like his attention was lost somewhere distant...and that did nothing to help him."
    savior "I'm not saying everything you have done is bad, [name]."
    savior "Ren finally left. That weak boy finally lost, thanks to you..."
    show R R31mask
    savior "Ren chose to pose another persona than to become evil. But I think that is actually better."
    show R R35mask
    savior "Because Sora has finally become whom I wanted him to be! Strong, full of rage, hurting before getting hurt...!"
    savior "That is what I wanted to see."
    show R R33mask
    savior "I wouldn't have suffered any pain from my past if I had behaved like him...."
    savior "His rage matured as much as he spent his time in agony."
    savior "Now he will be the most powerful weapon for me to brandish."
    show R R31mask
    savior "And I was able to draw out that razor-sharp rage from him...because you were here, [name]."
    savior "I'm sure you're unsettled to see Ren - I mean, Sora different, but just remember that your role was crucial."
    show R R33mask
    menu:
        "Can't we turn him back?":
            savior "You are free to hope so...but I would like to tell you that it is a hopeless hope."
            jump u83
        "Can't you save me...?":
            savior "I will, whenever you're ready."
            jump u83
        "Sora has become different...and now it feels like he is dragging me to a trap.":
            savior "[name]... That trap is called paradise. You shouldn't think about getting out."
            jump u83
label u83:
    show R R33mask
    savior "It's better to be a more-or-less-lunatic than a too-good-for-your-own-good-and-that's-why-you-suffer."
    show R R35mask
    savior "Just like me. And that's the way we live in this place."
    show R R33mask
    savior "When will you ever stop acting like a good girl?"
    savior "You look like you're waiting for someone to bend and break you..."
    show R R32mask
    savior "I'm actually on the verge of tears. I feel so bad for you... But my tears would be nothing but a bane to you."
    show R R33mask
    savior "You should rather give up some portion of your sanity. Like Sora."
    savior "...Then salvation will naturally find you."
    savior "Or you can just remain nice and become everyone's prey."
    savior "The choice is yours, [name]."
    hide R with dissolve
    hide MC with dissolve
    stop music
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "Correct!"
    u "You have answered the call within the limited time. Well done, my toy."
    u "Hey, toy. I'm very frustrated because of so much work... Do something to entertain me."
    menu:
        "It would have been great if I could help you with hacking skills.":
            u "Help? How can you help with your moronic brain..."
            u "I said entertain me not blab on nonsense. Do you really lack comprehensive skills?"
            u "When I'm bored, I kill insects that crawl beneath our feet... It makes me feel better a bit."
            u "How about it, should I test this on you? Since you're the same as an insect to me."
            u "Don't try to be something you're not. How cocky of you."
            jump u84
        "What if I don't want to?":
            u "Who said you can ask questions? What a cocky princess."
            u "It's going to take forever to train you properly. Why that moron Ren choose an idiot like you?..."
            u "Well, he would have chosen someone similar to him."
            jump u84
label u84:
    u "Hey, I'll tell you a fun game. Listen up."
    u "I'll tell you an escape route."
    u "Whether you trust that or not is your choice."
    u "Now, I'll wait for your so pull out something and draw a map."
    menu:
        "I'm ready.":
            u "That's great but you've voice is really off. Talk quietly! It's making me sick."
            u "Even if you act obedient, I doubt I'll think good of you."
            jump u85
        "No.":
            u "Fine, that's your choice. Which ever, I'm going to toy with you.... If you rebel, just not the time I toy with you will last longer."
            u "If you're fine with whatever I do with you, do as you like...."
            u "If you've understood what I said and if you're not some pervert, I'd assume you would have pen and paper in front of you."
            jump u85
label u85:
    u "Listen up."
    u "There's a long corridor when you come out of your room."
    u "You know how there's a long corridor when you come out of your room."
    u "Take to your left and go straight. At the end, there will be a desk."
    u "If you look beside the desk, you'll see a number of Ao Me manuals. The third row from the top of the bookshelf, place two manuals. Then the bookshelf will start moving."
    u "When the bookshelf opens, there will be a staircase going down to the basement."
    u "...What, why aren't you answering me? Are you listening carefully?"
    menu:
        "Basement?":
            u "Who said you can ask questions?"
            u "If you take up such behavior... then prepare yourself."
            u "I'll look over just this once. Be thankful of it."
            jump u86
        "I'm drawing it while listening. Keep talking please.":
            u "I'm curious how awful that drawing would be."
            u "It would be fun ripping that apart right before your eyes when you're done. Haha!"
            jump u86
label u86:
    u "Anyway, follow the stairs and go down."
    u "If you continue stumbling down the dark stairs... then...."
    u "there will be a prison to hold you!"
    u "When you scream looking at that horrific prison, the door will slam and close. It won't open!"
    u "Then you'd be calling my name balling your eyes out showing your ugly face!"
    u "Hahahaha!"
    menu:
        "...You're mean.":
            u "What, did you really expect something? Did you really think I'll let you escape?"
            u "Hahahaha! How ridiculous of you. How can you be so stupid?"
            jump u87
        "Don't imprison me....":
            u "Then you'll have to get on my good side, don't you? Try to input that into your head, you moron."
            u "Make a sobbing face. How much of your brows can you crunch up? Hahaha!"
            jump u87
        "It might be better there.":
            u "Testing whether you mean that might also be fun, wouldn't it?"
            u "I'll imprison you in the two where you hate the most!"
            jump u87
label u87:
    u "When I get bored, I'll give you a visit. I'd like to see your idiotic face myself."
    u "Though, it'll only be to kill time."
    u "I'm hanging up."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "09:03" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music5.ogg"
    scene bg21 with dissolve
    show Y Y17 at my_left with dissolve
    show G G11 at my_right with dissolve
    y "If the RFA gets the commendation...I think it could help me to get a job. Hehe."
    show Y Y14
    y "Wouldn't it help you in heaps, too? You should mention it during your auditions and stuff. "
    show Y Y18
    y "I'd pick someone who's got a reward or two!"
    show G G12
    g "I don't know... I didn't join the RFA for such a noble purpose, so I'm not sure if I deserve an award."
    show Y Y13
    y "Oh, come on! You deserve it if you understand why Reina founded the RFA!"
    show Y Y11
    y "Uhm...but it feels like K and Zero don't want to get the commendation."
    g "There you go again with your suspicions train."
    y "But...this time it's different. "
    y "I'm not suspicious because K is frustrating me again. I think I know why he's not really happy with it."
    show G G13
    g "Hmm? What do you mean?"
    y "This is just a guess, but I think K and Zero don't want the commendation because of Zero's status...."
    g "Zero's status....?"
    show Y Y13
    y "He works at an agency, remember? Wait, but that agency doesn't make the agents change their statuses, does it? "
    y "Zero often mentioned it on messenger while making a joke."
    show G G11
    g "Hey, I've seen Zero's driver's license. There's nothing to suspect about his status. "
    g "Do you think that's really going to happen? This isn't a movie...."
    show Y Y11
    y "But you never know. "
    y "What if...what if someone from the government finds Zero to investigate on him?"
    show G G14
    g "Hey, you should go home if you're done eating. "
    show G G11
    g "I'm all ready for the rehearsal, so why do you still hang around?"
    show Y Y17
    y "Got it. I'm already all set to leave. Your house is so lame. It's no fun."
    show G G15
    g "Lame...? Why, you...!"
    show Y Y19
    y "There's nothing to eat and nothing to play with. Now I'm out!"
    show G G12
    g "You are going to classes, right?"
    show Y Y17
    y "I'm not telling you - haha!"
    hide Y with dissolve
    hide G with dissolve
    scene black with dissolve
    play sound "music/doorclose.ogg"
    "(Door closed.)"
    scene bg21 with dissolve
    show G G12 with dissolve
    g "Finally, Mr. Noisy Kim is gone."
    g "He burned my pot damn well. It's going to take some time to get the stain off."
    show G G16
    g "Now, let's get ready for the rehearsal, shall we?"
    show G G13
    play sound "music/dingdong.ogg"
    "(Ding-dong)"
    show G G12
    g "That idiot... He must have left something. And talk about all that bragging about being all set to go..."
    play sound "music/dooropen.ogg"
    "(Door opened.)"
    show G G11
    g "Okay, what did you leave behind?"
    show G G13
    stop music
    g "...Huh?"
    whooo "Are you Mr. Hyun Ryu?"
    show G G11
    g "Yes...that's me.... But who are you?"
    prosecutor "We're from the prosecution service. We would like to ask for your cooperation with the investigation."
    show G G13
    g "Pardon...?"
    stop music
    hide G with dissolve
    scene black with dissolve
    show text "11:16" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music12.ogg"
    scene bg8 with dissolve
    show S S210 with dissolve
    s "K, where are you? What's wrong with your phone?"
    show S S212
    s "The hacker is not the issue right now."
    s "We must delete every info connected to Shun. That's our top priority."
    s "And then I'll hack the data stored at the prosecution service...."
    s "I bet this is related to the prime minister."
    show S S217
    s "I can swear he's trying to extract something about me from Shun!"
    show S S214
    s "Got it. I'll relay everything to you once I find them."
    show S S212
    s "I'm not doing anything stupid.... For now."
    s "Sure. Yeah. Catch you later."
    play sound "music/beep.ogg"
    "(...)"
    s "Did I ever slip anything about me to the members of the RFA?"
    s "He'll get his hands on my photo. That's inevitable...."
    s "But I'm sure he doesn't know my address...or my birth name...."
    show S S213
    s "First I must delete every information in connection with Shun's phone from the messenger."
    s "I hope Shun didn't download any photo...."
    s "Should I hack his phone first?"
    show S S212
    s "Even if he learns my face....there's no way that can lead to my location."
    s "But once he finds out I'm affiliated with the RFA, the rest of the members can be summoned for 'investigation.'"
    show S S214
    s "Damn it...."
    show S S212
    s "I should call Hiroki and tell him to get ready...."
    s "...That's right. This was bound to happen."
    stop music
    hide S with dissolve
    scene black with dissolve
    show text "13:58" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music12.ogg"
    scene black with dissolve
    io "Thank you for your cooperation, Mr. Ryu."
    g "Umm...I'll be in one hot water if I don’t cooperate, won't I?"
    io "Well, we didn't issue any official summons for you, but..."
    io "Ahem. But anyway, don't you want to know why you're sitting here?"
    g "I do... Did I do something wrong?"
    io "No....Though the interrogations might tell otherwise."
    g "Interrogations?"
    io "I'm not sure if you're aware of this, but the act of hiding certain details of information and interrupting investigation is subject to appropriate punishments."
    io "You already signed a form that you will truthfully and faithfully cooperate, so I hope you keep your words."
    g "I have nothing to hide. So please, tell me why I'm here."
    io "We got a report...that there's a person among your acquaintances affiliated with illegal informational activities."
    g "Illegal informational activities...?"
    io "Do you have anything on your mind regarding this?"
    g "Uhm...there is this friend who's working at an agency, but it's nothing illegal... I think...."
    io "Could you tell me his name?"
    g "Wait a sec. Did he do something wrong?"
    io "You only need to answer my questions."
    io "Please tell the truth if you'd like to protect your friend."
    g "...Alright."
    io "So let me ask again - what's the name of this friend working at an agency?"
    g "...Levan. Levan Choi."
    io "How close are you to this friend?"
    g "Enough to say I cherish him."
    io "Has he ever confessed that he's committed illegal activity?"
    g "He did make jokes on it, but that's it. He was just joking."
    g "He's a trickster. He likes making jokes like that."
    io "What is his contact information? Where does he live?"
    g "I saved his contact info on my phone... But I don't know where he lives."
    io "You don't even know his address? Are you sure you're his friend?"
    g "...He's no ordinary friend. He only tells me what he wants to."
    io "I see.... Here, I brought your phone."
    io "Now show me Mr. Choi's contact information."
    g "Just a sec...."
    g "What the...?"
    io "What is it?"
    g "Something's wrong with my phone.... His contact info is gone."
    io "Are you sure? You didn't delete it, did you?"
    g "Why do you think I'd delete it?! Huh? What's wrong with this thing...? Oh! Wait."
    g "It's back! But...is this the number?"
    io "Jot it down here for me, please."
    g "Sure... Uhmm...here."
    io "I'll try calling right now."
    io "Is there anything you can share with me about Mr. Choi?"
    g "I do have his cosplay photos, but it could be a pain for your eyes...."
    io "I would like to see all related photos."
    g "Just a minute. I need to log on to the RFA app...."
    g "Huh...? It's not working."
    io "Is there a problem?"
    g "Uh...is there something wrong with the signal here? The app is not working. It says I'm an invalid user...."
    io "Signal seems fine. Are you trying to hide evidence on purpose?"
    io "I must tell you, you don't want to do that."
    g "Hey, do I look like I'm doing this on purpose?"
    g "Give me a minute. I must have downloaded some photos on my device...."
    g "Huh...? All of his pictures are gone..."
    io "...Mr. Choi is not picking up."
    g "Uhm...I think the rest of the photos on my phone just vanished on their own...."
    io "Do you have anything else to share other than his profile photos?"
    g "Just what on earth did he do? Can't you at least fill me in on that?"
    io "I'm afraid I cannot disclose the contents of the investigation. I'd like to ask for your unadulterated cooperation, Mr. Ryu."
    g "Hah.... Just a sec... Let me see what I've got... Oh, here. Is this going to help?"
    g "It's...a truck."
    g "I saved it because it was just so damn ridiculous.... Convenience stores started to sell this drink he made."
    g "Another friend of mine saw that, and he was so flustered and shared this with me."
    io "...Vehicle number... 0000...."
    g "...How long is this going to take again? Haaa..."
    stop music
    scene black with dissolve
    "(....)"
    "(...)"
    "(..)"
    play music "music/music2.ogg"
    v "...Levan."
    scene bg22 with dissolve
    show V V13 with dissolve
    v "I see. So your father was behind the interrogations after all. I'm concerned about Hyun...."
    show V V13
    v "You didn't get any contact yet, did you?"
    show V V13
    v "Then that would mean they didn't find anything about you from Hyun yet. Which means your elimination of information was spotless."
    show V V13
    v "Wait... Please don't leave the RFA! Not yet...!"
    show V V19
    v "I won't stop you from preparing to leave. But just don't hurry... Not yet."
    show V V119
    v "...I know you have a lot troubling your mind, enough to make this issue with the hacker seem insignificant..."
    show V V17
    v "And you must be worried about your brother in particular...."
    v "For now...please leave it to me. I'm...on my way to check on Sora."
    show V V19
    v "...There's no need to thank me, Levan. It's just that... Haa..."
    show V V119
    v "Did you check the files I gave you?"
    show V V13
    v "You didn't? Make sure you do. I collected some intel on my own... I'm sure they can help you."
    v "There are lots of things missing...but you'll do a much better job filling in the empty spots."
    v "It's up to you to use them or not."
    v "Sure. I'll call again. Let's talk later."
    play sound "music/beep.ogg"
    show V V119
    v "Levan... I've never seen you so scared."
    show V V13
    v "That's right. He was terribly scared of his father since the moment we met."
    v "And I cannot question him...for fearing that man."
    v "The only thing I can do would be to protect him to the best of my abilities."
    show V V17
    v "He might as well leave RFA tomorrow or a day after that."
    v "One of us is undergoing interrogations....and something is not right with our newcomer."
    v "Based on her responses...something is definitely not right with the Mint Eye as well."
    v "I'm also worried that Sora's attack pattern changed..."
    show V V13
    v "I should see for myself what happened...."
    show V V17
    v "This is all our fault... This is all Reina and my fault."
    show V V13
    v "I should not just watch more innocent people made victim to this... So I should...cancel the party."
    v "Reina... I thought I can still save you, so I originally planned to play along with your plan...."
    show V V110
    v "But after witnessing how you made Sora your puppet..."
    show V V13
    v "I...I couldn't stop thinking that I'm no different from you."
    show V V13
    v "This stops now. I cannot...cause any more harm to people who have nothing to do with our relationship."
    stop music
    hide V with dissolve
    scene black with dissolve
    show text "16:04" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    show MC MC15 at my_right with dissolve
    play sound "music/dooropen.ogg"
    "(Door Opened)"
    play music "music/music19.ogg"
    show U U24 at my_left with dissolve
    u "You're not going to stop whining oh-so-nobly for that airhead just because you starved for a day, are you?"
    u "If you give up so soon, what will that do to that poor loser...?"
    show U U213
    menu:
        "...I believe he'll be back.":
            u "[name]...my precious. I'll do anything, so please don't leave me...."
            u "Haha...how stupid. Nobody would leave him if he behaves like this."
            u "I bet he wanted to lock you up like this from the beginning."
            u "He was just too much of a chicken to actually do that."
            jump u88
        "I don't deserve to make any thought.":
            u "You realized it now? You're an airhead. You're too stupid to do anything."
            u "And I'm going to treat you just right. Because unlike you, I'm extremely smart."
            u "Don't you dare dream about running away from here."
            u "Unless you want me to make you tremble like your life depends on it."
            jump u88
        "Let me go! I want to get out of here!":
            u "You will. Someday. After everything you've seen and heard at this place is all wiped clean."
            u "Do you hate me and think I'm horrible? Haha. You should."
            u "You were nervous which footsteps would be mine, weren't you?"
            u "Maybe I should tell them to step on it every time they walk."
            u "I would love to see you being too nervous to sleep."
            jump u88
label u88:
    show U U26
    u "The RFA's attack is so lame, and that really ticks me off."
    u "My savior told me it's not time yet, so I'm refraining from showing that one final blast...."
    show U U25
    u "But why are they slacking off?"
    show U U26
    u "I don't feel like doing this. And I feel so, so ticked off."
    u "Do I look like a joke to them? Who do they think they are?"
    show U U25
    menu:
        "I...think you're amazing.":
            u "Then go and tell that to them, you idiot! What are you waiting for?"
            u "This wouldn't have happened if you did your job right...!"
            jump u89
        "I think...you're overly interpreting.":
            u "Who said you could talk like that? I never told you that you can."
            u "Is your mouth the problem? Or is it your head...?"
            jump u89
label u89:
    u "Don't step back from me. I never told you that you can."
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    u "Now, do you regret it? You're the one who decided to step into this strange place."
    u "Your curiosity brought you here."
    scene bg5 with dissolve
    u "You're my prey. I got you in my hands."
    u "You're too lazy and stupid. That's why you let me get you."
    u "The world gave you a choice, and you chose wrong. Like an idiot."
    u "You don't think you've been living a good life, do you?"
    u "Haha....how very arrogant. You don't deserve to believe that."
    u "I can see at least hundreds of things you ruined..."
    u "You're nothing but a ball of faults. And you thought I'd never see that."
    u "Just because you did a couple good things doesn't mean they'll hide your disasters."
    u "You thought they'd be gone if no one tells me about them, didn't you? What an airhead."
    u "Trying to hide and avoid and give excuses for your faults is a foul trick."
    u "Such a foul person doesn't deserve to do what they want."
    u "You already know how deep your stupidity is taking you to your abyss."
    u "...But actually, the pits of darkness under the very base of your abyss is where you deserve to be."
    u "That's just how worthy you are. What? Surprised? You've been thinking you're greater than that."
    menu:
        "Ren...! Please save me!":
            u "You're the worst. You think you're something, and you think you're a princess. Who in the world would ever like you?"
            u "No one will ever like you. No one...! Hahaha...!"
            jump u810
        "I don't deserve to say no.":
            u "That's right. You don't deserve to move."
            u "You don't deserve to say what you think."
            u "You don't deserve to rebel."
            u "So zip it. Don't move. Don't think."
            jump u810
label u810:
    u "I'll tell you a game that you deserve."
    u "From now on you can say"
    u "only one thing to me - 'yes.'"
    u "Now say it."
    menu:
        "....":
            u "That's too small. I can't hear you... What? I can't hear you."
            u "I said I can't hear you!"
            u "How stupid can you get...?"
            jump u811
        "Yes....":
            u "What's wrong with your voice? It feels like my eardrums are bleeding. You should be ashamed of yourself."
            jump u811
label u811:
    u "You're nothing but a ball of arrogance. But you don't even deserve to be cleansed."
    u "You're an airhead. You don't even deserve to rebel. Do you get it?"
    menu:
        "Yes...":
            u "You should be thanking me. I'm teaching you your place."
            jump u812
        "...":
            u "All your hopes will turn out useless. Only hell awaits you."
            jump u812
label u812:
    u "Remember what I told you."
    u "You're good for nothing. So you should at least keep me entertained."
    menu:
        "No! Ren!":
            u "...Ugh, I should just mask your mouth or something. I'll listen to your voice after you're tamed."
            jump u813
        "Yes.":
            u "I think it'd be even better if you add a certain honorific term to my name."
            jump u813
label u813:
    u "Didn't I tell you I hate the way you smell?"
    u "You still smell like you do."
    u "Don't tell me you don't even know how to wash properly."
    u "You didn't just spray water like you've done so far, did you?"
    u "I'll tell you. I'll tell you how and how much you need to wash before you see me...."
    play sound "music/knock.ogg"
    "(Knock knock)"
    u "What the... Don't anyone dare interrupt me!"
    play sound "music/knock.ogg"
    "(Knock knock)"
    scene bg5 with dissolve
    show MC MC17 at my_right with dissolve
    show U U27 at my_left with dissolve
    u "...Consider yourself lucky."
    u "No, scratch that. I'll just double the lesson next time we meet."
    show U U27
    u "Get in here."
    believer "...Mr. Ren, the savior..."
    show U U25
    u "...What was that?"
    show U U23
    believer "I, I mean, Mr. Sora. The savior is looking for you."
    show U U25
    u "How dare an airhead like you speak about her? You don't even remember my name. I should run through the system at this place."
    believer "My apologies..."
    show U U27
    u "Once you find yourself in that room below, you'll realize there was no need to tell me that."
    stop music
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    scene bg5 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "[name], this is K."
    menu:
        "K... this is tough.":
            v "Did something happen?"
            jump u814
        "K, were you worried of me?":
            v "Yes. I was very worried. Worried if something happened..."
            v "...Are  you okay? You don't sound well."
            jump u814
label u814:
    v "If it's okay, you can tell me."
    menu:
        "...It's nothing.":
            v "Really?"
            v "[name]... tell me the truth."
            v "If your saying that to protect someone, then the more you have to..."
            jump u815
        "The thing is... Sora changed. He keeps harassing me!":
            v "...Is that true?"
            v "Deep down... I've speculated that. When I saw him at a distance, his looks weighed on my mind."
            v "I thought his ambiance changed fairly alot but.... I thought that was due changes in his apparel."
            v "I knew there was a reason behind his violent behavior. But..."
            v "Just as speculated..."
            jump u815
label u815:
    v "If it's difficult to discuss over the phone... hang on for a bit."
    v "I'm almost there, nearby."
    v "Let's talk about the details when we meet in person."
    v "I actually called as I was worried about holding the party."
    menu:
        "If the party is canceled, I might be eliminated from here.":
            v "Figured... My first feeling that you'd fall in danger when the party is canceled... was true."
            v "I'm sorry for things turning out like this. But please remember that the party is still on hold."
            v "You shouldn't fall in danger... never."
            jump u816
        "Would you have made that decision if you were truly worried of me? What are you going to do if I do fall in danger?":
            v "Does your safety and holding the party related like how I first thought?"
            v "...I'm sorry. I wanted to proceed after securing your safety to the maximum first."
            v "That's the reason why I said to put on hold... but seems they've showed an extreme reaction."
            v "... I can't do nothing and just watch you fall in danger."
            v "[name]... I'll do everything to prevent that you don't."
            jump u816
label u816:
    v "I've already obtained the believer's uniform and ID card to infiltrate in there."
    v "I still need to do more preparations but... it'll be very soon."
    v "I'm going to do my everything to protect my loved ones. In that pool of loved ones, you're also included."
    v "I want you to think about the ones precious to you to stay strong and hang tight."
    v "Well then..."
    v "We'll be in touch again."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "20:48" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    call phone_start from _call_phone_start_22
    call message_start("...", "...") from _call_message_start_343
    call message("Ryou", "Not to mention....") from _call_message_2712
    call message("Ryou", "though this is just a personal impression...") from _call_message_2713
    call message("Ryou", "[name]....") from _call_message_2714
    call message("Ryou", "I find your attitude") from _call_message_2715
    call message("Ryou", "a little bothering as of late.") from _call_message_2716
    call message("Ryou", "I did have such impression at your introduction") from _call_message_2717
    call message("Ryou", "but it feels like") from _call_message_2718
    call message("Ryou", "you are") from _call_message_2719
    call message("Ryou", "being controlled") from _call_message_2720
    call message("Ryou", "by someone else....") from _call_message_2721
    call screen phone_reply("I need to do as they tell me....", "cd824", "No one's controlling me. I'm only staying here because I wish to see someone again....", "cd825")
label cd824:
    call phone_after_menu from _call_phone_after_menu_323
    call message_start("[name]", "I need to do as they tell me....") from _call_message_start_344
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ryou", "[name]....") from _call_message_2722
    call message("Ryou", "I would like you") from _call_message_2723
    call message("Ryou", "to please tell us the truth") from _call_message_2724
    call message("Ryou", "Are you...") from _call_message_2725
    jump am810
label cd825:
    call phone_after_menu from _call_phone_after_menu_324
    call message_start("[name]", "No one's controlling me. I'm only staying here because I wish to see someone again....") from _call_message_start_345
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ryou", "See someone?") from _call_message_2726
    call message("Ryou", "Who is this someone?") from _call_message_2727
    call message("Ryou", "And where are you staying?") from _call_message_2728
    call message("Ryou", "I'd like you to tell us the truth....") from _call_message_2729
    call message("Ryou", "Oh") from _call_message_2730
    call message("Ryou", "perhaps you're...") from _call_message_2731
    jump am810
label am810:
    play sound "music/hack.ogg"
    call message("SYSTEM", "Ryou has left the chatroom.") from _call_message_2732
    call message("SYSTEM", "Sora has entered the chatroom.") from _call_message_2733
    play music "music/music19.ogg"
    call message("Sora", "[name]") from _call_message_2734
    call message_img("Sora", "What kind of stupid discussion are you having?", "images/questioning.png") from _call_message_img_164
    call message("Sora", "They're not even going to have the party.") from _call_message_2735
    call message("Sora", "Are you asking for help") from _call_message_2736
    call message("Sora", "to save yourself?") from _call_message_2737
    call message("Sora", "You didn't even do your job right") from _call_message_2738
    call message("Sora", "but you're breathing") from _call_message_2739
    call message("Sora", "so shamelessly.") from _call_message_2740
    call message("Sora", "I'm starting to feel bad for the air....") from _call_message_2741
    call screen phone_reply("I'm sorry....", "cd826", "The party was postponed because of the prosecution interrogation.", "cd827")
label cd826:
    call phone_after_menu from _call_phone_after_menu_325
    call message_start("[name]", "I'm sorry....") from _call_message_start_346
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "That's right.") from _call_message_2742
    call message("Sora", "This is all your fault.") from _call_message_2743
    call message("Sora", "Prosecution or whatever,") from _call_message_2744
    call message("Sora", "in the end") from _call_message_2745
    call message("Sora", "they might not hold a party.") from _call_message_2746
    call message("Sora", "And it's all your fault.") from _call_message_2747
    call message_img("Sora", "I'm going to let off some steam on you.", "images/well.png") from _call_message_img_165
    jump am811
label cd827:
    call phone_after_menu from _call_phone_after_menu_326
    call message_start("[name]", "The party was postponed because of the prosecution interrogation.") from _call_message_start_347
    call message("Sora", "Princess") from _call_message_2748
    call message("Sora", "are you playing detective or something?") from _call_message_2749
    call message("Sora", "So you want me to let off some steam") from _call_message_2750
    call message_img("Sora", "on the guys from the prosecution?", "images/questioning.png") from _call_message_img_166
    call message("Sora", "Seriously") from _call_message_2751
    call message("Sora", "you're such an airhead.") from _call_message_2752
    jump am811
label am811:
    call message("Sora", "Do you know that my savior found out") from _call_message_2753
    call message("Sora", "that the RFA is postponing the party?") from _call_message_2754
    call message("Sora", "And now") from _call_message_2755
    call message("Sora", "she's more than just angry.") from _call_message_2756
    call message("Sora", "She looked like") from _call_message_2757
    call message_img("Sora", "she's ready to kick you out any minute.", "images/expecting.png") from _call_message_img_167
    call message("Sora", "Do you know") from _call_message_2758
    call message("Sora", "what I told my savior?") from _call_message_2759
    call message("Sora", "I told her") from _call_message_2760
    call message("Sora", "that you're arrogant") from _call_message_2761
    call message("Sora", "and stupid") from _call_message_2762
    call message_img("Sora", "so you don't deserve this mission.", "images/expecting.png") from _call_message_img_168
    call message("Sora", "You kept telling Ren something like") from _call_message_2763
    call message("Sora", "You can do it -'") from _call_message_2764
    call message("Sora", "Look at the future waiting for you -'") from _call_message_2765
    call message("Sora", "and all this rubbish") from _call_message_2766
    call message("Sora", "so you ended up") from _call_message_2767
    call message("Sora", "ruining the project.") from _call_message_2768
    call screen phone_reply("Ren worked harder than anyone else...! He has never ruined things. Not even once.", "cd828", "Am I useless now?", "cd829")
label cd828:
    call phone_after_menu from _call_phone_after_menu_327
    call message_start("[name]"," Ren worked harder than anyone else...! He has never ruined things. Not even once.") from _call_message_start_348
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Are you seriously") from _call_message_2769
    call message_img("Sora", "defending someone who doesn't exist at all?", "images/questioning.png") from _call_message_img_169
    call message("Sora", "Wow -") from _call_message_2770
    call message("Sora", "that's so useless.") from _call_message_2771
    call message_img("Sora", "How stupid can you get?", "images/smile.png") from _call_message_img_170
    call message("Sora", "???") from _call_message_2772
    call message("Sora", "Now I'm really starting to wonder...") from _call_message_2773
    jump am812
label cd829:
    call phone_after_menu from _call_phone_after_menu_328
    call message_start("[name]", "Am I useless now?") from _call_message_start_349
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Yes.") from _call_message_2774
    call message("Sora", "You were useless from the start.") from _call_message_2775
    call message("Sora", "But at least you had an excuse to stay here haha.") from _call_message_2776
    call message("Sora", "But now") from _call_message_2777
    call message_img("Sora", "you don't even have an excuse -", "images/smile.png") from _call_message_img_171
    jump am812
label am812:
    call message("Sora", "I told her") from _call_message_2778
    call message("Sora", "that we never should've let you be part of this project.") from _call_message_2779
    call message("Sora", "I'm not wrong. I never am.") from _call_message_2780
    call message("Sora", "And now") from _call_message_2781
    call message("Sora", "as long as I get her the info she wants") from _call_message_2782
    call message("Sora", "I can do whatever I want with you") from _call_message_2783
    call message("Sora", "and whatever I want with the messenger.") from _call_message_2784
    call message("Sora", "Now I get to decide whether to throw you out or not.") from _call_message_2785
    call message("Sora", "I'm sure") from _call_message_2786
    call message("Sora", "Ren told you something like this....") from _call_message_2787
    call message("Sora", "Please don't leave me...!") from _call_message_2788
    call message("Sora", "And now") from _call_message_2789
    call message_img("Sora", "it's your turn to whine.", "images/smile.png") from _call_message_img_172
    call message("Sora", "So") from _call_message_2790
    call message("Sora", "try typing it") from _call_message_2791
    call message("Sora", "right now hehehe.") from _call_message_2792
    call message("Sora", "Tell me not to throw you away.") from _call_message_2793
    call screen phone_reply3("Please don't throw me away...", "cd830", "...I know Ren is still there in you.", "cd831", "Do you feel better if I play along?", "cd832")
label cd830:
    call phone_after_menu from _call_phone_after_menu_329
    call message_start("[name]"," Please don't throw me away...") from _call_message_start_350
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Hehehehehe") from _call_message_2794
    call message("Sora", "do you think you can walk out of here on your own") from _call_message_2795
    call message("Sora", "once I throw you out?") from _call_message_2796
    call message("Sora", "If you don't want me to throw you out") from _call_message_2797
    call message_img("Sora", "you should obey everything I tell you.", "images/smile.png") from _call_message_img_173
    jump am813
label cd831:
    call phone_after_menu from _call_phone_after_menu_330
    call message_start("[name]", "...I know Ren is still there in you.") from _call_message_start_351
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Whatever.") from _call_message_2798
    call message("Sora", "That's nothing but your imaginations.") from _call_message_2799
    call message("Sora", "Now I don't feel the need") from _call_message_2800
    call message_img("Sora", "to say something about that.", "images/well.png") from _call_message_img_174
    jump am813
label cd832:
    call phone_after_menu from _call_phone_after_menu_331
    call message_start("[name]", "Do you feel better if I play along?") from _call_message_start_352
    call message("Sora", "Wow -") from _call_message_2801
    call message("Sora", "did you actually say that?") from _call_message_2802
    call message("Sora", "You're pretty brave.") from _call_message_2803
    call message("Sora", "And regarding your bravery...") from _call_message_2804
    call message("Sora", "I'm going to check very soon") from _call_message_2805
    call message("Sora", "whether that was a bluff or not.") from _call_message_2806
    call message_img("Sora", "Just you wait ^^", "images/expecting.png") from _call_message_img_175
    jump am813
label am813:
    call message("Sora", "From now on") from _call_message_2807
    call message("Sora", "you're not allowed to chat") from _call_message_2808
    call message("Sora", "with the RFA.") from _call_message_2809
    call message("Sora", "You're not smart enough") from _call_message_2810
    call message("Sora", "to talk to them and gain info on them.") from _call_message_2811
    call screen phone_reply("My connection with the members was suddenly gone. You mean it'll stay gone?", "cd833", "Okay... What do you want me to do?", "cd834")
label cd833:
    call phone_after_menu from _call_phone_after_menu_332
    call message_start("[name]", "My connection with the members was suddenly gone. You mean it'll stay gone?") from _call_message_start_353
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "What's wrong?") from _call_message_2812
    call message("Sora", "Do you miss them? Hahahaha.") from _call_message_2813
    call message("Sora", "Don't worry.") from _call_message_2814
    jump am814
label cd834:
    call phone_after_menu from _call_phone_after_menu_333
    call message_start("[name]", "Okay... What do you want me to do?") from _call_message_start_354
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "So you admit that you're not smart enough, huh?") from _call_message_2815
    call message("Sora", "Ugh....") from _call_message_2816
    jump am814
label am814:
    call message("Sora", "From now on") from _call_message_2817
    call message("Sora", "once you see a chatroom open") from _call_message_2818
    call message("Sora", "you should log in right away") from _call_message_2819
    call message_img("Sora", "and entertain me hehe.", "images/smile.png") from _call_message_img_176
    call message("Sora", "And if you procrastinate again....") from _call_message_2820
    call message("Sora", "you don't want to know what will happen.") from _call_message_2821
    call message("Sora", "People say") from _call_message_2822
    call message("Sora", "the person who knows what cruelty is") from _call_message_2823
    call message("Sora", "the victim of the cruelty, right?") from _call_message_2824
    call message("Sora", "So...") from _call_message_2825
    call message("Sora", "I can show you....") from _call_message_2826
    call message("Sora", "one heck of hell....") from _call_message_2827
    call message("Sora", "painful enough") from _call_message_2828
    call message_img("Sora", "to make you beg for your death....", "images/expecting.png") from _call_message_img_177
    call message("Sora", "....") from _call_message_2829
    call message("Sora", "......") from _call_message_2830
    call message("Sora", "[name]") from _call_message_2831
    call message("Sora", "How do you feel") from _call_message_2832
    call message("Sora", "now that you can't laugh or chat with the RFA?") from _call_message_2833
    call screen phone_reply3("They must be worried about me... I'm worried.", "cd835", "I'm going to watch whether you can entertain me.", "cd836", "...I can only do what I'm told to, anyways.", "cd837")
label cd835:
    call phone_after_menu from _call_phone_after_menu_334
    call message_start("[name]", "They must be worried about me... I'm worried.") from _call_message_start_355
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "You're worried about someone else in this situation...?") from _call_message_2834
    call message("Sora", "Looks like you didn't suffer enough.. ^^") from _call_message_2835
    call message("Sora", "Should I take that as") from _call_message_2836
    call message("Sora", "Yes I want to suffer some more?' haha") from _call_message_2837
    jump am815
label cd836:
    call phone_after_menu from _call_phone_after_menu_335
    call message_start("[name]", "I'm going to watch whether you can entertain me.") from _call_message_start_356
    call message("Sora", "Yes") from _call_message_2838
    call message("Sora", "your highness.") from _call_message_2839
    call message("Sora", "^^") from _call_message_2840
    call message_img("Sora", "...Did you expect me to say that?", "images/questioning.png") from _call_message_img_178
    call message("Sora", "Now I think") from _call_message_2841
    call message("Sora", "you're getting delirious haha") from _call_message_2842
    call message("Sora", "You're seriously delirious haha.") from _call_message_2843
    jump am815
label cd837:
    call phone_after_menu from _call_phone_after_menu_336
    call message_start("[name]", "...I can only do what I'm told to, anyways.") from _call_message_start_357
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Sure") from _call_message_2844
    call message("Sora", "you think you can forgive yourself") from _call_message_2845
    call message("Sora", "no matter how stupid you are") from _call_message_2846
    call message_img("Sora", "if you treat yourself like a puppet?", "images/smile.png") from _call_message_img_179
    call message("Sora", "hehehehehehe") from _call_message_2847
    call message("Sora", "You're such an airhead hehehehehe") from _call_message_2848
    jump am815
label am815:
    call message("Sora", "Oh") from _call_message_2849
    call message("Sora", "I have an idea.") from _call_message_2850
    call message("Sora", "I wanna see that face of yours 24/7") from _call_message_2851
    call message("Sora", "so") from _call_message_2852
    call message("Sora", "I'm going to make them install a camera in your room -") from _call_message_2853
    call message("Sora", "I'll be watching you") from _call_message_2854
    call message("Sora", "so, at least try to pretend to be smart, will you?") from _call_message_2855
    call message("Sora", "Oh") from _call_message_2856
    call message("Sora", "one more thing.") from _call_message_2857
    call message("Sora", "do something about that smell in your room") from _call_message_2858
    call message("Sora", "until I see you again...") from _call_message_2859
    call message("Sora", "I'm sure I made myself") from _call_message_2860
    call message_img("Sora", "crystal clear.", "images/expecting.png") from _call_message_img_180
    call message("Sora", "So") from _call_message_2861
    call message("Sora", "look forward") from _call_message_2862
    call message("Sora", "to my return -") from _call_message_2863
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_2864
    call phone_end from _call_phone_end_23
    stop music
    show MC MC15 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Zero is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    play sound "music/break.ogg"
    s "Hello?"
    s "Huh... what's wrong with this?"
    s "Hello? [name]?"
    menu:
        "Zero! Help me! Contact K!":
            s "Sorry? What?"
            s "I can't hear you. Can you say that again?"
            jump u817
        "I've lost contact with the RFA members! Please let me get in touch!":
            s "I can't hear what you're saying! The static is really bad!"
            s "Uh... What is wrong with this..."
            jump u817
label u817:
    menu:
        "Ph.D.Pepper!!! I love you!!! Can't you hear me yet?":
            s "...What? Pepper... you ate? Oh was your dinner menu related to pepper?"
            s "Umm... I don't know why we're talking of dinner menus suddenly but.... hearing that, I'm getting hungry."
            s "Should we order something that's spiced up with pepper?"
            s "Uh... It's really frustrating to hear only short bits of what you're saying. Can't you go somewhere with better reception?"
            jump u818
        "Can you hear me? Tell K that we must have the party! If not, I....":
            s "What? Te....? Tell?"
            s "....Tell what to ... to who?"
            s "This can't do. I can't understand anything that you're saying."
            jump u818
label u818:
    s "Can you hear me?"
    s "I think this call is being jammed! Disrupted!"
    s "...*Sigh*.. Can you hear me... or not...?"
    s "A superbly skilled person who can cause disruption to the messenger I've created..."
    s "I don't know if you can hear me but I'll say it anyway!"
    s "I think someone's trying to jam our messenger!"
    s "I'll look into this! So even if we can't be reached, don't worry too much but calm down and wait. Got it?"
    s "I'll have to look into this right now, so I'm hanging up now!"
    s "Take care! You must!"
    s "Then... hang in there."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "22:14" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music19.ogg"
    call phone_start from _call_phone_start_23
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_358
    call message("Sora", "Hey") from _call_message_2865
    call message("Sora", "I'm bored.") from _call_message_2866
    call message("Sora", "What should we play?") from _call_message_2867
    call screen phone_reply3("....", "cd838", "Role play.", "cd839", "How's everything going at the RFA?", "cd840")
label cd838:
    call phone_after_menu from _call_phone_after_menu_337
    call message_start("[name]", "....") from _call_message_start_359
    call message("Sora", "You're not answering me.") from _call_message_2868
    call message("Sora", "Answer me.") from _call_message_2869
    call message("Sora", "Why") from _call_message_2870
    call message("Sora", "are you") from _call_message_2871
    call message("Sora", "not answering me?") from _call_message_2872
    call message_img("Sora", "Are you broken?", "images/questioning.png") from _call_message_img_181
    call message("Sora", "You're a toy") from _call_message_2873
    call message("Sora", "but you had to be useless...") from _call_message_2874
    call message("Sora", "Hmm") from _call_message_2875
    call message_img("Sora", "is it because I'm not that nice weakling Ren? ^^", "images/expecting.png") from _call_message_img_182
    call message("Sora", "In that case....") from _call_message_2876
    jump am816
label cd839:
    call phone_after_menu from _call_phone_after_menu_338
    call message_start("[name]", "Role play.") from _call_message_start_360
    call message("Sora", "Role play?") from _call_message_2877
    call message("Sora", "hehehehehe") from _call_message_2878
    call message("Sora", "ok") from _call_message_2879
    call message_img("Sora", "let's see if your imaginations are useful.", "images/expecting.png") from _call_message_img_183
    call message("Sora", "Since it's been a while") from _call_message_2880
    call message("Sora", "I'll be nice and...") from _call_message_2881
    jump am816
label cd840:
    call phone_after_menu from _call_phone_after_menu_339
    call message_start("[name]", H"ow's everything going at the RFA?") from _call_message_start_361
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Sora", "Hahahahaha") from _call_message_2882
    call message("Sora", "they're having a party") from _call_message_2883
    call message("Sora", "saying that they didn't need you in the first place!") from _call_message_2884
    call message_img("Sora", "^^", "images/smile.png") from _call_message_img_184
    call message("Sora", "Ugh") from _call_message_2885
    call message("Sora", "I'm bored") from _call_message_2886
    call message("Sora", "so I'll specially") from _call_message_2887
    call message("Sora", "play with you...") from _call_message_2888
    jump am816
label am816:
    call message_img("Sora", "I'll play Ren for you - !", "images/smile.png") from _call_message_img_185
    call message("Sora", "It's a piece of cake") from _call_message_2889
    call message("Sora", "to pretend to be him.") from _call_message_2890
    call message_img("Ren", "Tada!!!", "images/cg39.png") from _call_message_img_186
    call screen phone_reply3("Please don't do it....", "cd841", "Hello...Ren.", "cd842", "Sora, you can never be Ren...", "cd843")
label cd841:
    call phone_after_menu from _call_phone_after_menu_340
    call message_start("[name]", "Please don't do it....") from _call_message_start_362
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "I'm Ren....") from _call_message_2891
    jump am817
label cd842:
    call phone_after_menu from _call_phone_after_menu_341
    call message_start("[name]", "Hello...Ren.") from _call_message_start_363
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Hi -") from _call_message_2892
    call message_img("Ren", "I'm Ren....", "images/smile.png") from _call_message_img_187
    jump am817
label cd843:
    call phone_after_menu from _call_phone_after_menu_342
    call message_start("[name]", "Sora, you can never be Ren...") from _call_message_start_364
    call message("Ren", "Hi!") from _call_message_2893
    call message("Ren", "I'm Ren....") from _call_message_2894
    call message("Ren", "I can never be Sora...") from _call_message_2895
    call message("Ren", "Sora is much stronger...") from _call_message_2896
    call message("Ren", "and I'm a useless airhead good for nothing....") from _call_message_2897
    jump am817
label am817:
    call message("Ren", "I'm a sweet innocent boy who loves flowers!") from _call_message_2898
    call message("Ren", "And I...") from _call_message_2899
    call message("Ren", "missed you so, so much") from _call_message_2900
    call message("Ren", "[name]...!") from _call_message_2901
    call message("Ren", "I was so scared") from _call_message_2902
    call message("Ren", "that I will be trapped in this body") from _call_message_2903
    call message_img("Ren", "and disappear.", "images/sad.png") from _call_message_img_188
    call message("Ren", "But since") from _call_message_2904
    call message("Ren", "you still like me") from _call_message_2905
    call message("Ren", "I thought that") from _call_message_2906
    call message("Ren", "I shouldn't be gone....") from _call_message_2907
    call message("Ren", "[name]...") from _call_message_2908
    call message("Ren", "I miss you.") from _call_message_2909
    call message_img("Ren", "Where are you?", "images/sad.png") from _call_message_img_189
    call screen phone_reply3("I miss you too....", "cd844", "Please stop....", "cd845", "Sora and I are doing well...", "cd846")
label cd844:
    call phone_after_menu from _call_phone_after_menu_343
    call message_start("[name]", "I miss you too....") from _call_message_start_365
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Me too...") from _call_message_2910
    call message("Ren", "I...") from _call_message_2911
    call message("Ren", "I couldn't even eat....") from _call_message_2912
    call message("Ren", "Please come save me.") from _call_message_2913
    call message("Ren", "It's too cold and scary here...") from _call_message_2914
    jump am818
label cd845:
    call phone_after_menu from _call_phone_after_menu_344
    call message_start("[name]", "Please stop....") from _call_message_start_366
    call message("Ren", "My heart hurts so much") from _call_message_2915
    call message("Ren", "to see you suffering....") from _call_message_2916
    call message("Ren", "I hate myself...!") from _call_message_2917
    jump am818
label cd846:
    call phone_after_menu from _call_phone_after_menu_345
    call message_start("[name]", "Sora and I are doing well...") from _call_message_start_367
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Really...?") from _call_message_2918
    call message("Ren", "Are you hurt? Tell me you're not hurt.") from _call_message_2919
    call message("Ren", "But...") from _call_message_2920
    call message("Ren", "don't you need me anymore...?") from _call_message_2921
    jump am818
label am818:
    call message_img("Ren", "I try to think of what you looked like", "images/sad.png") from _call_message_img_190
    call message("Ren", "but your face is growing faint.") from _call_message_2922
    call message("Ren", "It must be because I'm an airhead.") from _call_message_2923
    call message("Ren", "I was so happy") from _call_message_2924
    call message("Ren", "when you held my hand....") from _call_message_2925
    call screen phone_reply3("I remember everything....", "cd847", "Even if I remember, there will be nothing but heartache....", "cd848", "I don't remember them very well...", "cd849")
label cd847:
    call phone_after_menu from _call_phone_after_menu_346
    call message_start("[name]", "I remember everything.... I wish we could go back in time...") from _call_message_start_368
    call message("Ren", "Yes...") from _call_message_2926
    call message("Ren", "I wished to stay with you a little longer") from _call_message_2927
    call message("Ren", "with just the two of us....") from _call_message_2928
    call message("Ren", "It was so sad we couldn't....") from _call_message_2929
    call message("Ren", "I didn't even see you a lot, did I?") from _call_message_2930
    call message("Ren", "I'm so sorry....") from _call_message_2931
    call message("Ren", "But") from _call_message_2932
    call message("Ren", "you know what...?") from _call_message_2933
    jump am819
label cd848:
    call phone_after_menu from _call_phone_after_menu_347
    call message_start("[name]", "Even if I remember, there will be nothing but heartache.... Because you're not Ren.") from _call_message_start_369
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Remember the day...") from _call_message_2934
    call message("Ren", "when you so generously enjoyed") from _call_message_2935
    call message("Ren", "the breakfast I made for you?") from _call_message_2936
    call message_img("Ren", "I was so happy.", "images/wink.png") from _call_message_img_191
    call message("Ren", "I was so happy to chat with you") from _call_message_2937
    call message("Ren", "walk with you") from _call_message_2938
    call message("Ren", "and everything.....") from _call_message_2939
    call message_img("Ren", "I treasure all of those memories....", "images/smile.png") from _call_message_img_192
    call message("Ren", "But....") from _call_message_2940
    jump am819
label cd849:
    call phone_after_menu from _call_phone_after_menu_348
    call message_start("[name]", "I don't remember them very well... I must have forgotten because I'm an airhead.") from _call_message_start_370
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I see...") from _call_message_2941
    call message("Ren", "I thought") from _call_message_2942
    call message("Ren", "all of our memories together") from _call_message_2943
    call message("Ren", "are so precious...") from _call_message_2944
    call message("Ren", "But they're all going away...!") from _call_message_2945
    call message("Ren", "Maybe...") from _call_message_2946
    jump am819
label am819:
    call message("Ren", "To be honest....") from _call_message_2947
    call message("Ren", "I...") from _call_message_2948
    call message("Ren", "I think I didn't really need you to stay here....") from _call_message_2949
    call message("Ren", "I'm...") from _call_message_2950
    call message("Ren", "I'm nothing....") from _call_message_2951
    call message("Ren", "nothing") from _call_message_2952
    call message("Ren", "but a fool and an airhead...") from _call_message_2953
    call message("Ren", "always tormented since childhood.") from _call_message_2954
    call message("Ren", "So I was lonely. That's all....") from _call_message_2955
    call message("Ren", "Actually") from _call_message_2956
    call message("Ren", "I don't think I really liked you....") from _call_message_2957
    call message("Ren", "I just....") from _call_message_2958
    call message("Ren", "I just needed somebody") from _call_message_2959
    call message("Ren", "who wouldn't leave me.....") from _call_message_2960
    call message("Ren", "But....") from _call_message_2961
    call message_img("Ren", "you didn't know that, did you...?", "images/smile.png") from _call_message_img_193
    call screen phone_reply3("Please stop pretending....", "cd850", "Is Ren really gone now...?", "cd851", "Now I know... That I'm nothing special...", "cd852")
label cd850:
    call phone_after_menu from _call_phone_after_menu_349
    call message_start("[name]", "Please stop pretending....") from _call_message_start_371
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Ren", "Don't tell me...") from _call_message_2962
    call message("Ren", "Did you think you're special") from _call_message_2963
    call message("Ren", "when you were with me...?") from _call_message_2964
    jump am820
label cd851:
    call phone_after_menu from _call_phone_after_menu_350
    call message_start("[name]", "Is Ren really gone now...?") from _call_message_start_372
    call message("Ren", "You realize that now...?") from _call_message_2965
    jump am820
label cd852:
    call phone_after_menu from _call_phone_after_menu_351
    call message_start("[name]", "Now I know... That I'm nothing special...") from _call_message_start_373
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "You realize that now...?") from _call_message_2966
    jump am820
label am820:
    call message_img("Ren", "Oh...", "images/questioning.png") from _call_message_img_194
    call message("Ren", "so you're an airhead") from _call_message_2967
    call message_img("Ren", "just like me....", "images/well.png") from _call_message_img_195
    call message("Ren", "People like us") from _call_message_2968
    call message("Ren", "can never be saved...") from _call_message_2969
    call message("Ren", "So that's why") from _call_message_2970
    call message("Ren", "I'm not going to save you....") from _call_message_2971
    call message("Ren", "I just have to") from _call_message_2972
    call message("Ren", "find someone to replace you") from _call_message_2973
    call message("Ren", "who's much smarter than you....") from _call_message_2974
    call message("Ren", "Goodbye.") from _call_message_2975
    call message("Ren", "........") from _call_message_2976
    call message("Ren", "....") from _call_message_2977
    call message("Sora", "Ugh") from _call_message_2978
    call message("Sora", "though it was for a moment") from _call_message_2979
    call message("Sora", "it was so frustrating") from _call_message_2980
    call message("Sora", "to play that wimpy freak.") from _call_message_2981
    call message("Sora", "I should go check out myself") from _call_message_2982
    call message_img("Sora", "how you look hehehehe", "images/expecting.png") from _call_message_img_196
    call message("Sora", "You better wait nice") from _call_message_2983
    call message("Sora", "in your room.") from _call_message_2984
    call message("Sora", "I'm coming right now.") from _call_message_2985
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_2986
    call phone_end from _call_phone_end_24
    stop music
    scene black with dissolve
    play music "music/music20.ogg"
    scene bg10 with dissolve
    show U U24 at my_left with dissolve
    u "Haha... Ha. What an idiot. Haa.... Yes, that's more like it...."
    show U U27
    u "Hahaha... Hahaha! Ha..."
    show R R33 at my_right with dissolve
    savior "You seem to be enjoying yourself."
    show U U22
    u "Welcome, my savior."
    savior "You don't need to stop laughing."
    u "I'm fine now. Being entertained by toys never last long."
    show R R32
    savior "Never last long....? "
    show R R33
    savior "Then why did you ask me not to dispose of her yet? I thought it was because you find her company entertaining."
    show U U24
    u "It is fun when she shivers or tries to fight back. "
    show U U22
    u "But after some time it feels just useless. That's why I find her more irritating and hateful."
    show U U26
    u "Stupid toy.... I feel like this is all useless because she's an airhead."
    savior "Then why don't you just dispose of her? I don't understand why you would leave her here."
    show U U22
    u "...At least the moment when I torment her is still fun.And I still have a long way to go on this."
    show R R32
    savior" Sora, you should think well. Are you sure Ren is not there anymore? He might be urging you to keep her alive."
    show U U26
    show R R33
    u "Do we have to talk about that dead boy? "
    u "Ren and [name] are nothing different. That's why they always lose."
    u "A good riddance, by the way. "
    u "I'll never live like him. I'll never be the one to be tormented... Never, ever."
    savior "You'll never experience any kind of misery from now on."
    u "I already had enough misery in my life. "
    show U U25
    u "And that's all because of Ren! I'm not the loser!"
    show U U26
    show R R32
    savior "You're right. You're now strong, enough to cause fear. "
    show R R33
    savior "No one can ever find that weak boy you used to be when you were a child."
    show U U24
    u "Watch me... Nobody will be able to stand in my sight without shivering in fear."
    u "I'll make them pay... I'll make anyone who rebels against you or me, anyone who treats us like fools or freaks..."
    show U U27
    u "I'll make all of them pay....as much as I suffered."
    savior "That's just how strong you are. That's just how strong we are."
    hide U with dissolve
    show R R32
    savior "Now that I see how strong you have become, looks like I worried for nothing. Now I see...."
    show R R33
    savior "Ren is truly gone for good."
    stop music
    hide R with dissolve
    scene black with dissolve
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC13 with dissolve
    play sound "music/answer.ogg"
    "(Sora is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    u "Picking up at the right time."
    u "Yes, that's how it should be done. What more is there for you to do but wait to play with me, is there?"
    u "The camera I installed last time seems to be working very well."
    u "I can't slack off monitoring you since I don't know what a moron like you will do."
    u "Such an handful toy."
    menu:
        "Do you think I'll get scared if you say you're watching me?":
            u "Trying to be brave. I can see you looking for the camera while saying that."
            u "I can see how you're shaking right now! Hahaha."
            u "Stop trying to be brave with words but rather show yourself begging for your life."
            u "You never know. I might give you a pass. Of course, I don't intend to do so."
            jump u819
        "I'm not up to something. I'll here quietly.":
            u "Do you now know where you stand?"
            u "Remember. You're only worthwhile when someone as useless as you is trampled by a stronger and capable person to provide entertainment."
            u "So, try to be obedient like how you are right now."
            jump u819
label u819:
    u "Right. Thinking of which, didn't you... get an official ID card?"
    u "I got rid of your personal code as I thought it's useless to someone like you.  Hahaha!"
    u "Now, you can't walk around the building as you like unlike before."
    u "Say goodbye to that garden you used to hangout with Ren."
    menu:
        "I don't need it since it's my will to stay here anyway.":
            u "What, do you still not know what position you're in? You take forever to understand the situation."
            u "Your will? How noble is that?"
            u "What use is it to someone as low as you?"
            u "Yor will, it has no meaning to someone strong like me."
            jump u820
        "Yes. I understand. Do as you like.":
            u "What? How boring. Did you forget you're imprisoned in that room?"
            u "Why are you acting so cool and calm when instead you should have been begging right now."
            u "But, it's not important now. Whatever. I don't need your approval. I'm going to do it my way."
            u "You're disconnected from the RFA... I'm the only you have right now. Hahaha!"
            jump u820
label u820:
    u "Now, why don't you come up with ways to entertain me with your dumb head."
    u "You never know. If you do a good job, I might let you stay here."
    u "Though I doubt you'll be able to come up with something great."
    u "Aren't I a fairly nice master? Telling you what to do in detail"
    u "for an insignificant toy."
    u "*Sigh* I'm annoyed that I feel like I've wasted my time. I'll see what you do next time."
    u "If keep acting this lamely.... I won't let it pass like today."
    u "If your curious, keep acting like that."
    u "I'm hanging up."
    play sound "music/beep.ogg"
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "23:39" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music19.ogg"
    call phone_start from _call_phone_start_24
    call message_start("SYSTEM", "Sora has entered the chatroom.") from _call_message_start_374
    call screen phone_reply3("Why won't you come see me?", "cd853", "....", "cd854", "I have no one to talk to other than you.", "cd855")

label cd853:
    call phone_after_menu from _call_phone_after_menu_352
    call message_start("[name]", "Why won't you come see me?") from _call_message_start_375
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "I decide to see you or not.") from _call_message_2987
    call message("Sora", "You sound like") from _call_message_2988
    call message("Sora", "we have an appointment") from _call_message_2989
    call message("Sora", "or something lol") from _call_message_2990
    jump am821
label cd854:
    call phone_after_menu from _call_phone_after_menu_353
    call message_start("[name]", "....") from _call_message_start_376
    call message_img("Sora", "??", "images/questioning.png") from _call_message_img_197
    call message("Sora", "You should type.") from _call_message_2991
    call message("Sora", "What are you waiting for?") from _call_message_2992
    call message("Sora", "How stupid hahahaha.") from _call_message_2993
    jump am821
label cd855:
    call phone_after_menu from _call_phone_after_menu_354
    call message_start("[name]", "I have no one to talk to other than you.") from _call_message_start_377
    call message("Sora", "Is that so, princess...?") from _call_message_2994
    call message_img("Sora", "I don't know what to say hehehehehe.", "images/smile.png") from _call_message_img_198
    call message("Sora", "You shouldn't sound so arrogant.") from _call_message_2995
    call message("Sora", "What are you going to do") from _call_message_2996
    call message("Sora", "with what will happen to you") from _call_message_2997
    call message("Sora", "later on?") from _call_message_2998
    jump am821
label am821:
    call message("Sora", "You think happiness will eventually find your way") from _call_message_2999
    call message_img("Sora", "if you behave like you have nothing to fear?", "images/questioning.png") from _call_message_img_199
    call message("Sora", "I'm guessing you're making me mad on purpose") from _call_message_3000
    call message("Sora", "so that you can be thrown away") from _call_message_3001
    call message("Sora", "as soon as you can") from _call_message_3002
    call message("Sora", "and find ease at last....") from _call_message_3003
    call screen phone_reply3("I never thought about that. I'm too stupid to reach that part.", "cd856", "...I'm just waiting.", "cd857", "You cannot bend my heart, Sora.", "cd858")
label cd856:
    call phone_after_menu from _call_phone_after_menu_355
    call message_start("[name]", "I never thought about that. I'm too stupid to reach that part.") from _call_message_start_378
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Sora", "Oh") from _call_message_3004
    call message("Sora", "that's right.") from _call_message_3005
    call message("Sora", "If you were a little smarter") from _call_message_3006
    call message("Sora", "I wouldn't be so bored like this.") from _call_message_3007
    call message("Sora", "But still...") from _call_message_3008
    jump am822
label cd857:
    call phone_after_menu from _call_phone_after_menu_356
    call message_start("[name]", "...I'm just waiting.") from _call_message_start_379
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "For what?", "images/questioning.png") from _call_message_img_200
    call message("Sora", "Don't tell me...") from _call_message_3009
    call message_img("Sora", "Are you waiting for your beloved Ren?", "images/smile.png") from _call_message_img_201
    call message("Sora", "Hahahahahahahahahahahaha") from _call_message_3010
    call message("Sora", "hahahahahahaha") from _call_message_3011
    call message("Sora", "ah...") from _call_message_3012
    call message("Sora", "my stomach hahahahaha") from _call_message_3013
    call message("Sora", "Ah...".) from _call_message_3014
    call message("Sora", "that was so ridiculous.") from _call_message_3015
    jump am822
label cd858:
    call phone_after_menu from _call_phone_after_menu_357
    call message_start("[name]", "You cannot bend my heart, Sora.") from _call_message_start_380
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message_img("Sora", "I'm not interested in your heart.", "images/smile.png") from _call_message_img_202
    call message("Sora", "The only thing") from _call_message_3016
    call message("Sora", "I'm interested in") from _call_message_3017
    call message("Sora", "is how much fun") from _call_message_3018
    call message("Sora", "you can give me hehehehe.") from _call_message_3019
    call message("Sora", "I'm sure your heart") from _call_message_3020
    call message("Sora", "is no more worth") from _call_message_3021
    call message("Sora", "than a dust in the air hehehe") from _call_message_3022
    call message("Sora", "hehehehehehehe.") from _call_message_3023
    call message("Sora", "Oh") from _call_message_3024
    call message("Sora", "that's right.") from _call_message_3025
    call message("Sora", T"here's something I need to tell you.") from _call_message_3026
    jump am822
label am822:
    call message_img("Sora", "You should be happy.", "images/smile.png") from _call_message_img_203
    call message("Sora", "I got a good news for once") from _call_message_3027
    call message("Sora", "for an idiot like you.") from _call_message_3028
    call message("Sora", "My savior") from _call_message_3029
    call message("Sora", "who so very graciously") from _call_message_3030
    call message("Sora", "chose to let you live") from _call_message_3031
    call message("Sora", "will spare her care") from _call_message_3032
    call message("Sora", "for this messenger") from _call_message_3033
    call message_img("Sora", "and you.....", "images/well.png") from _call_message_img_204
    call message("Sora", "I'm not sure") from _call_message_3034
    call message("Sora", "what good you can possibly do") from _call_message_3035
    call message("Sora", "but since my savior wants an access to the messenger") from _call_message_3036
    call message("Sora", "you'll get to see her soon.") from _call_message_3037
    call message("Sora", "Of course") from _call_message_3038
    call message("Sora", "I told savior") from _call_message_3039
    call message("Sora", "not to expect too much from you.") from _call_message_3040
    call message("Sora", "So try to be good") from _call_message_3041
    call message("Sora", "if you want to stay alive and breathing.") from _call_message_3042
    call message("Sora", "But you can't be saved.") from _call_message_3043
    call message("Sora", "That's impossible haha") from _call_message_3044
    call message("SYSTEM", "Reina has entered the chatroom.") from _call_message_3045
    call message("Sora", "My savior.") from _call_message_3046
    call message("Reina", "Thank you for granting access, Sora.") from _call_message_3047
    call message("Sora", "I can do anything") from _call_message_3048
    call message("Sora", "for the eternal paradise.") from _call_message_3049
    call message("Reina", "I find this interface familiar.") from _call_message_3050
    call message("Sora", "That's probably because") from _call_message_3051
    call message("Sora", "Ren that airhead") from _call_message_3052
    call message("Sora", "copied the RFA messenger.") from _call_message_3053
    call screen phone_reply3("Aren't you the founder of the RFA...?", "cd859", "Where is Ren?", "cd860", "My savior! Please love me...", "cd861")
label cd859:
    call phone_after_menu from _call_phone_after_menu_358
    call message_start("[name]", "Aren't you the founder of the RFA...?") from _call_message_start_381
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    jump am823
label cd860:
    call phone_after_menu from _call_phone_after_menu_359
    call message_start("[name]", "Where is Ren?") from _call_message_start_382
    jump am823
label cd861:
    call phone_after_menu from _call_phone_after_menu_360
    call message_start("[name]", "My savior! Please love me...") from _call_message_start_383
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "Oh my.") from _call_message_3054
    jump am823
label am823:
    call message("Sora", "I'm a little worried about this airhead...") from _call_message_3055
    call message("Sora", "I’m worried what kind of stupidity she'll show you....") from _call_message_3056
    call message("Reina", "That's alright. I'll be fine.") from _call_message_3057
    call message("Reina", "You can now run along, Sora. You must be busy.") from _call_message_3058
    call message("Sora", "Of course.") from _call_message_3059
    call message("Sora", "I'll now leave you in peace.") from _call_message_3060
    call message("Sora", "For eternal paradise.") from _call_message_3061
    call message("SYSTEM", "Sora has left the chatroom.") from _call_message_3062
    call message("Reina", "[name].....") from _call_message_3063
    call message("Reina", "Poor girl.") from _call_message_3064
    call message("Reina", "How did this happen?") from _call_message_3065
    call screen phone_reply3("That's because...you abused Ren.", "cd862", "I made a mistake... Please let me make up for it!", "cd863", "Are you V's beloved woman and the founder of the RFA?", "cd864")
label cd862:
    call phone_after_menu from _call_phone_after_menu_361
    call message_start("[name]", "That's because...you abused Ren.") from _call_message_start_384
    call message("Reina", "What do you mean, abuse...?") from _call_message_3066
    call message("Reina", "I have merely watched him from his side") from _call_message_3067
    call message("Reina", "and made him work faster.") from _call_message_3068
    call message("Reina", "But what about you?") from _call_message_3069
    call message("Reina", "If this isn't what you wanted") from _call_message_3070
    call message("Reina", "why did you speak") from _call_message_3071
    call message("Reina", "such dangerously sweet words to Ren?") from _call_message_3072
    call message("Reina", "Your words were sweet") from _call_message_3073
    call message("Reina", "but they threatened his existence.") from _call_message_3074
    call message("Reina", "That's why there was no choice") from _call_message_3075
    call message("Reina", "but for the real Sora to step out for his survival.") from _call_message_3076
    call message("Reina", "It is all your fault that you lost such a sweet friend.") from _call_message_3077
    jump am824
label cd863:
    call phone_after_menu from _call_phone_after_menu_362
    call message_start("[name]", "I made a mistake... Please let me make up for it!") from _call_message_start_385
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "It's too late.") from _call_message_3078
    call message("Reina", "Your role was to stay connected with the RFA 'for the party'") from _call_message_3079
    call message("Reina", "and help us collect information on them....") from _call_message_3080
    call message("Reina", "Without the party") from _call_message_3081
    call message("Reina", "you don't deserve a right to stay in that messenger.") from _call_message_3082
    call message("Reina", "They would've soon suspected you") from _call_message_3083
    call message("Reina", "tracked you down") from _call_message_3084
    call message("Reina", "and disposed of you.") from _call_message_3085
    call message("Reina", "So that's why Sora and I disconnected you by force before that happens.") from _call_message_3086
    call message("Reina", "You should be grateful to us.") from _call_message_3087
    call message("Reina", "If it were Ren....") from _call_message_3088
    call message("Reina", "he would've tried to make you hide behind him") from _call_message_3089
    call message("Reina", "and drove everyone to danger.") from _call_message_3090
    call message("Reina", "Now that we talk about it....") from _call_message_3091
    jump am824
label cd864:
    call phone_after_menu from _call_phone_after_menu_363
    call message_start("[name]", "Are you K's beloved woman and the founder of the RFA?") from _call_message_start_386
    call message("Reina", "You must be having multiple questions because of my username.") from _call_message_3092
    call message("Reina", "However, I have no reason to explain myself to you right now.") from _call_message_3093
    call message("Reina", "[name]") from _call_message_3094
    call message("Reina", "you'd better focus on the crisis you found yourself in.") from _call_message_3095
    call message("Reina", "Very soon") from _call_message_3096
    call message("Reina", "you might be disposed of by Sora.") from _call_message_3097
    call message("Reina", "Not to mention...") from _call_message_3098
    jump am824
label am824:
    call message("Reina", "It is all your fault") from _call_message_3099
    call message("Reina", "that things have turned out the way they are.") from _call_message_3100
    call message("Reina", "Oh....") from _call_message_3101
    call message("Reina", "maybe you'll blame me instead.") from _call_message_3102
    call message("Reina", "Maybe you'll blame me") from _call_message_3103
    call message("Reina", "that Sora emerged to torture you") from _call_message_3104
    call message("Reina", "all because I was too cruel to Ren.") from _call_message_3105
    call message("Reina", "The reason why I dragged Ren back into the abyss") from _call_message_3106
    call message("Reina", "was because of that unreasonable blindfold of hope you forced upon him.") from _call_message_3107
    call message("Reina", "And I am more than satisfied with the way Sora has turned out.") from _call_message_3108
    call message("Reina", "His agony would not have lasted long if he steeled himself much sooner....") from _call_message_3109
    call message("Reina", "Poor Ren....") from _call_message_3110
    call message("Reina", "He was so stubborn. He wanted to remain in that foolishly innocent world,") from _call_message_3111
    call message("Reina", "smiling at blossoms above his head....") from _call_message_3112
    call message("Reina", "He hated his brother") from _call_message_3113
    call message("Reina", "but in fact, he also missed his brother. He was so weak.") from _call_message_3114
    call message("Reina", "If I didn't teach him how to hack") from _call_message_3115
    call message("Reina", "he would have been lost and left behind ages ago.") from _call_message_3116
    call message("Reina", "[name]") from _call_message_3117
    call message("Reina", "Do you know why Sora is keeping you alive?") from _call_message_3118
    call message("Reina", "Sora") from _call_message_3119
    call message("Reina", "is much stronger than Ren...") from _call_message_3120
    call message("Reina", "but he could exist") from _call_message_3121
    call message("Reina", "only because there was weak Ren") from _call_message_3122
    call message("Reina", "to work against.") from _call_message_3123
    call message("Reina", "However") from _call_message_3124
    call message("Reina", "now that Ren is gone....") from _call_message_3125
    call message("Reina", "he's tormenting you to keep his dominance in check. You're weak.") from _call_message_3126
    call message("Reina", "So what I'm saying is....") from _call_message_3127
    call message("Reina", "you're nothing") from _call_message_3128
    call message("Reina", "but a loser to be trampled under our feet...") from _call_message_3129
    call message("Reina", "Because....") from _call_message_3130
    call message("Reina", "you're a weak") from _call_message_3131
    call message("Reina", "who is fond of Ren's innocence.") from _call_message_3132
    call message("Reina", "We are only living and behaving") from _call_message_3133
    call message("Reina", "based on our rules.") from _call_message_3134
    call screen phone_reply("You believe innocence is weak. But that's not true.", "cd865", "...Now I know that I'm weak.", "cd866")
label cd865:
    call phone_after_menu from _call_phone_after_menu_364
    call message_start("[name]", "You believe innocence is weak. But that's not true.") from _call_message_start_387
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end +=1
    call message("Reina", "I am the law here.") from _call_message_3135
    call message("Reina", "Which means....") from _call_message_3136
    call message("Reina", "I am the justice.") from _call_message_3137
    jump am825
label cd866:
    call phone_after_menu from _call_phone_after_menu_365
    call message_start("[name]", "...Now I know that I'm weak.") from _call_message_start_388
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Reina", "Weak is useful in some ways.") from _call_message_3138
    call message("Reina", "Because the weak makes the strong shine.") from _call_message_3139
    jump am825
label am825:
    call message("Reina", "Next time - if you ever get one, that is,") from _call_message_3140
    call message("Reina", "I hope you'd stand with the victors.") from _call_message_3141
    call message("Reina", "Now, if you realized that you're weak") from _call_message_3142
    call message("Reina", "you should think about") from _call_message_3143
    call message("Reina", "how to make yourself useful for Sora.") from _call_message_3144
    call message("Reina", "If you do....") from _call_message_3145
    call message("Reina", "Sora won't dispose of you so soon.") from _call_message_3146
    call message("Reina", "I wanted to tell you that.") from _call_message_3147
    call message("Reina", "Now I will leave you.") from _call_message_3148
    call message("SYSTEM", "Reina has left the chatroom.") from _call_message_3149
    call phone_end from _call_phone_end_25
    stop music
    hide MC with dissolve
    scene black with dissolve
    with Pause(2.0)
    play music "music/music15.ogg"
    scene bg18 with dissolve
    "..."
    "....."
    "......."
    show V V320 with dissolve
    v "Sora!"
    show V V312
    v "Sora! You can't go in there yet!"
    scene bg17 with dissolve
    yu "Mom...?"
    police "The crime scene must remain untouched. I must ask you to leave."
    show V V321
    v "Apologies, sir. Sora... Follow me."
    police "Is he her son by any chance? I thought I heard him say mom...."
    show V V313
    v "No, he's not. I'm so sorry about this."
    police "Please let us know if you find her child."
    police "We must see the father regarding the custody of the child."
    show V V321
    v "....Come on, Sora. Follow me."
    hide V with dissolve
    scene bg16 with dissolve
    r "Sora, I know how hard it is for you regarding your mother, but you must learn to deal with your pain...."
    yu "....Forget it."
    r "Hmm?"
    yu "I don't need to study anymore! Mom is gone... And Souta...is gone...."
    r "Sora... Souta left years ago."
    r "And your mother always treated you like...like that."
    r "That's a completely different story from studying and becoming strong."
    yu "But...but still...at least she didn't leave me!"
    yu "She still stayed with me."
    yu "I'm thin. I'm an airhead... And she...she was mean to me, but she didn't leave me...unlike Souta.... Sniffle."
    yu "Souta would have never left me....if I weren't such an airhead."
    yu "If I were different... If I were healthy and smart, I would have been loved...."
    yu "Nobody would have left me...."
    yu "Now neither of them is with me...."
    yu "I'll...I'll forever be alone... It doesn't matter whom I meet."
    r "Sora.... Even if you really are a useless boy, I'll never leave you."
    yu "Reina...."
    r "I'll be a mother to you."
    yu "....I don't deserve such warm and loving mother like you."
    yu "This is all my fault.... When Souta was with me, all I could do for him was to make him protect me..."
    yu "I'm good for nothing...."
    yu "Like mom said, I'm stupid and useless...."
    scene black with dissolve
    yu "Why was I ever born...?"
    yu "Can I ever be useful for something?"
    yu "I should rather carry on as nothing. That's what I deserve...."
    yu "I know that."
    yu "I know that so well. But...."
    scene bg18 with dissolve
    yu "Reina, please don't leave me.... Even though I'm a good-for-nothing...."
    yu "Even a weed stomped on would later bloom."
    yu "I...I, too, want to bloom."
    yu "Flowers can do that, can't they?"
    yu "So please water me. Please give me light."
    yu "Reina... K... Souta..."
    yu "Please water me. Please give me light..."
    yu "Please give me a chance to be loved...."
    stop music
    scene black with dissolve
    with Pause(1.0)
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Reina is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    r "Hello? It really works."
    r "Look forward to me calling you once in a while now."
    r "It's my first time talking to you through the phone. I wanted to talk to you but I didn't have to opportunity."
    r "Above all, there many lambs here waiting for my help."
    r "It's better to have something commemoration for first times always."
    r "What should we talk about if I'm to leave a deep impression to you of this first time call."
    r "So. I see that you're keeping close to Sora these days."
    r "I'll tell you a story that'll catch you attention. Don't you want to hear about Sora's childhood?"
    menu:
        "Did you know him since young?":
            r "Yes. Since long ago. I've known him since he was naive and weak."
            jump r81
        "Not interested. He's not Ren.":
            r "Since they're sharing the same body....Sora's childhood is also Ren's childhood."
            r "Still, don't you want to know?"
            r "No need to have your guards up. I'm only interested in you and just want to talk."
            jump r81
label r81:
    r "When Sora was young, he used to be a weak and soft-hearted."
    r "He couldn't stand on his own that his brother always protected him."
    r "Then on day, his brother disappeared and Sora was left all alone."
    r "Sora then was so weak, if left alone, he looked like we would dry up and die like picked flower."
    r "I can still remember what he said when he was all alone."
    r "What do I do if nobody loves me or remembers me like this forever?"
    r "Poor Sora... That child has been a weakling from birth. Never learned how to become strong."
    r "If it wasn't for me, that child wouldn't have lasted and died."
    menu:
        "Are you trying to say is that you saved him?":
            r "That's not what I'm trying to say but rather that I did save him."
            r "I took the weak child in so that he wouldn't get hurt from the harsh world."
            r "But Sora has been hurt so much already, he couldn't live on like that."
            r "That's why Ren was created."
            r "All the weak parts of Sora is collected into Ren. If Ren disappears, then Sora will be strong without weaknesses."
            jump r82
        "Then you should have cared for him with affection! Not abuse him like now!":
            r "I've protected him in my own way."
            r "Recognizing his usefulness and providing a path where he can really become strong."
            r "He was the first child I've ever rescued. I'm protecting him specially and preciously."
            r "If you want to belittle it as abuse, then be it. It still doesn't change to fact that my method has made him stronger."
            jump r82
label r82:
    r "That child is the same as a flower that couldn't grow because of its thin roots."
    r "Rather growing him into a weak and frail flower ready to crumple under the rain and wind in the prairie, I planted a tree."
    r "But you're trying to seduce that flower to move to a new place."
    r "Not even knowing how harsh and rough the new place would be or whether his roots can endure it or not."
    r "You say it's for the child but, in truth, you almost killed that child."
    r "Whether it's Ren or Sora, if you have affection, think about it until we talk again."
    r "Think about what's really for the child."
    r "Well then."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    call day9 from _call_day9
