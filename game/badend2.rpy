label badend2:
    stop music
    scene black with dissolve
    scene bg5 with dissolve
    show MC MC17 at my_right with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    stop sound
    mc "Who...who is it?"
    whooo "It's me..."
    play music "music/dooropen.ogg"
    "(Door opened)"
    play music "music/music14.ogg"
    show U U19 at my_left with dissolve
    show MC MC11
    ray "[name]...!"
    show U U19
    menu:
        "Ren!!":
            ray "[name]..."
            ray "I missed you...! Sniffle..."
            jump be91
        "Are you back now?":
            ray "[name], sorry I'm late..."
            jump be91
        "Where's Sora...?":
            ray "...What? You're not happy to see me...? I didn't expect that..."
            jump be91
label be91:
    show U U19
    ray "[name], I had no idea what you're going through. I'm so sorry I'm late..."
    show U U112
    show MC MC14
    ray "I was so scared. It was so terrible...! It was so painful being trapped in Sora..."
    show U U113
    ray "What scared me the most...was that I might never see you again..."
    ray "I'm so glad...I get to see you again..."
    show U U112
    ray "It was horrible, wasn't it? Hah...."
    ray "Look how thin you are. It's all my fault..."
    show U U19
    ray "I heard everything! I heard that my savior wants to get rid of you..."
    show U U12
    ray "I will save you!"
    ray "I will talk to savior to spare you."
    show MC MC13
    menu:
        "What happened to Sora? Is he gone?":
            ray "...What is the matter? You want him to be gone?"
            jump be92
        "Ren...you are not going away now, are you!?":
            ray "Sniffle... What is the matter?"
            jump be92
label be92:
    show U U19
    ray "Sora is another me also..."
    stop music
    ray "But you don't like him...?"
    scene black with dissolve
    show U U14 at my_left
    play music "music/music19.ogg"
    u "...Good."
    show MC MC15 at my_right
    show U U17
    u "Since I can see your horrid face disliking it."
    scene bg5 with dissolve
    show U U12 at my_left
    show MC MC15 at my_right
    u "I told you thousand times that Ren is gone."
    u "You thought I was Ren just because I changed my clothes? You are such a moron."
    show U U14
    u "You can't even know I'm not Ren... Looks like you don't really like him."
    u "But figures - ! You two were a set of morons busy clutching and comforting each other. And this emotion is so sophisticated for morons like you."
    show U U12
    u "You are mistaken if you think you can call Ren back..."
    u "Hang on..."
    show U U16
    show MC MC17
    u "Don't tell me... Are you enjoying whenever I do this?"
    u "How dare you... Who told you to enjoy yourself?"
    show U U15
    u "Give me your phone. Now it's forbidden to you."
    show U U12
    u "[name], you're useless. You are a useless tool that doesn't deserve to think or feel."
    scene black with dissolve
    show U U14 with dissolve
    u "...Your eyes lost focus. Yes, that's it."
    show U U17
    u "That's what you deserve... Moronic eyes without focus."
    u "You are a moron. You are useless..."
    u "You are my tool... My weak, pathetic tool... [name]."
    hide U with dissolve
    hide MC with dissolve
    scene bg18 with dissolve
    "..."
    "..........."
    "....................."
    show U U21 at my_right with dissolve
    show MC MC17 at my_left with dissolve
    u "...Here, it's a gift."
    show U U24
    u "Don't you dare to smile, it's not for you. It's not meant for you in the first place."
    u "You know all of the things here are not for you, right?"
    show U U21
    u "Only a lovely girl deserves all this."
    show U U24
    u "There is nothing you can have here, you useless tool."
    show U U22
    u "Give back that toy I gave you before. You are too arrogant. You shouldn't keep it."
    u "Don't even look at the food on the table. They are not for you."
    show U U24
    u "Only useful, good people like me can eat them."
    u "You just wait nicely."
    show U U21
    u "If you get lucky, you might get a biscuit that I decide to throw away."
    show U U27
    u "That's where you belong."
    show U U24
    u "Oh, that's right. I will soon get a girlfriend perfect for me."
    u "Someone smart and useful, nothing like you."
    show U U21
    u "You will be jealous of her to the point your jealousy chokes you. But you will be forever cursed with this realization that you can never be like her."
    show U U24
    u "Haha, you look like you gave up. Come on, now - there might be hope for you."
    u "So...get over here and give me a kiss. I will give you a chance."
    show U U27
    u "I might adore you a little if I like it."
    hide MC with dissolve
    hide U with dissolve
    scene black with dissolve
    u "......"
    u "That was the worst kiss of my life. You suck. It completely destroyed my imagination. I feel like I have to rinse my mouth right now."
    u "Maybe I will make you a slave to my girlfriend. That would be fun."
    u "I can't wait to see what kind of face you will make. Hahaha! That will be so fun."
    u "You should never stop getting hurt."
    u "Never, ever, ever! So then I get the fun out of torturing you."
    u "I will torture you forever and ever. I'll keep torturing you, to the point you are barely breathing."
    u "Because you are my prey. You're my toy."
    scene black with dissolve
    show U U25 at my_right with dissolve
    show MC MC17 at my_left with dissolve
    u "What are you staring at? You are so arrogant, [name]. How dare..."
    show U U22
    u "You can keep breathing only when you entertain me."
    show U U24
    u "Since you've been arrogant, I'm going to do something you hate the most."
    show U U27
    u "Don't give me that look. It's no use."
    u "Now I will leave you here alone..."
    show U U24
    u "Yes, that's it. That's the way you should look - like you are about to die from loneliness!"
    show U U27
    u "That's the face you deserve. You only deserve sadness and nothing more. Don't you forget it."
    show U U21
    u "Hahaha! You will forever be my toy."
    show U U27
    u "...Forever."
    hide U with dissolve
    hide MC with dissolve
    stop music
    scene badend with dissolve
    with Pause(3.0)
    scene black with dissolve
    $ MainMenu(confirm=False)()
