label prologue:
    "..."
    "......"
    "........."
    "(1 month after Reina's death)"
    scene white with dissolve
    v "Reina..."
    v "If I could go back to the past, I would have made a different choice...."
    "(......)"
    scene bg1 with dissolve
    play music "music/music2.ogg"
    show V V11 with dissolve
    v "It's already been 1 month since you left."
    show V V12
    v "Seems your absence is still grave within the members. Everyone's deeply grieving since you left."
    show V V13
    v "Feels like it's my fault that you left... from where did it go wrong...?"
    show V V11
    v "Guess blaming myself won't solve anything... I was determined to do anything to undo the past.. but...... will it happen how I want it to happen?"
    v "Every time my eyes pain me, I am reminded of you.. If this were your light-hearted way of testing me... I'd rather prefer that."
    show V V14
    v "Ah... this flower... I wonder who left it here."
    show V V13
    v "Narcissus... maybe Yoshiro?..."
    v "We talked of a flower shop yesterday, maybe this was why. He purposely got the one you like."
    show V V11
    v "...I'll give this to you in person."
    hide V
    scene white with dissolve
    stop music
    "(5 months later)"
    scene bg3 with dissolve

    call phone_start from _call_phone_start_32
    play sound "music/hack.ogg"
    call message_start("SYSTEM", "Unknown has entered the chatroom.") from _call_message_start_538
    call message("Unknown", "Hi!") from _call_message_4160
    call message("Unknown", "Finally, someone to talk to, thank god...") from _call_message_4161
    call screen phone_reply3("Hello.","choice1","Who are you? Where am I?","choice2","Hey ya howdy doodle doo","choice3")
    label choice1:
        call phone_after_menu from _call_phone_after_menu_509
        call message_start("[name]", "Hello.") from _call_message_start_539
        call message("Unknown", "Hi. Nice to meet you ^^") from _call_message_4162
        call message("Unknown", "[name]...") from _call_message_4163
        call message("Unknown", "I've been waiting for you.") from _call_message_4164
        jump aftermenu1
    label choice2:
        call phone_after_menu from _call_phone_after_menu_510
        call message_start("[name]", "Who are you? Where am I?") from _call_message_start_540
        call message("Unknown", "Hehe...") from _call_message_4165
        call message("Unknown", "I'm an app developer.") from _call_message_4166
        call message("Unknown", "and this is a chat room I created.") from _call_message_4167
        jump aftermenu1
    label choice3:
        call phone_after_menu from _call_phone_after_menu_511
        call message_start("[name]", "Hey ya howdy doodle doo") from _call_message_start_541
        call message("nadia", "You...you're quite square....") from _call_message_4168
        call message("Unknown", "No, never mind^^;;;") from _call_message_4169
        call message("Unknown", "You're saying hi, right? Hi^^") from _call_message_4170
        call message("Unknown", "Interesting. Worth the wait...") from _call_message_4171
        jump aftermenu1
    label aftermenu1:
        call message("Unknown", "Hope I didn't surprise you.") from _call_message_4172
        call message("Unknown", "Don't be nervous, I'm just an “ordinary” person~") from _call_message_4173
        call screen phone_reply("Why are you emphasizing the word ordinary...? It's making me even more nervous","choice4","You're the developer of this app?","choice5")
    label choice4:
        call phone_after_menu from _call_phone_after_menu_512
        call message_start("[name]", "Why are you emphasizing the word ordinary...? It's making me even more nervous") from _call_message_start_542
        call message("Unknown", "You're funny lol") from _call_message_4174
        call message("Unknown", "But I can't really say I'm extraordinary when I'm introducing myself.") from _call_message_4175
        call message("Unknown", "Shouldn't I be humble and modest?") from _call_message_4176
        jump aftermenu2
    label choice5:
        call phone_after_menu from _call_phone_after_menu_513
        call message_start("[name]", "You're the developer of this app?") from _call_message_start_543
        call message("Unknown", "Yeah! I made it myself...") from _call_message_4177
        call message("Unknown", "What do you think of this app? You like it?") from _call_message_4178
        call message("Unknown", "I was hoping you'd like it...") from _call_message_4179
        jump aftermenu2
    label aftermenu2:
        call message("Unknown", "In fact... I'm really excited.") from _call_message_4180
        call message("Unknown", "Cuz the first person to come in is you, [name]!") from _call_message_4181
        call message("Unknown", "I was so worried that no one will come in as I was making this app.") from _call_message_4182
        call screen phone_reply3("What's this app for?","choice6","I'd like to chat with a handsome/pretty guy right now.","choice7", "Is there something I can do for you?", "choice8")
    label choice6:
        call phone_after_menu from _call_phone_after_menu_514
        call message_start("[name]", "What's this app for?") from _call_message_start_544
        call message("Unknown", "Oh, I was about to explain it to you") from _call_message_4183
        call message("Unknown", "But before that, can I ask a favor?") from _call_message_4184
        jump aftermenu3
    label choice7:
        call phone_after_menu from _call_phone_after_menu_515
        call message_start("[name]", "I'd like to chat with a handsome/pretty guy right now.") from _call_message_start_545
        call message("Unknown", "You are") from _call_message_4185
        call message("Unknown", "right now") from _call_message_4186
        call message("Unknown", "with me...") from _call_message_4187
        call message("Unknown", "^^") from _call_message_4188
        call message("Unknown", "ummm...") from _call_message_4189
        jump aftermenu3
    label choice8:
        call phone_after_menu from _call_phone_after_menu_516
        call message_start("[name]", "Is there something I can do for you?") from _call_message_start_546
        call message("Unknown", "Oh, thanx for asking!") from _call_message_4190
        call message("Unknown", "cuz there is a favor I want to ask...") from _call_message_4191
        jump aftermenu3
    label aftermenu3:
        call message("Unknown", "Though I know it's too much to ask from a stranger") from _call_message_4192
        call message("Unknown", "there is something I really want you to help me with.") from _call_message_4193
        call screen phone_reply3("I'll do it!!", "choice9", "I'm not buying it.", "choice10", "What is it?", "choice11")
    label choice9:
        call phone_after_menu from _call_phone_after_menu_517
        call message_start("[name]", "I'll do it!!") from _call_message_start_547
        call message("Unknown", "You've already decided without hearing me out?") from _call_message_4194
        call message("Unknown", "My heart's fluttering ...") from _call_message_4195
        call message("Unknown", "I'm so lucky that you came") from _call_message_4196
        jump aftermenu4
    label choice10:
        call phone_after_menu from _call_phone_after_menu_518
        call message_start("[name]", "I'm not buying it.") from _call_message_start_548
        call message("Unknown", "Why don't you decide after hearing what I'm selling?") from _call_message_4197
        call message("Unknown", "It could turn out to be something fun.") from _call_message_4198
        call message("Unknown", "Even bed and board is provided!") from _call_message_4199
        call message("Unknown", "Isn't it amazing?") from _call_message_4200
        call message("Unknown", "Hear me out first ^^") from _call_message_4201
        jump aftermenu4
    label choice11:
        call phone_after_menu from _call_phone_after_menu_519
        call message_start("[name]", "What is it?") from _call_message_start_549
        call message("Unknown", "well...") from _call_message_4202
        jump aftermenu4
    label aftermenu4:
        call message("Unknown", "This app isn't just a messenger app, it's a messenger game app.") from _call_message_4203
        call message("Unknown", "I wanted to ask if you could test it out for me.") from _call_message_4204
        call message("Unknown", "Game concept is chatting with pretty/good-looking guys ^^") from _call_message_4205
        call screen phone_reply4("Messenger game app?", "choice12", "Doesn't sound that interesting...", "choice13", "Why don't you test it out yourself?", "choice14", "For free?", "choice15")
    label choice12:
        call phone_after_menu from _call_phone_after_menu_520
        call message_start("[name]", "Messenger game app?") from _call_message_start_550
        call message("Unknown", "This app...") from _call_message_4206
        call message("Unknown", "lets you chat with me but also with good-looking/handsome AIs.") from _call_message_4207
        call message("Unknown", "I might be a good-looking guy too. But that's for you to decide -") from _call_message_4208
        jump aftermenu5
    label choice13:
        call phone_after_menu from _call_phone_after_menu_521
        call message_start("[name]", "Doesn't sound that interesting...") from _call_message_start_551
        call message("Unknown", "Don't be so mean, my first tester. Why not try it out?") from _call_message_4209
        call message("Unknown", "You'll definitely have fun, I made this messenger with great ambition.") from _call_message_4210
        call message("Unknown", "I think it's fate that you stumbled on this app...") from _call_message_4211
        call message("Unknown", "The game's not that hard! You just need to talk to the AIs I created.") from _call_message_4212
        jump aftermenu5
    label choice14:
        call phone_after_menu from _call_phone_after_menu_522
        call message_start("[name]", "Why don't you test it out yourself?") from _call_message_start_552
        call message("Unknown", "That's quite a reasonable remark...") from _call_message_4213
        call message("Unknown", "but it's difficult to gain an objective evaluation because I'm the creator.") from _call_message_4214
        call message("Unknown", "I want to know what others think of this!") from _call_message_4215
        jump aftermenu5
    label choice15:
        call phone_after_menu from _call_phone_after_menu_523
        call message_start("[name]", "For free?") from _call_message_start_553
        call message("Unknown", "Wouldn't that be a win-win for us both?") from _call_message_4216
        call message("Unknown", "You'll have fun playing the game, and I can achieve my goal.") from _call_message_4217
        call message("Unknown", "And... wouldn't it be more suspicious if") from _call_message_4218
        call message("Unknown", "a stranger paid you to do this? ^^") from _call_message_4219
        jump aftermenu5
    label aftermenu5:
        call message("Unknown", "The chats aren't everything.") from _call_message_4220
        call message("Unknown", "There are also hidden stories...") from _call_message_4221
        call message("Unknown", "Amusing stories") from _call_message_4222
        call message("Unknown", "that will stir your imagination!") from _call_message_4223
        call message("Unknown", "All you need to do...") from _call_message_4224
        call message("Unknown", "is to play the game and tell me your honest opinion.") from _call_message_4225
        call message_img("Unknown", "These are the characters that comes out in this game ^^", "images/cg1.png") from _call_message_img_258
        call message("Unknown", "They're not real people but characters created with AI.") from _call_message_4226
        call message("Unknown", "What do you think? Don't you think it'll be fun?") from _call_message_4227
        call screen phone_reply4("OK! I'll try it.", "choice16", "No thanks. Not my style.", "choice17", "Do I just need to choose one and date?", "choice18", "Dunno about fun but if there's no reward...", "choice19")
    label choice16:
        call phone_after_menu from _call_phone_after_menu_524
        call message_start("[name]", "OK! I'll try it.") from _call_message_start_554
        call message("Unknown", "Did you find a pretty face that you like in the poster?") from _call_message_4228
        call message("Unknown", "If so, you are truly fascinating lol") from _call_message_4229
        call message("Unknown", "I've studied real hard to make this messenger game.") from _call_message_4230
        call message("Unknown", "Hope you enjoy it ^^") from _call_message_4231
        jump aftermenu6
    label choice17:
        call phone_after_menu from _call_phone_after_menu_525
        call message_start("[name]", "No thanks. Not my style.") from _call_message_start_555
        call message("Unknown", "Just a little... ") from _call_message_4232
        call message("Unknown", "couldn't you try it out for just a little?") from _call_message_4233
        call message("Unknown", "It wouldn't be late to decide whether it's your style or not after trying it out") from _call_message_4234
        call reply_message("Fine. I'll try it out.") from _call_reply_message_2
        call message("Unknown", "WOW! Thanks") from _call_message_4235
        call message("Unknown", "You're a lifesaver...^^") from _call_message_4236
        jump aftermenu6
    label choice18:
        call phone_after_menu from _call_phone_after_menu_526
        call message_start("[name]", "Do I just need to choose one and date?") from _call_message_start_556
        call message("Unknown", "You can choose one you like and chat.") from _call_message_4237
        call message("Unknown", "As for dating - why don't you start it after getting my permission? lololol") from _call_message_4238
        jump aftermenu6
    label choice19:
        call phone_after_menu from _call_phone_after_menu_527
        call message_start("[name]", "Dunno about fun but if there's no reward...") from _call_message_start_557
        call message("Unknown", "lol") from _call_message_4239
        call message("Unknown", "Hmm... Well..") from _call_message_4240
        call message("Unknown", "About the reward and all") from _call_message_4241
        jump aftermenu6
    label aftermenu6:
        call message("Unknown", "I think it'll be better if we talk over the phone about the details.") from _call_message_4242
        call message("Unknown", "Let's talk over the phone.") from _call_message_4243
        call message("Unknown", "Don't freak out when you see an unknown number and answer the phone plz ^^") from _call_message_4244
        call message ("SYSTEM", "Unknown has left the chatroom.") from _call_message_4245
        call phone_end from _call_phone_end_33

    show MC MC16 with dissolve
    play sound "music/answer.ogg"
    "(Cellphone vibrating.)"
    "(Incoming Call.)"
    menu:
        "(Answer the call.)":
            play sound "music/beep.ogg"
            jump vn1
        "(Don't answer the call.)":
            mc "(The phone automatically answered the call...)"
            play sound "music/beep.ogg"
            show MC MC15
            mc "(What's this?)"
            jump vn1
    label vn1:
        show MC MC14
        unknown "Hey. It's me, the one who was just chatting with you."
        menu:
            "Unknown?":
                unknown "Yeah, it's me."
                unknown "Your voice over the phone is cute. I can't wait to see you..."
                jump vn2
            "You've got the wrong number.":
                unknown "There's no way that's true..."
                unknown "My algorithm is never wrong."
                jump vn2
            "How did you know my number?":
                unknown "It's automatically collected when you log-on to the messenger."
                unknown "Oh ummm, don't get me wrong, as it's only dialable within the messenger app."
                jump vn2
    label vn2:
        unknown "As I mentioned in the chat room, I called to explain to you about this app."
        unknown "And I also thought that talking while you hear my voice will be more credible."
        unknown "In truth, a tutorial within the game was supposed to explain everything"
        unknown "but it's still being modified."
        unknown "Do you remember the characters I sent you?"
        menu:
            "Yes, I remember.":
                unknown "Yeah, the image I sent you of the game characters I'm currently developing."
                unknown "Those characters will be your chatting partners."
                jump vn3
            "I can barely recall.":
                unknown "Maybe they weren't as good-looking... sorry, but could you check your chat-log again later?"
                unknown "The image of the five characters should still be in there."
                jump vn3
    label vn3:
        unknown "All five characters in the image are part of an association called R.F.A."
        menu:
            "R.F.A.?":
                jump vn4
            "Is that short for Ramen Filled Aristocrat?":
                unknown "...wow. For..real...?"
                unknown "I'll pretend I didn't hear that."
                jump vn4
    label vn4:
        unknown "RFA is a closed organization with the purpose of holding fundraising parties."
        unknown "The plot is set to open a fundraising party with your help."
        unknown "You've been set as the party coordinator, in charge of inviting guests."
        unknown "Just think that you are the one choosing whom to invite to the party."
        menu:
            "I can just choose guests however I like?":
                unknown "The characters inside the game will recommend you the guests."
                unknown "Depending on how you answer them, the guests may or may not come."
                jump vn5
            "Are there any merits in gathering more guests?":
                unknown "Merits? Yeah, of course. Maybe increase my sales performance?"
                unknown "Haha... JK. I'll spoil the fun if I tell you right now."
                unknown "Isn't it no fun if you know in advance? You should find out yourself by playing the game."
                jump vn5
    label vn5:
        unknown "Of course it'll be a disappointment if it's only inviting guests."
        unknown "You will find out their secrets by chatting/answering phone calls with the characters."
        unknown "It'll be much faster to understand by playing, instead of listening to all this."
        menu:
            "Alright, fine, I'll do it.":
                unknown "Thanx! I was terrified that you'll refuse."
                jump vn6
            "I want to know what the hourly pay is.":
                unknown "Umm... ummmm......."
                unknown "Couldn't you think of it as a volunteer work?"
                jump vn6
            "So it's something like the currently trending e-boyfriend thing.":
                unknown "Haha, there's an expression like that?"
                unknown "I mean, having a boyfriend in your screen is pretty common in this era."
                unknown "Oh, and as you've seen from the image, there are not only male characters but also a female one."
                jump vn6
    label vn6:
          unknown "Right. But this game hasn't been released yet and cannot be revealed to the market."
          unknown "So, to perform the tests, you have to come over here."
          show MC MC13
          menu:
            "Where's here?":
                unknown "You probably won't know even if I told you. It's in the mountains and doesn't appear on the map."
                unknown "To maintain confidentiality before the release, it's being developed in a sparsely populated area."
                unknown "Of course, you don't have to come here yourself as it's troublesome."
                unknown "I'll send a car over to your place if you tell me your address."
                unknown "Hop onto that and head this way."
                show MC MC11
                menu:
                    "Ummm... Alright.":
                        unknown "Wow, I didn't expect an okay at one go. You trust me, right? Thanks."
                        jump vn7
                    "My address? I don't think that'll be possible.":
                        unknown "Ah, it doesn't have to be your address."
                        unknown "Tell me the nearest address. I'll send the car over where it's convenient for you."
                        jump vn7
                    "Fine. As long as if it doesn't have to be my home address.":
                        unknown "No, it doesn't have to. Can you send me an address by text msg?"
                        jump vn7
    label vn7:
        mc "(Send address.)"
        unknown "I'll send a car right now to the address you've sent me. Please be there."
        unknown "Oh. I'm telling you this in advance, so don't freak out."
        unknown "The location I'm at is also confidential, and it can't be revealed."
        unknown "You won't be able to see the road to this place. Don't be scared and do as I tell you."
        unknown "I should hang up now. I should also get ready."
        menu:
            "Get ready?":
                unknown "Yes. Get ready to greet you."
                jump vn8
            "Don't hang up.":
                unknown "...in fact, I don't want to either."
                unknown "But I have to prepare for our meeting, so I have no choice. I mean, I should be ready and greet you."
                jump vn8
    label vn8:
        unknown "My heart's fluttering from the thought that I can meet you soon..."
        unknown "Everything will be complete once you're here."
        unknown "Then...I'll see you later."
        play sound "music/beep.ogg"
        "The phone disconnected."
        "......"
        mc "(Go outside.)"
        scene bg4 with dissolve
        play music "music/music3.ogg"
        show MC MC13
        driver "Are you [name]? I've been ordered to pick you up."
        driver "The location is confidential, can you put on these sleep masks?"
        menu:
            "Can I not use it?":
                driver "I'm sorry but please understand. Even the road to the place is confidential..."
                show MC MC11
                menu:
                    "Alright, then.":
                        driver "Thank you for understanding."
                        jump vn9
                    "Still, no.":
                        driver "I also have sleeping pills ready for use."
                        driver "If using the sleep mask is uncomfortable, would you take a sleeping pill?"
                        show MC MC15
                        mc "Oh my god... Give me the sleep mask."
                        driver "Here."
                        jump vn9
                    "Yes. Thank you.":
                        driver "Thank you for being considerate."
                        jump vn9
    label vn9:
        hide MC
        scene black with dissolve
        driver "We should get going."
        "(Moving...)"
        "..."
        "......"
        scene bg4 with dissolve
        show MC MC14 with dissolve
        "(Car stopped)"
        driver "We have arrived. Please don't get off yet."
        driver "And I'm sorry, but you have to wear the sleeping mask again."
        hide MC with dissolve
        scene black with dissolve
        driver "...Mr. Ren has arrived."
        play sound "music/dooropen.ogg"
        "(Car door opened)"
        whooo "Welcome. I've been waiting. Welcome to this wonderful place."
        whooo "It wouldn't have been an easy decision to come here... Thanks for trusting me."
        menu:
            "Who are you?":
                whooo "Someone who's been anxiously waiting for you."
                whooo "Don't you recognize my voice? We just talked on the phone."
                whooo "Would it be easier for you to understand if I say the username Unknown?"
                jump vn10
            "Unknown?":
                whooo "You knew who I was by just listening to my voice?"
                whooo "Yeah, it's me. Username Unknown."
                jump vn10
            "Is this that awesome place?":
                whooo "Oh, it's nothing really. I meant that staying here will be fun like being in paradise."
                whooo "But before that, I haven't introduced myself, have I?"
                jump vn10
    label vn10:
        whooo "Didn't you think Unknown was a funny name? It was a default name because I didn't set up a username. No special meaning."
        whooo "My name is Ren."
        ray "Thanks for coming all this way."
        ray "I really want to show you around, I prepped up this and that while waiting for you."
        ray "Oh, don't take off the mask yet. You can't take it off until we reach your room."
        menu:
            "My room?":
                ray "Yes. I prepared a comfortable space for you while you're here."
                ray "I mean, you have to stay here throughout the testing."
                ray "I was worried because I didn't know what you'll like..."
                ray "I hope you like it."
                jump vn11
            "Why can't I take the mask off?":
                ray "Because there's a lot of secrets here."
                ray "If you happen to learn something you're not supposed to, it might endanger you. I don't want that."
                ray "Don't be alarmed. You'll be fine as long as you do what I tell you to do."
                ray "You're scared because you can't see, aren't you? Listen to my voice. I'll try my best to comfort you."
                jump vn11
            "That's shady.":
                ray "There's nothing shady here. This place is like my spiritual hometown."
                ray "Trust me... There's no way I'm going to hurt you."
                ray "You're my priceless guest. It was tough bringing you here, girl."
                jump vn11
    label vn11:
        ray "I'll hold your hand on your way down the car."
        menu:
            "Thank you.":
                ray "You're thanking me?"
                ray "No, thank you. You are doing what I tell you to do."
                jump vn12
            "I can get down by myself.":
                ray "...I'm worried."
                ray "You can't even see. What if you fall as you come down by yourself?"
                ray "I'll help you if I think you're going to fall. Calm down, and take your time."
                jump vn12
    label vn12:
        play sound "music/doorclose.ogg"
        "(Car door closed)"
        ray "Shall we go? Just trust me and follow me."
        ray "Hold on to my hand."
        "(Walking...)"
        "...."
        "..."
        ".."
        "(After walking about 5 minutes. )"
        ray "You have no idea what a relief it was when you said you'll help."
        ray "What a relief, thanks to you,"
        ray "You'll be a big help."
        menu:
            "What were you going to do if I said no?":
                ray "...well"
                ray "I don't want to think about it."
                ray "We could have delayed the game release date."
                ray "Or it could have been a disaster, nobody knows."
                jump vn13
            "I'm getting straight out of here if it's boring after playing the game.":
                ray "I guess yes, if that's what you really want to do."
                ray "On the flip side, you might ask to play more because it's fun."
                ray "And even though you chose to come here... I'm not sure you can go out even if you wanted to."
                ray "Hahaha, just joking. You can leave whenever you want."
                jump vn13
            "I'm here to have fun.":
                ray "Of course. Enjoy yourself."
                ray "I'll do my best to make you happy"
                ray "because... it's connected to the others' happiness."
                jump vn13
    label vn13:
        ray "We're here. Come, I'll remove your mask."
        scene white with dissolve
        stop music
        scene bg5 with dissolve
        play music "music/music4.ogg"
        show MC MC15 at my_right with dissolve
        show U U11 at my_left with dissolve
        ray "Hi!"
        show U U12
        ray "First time seeing each other face-to-face, isn't it?"
        show MC MC16
        menu:
            "Wow, seeing you, I want to pursue you first!":
                show U U11
                ray "Me? Haha...but I'm not a character in the game..."
                ray "Anyway, you like my looks, [name]? That feels good!"
                jump vn14
            "Hello..... Nice to meet you.":
                show U U11
                ray "Yeah. It feels great to talk to you in person, eye-to-eye."
                jump vn14
    label vn14:
        show U U11
        show MC MC11
        ray "Thanks for trusting me and coming here."
        show U U12
        ray "This is your room."
        show U U11
        ray "I've tried my best in preparing all this...still, let me know of any inconvenience"
        show U U12
        ray "and you're free to roam this floor, but for other floors you'll have to tell me first."
        show U U11
        ray "The reason... you know why, right?"
        menu:
            "No idea.":
                show U U11
                ray "It's because of security. We can't have any information leaked."
                ray "I mean, wouldn't it be a big deal if someone found out what game we're developing even before it's completed? You understand, right?"
                jump vn15
            "Because it's confidential?":
                show U U11
                ray "Correct. You know very well. You have good memory."
                ray "I don't hate smart people."
                ray "I get the feeling that you'll pull this off quite well."
                jump vn15
            "Why are there so many things forbidden to do?":
                show U U11
                ray "I know it can be a bit frustrating, but I'm only telling you what you shouldn't do for your sake."
                ray "There's a reason why a secret's a secret."
                jump vn15
    label vn15:
        show U U12
        ray "We still have some time left, I'll explain a bit about the game."
        ray "As I mentioned before, the purpose of the game is to hold a party with RFA."
        ray "All the characters that appear there are AIs that I've designed."
        show U U11
        ray "There's one problem as I've tried to make it super-realistic with the AI."
        menu:
            "What problem?":
                show U U12
                ray "It's... Umm..."
                jump vn16
            "Is there a bug?":
                show U U12
                ray "aha. No, not a bug but"
                jump vn16
    label vn16:
        show U U11
        ray "The AIs turn super-suspicious when a new person comes in."
        show U U12
        ray "Your concept is that someone hacked your messenger and that's how you got into the RFA chatroom."
        show U U11
        ray "Quite suspicious, isn't it?"
        mc "Someone hacked the messenger?"
        show U U11
        ray "Yes. And then you join the chatroom."
        show U U11
        ray "So, everyone will be cautious of you."
        show U U11
        ray "That's the setting of the game. It's no fun when everyone likes you from the start, is it?"
        show U U11
        ray "They'll ask you about the hacker. But you can't answer them because you know nothing..."
        show U U12
        ray "They'll try to find out how you got to download this app, where you currently are..."
        ray "and might even try to pry out about the creator, me."
        show U U11
        ray "At that, you can't reveal the truth. You must keep the secret to the end."
        show U U11
        ray "Can you promise me that you won't reveal what we've been talking about to the AIs?"
        menu:
            "Umm... Okay.":
                show U U12
                ray "Thanks! I'm so glad that you're so cooperative."
                ray "Meeting you must have been... Fate."
                ray "You have to be careful! The moment you reveal the truth, it's game over."
                jump vn17
            "What happens if I speak the truth?":
                show U U12
                ray "Game... over?"
                ray "I'll also be devastated when you finish as game over... So never ever tell the truth."
                jump vn17
    label vn17:
        show U U11
        ray "Don't forget –"
        show U U12
        ray "How you got this app, your current location... it's all a secret."
        ray "and who I am, the fact they're AIs are obviously a secret."
        show U U11
        ray "You have be careful, if the game goes through forced shutdown, you have start all over again from the beginning."
        show U U11
        ray "There are slight difference depending on how you talk to them"
        show U U11
        ray "but if they don't accept you into the association easily, say, ‘Someone called Reina sent me to hold the party.'"
        menu:
            "Who's Reina?":
                show U U12
                ray "She's a character within the game."
                ray "The other characters will tell you what kind of character she is, but simply put, she's the one who founded the group in the game."
                jump vn18
            "What happens when I tell them that Reina sent me?":
                show U U12
                ray "The game mode changes. The characters will show you some colorful reactions."
                ray "It's best if you try this out yourself. Try it out later."
                jump vn18
    label vn18:
        ray "And as I mentioned through the phone, your role in the game is party coordinator."
        ray "Depending on how you talk to your potential party guests, they will either come to the party or decline."
        ray "Inviting the guests is more crucial than you think, try to invite them with your best effort."
        show U U11
        ray "Don't forget that there might be more interesting events than the RFA party, depending on what choices you make."
        show U U11
        ray "And last of all, this is the most important thing."
        show U U12
        ray "You have to tell me what you think as you play the game."
        ray "Even the smallest thing is okay. Tell me everything, how the party went, what the characters said to you..."
        show U U11
        ray "That way, I'll know how well the game is going... Your role is important."
        menu:
            "Yes, leave it to me.":
                show U U11
                ray "You're very confident! I'm also getting more confident thanks to you. I'll work even harder making this app."
                ray "It was worth waiting for a tester..."
                jump vn19
            "I think it'll end with hosting parties and no chance to build a relationship.":
                show U U11
                ray "Hahaha, you don't have to worry about that."
                ray "You're already truly attractive, the AIs will notice your charm."
                ray "Just keep the secrets well, okay?"
                jump vn19
    label vn19:
        show U U12
        ray" Well then, can you give me your phone for a sec? I'll install the game."
        menu:
            "Here.":
                show U U11
                ray "Thanks, I only need it for a few minutes."
                jump vn20
            "Do I have to?":
                show U U11
                show MC MC13
                ray "Yeah. I can't tell you the download route of the app for security reasons."
                ray "That's why someone who knows it has to install it directly."
                ray "It'll take only a few minutes."
                show MC MC11
                mc "Alright, fine. Here."
                jump vn20
    label vn20:
        show U U12
        ray "A few moments, please."
        show U U11
        ray "It's done. Here, it'll finish installing in a few minutes."
        show U U12
        ray "Do you have any other questions about the game?"
        menu:
            "How do I pursue you, Ren?":
                show U U11
                ray "Me? Hahaha..."
                ray "Though I'm not a character inside the game..."
                ray "I've always dreamed of someone who's a good listener to what I say."
                ray "My voice is quite small... most people don't seem to listen very well."
                jump vn21
            "Until when do I have to stay here?":
                show U U11
                ray "Can't really give you a definite date..."
                ray "but you'll have to stay here for the time being until the test is over."
                ray "Of course, if you want to stay longer, you can."
                jump vn21
            "Not really.":
                show U U11
                ray "Really?"
                ray "Speaking of which, if you have any questions later, you can ask me anytime. Whenever is fine. Anything is okay."
                jump vn21
            "Do you by chance have a walkthrough guide for this?":
                show U U11
                ray "Sadly, no."
                ray "But it'll be fun to make one with your play data."
                ray "I'll think some more about that."
                jump vn21
    label vn21:
        show U U12
        ray "Oh, and lastly... I know I keep emphasizing this but... don't forget to keep the secret."
        show U U11
        ray "Don't ever... try to get the game over easily. Promise me that, please?"
        menu:
            "I promise.":
                show U U11
                ray "Thanks a ton!"
                jump vn22
            "Umm, they're really AIs, right?":
                show U U11
                show MC MC13
                ray "No doubt."
                ray "You'll find out right away that they're not as interesting as me..."
                jump vn22
    label vn22:
        show MC MC11
        show U U11
        ray "I want to talk to you a bit more, but I have to get going now."
        show U U12
        ray "The plot is that when you start the game, the messenger has been hacked and they're all surprised that you suddenly entered the chatroom."
        show U U11
        ray "Okay then, give it your best, coordinator."
        hide U with dissolve
        "......"
        "There is a game application on the screen I've never seen before."
        mc "(Start the game.)"
        stop music
        hide MC

    play music "music/music2chat-17.ogg"
    call phone_start from _call_phone_start_33
    call message_start("SYSTEM", "[name] has entered the chatroom.") from _call_message_start_558
    call message("Shun", "I saw on TV yesterday, Ryou's book is now a bestseller.") from _call_message_4246
    call message("Yoshiro", "Oh that -") from _call_message_4247
    call message("Yoshiro", "You mean Ryou's quote collection book?") from _call_message_4248
    call message("Shun","Yea that book. Isn't the title so strange too?") from _call_message_4249
    call message_img("000","The Successful Path of a Certain Man,,,,", "images/ohyeah.png") from _call_message_img_259
    call message("Yoshiro","lollll") from _call_message_4250
    call message("Yoshiro","So cringy lollll") from _call_message_4251
    call message("Shun","yes.....") from _call_message_4252
    call message("Shun","Why is it even selling?") from _call_message_4253
    call message("Shun","Who the hell buys it? Even the name of the book is so full of it") from _call_message_4254
    call message("000","Someone who wanna succeed like him?") from _call_message_4255
    call message("Shun","I don't get it") from _call_message_4256
    call message("Shun","for real-_-") from _call_message_4257
    call message("Shun","He's the standard in success?") from _call_message_4258
    call message("Shun","One with a golden heart should be successful!") from _call_message_4259
    call message("Yoshiro","Then how golden is ur heart?") from _call_message_4260
    call message("Shun","More golden than the next-to-be CEO dude;;;") from _call_message_4261
    call message("000","I like his book, its hilarious lol") from _call_message_4262
    call message("000","I can literally hear Ryou's voice dubbed when reading lol") from _call_message_4263
    call message_img("Shun","argh;;;", "images/well.png") from _call_message_img_260
    call message("Shun","It creeps me out just thinking about it.") from _call_message_4264
    call message("000","Success is not something you work hard for. If you are competent, success will naturally follow.") from _call_message_4265
    call message("Yoshiro","oh god") from _call_message_4266
    call message("Shun","Hey") from _call_message_4267
    call message("Shun","Stop it") from _call_message_4268
    call message("Yoshiro","I think I just heard Ryou's voice") from _call_message_4269
    call message("Ryou","That quote must have been your favorite.") from _call_message_4270
    call message("Ryou","I will have that in mind when the sequel is out.") from _call_message_4271
    call message("Shun","Don't do that") from _call_message_4272
    call message_img("Yoshiro","There's a sequel?!", "images/shocked.png") from _call_message_img_261
    call message("Shun","Don't write it") from _call_message_4273
    call message("Ryou","Yes") from _call_message_4274
    call message("Shun","I said don't write it.") from _call_message_4275
    call message("Sayaka","...The meeting with the publisher just ended.") from _call_message_4276
    call message("K","If the sequel is coming out, I guess a lot of people are reading it.") from _call_message_4277
    call message("K","Is it coming out as a series?") from _call_message_4278
    call message("Ryou","Not sure. Today's contract was finalized with Book 3, for now.") from _call_message_4279
    call message_img("Shun","OMG.....", "images/well.png") from _call_message_img_262
    call message("000","Ooooh, must preorder lol") from _call_message_4280
    call message("Shun","There are two more follow-ups of that weird book?") from _call_message_4281
    call message("000","I shud memorize all the call messages") from _call_message_4282
    call message_img("Ryou","I am pleased to have such a loyal reader.","images/smile.png") from _call_message_img_263
    call message("000","Villains twinge in pain when u recite 1 by 1 to crush them. Lol") from _call_message_4283
    call message("Shun","That's your purpose? ;;") from _call_message_4284
    call message("000","K, have u read the book too?") from _call_message_4285
    call message("K","No. I do have the book Ryou sent me but haven't read it yet.") from _call_message_4286
    call message("K","Seeing how everyone's reacting, it's making me curious...") from _call_message_4287
    call message("K","I should spare sometime someday to read it.") from _call_message_4288
    call message("Shun","No, K...;;; I think it's better if you don't at all...") from _call_message_4289
    call message("Yoshiro","Ryou's book...") from _call_message_4290
    call message("Yoshiro","seems like it's selling super well.") from _call_message_4291
    call message("Yoshiro","Do you think it would come into my school's library?") from _call_message_4292
    call message("Sayaka","I do not know. If requested, it might.") from _call_message_4293
    call message("Shun","I don't know who'll request such a book. ;;;") from _call_message_4294
    call message("Shun","Have you read the book too, Sayaka?") from _call_message_4295
    call message("Sayaka","Ah, yes. I did read it.") from _call_message_4296
    call message("Sayaka","But I closed it after reading one page.") from _call_message_4297
    call message("000","lol") from _call_message_4298
    call message("Sayaka","Felt like I was working...") from _call_message_4299
    call message("Shun","If I") from _call_message_4300
    call message("Shun","get to have his book") from _call_message_4301
    call message("Shun", "I think that'll make the two of us") from _call_message_4302
    call message("Sayaka","As Levan said, I could hear Mr. Han's voice from the first line of the book;;") from _call_message_4303
    call message("Shun","Omg never ever want that to happen") from _call_message_4304
    call message("Ryou","You don't like my voice?") from _call_message_4305
    call message("Sayaka","I did not want to feel as if I as working at home.") from _call_message_4306
    call message("Yoshiro","You're a secretary, is it okay not to read it?") from _call_message_4307
    call message("Ryou","I cannot force her to read my book if it is not her taste.") from _call_message_4308
    call message("Ryou","No need to force her. It's selling well already.") from _call_message_4309
    call message("Shun","Geez;;; what a show-off") from _call_message_4310
    call message("K","It's true.") from _call_message_4311
    call message("K","It's been No.1 bestseller for some time now.") from _call_message_4312
    call message("Ryou","I'm telling you...") from _call_message_4313
    call message("Ryou","I've never been a show-off") from _call_message_4314
    call message("Ryou","I was just born extraordinary") from _call_message_4315
    call message("Yoshiro","Frankly, I can't refute that.") from _call_message_4316
    call message("Yoshiro","Woohoo Boo I so don't wanna study~ ^^.") from _call_message_4317
    call message_img("Sayaka","Everything will pass if you just roll with it.", "images/happy.png") from _call_message_img_264
    call message("000","?") from _call_message_4318
    call message("000","??") from _call_message_4319
    call message("000","?????") from _call_message_4320
    call message("Yoshiro","Why, what's wrong?") from _call_message_4321
    call message("Sayaka","Levan, please do not start hogging the chatroom.") from _call_message_4322
    call message_img("000","??????!?!?!?!", "images/shocked.png") from _call_message_img_265
    call message("Shun","What's wrong with you?") from _call_message_4323
    call message("Shun","Did a cockroach pop out or something?") from _call_message_4324
    call message("000","Intruder") from _call_message_4325
    call message("000","Intruder") from _call_message_4326
    call message("Sayaka","If it's a pest, kill with Kombat.") from _call_message_4327
    call message("000","WEEEOOOEEE!") from _call_message_4328
    call message("000","WEEEOOOEEE!") from _call_message_4329
    call message("000","Issuing security alert!") from _call_message_4330
    call message("Yoshiro","Zero;;;") from _call_message_4331
    call message("Sayaka","The hogging has started.") from _call_message_4332
    call message("Shun","What do you mean by intruder? lol") from _call_message_4333
    call message("Shun","In your room?") from _call_message_4334
    call message_img("Shun","Or in our chat room?", "images/shocked.png") from _call_message_img_266
    call message("K","What did I just see...?") from _call_message_4335
    call message("K","[name]?") from _call_message_4336
    call message("Yoshiro","Forrrealll!!!") from _call_message_4337
    call message("Yoshiro","Thers someone we dontl know in here!!!") from _call_message_4338
    call message("Sayaka","It's true... I have never seen that username.") from _call_message_4339
    call message("Shun","It's not someone among us who logged in after changing username, is it?...") from _call_message_4340
    call message("000","its not, we r all here already") from _call_message_4341
    call message("Shun","right;") from _call_message_4342
    call message("Yoshiro","thenwhoois thayt?!") from _call_message_4343
    call message("Ryou","Did you not say only the RFA members can use this app in the first place?") from _call_message_4344
    call message("000","yyes....") from _call_message_4345
    call message("Ryou","Then what is this?") from _call_message_4346
    call message("Ryou","K, did you allow a new member to come aboard?") from _call_message_4347
    call message("K","No, I don't recall such thing...") from _call_message_4348
    call message("K","It's my first time seeing that person too.") from _call_message_4349
    call screen phone_reply4("Hi!","choice21","How do you do?","choice22", "Aren't you all pretty slow in noticing me? It's been a while since I came in.", "choice23", "Why don't you finish up talking about the quotes? Do you have any other famous quotes?", "choice24")
    label choice21:
        call phone_after_menu from _call_phone_after_menu_528
        call message_start("[name]", "Hi!") from _call_message_start_559
        call message("Ryou","Hi") from _call_message_4350
        call message("Ryou","...is not something we would say.") from _call_message_4351
        jump aftermenu7
    label choice22:
        call phone_after_menu from _call_phone_after_menu_529
        call message_start("[name]", "How do you do?") from _call_message_start_560
        call message("Yoshiro","Umm...") from _call_message_4352
        call message("Yoshiro","Hello?") from _call_message_4353
        call message("Sayaka","Greetings are great, but first") from _call_message_4354
        jump aftermenu7
    label choice23:
        call phone_after_menu from _call_phone_after_menu_530
        call message_start("[name]", "Aren't you all pretty slow in noticing me? It's been a while since I came in.") from _call_message_start_561
        call message("000","Srry") from _call_message_4355
        call message("000","We didn't kno cuz u weren't talking lolololllll") from _call_message_4356
        jump aftermenu7
    label choice24:
        call phone_after_menu from _call_phone_after_menu_531
        call message_start("[name]", "Why don't you finish up talking about the quotes? Do you have any other famous quotes?") from _call_message_start_562
        call message_img("000","u curious?", "images/expecting.png") from _call_message_img_267
        call message("000","If u r curious") from _call_message_4357
        call message("000","order") from _call_message_4358
        call message("000","right") from _call_message_4359
        call message("000","now!") from _call_message_4360
        call message("Yoshiro","For a moment, I thought it was a home shopping ad;") from _call_message_4361
        call message("000","lolololol") from _call_message_4362
        call message("Ryou","I'm more interested in knowing who you are instead of talking about what's already on television.") from _call_message_4363
        call message("Shun","This is not the time to joke around!") from _call_message_4364
        call message("Sayaka","Username [name].") from _call_message_4365
        jump aftermenu7
    label aftermenu7:
        call message("Sayaka","Identify yourself.") from _call_message_4366
        call message("Ryou","Suspicious how K has no idea of what is going on.") from _call_message_4367
        call message("Shun","Zero, did you distribute this app as a public app?") from _call_message_4368
        call message_img("000","Nope;;", "images/questioning.png") from _call_message_img_268
        call message("000","huh?") from _call_message_4369
        call message("000","W-a") from _call_message_4370
        call message("000","i-t") from _call_message_4371
        call message("Yoshiro","What what;") from _call_message_4372
        call message("000","lololol") from _call_message_4373
        call message("000","There's a written challenge email in my spam box.") from _call_message_4374
        call message_img("Shun","Did you stir up trouble somewhere?", "images/questioning.png") from _call_message_img_269
        call message("000","Yes... Seems likely.") from _call_message_4375
        call message_img("Sayaka","Why don't you explain the whole story from the beginning?", "images/well.png") from _call_message_img_270
        call message("Ryou","Why are we moving on from an intruder/intrusion to a challenge?") from _call_message_4376
        call message("000","haha....hahahahaha...") from _call_message_4377
        call message("Ryou","Explain the casuality.") from _call_message_4378
        call message("000","Actually, I hacked into a server that hackers use four days ago.") from _call_message_4379
        call message("000","I left a cat paw print") from _call_message_4380
        call message("000","meowing") from _call_message_4381
        call message("000","They sent me a challenge to take revenge.") from _call_message_4382
        call message("Shun","To the spam mail box...?") from _call_message_4383
        call message("Ryou","Cat paw print...?") from _call_message_4384
        call message("000","I'm thinking they hacked this messenger server to take revenge hehehehehehe") from _call_message_4385
        call message_img("Shun","What in the world have you been doing?", "images/well.png") from _call_message_img_271
        call message_img("000","They were bragging how their server security is iron tight!", "images/sad.png") from _call_message_img_272
        call message("000","And said they're #1 in world rankings...") from _call_message_4386
        call message("000","So I took the courage and challenged them!!") from _call_message_4387
        call message("Ryou","Therefore, an unidentified person's intrusion on this private messenger is the result of your challenge?") from _call_message_4388
        call message("000","Maybe...") from _call_message_4389
        call message("000","maybe not..!") from _call_message_4390
        call message_img("Sayaka","In conclusion, there's a high change that this has to do with Levan's private affairs...", "images/well.png") from _call_message_img_273
        call screen phone_reply("Exactly.","choice25","Can this game be hacked too?","choice26")
    label choice25:
        call phone_after_menu from _call_phone_after_menu_532
        call message_start("[name]", "Exactly.") from _call_message_start_563
        call message("000","Whoohoo lol?") from _call_message_4391
        call message("000","You, me, hacking match, wanna give it a go?") from _call_message_4392
        call message_img("Shun","I think that's enough...", "images/well.png") from _call_message_img_274
        jump aftermenu8
    label choice26:
        call phone_after_menu from _call_phone_after_menu_533
        call message_start("[name]", "Can this game be hacked too?") from _call_message_start_564
        call message("Sayaka","Game?", "images/questioning.png") from _call_message_4393
        call message("Yoshiro","Umm, this is a messenger app, not a game...") from _call_message_4394
        call message("Ryou","Don't be reckless and refrain from yielding information to an unidentified stranger, if you please.") from _call_message_4395
        jump aftermenu8
    label aftermenu8:
        call message("Shun","Then, is that someone from the hackers' group that Zero mentioned?") from _call_message_4396
        call message("000","Umm...") from _call_message_4397
        call message("000","No...") from _call_message_4398
        call message("000","That's just my assumption. ........") from _call_message_4399
        call message("000","Hehehe >_<") from _call_message_4400
        call message_img("Yoshiro","Stop fooling around and do something...", "images/well.png") from _call_message_img_275
        call message("000","kay") from _call_message_4401
        call message("000","I'm actually trying to find traces of the intrusion right now lololl") from _call_message_4402
        call message("Ryou","For such unexpected occurrence, it's time for the head of RFA to take action - that means you, K.") from _call_message_4403
        call message("K","I'm having a slight meltdown myself...") from _call_message_4404
        call message("Shun","K in a meltdown...? T-T") from _call_message_4405
        call message("K","I didn't think someone could actually break into an app that Levan made.") from _call_message_4406
        call message("000","Yessss It's my first time too") from _call_message_4407
        call message("000","So actually, I'm slightly...") from _call_message_4408
        call message_img("000","excited...", "images/wink.png") from _call_message_img_276
        call message_img("Sayaka","How can you even say 'excited' right now?", "images/well.png") from _call_message_img_277
        call message_img("000","I will immediately begin the search for its identity.", "images/sad.png") from _call_message_img_278
        call message("000","~Today's HW~") from _call_message_4409
        call message("Yoshiro","But first, why don't we just ask directly who he/she is?") from _call_message_4410
        call message("Ryou","Doubt it'll be honest with us... but not a bad idea.") from _call_message_4411
        call message("Yoshiro","U don't know until u try.") from _call_message_4412
        call message("Yoshiro","hey. Where are you from, [name]?") from _call_message_4413
        call screen phone_reply3("From another planet", "choice27", "From the bottom of your heart.", "choice28", "I've come to tell you something good.", "choice29")
    label choice27:
        call phone_after_menu from _call_phone_after_menu_534
        call message_start("[name]", "From another planet") from _call_message_start_565
        call message_img("000","No wonder you felt so familiar", "images/shocked.png") from _call_message_img_279
        call message("000","are u from my hometown!?") from _call_message_4414
        call message("Yoshiro","What r u talking about...?") from _call_message_4415
        call message("Shun","Oh god, you're giving me a headache...") from _call_message_4416
        jump aftermenu9
    label choice28:
        call phone_after_menu from _call_phone_after_menu_535
        call message_start("[name]", "From the bottom of your heart.") from _call_message_start_566
        call message("Ryou","I'm curious what kind of environment this guy is at.") from _call_message_4417
        call message_img("Sayaka","I see you have no intention of revealing your identity for the time being.", "images/well.png") from _call_message_img_280
        call message("Shun","Perhaps you came to see me?") from _call_message_4418
        call message("K","Hyun... that does not seem to be the most appropriate joke.") from _call_message_4419
        call message("Yoshiro","It's been a long time since I've agreed with K's opinion.") from _call_message_4420
        call message("Shun","T-T") from _call_message_4421
        jump aftermenu9
    label choice29:
        call phone_after_menu from _call_phone_after_menu_536
        call message_start("[name]", "I've come to tell you something good.") from _call_message_start_567
        call message_img("Shun","No way. You're not the person who came by my house yesterday to offer good words...are you?", "images/shocked.png") from _call_message_img_281
        call message("Yoshiro","They went to your house too? They came to mine too!") from _call_message_4422
        call message("Shun","Hey, don't even mention it;;") from _call_message_4423
        call message("Shun","I was stuck with them for more than an hour at the door yesterday.") from _call_message_4424
        call message("Sayaka","We're heading off-topic.") from _call_message_4425
        jump aftermenu9
    label aftermenu9:
        call message("Sayaka","Levan, are you done yet?") from _call_message_4426
        call message_img("000","!", "images/shocked.png") from _call_message_img_282
        call message("Ryou","Levan, did you find something?") from _call_message_4427
        call message("000","What's wrong with this...?") from _call_message_4428
        call message("000","Is it not working?") from _call_message_4429
        call message("Yoshiro","Hey, what's wrong?") from _call_message_4430
        call message("000","I can't track down") from _call_message_4431
        call message_img("000","the location at all.", "images/questioning.png") from _call_message_img_283
        call message("Yoshiro","Maybe u've become rusty?") from _call_message_4432
        call message("Shun","...") from _call_message_4433
        call message("K","Levan...") from _call_message_4434
        call message("Ryou","Hmmm...") from _call_message_4435
        call message("Ryou","[name], I'll change the question.") from _call_message_4436
        call message("Ryou","For what purpose did you log into this messenger?") from _call_message_4437
        call screen phone_reply("I was told to host a party.", "choice30", "I just came to chat with pretty guys.", "choice31")
    label choice30:
        call phone_after_menu from _call_phone_after_menu_537
        call message_start("[name]", "I was told to host a party.") from _call_message_start_568
        call message("Ryou","Sounds like you've heard that from someone.") from _call_message_4438
        call message("000","Eh, party? The party's ur purpose?") from _call_message_4439
        call message("Sayaka","Our party?") from _call_message_4440
        call message("000","U didn't come here for me?") from _call_message_4441
        call message("K","By party... are you talking about the RFA fundraising party?") from _call_message_4442
        call message("Sayaka","If you really did come in here knowing about the party, you're even more suspicious.") from _call_message_4443
        call message("Ryou","Someone K's seen for the first time has come to plan the party, huh...?") from _call_message_4444
        call message("Ryou","Sadly, the decisions regarding the party can only be made by K, the head of RFA.") from _call_message_4445
        call message("Yoshiro","I can actually think of someone other than K who can start the party...") from _call_message_4446
        call message("Sayaka","Yoshiro...") from _call_message_4447
        call message("Shun","Yoshiro...") from _call_message_4448
        call message("Ryou","I know how you feel, Yoshiro, but...") from _call_message_4449
        call message("Ryou","As we all know, only K can make the decision right now.") from _call_message_4450
        jump aftermenu10
    label choice31:
        call phone_after_menu from _call_phone_after_menu_538
        call message_start("[name]", "I just came to chat with pretty guys.") from _call_message_start_569
        call message("Shun","Umm, me? lol") from _call_message_4451
        call message("Shun","ME????") from _call_message_4452
        call message("Shun","omg") from _call_message_4453
        call message("Shun","You came here to chat with me?") from _call_message_4454
        call message("Ryou","With what evidence do you think the pretty guy means you?") from _call_message_4455
        call message("Yoshiro","His face.") from _call_message_4456
        call message("Yoshiro","I'm just gonna go sit in the corner facing the wall.") from _call_message_4457
        call message("Ryou","If you think your look is pleasing to the general public, Shun, why not make a photo book instead of envying my book?") from _call_message_4458
        call message_img("Shun","There is one already.", "images/well.png") from _call_message_img_284
        call message("Sayaka","I purchased two.") from _call_message_4459
        call message("Yoshiro","I'm not buying.") from _call_message_4460
        call message("K","I received one too, but...It's slightly embarrassing to look at half-naked Hyun. ^^;") from _call_message_4461
        call message("Shun","All you need to do is to worship the beauty, K.") from _call_message_4462
        call message("000","Whoa") from _call_message_4463
        call message("000","Calm down everyone") from _call_message_4464
        call message("000","RFA's ultimate beauty is Levan Choi.") from _call_message_4465
        call message("Shun","Wha") from _call_message_4466
        call message("000","((((Serious Mode ON ))))") from _call_message_4467
        jump aftermenu10
    label aftermenu10:
        call message("000","K") from _call_message_4468
        call message("K","Hmm?") from _call_message_4469
        call message("000","What do u think?") from _call_message_4470
        call message("000","Though it's currently difficult to uncover that person's identity,") from _call_message_4471
        call message("000","we can make-do by cutting off access") from _call_message_4472
        call message("000","if you want, K.") from _call_message_4473
        call message("Yoshiro","Why do you suddenly need K's opinion?") from _call_message_4474
        call message("Ryou","Because K is the head of RFA.") from _call_message_4475
        call message("Yoshiro","His participation level in RFA makes me suspicious of his qualifications as the Head.") from _call_message_4476
        call message("Sayaka","Yoshiro ...") from _call_message_4477
        call message("Ryou","We have an unexpected situation here at the moment. You should restrain yourself from such outburst.") from _call_message_4478
        call message("Yoshiro","Srry;;") from _call_message_4479
        call message("Yoshiro","Since the party issue came about, it's true I couldn't stay chill.") from _call_message_4480
        call message("Shun","You can explode later;;") from _call_message_4481
        call message("Shun","Right now, let's do something about this person -> [name]") from _call_message_4482
        call message("Ryou","I hope you'd understand for now... it's obvious that K's opinion is prioritized in taking care of these crucial incidents.") from _call_message_4483
        call message("000","It's also written in the Association's Rules.") from _call_message_4484
        call message("000","Article 5 (1). All matters of consultation shall be subject to the final decision of the Representative K.") from _call_message_4485
        call screen phone_reply("Am I going to be blocked? I'm here because someone called Reina sent me!", "choice32", "When can I see Reina? I'm here because of her.", "choice33")
    label choice32:
        call phone_after_menu from _call_phone_after_menu_539
        call message_start("[name]","Am I going to be blocked? I'm here because someone called Reina sent me!") from _call_message_start_570
        call message("Yoshiro","WHAT?????") from _call_message_4486
        call message("Shun","OMG...") from _call_message_4487
        call message("000","Wait, I think there's something wrong with my eyes.") from _call_message_4488
        call message("Yoshiro","Reina?") from _call_message_4489
        call message("Yoshiro","Ur someone Reina sent?") from _call_message_4490
        call message("Ryou","Astonishing ...") from _call_message_4491
        call message("Yoshiro","She contacted you, [name]?") from _call_message_4492
        call message("Yoshiro","When...???") from _call_message_4493
        call message("K","...") from _call_message_4494
        jump aftermenu11
    label choice33:
        call phone_after_menu from _call_phone_after_menu_540
        call message_start("[name]", "When can I see Reina? I'm here because of her.") from _call_message_start_571
        call message("Yoshiro",".,") from _call_message_4495
        call message("000","OH MY GOD") from _call_message_4496
        call message("Shun","Someone tell me I misread that??") from _call_message_4497
        call message("Ryou","Quite baffling ...") from _call_message_4498
        call message("Yoshiro","[name], you know Reina? ?!") from _call_message_4499
        call message("Sayaka","I'm also curious about that...") from _call_message_4500
        call message("Yoshiro","What do you mean, when can you see her...?") from _call_message_4501
        call message("Ryou","It's certain [name] isn't up-to-date with Reina's state.") from _call_message_4502
        jump aftermenu11
    label aftermenu11:
        call message("Ryou","K is here with us. I hope you'd refrain from thoughtlessly mentioning Reina.") from _call_message_4503
        call message("K","No. it's okay. I'm okay.") from _call_message_4504
        call message("K","[name].") from _call_message_4505
        call message("K","I don't know how you know Reina") from _call_message_4506
        call message("K","but she's not with us anymore... in this world...") from _call_message_4507
        call message("K","She left our side 6 months ago.") from _call_message_4508
        call message("Yoshiro","That might not be true.") from _call_message_4509
        call message("Ryou","With what intentions do you say that?") from _call_message_4510
        call message("Yoshiro","That's how I believe it...") from _call_message_4511
        call message("Sayaka","Yoshiro...") from _call_message_4512
        call message("K","Levan, I have a favor to ask.") from _call_message_4513
        call message("000","Should I cut off this [name] guy's access?") from _call_message_4514
        call message("Yoshiro","NOOO!!!!") from _call_message_4515
        call message("Yoshiro","WAITTT!") from _call_message_4516
        call message("Yoshiro","Don't cut off yet!") from _call_message_4517
        call message("Yoshiro","This person might know something about Reina!!") from _call_message_4518
        call message("Yoshiro","Aren't you guys even curious?!") from _call_message_4519
        call message("Shun","I'm curious") from _call_message_4520
        call message("Shun","but more suspicious") from _call_message_4521
        call message("Ryou","I'm also starting to think this person might be dangerous.") from _call_message_4522
        call message("Yoshiro","I can't calm down...") from _call_message_4523
        call message("Yoshiro","I didn't even get to see any evidence that she's dead...!") from _call_message_4524
        call message("Ryou","Try not to distress yourself. You will miss out on something you if you act with haste.") from _call_message_4525
        call message("Sayaka","I think you're overly calm, Mr. Han.") from _call_message_4526
        call message("Ryou","There should at least be one person who can critically assess the situation. It balances what's happening.") from _call_message_4527
        call message("Shun","This guy's not even human;;") from _call_message_4528
        call message("Sayaka",";;") from _call_message_4529
        call message("K","No.") from _call_message_4530
        call message("K","I wasn't going to ask Levan to deny [name]'s access.") from _call_message_4531
        call message("K","It's actually the opposite.") from _call_message_4532
        call message("Yoshiro","Opposite?") from _call_message_4533
        call message("K","Levan, I want to make a call to [name].") from _call_message_4534
        call message("K","Can you connect me through?") from _call_message_4535
        call message("000","Umm... It's not impossible...") from _call_message_4536
        call message("000","Really?") from _call_message_4537
        call message("000","Should I connect?") from _call_message_4538
        call message("K","Yes. There's something I want to ask [name].") from _call_message_4539
        call screen phone_reply("You can just ask me here.","choice34", "Yeah! I wanna to talk to you, K! Put me through~ !", "choice35")
    label choice34:
        call phone_after_menu from _call_phone_after_menu_541
        call message_start("[name]", "You can just ask me here.") from _call_message_start_572
        call message("Yoshiro","Yea! You can just say it here!") from _call_message_4540
        call message("K","It's a slightly delicate subject to discuss in this chatroom.") from _call_message_4541
        call message("Yoshiro","Discuss what?") from _call_message_4542
        call message("Yoshiro","Is it about Reina?") from _call_message_4543
        call message("Yoshiro","Another one of your secrets behind our backs again?") from _call_message_4544
        call message("Shun","Yoshiro;;") from _call_message_4545
        call message("Shun","I understand this is upsetting") from _call_message_4546
        call message("Shun","But let's first see what happens from here...") from _call_message_4547
        call message("K","Sorry... It looks like I'm causing disputes.") from _call_message_4548
        jump aftermenu12
    label choice35:
        call phone_after_menu from _call_phone_after_menu_542
        call message_start("[name]", "Yeah! I wanna to talk to you, K! Put me through~ !") from _call_message_start_573
        call message("Ryou","Such an energetic person, regardless of the situation.") from _call_message_4549
        call message("Sayaka","Is that a compliment?") from _call_message_4550
        call message("000","Um... Um...") from _call_message_4551
        jump aftermenu12
    label aftermenu12:
        call message("000","K...") from _call_message_4552
        call message("000","I'm gonna put you two through. Is that ok?") from _call_message_4553
        call message("Yoshiro","Can we talk to [name] as well?") from _call_message_4554
        call message("000","Probably...") from _call_message_4555
        call message("000","The fact that this person is in here...means we're already connected to him/her.") from _call_message_4556
        call message("000","But K's connection is in manual mode") from _call_message_4557
        call message("000","So I have to open that connection to make it alive...") from _call_message_4558
        call message("Yoshiro","I... I wanna hurry up and talk with [name].") from _call_message_4559
        call message("000","K, is it okay to put you through?") from _call_message_4560
        call message("K","Yes, it's okay. Put me through.") from _call_message_4561
        call message("Ryou","K, I believe you would have thoroughly assessed and examined the situation. Hope it's not a haste decision") from _call_message_4562
        call message("K","I appreciate for your concern, Ryou.") from _call_message_4563
        call message("K","There is something I must ask [name], even if there are risks I need to endure.") from _call_message_4564
        call message("K","If, as [name] has said, 'Reina' is the one who has sent [name] to this messenger...") from _call_message_4565
        call message("K","I'd like to think Reina wanted the RFA parties to continue even when she's gone.") from _call_message_4566
        call message("Shun","If that's what you think, K, I guess it could be.") from _call_message_4567
        call message("Sayaka","If this is what Reina really hoped for...") from _call_message_4568
        call message("Yoshiro",".......") from _call_message_4569
        call message("K","[name], I'll call you in a bit...") from _call_message_4570
        call screen phone_reply("Was K close to Reina?", "choice36", "I was only told to open a party.", "choice37")
    label choice36:
        call phone_after_menu from _call_phone_after_menu_543
        call message_start("[name]", "Was K close to Reina?") from _call_message_start_574
        call message("000","Yes, very close") from _call_message_4571
        call message("Ryou","They were engaged.") from _call_message_4572
        jump aftermenu13
    label choice37:
        call phone_after_menu from _call_phone_after_menu_544
        call message_start("[name]", "I was only told to open a party.") from _call_message_start_575
        call message("K","Yes. I feel that is what Reina wants to happen.") from _call_message_4573
        call message("K","That is, if Reina really did send you, [name].") from _call_message_4574
        call message("Ryou","Since you are Reina's fiance, K, and if you feel that way... that could be what Reina wants.") from _call_message_4575
        jump aftermenu13
    label aftermenu13:
        call message("K","If Reina wanted the parties to continue") from _call_message_4576
        call message("K","I want to make that come true.") from _call_message_4577
        call message("Ryou","You are the final decision maker, so we should comply") from _call_message_4578
        call message("Ryou","but I'm concerned that [name]'s identity remains a mystery.") from _call_message_4579
        call message("Sayaka","That is true, since we do not know anything about [name].") from _call_message_4580
        call message("Sayaka","So I'm not sure if we can leave Reina's role to [name]. ...") from _call_message_4581
        call message("Yoshiro","What kind of person do you think [name] is?") from _call_message_4582
        call screen phone_reply("I'm not a shady person.", "choice38", "I can't tell you because of my mystic concept. Sorry", "choice39")
    label choice38:
        call phone_after_menu from _call_phone_after_menu_545
        call message_start("[name]", "I'm not a shady person.") from _call_message_start_576
        call message("Ryou","Even so, there is no proof to prove your innocence at this stage.") from _call_message_4583
        call message("Shun","Umm... I don't think [name] is a bad person.") from _call_message_4584
        call message("000","Yes. But the situation itself is fishy.") from _call_message_4585
        call message("000","I can't believe no location popped up under my search T-T") from _call_message_4586
        call message("000","There's a chance that this person's a real alien or highly intelligent creature from outside the Milky Way...") from _call_message_4587
        call message("Sayaka","Aren't those the same?") from _call_message_4588
        call message("000","..") from _call_message_4589
        jump aftermenu14
    label choice39:
        call phone_after_menu from _call_phone_after_menu_546
        call message_start("[name]"," I can't tell you because of my mystic concept. Sorry") from _call_message_start_577
        call message("000","Then my concept is an invincible secret agent!") from _call_message_4590
        call message("Ryou","Isn't that your job rather than a concept?") from _call_message_4591
        call message("000","O.<~") from _call_message_4592
        call message("Sayaka","And you are not invincible, as this messenger got hacked...") from _call_message_4593
        call message_img("000", "omg", "images/shocked.png") from _call_message_img_285
        jump aftermenu14
    label aftermenu14:
        call message("Yoshiro","Then are we throwing parties again?") from _call_message_4594
        call message("K","Yes. We will.") from _call_message_4595
        call message("K","As [name] will take Reina's role for us.") from _call_message_4596
        call message("K","But since [name]'s identity has not been identified, we will have to see.") from _call_message_4597
        call message("Ryou","A temporary hold.") from _call_message_4598
        call message("Sayaka","Even so, [name]'s situation is quite perplexing.") from _call_message_4599
        call message("Sayaka","Is it okay for a non-member to take Reina's place?") from _call_message_4600
        call message("K","That's what I wanted to talk about.") from _call_message_4601
        call message("K","I'm thinking of accepting [name] as a new member of RFA, if [name] agrees.") from _call_message_4602
        call message("Sayaka","It seems a bit abrupt...") from _call_message_4603
        call message("Ryou","I agree. Accepting a stranger as a new member...?") from _call_message_4604
        call screen phone_reply(" What's so good about being a member?", "choice40", "What happens if I refuse?", "choice41")
    label choice40:
        call phone_after_menu from _call_phone_after_menu_547
        call message_start("[name]", "What's so good about being a member?") from _call_message_start_578
        call message("Shun","This one's quite straightforward...") from _call_message_4605
        call message("000","If u become a member, guess what!") from _call_message_4606
        call message("Ryou","You can use the RFA's messenger.") from _call_message_4607
        call message("Sayaka","That's not all, is it..?") from _call_message_4608
        call message("Ryou","You can live a contributing life.") from _call_message_4609
        call message("Yoshiro","Sounds big but not wrong") from _call_message_4610
        call message("Yoshiro","and... Zero won't get rid of u right away") from _call_message_4611
        jump aftermenu15
    label choice41:
        call phone_after_menu from _call_phone_after_menu_548
        call message_start("[name]", "What happens if I refuse?") from _call_message_start_579
        call message("000","Umm...") from _call_message_4612
        call message("000","Deny access?") from _call_message_4613
        call message("Ryou","To tell the truth, that's more convenient for us.") from _call_message_4614
        jump aftermenu15
    label aftermenu15:
        call message("K","It would be best if you cooperate.") from _call_message_4615
        call message("K","Since you have come here to host parties, [name], our purposes are the same.") from _call_message_4616
        call message("Ryou","I can't deny the fact that our situations and purposes fall perfectly, so things are quite suspicious.") from _call_message_4617
        call message("Shun","Hey,") from _call_message_4618
        call message("Shun","if you start questioning, there's no end in that;;") from _call_message_4619
        call message("Shun","I mean the situation is all so sudden but......") from _call_message_4620
        call message("Yoshiro","For now... I agree in holding parties again.") from _call_message_4621
        call message("Ryou","There is no reason to oppose holding parties itself.") from _call_message_4622
        call message("Shun","K has decided, so I'm going to follow that") from _call_message_4623
        call message("Sayaka","Of course, reopening the party is welcome, but") from _call_message_4624
        call message("Sayaka","It would have been better if we know exactly what kind of person [name] is.") from _call_message_4625
        call message("K","I am fully aware of that.") from _call_message_4626
        call message("K","So Levan, I want you to keep searching about [name].") from _call_message_4627
        call message("000","Um...") from _call_message_4628
        call message("000","Is it okay, [name]?") from _call_message_4629
        call screen phone_reply("Yeah, gladly, if you're doing the research, Zero ^ _ ^", "choice42", "Don't, I'm not a weird person -", "choice43")
    label choice42:
        call phone_after_menu from _call_phone_after_menu_549
        call message_start("[name]", "Yeah, gladly, if you're doing the research, Zero ^ _ ^") from _call_message_start_580
        call message("000","!!") from _call_message_4630
        call message("Ryou","What an unusual response.") from _call_message_4631
        jump aftermenu16
    label choice43:
        call phone_after_menu from _call_phone_after_menu_550
        call message_start("[name]"," Don't, I'm not a weird person -") from _call_message_start_581
        call message("Yoshiro","[name] T_T") from _call_message_4632
        call message("Yoshiro","He'll find out about u even if u refuse. T_T") from _call_message_4633
        call message("000","may b...") from _call_message_4634
        jump aftermenu16
    label aftermenu16:
        call message("000","A background check is compulsory if we want to open the party with your help, [name]!") from _call_message_4635
        call message("Shun","Don't collect any irrelevant info;;") from _call_message_4636
        call message("000","Yes, sir!") from _call_message_4637
        call message("000","K, Shall I proceed?") from _call_message_4638
        call message("K","Yes, I have a thought, so go on with it.") from _call_message_4639
        call message("Ryou","K, I expect you will later tell us what your thought is.") from _call_message_4640
        call message("K","I'll tell you when I can collect my thoughts together.") from _call_message_4641
        call screen phone_reply3("Then, do I just need to prepare for the party?", "choice44", "Why is everyone so guarded against me?", "choice45", "This game is so complicated", "choice46")
    label choice44:
        call phone_after_menu from _call_phone_after_menu_551
        call message_start("[name]", "Then, do I just need to prepare for the party?") from _call_message_start_582
        call message("K","The date hasn't been set yet, so no need to rush.") from _call_message_4642
        call message("K","I'll send a notice when the date is fixed.") from _call_message_4643
        jump aftermenu17
    label choice45:
        call phone_after_menu from _call_phone_after_menu_552
        call message_start("[name]", "Why is everyone so guarded against me?") from _call_message_start_583
        call message("Ryou","Because your entry route to here is very suspicious.") from _call_message_4644
        call message("000","Yup...") from _call_message_4645
        call message("000","Oh, I created and distributed this messenger app so that only our members could use.") from _call_message_4646
        call message("000","It's scandalous enough that someone outside of this group came in ...gawd") from _call_message_4647
        call message("Shun","Not to mention you know about Reina, [name]...") from _call_message_4648
        call message("Yoshiro","What a chaos...") from _call_message_4649
        jump aftermenu17
    label choice46:
        call phone_after_menu from _call_phone_after_menu_553
        call message_start("[name]", "This game is so complicated") from _call_message_start_584
        call message_img("Shun","Game?", "images/questioning.png") from _call_message_img_286
        call message("Ryou","I don't know why we are talking about games all of a sudden. This is not a game.") from _call_message_4650
        call message("Sayaka","Everything that's going on right now is not a joke.") from _call_message_4651
        call message("Sayaka","I don't know what this situation looks like to you, [name], but please note this situation is an emergency to us.") from _call_message_4652
        jump aftermenu17
    label aftermenu17:
        call message("000","Rationally speaking") from _call_message_4653
        call message("000","There is a simple solution - denying ur access") from _call_message_4654
        call message("000","I'm not sure if such risk is necessary...T_T") from _call_message_4655
        call message("K","Speaking of which, there is something I need to tell you. I'll call you right now.") from _call_message_4656
        call message("Yoshiro","One of those secret talks again...") from _call_message_4657
        call message("Yoshiro","[name], just so you know in advance, K is full of secrets;") from _call_message_4658
        call message("K","Yoshiro...") from _call_message_4659
        call message("Yoshiro","It's nothing new...") from _call_message_4660
        call message("Ryou","Let's all think that he has a good reason.") from _call_message_4661
        call message("000","Except for Yoshiro") from _call_message_4662
        call message("Yoshiro","Well, I do not understand;;") from _call_message_4663
        call message("K","I'll explain everything to you when it's time. I'm sorry.") from _call_message_4664
        call message("K","I should get going. Please excuse me first.") from _call_message_4665
        call message("Shun","See ya later K") from _call_message_4666
        call message("Sayaka","Take care.") from _call_message_4667
        call message("K","Thanks everyone. I'll see you, too, later...[name].") from _call_message_4668
        call screen phone_reply("Weren't you going to give me a call?", "choice47", "I'll see you around.", "choice48")
    label choice47:
        call phone_after_menu from _call_phone_after_menu_554
        call message_start("[name]", "Weren't you going to give me a call?") from _call_message_start_585
        call message("K","Oh, yes. I'll contact you in a bit after I get out of the chatroom") from _call_message_4669
        call message("K","Just give me a moment.") from _call_message_4670
        jump aftermenu18
    label choice48:
        call phone_after_menu from _call_phone_after_menu_555
        call message_start("[name]", "I'll see you around.") from _call_message_start_586
        call message("K","Yes, we will.") from _call_message_4671
        jump aftermenu18
    label aftermenu18:
        call message("SYSTEM", "K has left the chatroom.") from _call_message_4672
        call message("Ryou","Give us a reply la") from _call_message_4673
        call message("000","Srry") from _call_message_4674
        call message("Ryou","He left while I was typing.") from _call_message_4675
        call message("000","Hecalledrightaway assaid") from _call_message_4676
        call message("000","Imgonnatakethiscall bbye") from _call_message_4677
        call message("SYSTEM", "000 has left the chatroom") from _call_message_4678
        call message("Yoshiro","...") from _call_message_4679
        call message("Yoshiro","I wonder what secret K is up to again.") from _call_message_4680
        call message("Sayaka","Do you want to install a wiretap or something?") from _call_message_4681
        call message("Yoshiro","Yes, if secrets keep building;;") from _call_message_4682
        call message("Yoshiro","Ever since Reina died,") from _call_message_4683
        call message("Yoshiro","it feels like K only talks to Zero.") from _call_message_4684
        call message("Ryou","It's simple. Levan is in charge of RFA's confidential information.") from _call_message_4685
        call message("Yoshiro","...") from _call_message_4686
        call message("Shun","As the head of this association, K probably has a lot on his mind...") from _call_message_4687
        call message("Shun","He said he'll explain later.") from _call_message_4688
        call message("Shun","Let's trust him and wait a bit, okay?") from _call_message_4689
        call message("Yoshiro","Exactly when is later?") from _call_message_4690
        call message("Yoshiro","What I'm angry about is...") from _call_message_4691
        call message("Yoshiro","That it's been like this ever since Reina passed away") from _call_message_4692
        call message("Sayaka","Calm down, Yoshiro.") from _call_message_4693
        call message("Yoshiro","He's locked himself up in secrets about Reina's death too...!") from _call_message_4694
        call message("Sayaka","I'd like to think that was K's best decision, made for our sake.") from _call_message_4695
        call message("Ryou","So do I.") from _call_message_4696
        call message("Ryou","But he wasn't himself today, a bit flustered.") from _call_message_4697
        call message("Shun","K? flustered?") from _call_message_4698
        call message("Shun","Didn't seem like it;") from _call_message_4699
        call message("Ryou","I could see he was baffled today compared to his usual stance.") from _call_message_4700
        call message("Sayaka","Is that so? I think you're the only one who could see that, Mr. Han.") from _call_message_4701
        call message("Ryou","Asking to open up the call function...? That was a shock.") from _call_message_4702
        call message("Shun","Huh;") from _call_message_4703
        call screen phone_reply("I presume you and K are close?", "choice49", "Aren't you all trusting K too much?", "choice50")
    label choice49:
        call phone_after_menu from _call_phone_after_menu_556
        call message_start("[name]", "I presume you and K are close?") from _call_message_start_587
        call message("Ryou","Yes.") from _call_message_4704
        call message("Yoshiro","They're childhood friends.") from _call_message_4705
        call message("Ryou","Why are you explaining that?") from _call_message_4706
        call message("Yoshiro","[name] will eventually find out as he/she hangs out in the chatroom.") from _call_message_4707
        call message("Yoshiro","And that's not the real issue here, is it?") from _call_message_4708
        jump aftermenu19
    label choice50:
        call phone_after_menu from _call_phone_after_menu_557
        call message_start("[name]", "Aren't you all trusting K too much?") from _call_message_start_588
        call message("Yoshiro","That's what I wanted to say!") from _call_message_4709
        call message("Ryou","Because K is a trustworthy person.") from _call_message_4710
        call message("Ryou","In fact, has K's decision so far threatened our association even once?") from _call_message_4711
        call message("Yoshiro","Not yet but...") from _call_message_4712
        call message("Yoshiro","The way he closed Reina's death was a bit suspicious...") from _call_message_4713
        call message("Shun","Hey;;") from _call_message_4714
        call message("Yoshiro","He was strange even on her funeral") from _call_message_4715
        call message("Shun","K would have been the most agonized one;") from _call_message_4716
        call message("Yoshiro","But I still don't get it...") from _call_message_4717
        jump aftermenu19
    label aftermenu19:
        call message("Yoshiro","......") from _call_message_4718
        call message("Sayaka","Umm... Actually, I wanted to say this for some time now...") from _call_message_4719
        call message("Sayaka","Is it okay to talk about our personal lives when we don't even know what kind of person [name] is?") from _call_message_4720
        call message("Shun","I don't think we talked much about ourselves...") from _call_message_4721
        call message("Ryou","Well, there's nothing bad being careful, I guess.") from _call_message_4722
        call message("Ryou","I will be watching you closely, [name].") from _call_message_4723
        call message("Shun","What are you, a PI now? Gawd") from _call_message_4724
        call message("Sayaka","In my opinion, our most crucial issue here") from _call_message_4725
        call message("Sayaka","is whether [name] is a threat or not to RFA.") from _call_message_4726
        call message("Shun","A threat? I think that's too much;;") from _call_message_4727
        call message("Yoshiro","It's someone Reina sent.") from _call_message_4728
        call message("Yoshiro","Reina would never send a threat to us.") from _call_message_4729
        call message("Ryou","If [name]'s words are true, that would be so.") from _call_message_4730
        call message("Shun",";;; Suspecting isn't really my thing") from _call_message_4731
        call message("Shun","I hope we can soon find out that [name] is a nice person.") from _call_message_4732
        call message("Shun","Then, we can get the parties ready or something.") from _call_message_4733
        call message("Sayaka","Levan is on it. We will soon find out.") from _call_message_4734
        call message("Ryou","Anyhow, it seems the important issues have been taken care of.") from _call_message_4735
        call message("Ryou","I should get going. I need to read a new business plan proposal...") from _call_message_4736
        call message("Sayaka","Mr. Han, there is something I need to brief you about that.") from _call_message_4737
        call message("Sayaka","May I call you now?") from _call_message_4738
        call message("Ryou","Go ahead.") from _call_message_4739
        call screen phone_reply("Good bye, both of you.", "choice51", "Are you two leaving together? I wonder what kind of relationship you two have...", "choice52")
    label choice51:
        call phone_after_menu from _call_phone_after_menu_558
        call message_start("[name]", "Good bye, both of you.") from _call_message_start_589
        call message("Sayaka","Yes. I guess I'll see you more often.") from _call_message_4740
        call message("Ryou","Hope all the questions about you are cleared soon, [name].") from _call_message_4741
        call message("Ryou","Hopefully in a good way.") from _call_message_4742
        call message("Sayaka","I hope so too.") from _call_message_4743
        jump aftermenu20
    label choice52:
        call phone_after_menu from _call_phone_after_menu_559
        call message_start("[name]", "Are you two leaving together? I wonder what kind of relationship you two have...") from _call_message_start_590
        call message_img("Sayaka","Do not take it the wrong way.", "images/well.png") from _call_message_img_287
        call message("Sayaka","Mr. Han and I are connected money-wise.") from _call_message_4744
        call message("Ryou","Money and contract.") from _call_message_4745
        call message("Sayaka","Couldn't have phrased it better.") from _call_message_4746
        jump aftermenu20
    label aftermenu20:
        call message("Ryou","Well then, I'm off.") from _call_message_4747
        call message("Sayaka","I should get going too.") from _call_message_4748
        call message("SYSTEM", "Ryou has left the chatroom.") from _call_message_4749
        call message("SYSTEM", "Sayaka has left the chatroom.") from _call_message_4750
        call message("Shun","Only three of us left in a flash.") from _call_message_4751
        call message("Shun","I wonder what kind of person you are, [name].") from _call_message_4752
        call message("Yoshiro","Yeah, me too.") from _call_message_4753
        call screen phone_reply("We will take our time, talk a lot, and get to know each other.", "choice53", "I have lots of secrets. I will not tell them easily.", "choice54")
    label choice53:
        call phone_after_menu from _call_phone_after_menu_560
        call message_start("[name]", "We will take our time, talk a lot, and get to know each other.") from _call_message_start_591
        call message("Shun","You're positive lol") from _call_message_4754
        call message("Shun","I luv that kind of attitude.") from _call_message_4755
        call message("Yoshiro","I also have lots I want to hear from you, [name].") from _call_message_4756
        call message("Yoshiro","Especially about Reina...") from _call_message_4757
        jump aftermenu21
    label choice54:
        call phone_after_menu from _call_phone_after_menu_561
        call message_start("[name]", "I have lots of secrets. I will not tell them easily.") from _call_message_start_592
        call message("Yoshiro","Umm... You don't have to be wary of us. We are all good people.") from _call_message_4758
        call message_img("Shun","I'm the nicest of all.", "images/ohyeah.png") from _call_message_img_288
        call message_img("Yoshiro","See? You can just think this is what we are.", "images/well.png") from _call_message_img_289
        call message_img("Shun","Hey, what is that supposed to mean?-_-", "images/angry.png") from _call_message_img_290
        call message("Yoshiro","Even if you keep secrets, Zero can expose them all...") from _call_message_4759
        call message("Shun","I hope he hasn't looked into our backs, has he?") from _call_message_4760
        call message("Yoshiro","n.. no way....") from _call_message_4761
        jump aftermenu21
    label aftermenu21:
        call message("Shun","I want to chat more but I gotta get going too.") from _call_message_4762
        call message("Shun","Duty calls.") from _call_message_4763
        call message("Shun","[name], I look forward to know you.") from _call_message_4764
        call message("Yoshiro","Shun, aren't you gonna say bye to me?") from _call_message_4765
        call screen phone_reply("Good bye~~", "choice55", "lol you should say bye to Yoshiro too loll", "choice56")
    label choice55:
        call phone_after_menu from _call_phone_after_menu_562
        call message_start("[name]", "Good bye~~") from _call_message_start_593
        call message_img("Shun","Yeah. See you later.", "images/happy.png") from _call_message_img_291
        jump aftermenu22
    label choice56:
        call phone_after_menu from _call_phone_after_menu_563
        call message_start("[name]", "lol you should say bye to Yoshiro too loll") from _call_message_start_594
        call message("Shun","Why not?") from _call_message_4766
        call message("Shun","bye") from _call_message_4767
        call message("Yoshiro","yup, byebye") from _call_message_4768
        jump aftermenu22
    label aftermenu22:
        call message("SYSTEM", "Shun has left the chatroom.") from _call_message_4769
        call message("Yoshiro","Everyone's gone.") from _call_message_4770
        call message("Yoshiro","[name]...") from _call_message_4771
        call message("Yoshiro","Listen...") from _call_message_4772
        call message("Yoshiro","Can't you at least tell me") from _call_message_4773
        call message("Yoshiro","what you heard from Reina?") from _call_message_4774
        call message("Yoshiro","Doesn't matter how small it is.") from _call_message_4775
        call screen phone_reply("I was only told to hold parties.", "choice57", "Why are you so curious?", "choice58")
    label choice57:
        call phone_after_menu from _call_phone_after_menu_564
        call message_start("[name]"," I was only told to hold parties.") from _call_message_start_595
        call message("Yoshiro","Then can you tell me when and how Reina said those words?") from _call_message_4776
        call message("Yoshiro","Did you talk with Reina directly?") from _call_message_4777
        call message("Yoshiro","Did she leave any other words?") from _call_message_4778
        call reply_message("I didn't get to meet Reina in person. I was told thirdhand...") from _call_reply_message_3
        call message("Yoshiro","really?") from _call_message_4779
        call message("Yoshiro","oh....") from _call_message_4780
        jump aftermenu23
    label choice58:
        call phone_after_menu from _call_phone_after_menu_565
        call message_start("[name]", "Why are you so curious?") from _call_message_start_596
        call message("Yoshiro",".....") from _call_message_4781
        call message("Yoshiro","Cuz I can't believe it.") from _call_message_4782
        jump aftermenu23
    label aftermenu23:
        call message("Yoshiro","K told me she committed suicide.") from _call_message_4783
        call message("Yoshiro","but I couldn't believe that...") from _call_message_4784
        call message("Yoshiro","there was no other evidence other than K's words..") from _call_message_4785
        call message("Yoshiro","then you pop out") from _call_message_4786
        call message("Yoshiro","and say Reina sent you...") from _call_message_4787
        call message("Yoshiro","how do you think I'd feel...?") from _call_message_4788
        call screen phone_reply("It must be difficult... I understand.", "choice59", "If I learn something, I'll tell you.", "choice60")
    label choice59:
        call phone_after_menu from _call_phone_after_menu_566
        call message_start("[name]", "It must be difficult... I understand.") from _call_message_start_597
        call message("Yoshiro","Thanks for saying that...") from _call_message_4789
        jump aftermenu24
    label choice60:
        call phone_after_menu from _call_phone_after_menu_567
        call message_start("[name]", "If I learn something, I'll tell you.") from _call_message_start_598
        call message("Yoshiro","Okay! You must!") from _call_message_4790
        jump aftermenu24
    label aftermenu24:
        call message("Yoshiro","I really cannot believe Reina killed herself...") from _call_message_4791
        call message("Yoshiro","I still can't believe it...") from _call_message_4792
        call message("Yoshiro",",.") from _call_message_4793
        call message("Yoshiro","oh, I'm sorry") from _call_message_4794
        call message("Yoshiro","My professor just called;;") from _call_message_4795
        call message("Yoshiro","I'm a college student.. I forgot that I set an appointment earlier on") from _call_message_4796
        call message("Yoshiro","I gotta go immediately T-T") from _call_message_4797
        call screen phone_reply("You should go.", "choice61", "Let's talk again ^^", "choice62")
    label choice61:
        call phone_after_menu from _call_phone_after_menu_568
        call message_start("[name]", "You should go.") from _call_message_start_599
        call message("Yoshiro","Oh god, why now;;") from _call_message_4798
        call message("Yoshiro","I think I have to take this call, seems important.") from _call_message_4799
        jump aftermenu25
    label choice62:
        call phone_after_menu from _call_phone_after_menu_569
        call message_start("[name]", "Let's talk again ^^") from _call_message_start_600
        call message("Yoshiro","Yes!!") from _call_message_4800
        call message("Yoshiro","I'll be back..") from _call_message_4801
        call message("Yoshiro","Talk to me more often ^^") from _call_message_4802
        jump aftermenu25
    label aftermenu25:
        call message("Yoshiro","You must") from _call_message_4803
        call message("Yoshiro","talk with me again!") from _call_message_4804
        call message("SYSTEM", "Yoshiro has left the chatroom.") from _call_message_4805
        call phone_end from _call_phone_end_34
        call day1 from _call_day1
