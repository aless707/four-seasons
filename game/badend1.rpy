label badend1:
    scene black with dissolve
    play music "music/music4.ogg"
    scene bg7 with dissolve
    show MC MC11 with dissolve
    menu:
        "(Go outside.)":
            jump be61
        "(Call Ren.)":
            play sound "music/call.ogg"
            "(... .... ...)"
            "(He does not pick up.)"
            stop sound
            mc "(Go outside.)"
            jump be61
label be61:
    hide MC with dissolve
    scene bg11 with dissolve
    show MC MC11 at my_left with dissolve
    believer "Step aside, please. The savior is coming through."
    show R R33mask at my_right with dissolve
    savior "Wait, she's... Hold on."
    believer "Yes, ma'am."
    savior "Let's postpone the service for tomorrow. I have something to discuss with her in private."
    believer "Understood. I'll leave you in peace, my savior..."
    show R R32mask
    savior "[name]. For the paradise. It's a beautiful night."
    show R R33mask
    menu:
        "My savior... Weren't you going somewhere?":
            savior "I was on my way to the worship chamber."
            savior "I originally planned to take the lead of the service, but I remembered that I had to do something else as soon as I saw you."
            jump be62
        "For the paradise.":
            savior "You're adapting well. That is good."
            savior "I knew it... You seem to be a very bright girl."
            jump be62
        "Have you seen Ren?":
            savior "No, but reports tell me that his attentions are directed towards something other than his work."
            savior "Which is why..."
            jump be62
label be62:
    savior "I wanted to talk to you..."
    show R R32mask
    savior "I know very well that you're doing a splendid job."
    show R R33mask
    savior "Contacting the RFA through the messenger, that is."
    savior "I'm sure we'll soon be rewarded with great prizes if things progress just a bit more..."
    savior "But Ren is unexpectedly experiencing confusion."
    show R R32mask
    savior "He is a believer I am particularly fond of."
    savior "He was brought up in a tragic childhood... Unfortunately, as a result, his body and mind were horribly destroyed."
    show R R33mask
    savior "However, I believe he will reciprocate with the best results if there is someone looking after him."
    show R R31mask
    savior "There's no doubt that boy's got talents."
    show R R33mask
    savior "So far I've been his caretaker."
    show R R32mask
    savior "I would say there's no one else who knows him better than I do..."
    show R R33mask
    savior "Even if someone shows him what love is, he'll never accept it the way it is."
    savior "Rather, he feels safe in an environment where freedom is compromised, probably because of his horrid childhood."
    show R R32mask
    savior "Same could be said of the majority of believers in this place, however..."
    show R R33mask
    savior "As the Ao Me started to wax, the time I could spare for him waned."
    savior "Perhaps that's why...Ren is lost again. And this time it's taking longer for him to return."
    savior "So I needed someone else to administer him for me..."
    show R R31mask
    savior "And then I realized that Ren is more than just fond of you."
    show R R32mask
    savior "I'm the one who saved him from the abysmal world, but maybe..."
    show R R33mask
    savior "maybe you'd be able to give him wings to fly."
    show R R31mask
    savior "If you truly cherish him...why don't you take on my role as his guardian?"
    show R R33mask
    savior "My eyes make no mistake in determining the limits and potentials of people."
    show R R35mask
    savior "And my eyes tell me...that you've got enough talent and desire to control him however you want."
    savior "I'll give you all the authority you need."
    savior "Take a good care of him...to make him bring me the best of results."
    hide R with dissolve
    hide MC with dissolve
    stop music
    scene black with dissolve
    "......"
    "..."
    "........."
    "(A few months later)"
    scene bg7 with dissolve
    play music "music/music15.ogg"
    ray "[name], ma'am..."
    ray "I've completed collecting all intel, including the last piece of information on the RFA members as stated by the requirement No. 27."
    ray "And... here is the item you've demanded, too. If you don't have anything else you need..."
    menu:
        "Ren, come over here. Come and entertain me.":
            ray "Of course.... How may I serve you today?"
            jump be63
        "I don't like that face of yours.":
            ray "My apologies. What face would you like to see?"
            jump be63
        "Are you sure you're not skipping your meals?":
            ray "Yes, I am taking care of my meals. I don't want you worried..."
            ray "But if there's anything you don't like, please let me know."
            jump be63
label be63:
    scene black with dissolve
    ray "[name], please tell me if there's anything that bothers you."
    ray "I feel alive and breathing only when your eyes scan me from head to toe and tame me."
    ray "I am made useful only because you are here."
    ray "Please tell me how I can be more useful to you."
    ray "Please tell me what I should do, what I should think, what I should feel."
    ray "I'll be just the way you want."
    ray "I can become anything for you, my goddess."
    menu:
        "What can you possibly do for me?":
            ray "Anything. I can rip out my heart for you if that can entertain you..."
            jump be64
        "This is boring now.":
            ray "Your expression pains my heart."
            ray "I can turn into any type of person for you..."
            ray "So, please tell me what you wish."
            jump be64
label be64:
    ray "I don’t even deserve to breathe if I cannot make you happy. I worship you."
    ray "So please, use me."
    ray "Bend me and break me for your happiness. Your laughter of joy is like oxygen to me."
    ray "[name], I worship you. I only wish you to stay with me..."
    ray "You give me the purpose of life... You are my goddess."
    ray "Please stay with me...until I completely lose my worth."
    stop music
    scene badend with dissolve
    with Pause(3.0)
    scene black with dissolve
    $ MainMenu(confirm=False)()
