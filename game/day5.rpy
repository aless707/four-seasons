label day5:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Reminder: From now on the collected hearts will determine the ending." with dissolve
    with Pause(3.0)
    hide text with dissolve
    with Pause(2.0)
    show text "Day 5" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "00:21" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music13.ogg"
    scene bg15 with dissolve
    show U U19 at my_left with dissolve
    ray "K threatened [name] and gave us a bunch of lies that what I know is nothing but lies before he left."
    show R R23 at my_right with dissolve
    savior "...You don't believe in his lies, do you?"
    show U U15
    ray "No! Of course not...not even a bit..."
    show U U16
    ray "I'm scared."
    ray "What if he comes back and takes her away?"
    ray "I lost him right under my nose."
    show U U19
    ray "I don't think I even deserve to see her now..."
    show U U12
    ray "My only dream was to make her happy at this place..."
    show U U13
    ray "What should I do, my savior?"
    ray "You told me that I must not fear him, but I..."
    show R R22
    savior "...Ren. Poor child."
    show R R23
    savior "Look at you. You've become a coward. You've lost control over your fear and feelings. You're lost in your feelings towards that girl."
    savior "Not to mention you realized just now that shameless intruder was lurking in this place for months..."
    savior "Now you're a coward and a good-for-nothing."
    show U U13
    ray "...I knew it. I've done something wrong, didn't I? Is everything my fault...?"
    show R R23
    savior "The answer lies within you, Ren."
    show U U13
    ray "What should I do? Help me, my savior..."
    savior "Of course. I'll always be there for you."
    savior "I can help you show me how different you've become."
    show R R22
    savior "But if that's not the case..."
    show R R23
    savior "Perhaps this place doesn't need you, Ren."
    hide R with dissolve
    hide U with dissolve
    stop music
    scene black with dissolve
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "Hello, [name]. This is K, the head of RFA."
    v "I figured you would have been surprised seeing me like that... and have called you like this."
    mc "I still can't believe that... K, you're an actual person and not an AI."
    v "There are parts where I now finally understand."
    v "You did exhibit some suggestive things, but I didn't know it would mean that..."
    v "I'm an actual living person and this is all happening in reality."
    v "Though someone might want to believe this is all a dream."
    mc "Is there something you want to say to me?"
    v "Even if I say to look after the child, there won't be anything atypical."
    v "I want you to just look after what kind of situation he is in, how he's feeling while talking to him more often. That's all I ask."
    menu:
        "It has nothing to do with you, K, doesn't' it?":
            v "I cannot tell the details, but looking after Sora is something that I must do."
            v "But being in the position I'm in, it's not easy to contact the child or look into him."
            v "I'm asking you this favor after thinking thoroughly of it."
            v "Because, currently, the person who is the closest to Sora is you."
            jump v51
        "Are you telling me to keep an eye on Ren?":
            v "Oh... No. It would be a misunderstanding if you understood it like that."
            v "It's more of just watching over him, not monitoring him."
            v "It will be enough to know that nothing happens to that child."
            v "What I'm worried about is that something might happen to Sora."
            v "And when that something does happen... I'm very afraid... that I won't be able to handle it at the right time."
            v "Since you seem close with that child, I thought you would know right away if something happened."
            v "I don't have a bad intent. All the decisions I make... is for you, Sora and our RFA."
            jump v51
label v51:
    v "Also, there is one more thing I wanted to tell you."
    v "The party will be held as originally planned."
    v "So if other members recommend guests to you, please proceed how you always did."
    mc "I want to know why you're pushing so hard to have the party."
    v "It's because..."
    v "... All I can say is that it's for the sake of Sora."
    v "If the party is canceled, the child might fall in danger."
    v "All I want is to avoid that as much as I can."
    v "Even for you... I believe you wouldn't want anything dangerous happening to him."
    mc "Ren, in danger? Why?"
    v "...According to the data I've collected, the child is in charge of handling the RFA party."
    v "If something suddenly goes wrong... he will have to take responsibility for everything."
    v "What the result of such responsibility would come to be.... I would rather not say."
    v "I do not want to burden you with the thought."
    v "So please, stop asking anymore questions."
    v "The child has been scarred numerous times internally and needs someone to rely on. I think you are that person for him."
    menu:
        "I want to know your relationship with Ren.":
            v "Relationship.... How should I put it."
            v "The child hates me a lot right now but... I don't hate him."
            v "It' all because of a misunderstanding that Sora is despising me."
            v "I don't really care if I'm being hated or not but it pains me to see how tough it is for the child trying to hate me."
            jump v52
        "Is it you, K, that has emotionally hurt Ren?":
            v "....I don't think I should be the one answering this questions."
            v "The wounded should be the one to judge that and me giving my opinion, would be an arrogant behavior."
            jump v52
label v52:
    v "The fact that the child can talk to you will be great strength to him. Please look after Sora."
    v "Of course, you should take care as well. This is a favor I ask as the head of our association."
    v "...I must hang up now. I'll contact you again."
    v "Well then."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "02:44" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(000 is calling)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    s "Hello? [name]? Do you have some time?"
    s "No, it doesn't matter even if you don't have time. I'll talk twice no, 16 times faster so could you please listen?"
    s "My mentality has been smashed to pieces right now. It smashed, turned into dust and flying around."
    s "Haaaaaaa----- (*sigh)"
    menu:
        "What's up with the big sigh? It'll make the ground sink ":
            s "Would be great if I can make the ground sink."
            s "If the ground sinks, then the ground we stand on will become the sky, right?"
            s "Then I'll be floating above the sky... then maybe I can float away and away to space...."
            s "...then I'll have no troubles to worry about."
            jump s51
        "Haaaaaaa----- (*sigh)":
            s "...You're troubled with something too?"
            s "Or is that a signal to have a deep sigh battle with me?"
            jump s51
label s51:
    s "...I'm sorry. I don't even know what I'm saying right now..."
    s "You heard what situation I'm in, right? K telling me to take my hands off from searching the hacker."
    s "I mean, yeah, okay. I can understand his intent in trying to lessen my workload."
    s "I'm  thankful to K for being considerate of my and asking Ryou for a favor to do so but...."
    mc "It'll feel like someone stole your work."
    s "Correct! That is correct!"
    s "No... maybe a bit different...?"
    s "Rather than feeling like as if someone stole my work, asking another to help because of my incompetence is a bit...."
    s "It has made a long scratch to this GOD0's pride... (*sigh)"
    s "It's those kind of things lie, the teacher gave me a difficult homework."
    s "Seeing what a hard time I'm having"
    s "says 'Seems it's a bit early for you to do the homework. I'll give you an easier one.' That kind of feeling."
    s "Kind of makes me feel discouraged or something, wondering why I was trying to do my best and all...."
    s "Am I that stupid...stuff like that."
    s "I kind of feel blue and sand and all."
    s "Kind of makes me question if I'm not that trustworthy..."
    menu:
        "There might be a reason behind it.":
            s "A reason? Like what?"
            s "Nothing is coming to my mind... is there something that comes to your mind?"
            mc "There is but I can't tell you."
            s "Uh....Did you promise something to K?"
            s "If so, I won't ask any further."
            s "But the idea that there may be a different reason... It kind of makes me feel relieved but at the same time more anxious..."
            jump s52
        "It might be K's thoughtfulness in supporting Zero's drink business.":
            s "No way... not even someone else but K acting like that?"
            s "K would have criticized with a smile and wouldn't support my illegal business... no, my hobby."
            jump s52
label s52:
    s "Still, I do feel better talking to you like this. Thanks."
    s "Like how you said, K must have a plan. I'm going to believe in that."
    s "Oomph!"
    s "Well then, since I've gained some energy thanks to you, I shall go back to work."
    s "Thanks for listening to me."
    s "Let's talk again later! Bye!"
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "06:03" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music14.ogg"
    call phone_start from _call_phone_start_18
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_269
    call message("Ren", "Hi...[name].") from _call_message_2208
    call message("Ren", "You're logged in.") from _call_message_2209
    call screen phone_reply("I was so worried you disappeared in such a hurry... Are you alright now?", "cd51", "Zero won't be after you anymore... What do you think about that?", "cd52")
label cd51:
    call phone_after_menu from _call_phone_after_menu_254
    call message_start("[name]", "I was so worried you disappeared in such a hurry... Are you alright now?") from _call_message_start_270
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message_img("Ren", "...I don't even deserve your caring.", "images/sad.png") from _call_message_img_119
    call message("Ren", "[name], are you alright?") from _call_message_2210
    jump am51
label cd52:
    call phone_after_menu from _call_phone_after_menu_255
    call message_start("[name]", "Zero won't be after you anymore... What do you think about that?") from _call_message_start_271
    call message("Ren", "...K must have made him stop.") from _call_message_2211
    call message("Ren", "Because he would feel betrayed") from _call_message_2212
    call message("Ren", "if he finds out himself that I'm his brother.") from _call_message_2213
    call message("Ren", "So K must be using Ryou Han") from _call_message_2214
    call message("Ren", "as a shield") from _call_message_2215
    call message("Ren", "to protect himself even if the secret's loose.") from _call_message_2216
    call message("Ren", "That's who K really is. He's such a selfish creature...") from _call_message_2217
    call message("Ren", "But I'm more worried about you.") from _call_message_2218
    call message("Ren", "[name]...what about you?") from _call_message_2219
    jump am51
label am51:
    call message("Ren", "How are you doing in your room?") from _call_message_2220
    call message("Ren", "I'm sure you were disappointed a lot") from _call_message_2221
    call message_img("Ren", "after seeing me last night....", "images/sad.png") from _call_message_img_120
    call screen phone_reply("I'm not disappointed.... My heart is starting to break for you.", "cd53", "I was surprised, actually. You're weaker than I thought.", "cd54")
label cd53:
    call phone_after_menu from _call_phone_after_menu_256
    call message_start("[name]", "I'm not disappointed.... My heart is starting to break for you.") from _call_message_start_272
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Your heart is breaking?") from _call_message_2222
    call message("Ren", "That's also my fault....") from _call_message_2223
    jump am52
label cd54:
    call phone_after_menu from _call_phone_after_menu_257
    call message_start("[name]", "I was surprised, actually. You're weaker than I thought.") from _call_message_start_273
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren"," ...I'm sorry.") from _call_message_2224
    call message("Ren", "I'm so sorry...for disappointing you.") from _call_message_2225
    jump am52
label am52:
    call message("Ren", "I...") from _call_message_2226
    call message("Ren", "I...") from _call_message_2227
    call message("Ren", "I thought I grew somewhat...") from _call_message_2228
    call message("Ren", "But now it looks like I'm mistaken.") from _call_message_2229
    call message("Ren", "The whole world is against me...") from _call_message_2230
    call message("Ren", "but looks like I forgot my place") from _call_message_2231
    call message("Ren", "and thought") from _call_message_2232
    call message("Ren", "that my dreams will soon come true...") from _call_message_2233
    call message("Ren", "....") from _call_message_2234
    call screen phone_reply("Arrogance results in misfortunes.", "cd55", "No, please don't blame yourself. This is not your fault at all.", "cd56")
label cd55:
    call phone_after_menu from _call_phone_after_menu_258
    call message_start("[name]", "Arrogance results in misfortunes.") from _call_message_start_274
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I'm sorry.") from _call_message_2235
    call message("Ren", "I'm so sorry.") from _call_message_2236
    call message("Ren", "I won't be arrogant.") from _call_message_2237
    jump am53
label cd56:
    call phone_after_menu from _call_phone_after_menu_259
    call message_start("[name]", "No, please don't blame yourself. This is not your fault at all.") from _call_message_start_275
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "You care for me") from _call_message_2238
    call message("Ren", "even in this situation...") from _call_message_2239
    call message("Ren", "I don't deserve...") from _call_message_2240
    call message("Ren", "to protect someone so kind") from _call_message_2241
    call message("Ren", "and so good like you.") from _call_message_2242
    jump am53
label am53:
    call message("Ren", "This is all my fault.") from _call_message_2243
    call message_img("Ren", "I’m sorry...", "images/sad.png") from _call_message_img_121
    call message("Ren", "I couldn't even do my job right") from _call_message_2244
    call message("Ren", "and I boasted to you that I can protect you...") from _call_message_2245
    call message("Ren", "You must be thinking I'm such an idiot...") from _call_message_2246
    call screen phone_reply("Hmm...are you sure I'll be safe here?", "cd57", "You're already doing more than enough to protect me.", "cd58")
label cd57:
    call phone_after_menu from _call_phone_after_menu_260
    call message_start("[name]", "Hmm...are you sure I'll be safe here?") from _call_message_start_276
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I don't know if you can trust me") from _call_message_2247
    call message("Ren", "but to me") from _call_message_2248
    call message("Ren", "your safety is the top priority...") from _call_message_2249
    call message("Ren", "Of course") from _call_message_2250
    call message("Ren", "after what happened last night") from _call_message_2251
    call message("Ren", "I'm sure you lost your faith in me...") from _call_message_2252
    jump am54
label cd58:
    call message_start("[name]", "You're already doing more than enough to protect me.") from _call_message_start_277
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "...") from _call_message_2253
    call message("Ren", "You really think so?") from _call_message_2254
    call message("Ren", "Even after what happened last night?") from _call_message_2255
    call message("Ren", "I'm doing my best") from _call_message_2256
    call message("Ren", "but it's never good enough....") from _call_message_2257
    jump am54
label am54:
    call message("Ren", "I don't want you to hate me.") from _call_message_2258
    call message("Ren", "Not you too.") from _call_message_2259
    call message("Ren", "And I must become a better person") from _call_message_2260
    call message("Ren", "to make sure that never happens...") from _call_message_2261
    call message("Ren", "I...") from _call_message_2262
    call message("Ren", "I'm a moron") from _call_message_2263
    call message("Ren", "I was like this from birth") from _call_message_2264
    call message("Ren", "That must be why") from _call_message_2265
    call message("Ren", "I'm hurting people who care for me....") from _call_message_2266
    call screen phone_reply("No one is an airhead from birth. Please don't say that, Ren...", "cd59", "I think you should work harder if you have time to chat like this.", "cd510")
label cd59:
    call phone_after_menu from _call_phone_after_menu_261
    call message_start("[name]", "No one is an airhead from birth. Please don't say that, Ren...") from _call_message_start_278
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "No") from _call_message_2267
    call message("Ren", "it's true.") from _call_message_2268
    call message("Ren", "You might be disappointed in me") from _call_message_2269
    call message("Ren", "but I....") from _call_message_2270
    call message("Ren", "I'm so stupid...") from _call_message_2271
    call message("Ren", "I'm weak...") from _call_message_2272
    call message("Ren", "I hate myself.") from _call_message_2273
    call message("Ren", "I hate myself so much...") from _call_message_2274
    call message("Ren", "I couldn't protect you...!") from _call_message_2275
    jump am55
label cd510:
    call phone_after_menu from _call_phone_after_menu_262
    call message_start("[name]", "I think you should work harder if you have time to chat like this.") from _call_message_start_279
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Uh...") from _call_message_2276
    call message("Ren", "yeah...") from _call_message_2277
    call message("Ren", "it hurts but you're right...") from _call_message_2278
    call message("Ren", "Thank you for telling me that, [name]...") from _call_message_2279
    jump am55
label am55:
    call message("Ren", "I think for a while...") from _call_message_2280
    call message("Ren", "I'll just focus on proving my worth.") from _call_message_2281
    call message("Ren", "I should be thankful") from _call_message_2282
    call message("Ren", "that there's something to do") from _call_message_2283
    call message("Ren", "for an airhead like me...") from _call_message_2284
    call screen phone_reply("You should stop chatting and get to work now.", "cd511", "Please don't blame yourself like that... My heart is aching as well.", "cd512")
label cd511:
    call phone_after_menu from _call_phone_after_menu_263
    call message_start("[name]", "You should stop chatting and get to work now.") from _call_message_start_280
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Yeah") from _call_message_2285
    call message("Ren", "I will.") from _call_message_2286
    call message("Ren", "Sorry to waste your time...") from _call_message_2287
    call message("Ren", "I'm sure...") from _call_message_2288
    call message("Ren", "you don't want to see me") from _call_message_2289
    call message("Ren", "or hear my voice right now") from _call_message_2290
    call message("Ren", "so I won't type anymore...") from _call_message_2291
    jump am56
label cd512:
    call phone_after_menu from _call_phone_after_menu_264
    call message_start("[name]", "Please don't blame yourself like that... My heart is aching as well.") from _call_message_start_281
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "[name], I don't want you in pain.") from _call_message_2292
    call message("Ren", "So I won't blame myself!") from _call_message_2293
    call message("Ren", "Never, ever!") from _call_message_2294
    call message("Ren", "But....") from _call_message_2295
    call message("Ren", "how can I not blame myself?") from _call_message_2296
    call message("Ren", "Ugh....") from _call_message_2297
    call message("Ren", "I promised I won't blame myself when I don't even know how...") from _call_message_2298
    call message("Ren", "I'm such an airhead.") from _call_message_2299
    call message("Ren", "I'm sorry....") from _call_message_2300
    call message("Ren", "I hate myself so much T_T") from _call_message_2301
    jump am56
label am56:
    call message("Ren", "[name] but I'm glad") from _call_message_2302
    call message("Ren", "that you haven't left me yet.") from _call_message_2303
    call message("Ren", "And now...") from _call_message_2304
    call message("Ren"," ...Thanks for chatting with me.") from _call_message_2305
    call message("SYSTEM", "Ren has left the chatroom") from _call_message_2306
    call phone_end from _call_phone_end_19
    stop music
    show MC MC14 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "Hello? [name]? You've answered my call. Thanks."
    ray "Truth is, I'm not sall when I'm working but... I called as I was worried of you."
    ray "How are you doing? Did anything happen while I couldn't be in touch with you?"
    ray "If... K calls you and says strange things, please tell me."
    menu:
        "What things?":
            ray "Oh, no, I don't mean anything specific... But I only said that because I was worried that you'll get confused when someone says strange things to you..."
            jump u51
        "No, nothing like that happened.":
            ray "...That's a relief."
            ray "You do know you can ask me anything you want to know whenever, right?"
            jump u51
label u51:
    ray "No, it doesn't matter even if you don't have time. I'll talk twice, no, 16 times faster so could you please listen?"
    menu:
        "Were you monitoring my room?":
            ray "Um... it's that... I'm sorry. I didn't intend to monitor you! If you're angry, I'm sorry...."
            ray "I just sometimes check when I have the time.... I'm sorry..."
            ray "Whether you've eaten, sleeping well, if you aren't bored, if you're crying or not... I'm constantly worried of you..."
            jump u52
        "Don't worry. I'm fine.":
            ray "Yes. that's what I wanted to hear. If you're sad or sick because of me... I don't think I can stand that. Since I'm used to that type of agony... I want to take away everything that'll make you uncomfortable."
            jump u52
label u52:
    ray "Listening to your voice like this, I feel a bit relieved. If something happens, you have to tell me! You must!"
    menu:
        "I'll do that if you promise me that you'll contact me more often.":
            ray "Sure! I'll keep in touch with you whenever I have the time!"
            ray "Since I cannot shorten the hours I work...I'll try to lessen some other time I use!"
            ray "It'll be difficult to lessen the time I eat to less than 10 minutes...."
            ray "I should lessen the time I sleep...."
            ray "Since even if I sleep, I can't get quality sleep as I dream nightmares. It would be better to talk with you at that time!"
            ray "If it's okay to contact you at late hours, then I'll do it more often."
            jump u53
        "Why? Is there something on your mind that'll happen?":
            ray "That suspicious shadow I mentioned before... It bothers me that it looked like K."
            ray "I'm scared and can't focus on work....that K... might appear again and take you away this time."
            ray "I'm so pathetic, aren't I?"
            jump u53
label u53:
    ray "Consider that I'm here for you. You won't feel lonely even when you're alone. Since I'm continuously thinking of you..."
    ray "If you're happy, I'm happy. Just remember that."
    ray "...Oh, savior is looking for me. I should hang I want to keep on talking with you but... I should go..."
    ray "I hope you have a good day."
    play sound "music/beep.ogg"
    hide MC with dissolve
    stop music
    scene black with dissolve
    show text "12:21" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2.ogg"
    scene bg9 with dissolve
    show V V12 with dissolve
    v "Thanks for looking into it, Ryou."
    v "I didn't think you'd do that so fast."
    v "I'm sure Levan will be pleased as well. Even though he sounded like he didn't in the messenger."
    v "Sure, go ahead. Please tell Sayaka that I said thanks. Yes, yes... I'll talk to you again."
    play sound "music/beep.ogg"
    "(...)"
    show V V13
    v "...Phew..."
    v "...I know I can't hide him from Levan forever, but..."
    show V V11
    v "I cannot let him hunt down his own brother."
    show V V118
    v "Sora...looked weak and insecure."
    show V V17
    v "There's no question that it's my fault he turned out like that..."
    v "I must turn him back no matter what. For Levan's sake..."
    v "I promised that I'll keep him safe and happy.... But I let things end s...."
    show V V118
    v "I pray...that he'll stay healthy...until everything finds its original place..."
    hide V with dissolve
    scene black with dissolve
    scene white with dissolve
    whooo "...K."
    whooo "K!"
    v "What is it, Sora?"
    yu "There's a new flower in the garden! I've never seen it before... The wind must have brought the seed here."
    yu "Look at it, K. Isn't it pretty?"
    yu "What flower could it be?"
    v "Oh...this one...is a weed. It's called a whitlow grass."
    yu "A weed...? But it's so pretty..."
    yu "...The name whitlow grass sounds sad. It sounds like people didn't really care what its name is."
    yu "Can I give it a new name? A pretty one, too!"
    v "Of course. Since you adore flowers, I'm sure you can give it a much more beautiful name."
    yu "Hehe... I'm sure the flower will be happy if I give it a pretty name."
    v "Yes, of course. You've been taking a great care of the cathedral garden, and look how beautiful it is."
    yu "I thought a lot at home how I can make flowers more happy."
    v "You can do what you're doing right now. The most important thing is to love flowers, and you're already doing that. You always do."
    yu "Thank you, K..."
    yu "I want to know...about all flowers in the world."
    yu "I might not deserve it...but I want to see all pretty and beautiful things in the world."
    yu "Because I can see nothing but sad and gloomy things at home..."
    v "...."
    v "Sora, you deserve to witness the beauty of all flowers in this world."
    yu "Do you really think so?"
    v "I do. Everyone in this world has a right to experience and pursue the beauty of nature."
    v "So...I hope you keep in mind that you can be free in whatever situation."
    yu "Is it okay to do that? I don't think I should..."
    v "...Next time when I leave on a trip to take photos, it would be great if I could take you with me."
    yu "You mean it!? But I don't know anything... But still, I want to go with you."
    v "I mean it...though Reina has to say yes."
    v "I talked to her about it days ago, and she said it would be too dangerous."
    yu "Thank you, K!"
    yu "I'm so happy just thinking about it."
    v "It doesn't have to be now. When you grow leave on a trip together."
    yu "Yes! Let's do that! I think I'm born again...all thanks to you and Reina."
    yu "I'm so happy...but do you think Souta will be happy, too?"
    v "...I'm sure he is. He'll be happy just by learning that you're smiling like this."
    yu "...I miss him so much. I hope at least he can feel how much I miss him."
    scene black with dissolve
    yu "I gave my prayer of thanksgiving today as well...to thank that Souta and I are under the same sky."
    yu "He must be working so hard right now. I hope there are flowers where he is, too."
    stop music
    show text "14:37" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene black with dissolve
    "(6 years ago)"
    play music "music/music9.ogg"
    scene bg17 with dissolve
    mother "Where did Souta run off to again?"
    mother "Where's that damn brother of yours? I know you know where."
    mother "Souta, that rotten... As soon as the money's mine, I'll no longer have to keep him."
    mother "The only reason why I'm looking for him is because he's the reason why his father will spit out some money for me."
    mother "But anyways, I'll keep him alive until the transaction is complete... And just where would that termite run off to all the time!?"
    u "...Please don't be mad... I don't know where he is... I mean it..."
    mother "And how come you know nothing all the time? Seriously, you're such a useless airhead."
    mother "Just you wait. I'm throwing you out as soon as your father sends the money..."
    mother "What a useless parasite."
    mother "And talk about that creepy atmosphere of yours... Even the sight of you is pissing me off. And who do you think you're staring at?"
    u "...."
    mother "What, are you now rebelling just because you've grown a millimeter taller?"
    mother "Listen"
    u "Ugh... I can't breathe. You're choking me... Mom..."
    mother "People like us can't survive in this world without losing sanity...without living like scoundrels."
    mother "This is how bugs like us are ssurvive..."
    mother "We need to be heartless, unless we want those guys with the crowns to crush our heads..."
    mother "Nobody among us should dare try to keep their head e, family or otherwise, in the battlefield called survival. You should know your place..."
    mother "So stop making a fuss and stay silent like a grave! Stop pissing me off, for Christ's sake...!"
    u "Cough... Cough..."
    mother "And how dare you look into my eyes like that? You think I'm a bad mother?"
    mother "How dare you try to disobey me just because you've managed to survive a coears now...? But there's no point in growing t."
    mother "Nothing will change even if you grow bug, always a bug."
    mother "You're weak and st should consider yourself blessed if you manage to keep breathing."
    scene black with dissolve
    mother "What an airhead..."
    mother "What a good-for-nothing..."
    mother "You're useless, Sora Choi."
    mother "That won't change even if you grow up."
    scene bg18 with dissolve
    show R R22 with dissolve
    savior "No, that's not true."
    show R R23
    savior "I'll save you, Sora..."
    savior "You'll learn how to make yourself useful if you try hard."
    scene black with dissolve
    show R R25
    savior "So all you need to do is to obey me."
    hide R
    show U U13 with dissolve
    scene bg10 with dissolve
    show U U19
    "(....!)"
    show U U110
    ray "Hah..."
    ray "That was close."
    ray "I must not rest. I must not rest... I need to stay alert."
    play sound "music/knock.ogg"
    "(Knock knock)"
    show U U111
    ray "Who is it..?"
    believer "Mr. Ren."
    show U U12
    ray "Come in."
    "(....)"
    ray "What is it?"
    believer "Uh... The savior sent you this."
    show U U111
    ray "...Oh. It's time for the elixir."
    believer "...."
    show U U12
    ray "....You saw me receiving it. Now go."
    believer "Uhm..."
    ray "?"
    believer "I need to see you actually taking it..."
    show U U111
    ray "...Fine."
    hide U with dissolve
    stop music
    scene black with dissolve
    show text "17:45" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg8 with dissolve
    play music "music/music2chat-17.ogg"
    show S S19 with dissolve
    s "...This guy's on rampage today.... He's literally trying to bite my monitor off."
    s "Though he did make some mistakes.... They're fixed as soon as I try to turn them into troubles for him."
    s "This hacker's concentration is something. Just what planet is he from...?"
    show S S12
    s "It would take at least a month to figure out who's on the other side of this fight, no matter how fast Ryou manages to make that intelligence unit."
    s "....At least it felt like I'm against a human being the other night... But now it feels like I'm fighting a robot...."
    s "All I can get from him is attack and defense. He's like an emotionless machine... And it's been going on non-stop for hours...."
    show S S19
    s "I admit he's good, but why does he have to go this far...? It's as if he'd be in huge trouble if he doesn't do this...."
    s "Is he in some kind of a life-or-death situation or something?"
    s "Just why would he be so desperate...? RFA is simply a charity fundraising organization...!"
    stop music
    hide S with dissolve
    scene black with dissolve
    show text "19:23" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music14.ogg"
    call phone_start from _call_phone_start_19
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_282
    call message("Ren", "stealth -9987 -2005620615") from _call_message_2307
    call message("Ren", "1771540208") from _call_message_2308
    call message("Ren", "9433062027") from _call_message_2309
    call message("Ren", "4478718243") from _call_message_2310
    call message("Ren", "2444064239") from _call_message_2311
    call message("Ren", "27") from _call_message_2312
    call message("Ren", "Huh...?") from _call_message_2313
    call screen phone_reply("What was that? You screwed up just now, didn't you?", "cd513", "You must still be busy. Are you having your meals properly?", "cd514")
label cd513:
    call phone_after_menu from _call_phone_after_menu_265
    call message_start("[name]", "What was that? You screwed up just now, didn't you?") from _call_message_start_283
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Sorry!") from _call_message_2314
    call message("Ren", "I typed in the wrong command") from _call_message_2315
    call message("Ren", "and opened a chatroom....") from _call_message_2316
    jump am57
label cd514:
    call phone_after_menu from _call_phone_after_menu_266
    call message_start("[name]", "You must still be busy. Are you having your meals properly?") from _call_message_start_284
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message_img("Ren", "Oh", "images/questioning.png") from _call_message_img_122
    call message("Ren", "I opened a chatroom by mistake.") from _call_message_2317
    jump am57
label am57:
    call message("Ren", "I tried not to log in") from _call_message_2318
    call message("Ren", "until I'm done with my job...") from _call_message_2319
    call message("Ren", "I'm sorry....") from _call_message_2320
    call screen phone_reply("Looks like you're full of faults.", "cd515", "That's alright. I wanted to talk to you ^^", "cd516")
label cd515:
    call phone_after_menu from _call_phone_after_menu_267
    call message_start("[name]", "Looks like you're full of faults.") from _call_message_start_285
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "...") from _call_message_2321
    call message("Ren", "I should check everything again.") from _call_message_2322
    call message("Ren", "I didn't want you to see me like this....") from _call_message_2323
    jump am58
label cd516:
    call phone_after_menu from _call_phone_after_menu_268
    call message_start("[name]", "That's alright. I wanted to talk to you ^^") from _call_message_start_286
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "....") from _call_message_2324
    call message("Ren", "I wish I could chat with you") from _call_message_2325
    call message("Ren", "even for a moment, but....") from _call_message_2326
    jump am58
label am58:
    call message("Ren", "I know right now I don't deserve to talk to you.") from _call_message_2327
    call message("Ren", "I'll remember my place. I'll try.") from _call_message_2328
    call message("Ren", "I'm") from _call_message_2329
    call message("Ren", "just so grateful") from _call_message_2330
    call message("Ren", "that you're still here....") from _call_message_2331
    call screen phone_reply("You don't have to worry that I might leave... I'm not going anywhere without you.", "cd517", "Work harder. So that I can see you're useful.", "cd518")
label cd517:
    call phone_after_menu from _call_phone_after_menu_269
    call message_start("[name]", "You don't have to worry that I might leave... I'm not going anywhere without you.") from _call_message_start_287
    call message("Ren", "Do you...") from _call_message_2332
    call message("Ren", "really mean it?") from _call_message_2333
    call message("Ren", "What should I do") from _call_message_2334
    call message("Ren", "to see you here?") from _call_message_2335
    call message("Ren", "Please tell me everything you need me to do.") from _call_message_2336
    jump am59
label cd518:
    call phone_after_menu from _call_phone_after_menu_270
    call message_start("[name]"," Work harder. So that I can see you're useful.") from _call_message_start_288
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Yes... I will.") from _call_message_2337
    call message("Ren", "I'm doing the best as I can") from _call_message_2338
    call message("Ren", "but looks like I'm not good enough...") from _call_message_2339
    call message("Ren", "I'm so sorry I'm not good enough...") from _call_message_2340
    call message_img("Ren", "But please, just don't leave me...!", "images/sad.png") from _call_message_img_123
    jump am59
label am59:
    call message("Ren", "Oh...that's right.") from _call_message_2341
    call message("Ren", "There's something I want to tell you.") from _call_message_2342
    call message("Ren", "By tomorrow") from _call_message_2343
    call message("Ren", "you'd be able to roam around this place as you like.") from _call_message_2344
    call message("Ren", "Though I screwed up so badly the other day") from _call_message_2345
    call message("Ren", "I made sure my savior knows") from _call_message_2346
    call message("Ren", "that you're now one of us....") from _call_message_2347
    call screen phone_reply3("Why tomorrow?", "cd519", "Are you sure the savior trusts you?", "cd520", "Don't worry about me... Are you alright?", "cd521")
label cd519:
    call phone_after_menu from _call_phone_after_menu_271
    call message_start("[name]", "Why tomorrow?") from _call_message_start_289
    call message("Ren", "You'd need a passing card for this place....") from _call_message_2348
    call message("Ren", "but it usually takes a day to make one.") from _call_message_2349
    jump am510
label cd520:
    call phone_after_menu from _call_phone_after_menu_272
    call message_start("[name]", "Are you sure the savior trusts you?") from _call_message_start_290
    call message("Ren", "...Right now") from _call_message_2350
    call message("Ren", "I think my savior doesn't trust me much.") from _call_message_2351
    call message_img("Ren", "Sorry things ended up like this...", "images/sad.png") from _call_message_img_124
    jump am510
label cd521:
    call phone_after_menu from _call_phone_after_menu_273
    call message_start("[name]", "Don't worry about me... Are you alright?") from _call_message_start_291
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Don't worry about me.") from _call_message_2352
    call message("Ren", "I don't deserve to have you care for me...") from _call_message_2353
    jump am510
label am510:
    call message("Ren", "You're doing so well for me") from _call_message_2354
    call message("Ren", "in the RFA chat rooms") from _call_message_2355
    call message("Ren", "but compared to you....") from _call_message_2356
    call message_img("Ren", "I think I'm not even close to good enough....", "images/sad.png") from _call_message_img_125
    call screen phone_reply("That's not true. Please don't say that...", "cd522", "I expect you to be better.", "cd523")
label cd522:
    call phone_after_menu from _call_phone_after_menu_274
    call message_start("[name]", "That's not true. Please don't say that...") from _call_message_start_292
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "...") from _call_message_2357
    call message("Ren", "I'm a moron") from _call_message_2358
    call message("Ren", "and weak...") from _call_message_2359
    call message("Ren", "I'll be good for nothing") from _call_message_2360
    call message("Ren", "if I can't even do the hacking right....") from _call_message_2361
    jump am511
label cd523:
    call phone_after_menu from _call_phone_after_menu_275
    call message_start("[name]", "I expect you to be better.") from _call_message_start_293
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Ok...") from _call_message_2362
    call message("Ren", "and") from _call_message_2363
    call message("Ren", "once I become a great") from _call_message_2364
    call message("Ren", "and useful person") from _call_message_2365
    call message("Ren", "I can stay with you...") from _call_message_2366
    call message("Ren", "can't i?") from _call_message_2367
    call message("Ren", "But") from _call_message_2368
    call message("Ren", "I wonder if that can ever happen....") from _call_message_2369
    jump am511
label am511:
    call message("Ren", "I won't be able") from _call_message_2370
    call message("Ren", "to fix all the parts that I lack") from _call_message_2371
    call message("Ren", "but if I try really hard") from _call_message_2372
    call message("Ren", "I might become a person") from _call_message_2373
    call message("Ren", "at least you won't dislike.") from _call_message_2374
    call screen phone_reply("I think what matters is what you think about yourself.", "cd524", "I'll see what you can do and let you know whether I like it or not.", "cd525")
label cd524:
    call phone_after_menu from _call_phone_after_menu_276
    call message_start("[name]", "I think what matters is what you think about yourself.") from _call_message_start_294
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Myself?") from _call_message_2375
    call message("Ren", "I..") from _call_message_2376
    call message("Ren", "I think") from _call_message_2377
    call message("Ren", "I'm a moron") from _call_message_2378
    call message("Ren", "and good-for-nothing....") from _call_message_2379
    call message("Ren", "My thought doesn't matter.") from _call_message_2380
    call message("Ren", "What matters is what you think about me.") from _call_message_2381
    call message("Ren", "What do you want me to work on...?") from _call_message_2382
    jump am512
label cd525:
    call phone_after_menu from _call_phone_after_menu_277
    call message_start("[name]", "I'll see what you can do and let you know whether I like it or not.") from _call_message_start_295
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Thank you....") from _call_message_2383
    call message("Ren", "for not giving up on me...") from _call_message_2384
    call message("Ren", "I think I'm going to cry....") from _call_message_2385
    jump am512
label am512:
    call message_img("Ren", "I...", "images/sad.png") from _call_message_img_126
    call message("Ren", "I didn't even know that K was hiding here") from _call_message_2386
    call message("Ren", "so I think even my savior doesn't trust me anymore.") from _call_message_2387
    call screen phone_reply("I also think that was a huge mistake.", "cd526", "But it looked like K wanted all of us to get together in peace...", "cd527")
label cd526:
    call phone_after_menu from _call_phone_after_menu_278
    call message_start("[name]", "I also think that was a huge mistake.") from _call_message_start_296
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I know right...?") from _call_message_2388
    call message("Ren", "But still...") from _call_message_2389
    call message("Ren", "I should be grateful I wasn't thrown away.") from _call_message_2390
    jump am513
label cd527:
    call phone_after_menu from _call_phone_after_menu_279
    call message_start("[name]", "But it looked like K wanted all of us to get together in peace...") from _call_message_start_297
    call message("Ren", "You shouldn't trust him, [name]...") from _call_message_2391
    call message("Ren", "I'll never let him") from _call_message_2392
    call message("Ren", "get close to you again.") from _call_message_2393
    jump am513
label am513:
    call message("Ren", "If I don't come up with a good accomplishment") from _call_message_2394
    call message("Ren", "until morning") from _call_message_2395
    call message("Ren", "then maybe...") from _call_message_2396
    call message("Ren", "I won't be able to see you") from _call_message_2397
    call message("Ren", "for a while....") from _call_message_2398
    call screen phone_reply("But do you have to push yourself so much?", "cd528", "Do your best.", "cd529")
label cd528:
    call phone_after_menu from _call_phone_after_menu_280
    call message_start("[name]", "But do you have to push yourself so much? I think the Ao Me is forcing you to do something too demanding....") from _call_message_start_298
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "No") from _call_message_2399
    call message("Ren", "I should do this. It proves how useful I am....") from _call_message_2400
    jump am514
label cd529:
    call phone_after_menu from _call_phone_after_menu_281
    call message_start("[name]", "Do your best.") from _call_message_start_299
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Ok") from _call_message_2401
    call message("Ren", "I will...") from _call_message_2402
    call message("Ren", "but...") from _call_message_2403
    jump am514
label am514:
    call message("Ren", "There's something....") from _call_message_2404
    call message("Ren", "that bothers me....") from _call_message_2405
    call message("Ren", "Please don't hate me...") from _call_message_2406
    call message("Ren", "even if I tell you what...") from _call_message_2407
    call message("Ren", "I'm scared") from _call_message_2408
    call message("Ren", "that my savior will find out") from _call_message_2409
    call message("Ren", "I made you one of us without making you drink the elixir.") from _call_message_2410
    call message("Ren", "...To be honest") from _call_message_2411
    call message("Ren", "that's my greatest fear these days.") from _call_message_2412
    call screen phone_reply("I'm not going to get in trouble because of you, am I?", "cd530", "You took a risk to protect me... Thank you...", "cd531")
label cd530:
    call phone_after_menu from _call_phone_after_menu_282
    call message_start("[name]", "I'm not going to get in trouble because of you, am I?") from _call_message_start_300
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I'll do my best") from _call_message_2413
    call message("Ren", "so that there will be nothing wrong!") from _call_message_2414
    call message("Ren", "I wanna protect you") from _call_message_2415
    call message("Ren", "I wanna sacrifice my body") from _call_message_2416
    call message("Ren", "if I can....") from _call_message_2417
    jump am515
label cd531:
    call phone_after_menu from _call_phone_after_menu_283
    call message_start("[name]", "You took a risk to protect me... Thank you...") from _call_message_start_301
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Don't thank me...") from _call_message_2418
    call message("Ren", "I'm sorry this is all I can do") from _call_message_2419
    call message("Ren", "when you trust me so much.") from _call_message_2420
    jump am515
label am515:
    call message("Ren", "I like everything about this place") from _call_message_2421
    call message("Ren", "except dealing with the elixir...") from _call_message_2422
    call message("Ren", "It's so painful") from _call_message_2423
    call message_img("Ren", "so I don’t want to force it on you....", "images/sad.png") from _call_message_img_127
    call message("Ren", "Only people like me...") from _call_message_2424
    call message("Ren", "people who are too stupid") from _call_message_2425
    call message("Ren", "to understand my savior") from _call_message_2426
    call message("Ren", "need that elixir.") from _call_message_2427
    call message("Ren", "So [name]....") from _call_message_2428
    call message("Ren", "you don't need to take it.") from _call_message_2429
    call screen phone_reply("I don't think you need it, either. Please don't drink it if it hurts you...", "cd532", "When does the elixir start to take effect?", "cd533")
label cd532:
    call phone_after_menu from _call_phone_after_menu_284
    call message_start("[name]", "I don't think you need it, either. Please don't drink it if it hurts you...") from _call_message_start_302
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren"," No...") from _call_message_2430
    call message("Ren", "I'm an airhead...") from _call_message_2431
    call message("Ren", "I don't know what's the best for me.") from _call_message_2432
    call message("Ren", "I wish...") from _call_message_2433
    call message("Ren", "I could do something about this weak body of mine") from _call_message_2434
    call message("Ren", "with the elixir.") from _call_message_2435
    jump am516
label cd533:
    call phone_after_menu from _call_phone_after_menu_285
    call message_start("[name]", "When does the elixir start to take effect?") from _call_message_start_303
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "It depends....") from _call_message_2436
    call message("Ren", "I can feel the pain in no time.") from _call_message_2437
    call message("Ren", "But it works on my mind...") from _call_message_2438
    call message("Ren", "after my body goes through pain.") from _call_message_2439
    jump am516
label am516:
    call message("Ren", "Anyways") from _call_message_2440
    call message("Ren", "[name],") from _call_message_2441
    call message("Ren", "Even if someone tries to make you take it") from _call_message_2442
    call message("Ren", "just tell them that I'll get you a glass later.") from _call_message_2443
    call message("Ren", "Okay...?") from _call_message_2444
    call screen phone_reply("Okay.", "cd534", "Don't tell me what to do.", "cd535")
label cd534:
    call phone_after_menu from _call_phone_after_menu_286
    call message_start("[name]", "Okay.") from _call_message_start_304
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "Ok!") from _call_message_2445
    call message_img("Ren", "Now I feel a little better.", "images/happy.png") from _call_message_img_128
    jump am517
label cd535:
    call phone_after_menu from _call_phone_after_menu_287
    call message_start("[name]", "Don't tell me what to do.") from _call_message_start_305
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "I didn't mean to...") from _call_message_2446
    call message("Ren", "sorry...") from _call_message_2447
    call message("Ren", "I'm so sorry...") from _call_message_2448
    call message("Ren", "....") from _call_message_2449
    jump am517
label am517:
    call message("Ren"," I hope you had your dinner.") from _call_message_2450
    call screen phone_reply("You don't have to worry about me...", "cd536","I don't like the menu.", "cd537")
label cd536:
    call phone_after_menu from _call_phone_after_menu_288
    call message_start("[name]", "You don't have to worry about me...") from _call_message_start_306
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "But I can't help it...") from _call_message_2451
    call message("Ren", "You matter more than me.") from _call_message_2452
    call message("Ren", "Even if I'm sick") from _call_message_2453
    call message("Ren", "I hope you're never sick...") from _call_message_2454
    call message("Ren", "I need to protect you but...") from _call_message_2455
    jump am518
label cd537:
    call phone_after_menu from _call_phone_after_menu_289
    call message_start("[name]", "I don't like the menu.") from _call_message_start_307
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Oh") from _call_message_2456
    call message("Ren", "I'll tell them") from _call_message_2457
    call message("Ren", "to take better care.") from _call_message_2458
    call message("Ren", "Sorry about that too...") from _call_message_2459
    jump am518
label am518:
    call message("Ren", "I'm sorry I'm not good enough.") from _call_message_2460
    call message("Ren", "It doesn't matter if you hate me.") from _call_message_2461
    call message("Ren", "But please don't leave me... I beg you.") from _call_message_2462
    call screen phone_reply("I won't leave you. So I hope your heart will find peace...", "cd538", "I'll never leave if you're good enough.", "cd539")
label cd538:
    call phone_after_menu from _call_phone_after_menu_290
    call message_start("[name]", "I won't leave you. So I hope your heart will find peace...") from _call_message_start_308
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "..Really...?") from _call_message_2463
    call message("Ren", "Do you really mean it? For me?") from _call_message_2464
    call message("Ren", "Thanks.") from _call_message_2465
    call message("Ren", "...I'll keep your words in this heart of mine.") from _call_message_2466
    call message("Ren", "I...") from _call_message_2467
    jump am519
label cd539:
    call phone_after_menu from _call_phone_after_menu_291
    call message_start("[name]", I"'ll never leave if you're good enough.") from _call_message_start_309
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Please, please... I beg you.") from _call_message_2468
    call message("Ren", "As long as you're here") from _call_message_2469
    call message("Ren", "I want to become useful") from _call_message_2470
    call message("Ren", "even if I destroy myself....") from _call_message_2471
    jump am519
label am519:
    call message("Ren", "I don't care") from _call_message_2472
    call message("Ren", "how much I suffer") from _call_message_2473
    call message("Ren", "as long as you're here....") from _call_message_2474
    call screen phone_reply("Then do your best.", "cd540", "I don't want you suffering because of me...", "cd541")
label cd540:
    call phone_after_menu from _call_phone_after_menu_292
    call message_start("[name]", "Then do your best.") from _call_message_start_310
    show bh at heart_pos with dissolve
    hide bh with dissolve
    $bad_end +=1
    call message("Ren", "Ok...!") from _call_message_2475
    call message_img("Ren", "I will.", "images/smile.png") from _call_message_img_129
    call message("Ren", "I'll make sure both you and the savior will be happy...") from _call_message_2476
    jump am520
label cd541:
    call phone_after_menu from _call_phone_after_menu_293
    call message_start("[name]", "I don't want you suffering because of me...") from _call_message_start_311
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "I'm willing to suffer") from _call_message_2477
    call message("Ren", "as much as I can...") from _call_message_2478
    call message("Ren", "if only I can protect you.") from _call_message_2479
    call message("Ren", "That's how much") from _call_message_2480
    call message("Ren", "I don't want to lose you...") from _call_message_2481
    jump am520
label am520:
    call message("Ren", "Is your room alright?") from _call_message_2482
    call message("Ren", "It's not too hot or too cold, is it?") from _call_message_2483
    call message("Ren", "I hope you find the temperature just right") from _call_message_2484
    call message("Ren", "and enjoy your rest....") from _call_message_2485
    call screen phone_reply3("It is a little cold.", "cd542", "It's hot...", "cd543", "I'm fine. But I miss you so much...", "cd544")
label cd542:
    call phone_after_menu from _call_phone_after_menu_294
    call message_start("[name]", "It is a little cold.") from _call_message_start_312
    call message("Ren"," I'll tell them to turn the heat some more.") from _call_message_2486
    call message("Ren", "I'll send a blanket too!") from _call_message_2487
    call message("Ren", "You should stay warm") from _call_message_2488
    call message("Ren", "to make sure you don't catch cold...") from _call_message_2489
    jump am521
label cd543:
    call phone_after_menu from _call_phone_after_menu_295
    call message_start("[name]", "It's hot...") from _call_message_start_313
    call message("Ren", "I'll tell them to turn the heat down.") from _call_message_2490
    call message("Ren", "And I should also tell them") from _call_message_2491
    call message("Ren", "to fetch you couple more clothing.") from _call_message_2492
    jump am521
label cd544:
    call phone_after_menu from _call_phone_after_menu_296
    call message_start("[name]", "I'm fine. But I miss you so much...") from _call_message_start_314
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message("Ren", "...Me too.") from _call_message_2493
    call message("Ren", "But...") from _call_message_2494
    call message("Ren", "I shouldn't think about that.") from _call_message_2495
    jump am521
label am521:
    call message("Ren", "Oh") from _call_message_2496
    call message("Ren", "sorry...") from _call_message_2497
    call message("Ren", "you might be upset that I'm caring for you....") from _call_message_2498
    call message("Ren", "I...will go now. For real.") from _call_message_2499
    call screen phone_reply("That's alright. But you have to come back and chat with me again...!", "cd545", "Keep up the good work.", "cd546")
label cd545:
    call phone_after_menu from _call_phone_after_menu_297
    call message_start("[name]", "That's alright. But you have to come back and chat with me again...!") from _call_message_start_315
    show gh at heart_pos with dissolve
    hide gh with dissolve
    $good_end += 1
    call message_img("Ren", "...Thank goodness if that's the case.", "images/smile.png") from _call_message_img_130
    call message("Ren", "I'll do my best.") from _call_message_2500
    call message("Ren", "...Now bye.") from _call_message_2501
    jump am522
label cd546:
    call phone_after_menu from _call_phone_after_menu_298
    call message_start("[name]", "Keep up the good work.") from _call_message_start_316
    call message("Ren", "Thanks for reading...") from _call_message_2502
    call message("Ren", "Bye.") from _call_message_2503
    jump am522
label am522:
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_2504
    call phone_end from _call_phone_end_20
    stop music
    scene black with dissolve
    play music "music/music14.ogg"
    scene bg10 with dissolve
    play sound "music/knock.ogg"
    "(Knock knock)"
    show U U111 with dissolve
    ray "Who is it....?"
    believer "Mr. Ren."
    ray "Oh....the elixir."
    show U U12
    "(....)"
    believer" Here it is."
    show U U111
    ray "...Am I mistaken? Or did you bring more than you brought earlier today?"
    believer "The savior prepared it herself, so I don’t know anything regarding the amount."
    show U U12
    ray "I need to take it all, right?"
    show U U13
    believer "I can wait."
    show U U19
    ray "......Huff... Cough..."
    believer "I'll take the bottle for you. Then I'll see you tomorrow morning."
    ray "...Cough... Huff..."
    scene black with dissolve
    show U U112
    ray "...My head... My body is burning..."
    show U U19
    ray "Though I have to take it... Cough..."
    ray "At this rate... I can't concentrate on my work... This is bad."
    show U U112
    ray "Damn it... Why am I so weak...? My savior loves me. She's only looking out for me..."
    show U U19
    ray "Huff... No. She said what matters is my faith."
    ray "I should...believe...that I'm strong. Ugh..."
    scene bg10 with dissolve
    show U U19
    ray "It's too hot... What do I do...?"
    ray "...Cold water. I need cold water."
    scene black with dissolve
    hide U with dissolve
    scene black with dissolve
    "(...)"
    ray "Huff... Huff..."
    ray "Cough..."
    ray "Get a grip.... This isn't time for this..."
    ray "I must prove I'm useful. Otherwise, I can't survive..."
    ray "Or else I can't stay with her..."
    ray "...Just what good is my body for...?"
    ray "It's too weak..."
    ray "These arms are too thin... This whole body is too thin..."
    ray "It gets bruises every time I fall..."
    ray "This body is useless..."
    ray "It doesn't deserve anything good to eat."
    ray "It doesn't deserve good sleep."
    ray "It doesn't deserve warm shower..."
    ray "It just has to get slapped with cold water to stay working.... Snap out of it..."
    ray "Weak Ren... Useless Ren.."
    ray "I'm not Sora..."
    ray "I'm Ren, a walking ball of faults...."
    ray "Ren needs to hit the road someday."
    ray "His brain, his arms, his eyes, his whole body... I hate them..."
    ray "I hate them all... Ren's too weak..."
    scene white with dissolve
    stop music
    "...."
    scene bg9 with dissolve
    play music "music/music2.ogg"
    show V V12 with dissolve
    v "I'm sure these data are enough to get the intelligence unit started."
    v "But I didn't think things will be this fast. I must admit Ryou's progress is incredible."
    show V V13
    v "The best-case scenario...would be to have Reina change her mind and free Sora..."
    show V V11
    v "But that would be impossible."
    show V V118
    v "She's full of rage. She's using Sora to get her hands on the RFA's information system..."
    show V V17
    v "If we don't hold the party, she'll employ even more unpredictable schemes to attack us."
    v "And if that's case, Sora will stay attached to her strings..."
    v "...Should I just tell everything to Ryou? Now that Sora is part of this matter...."
    v "Leaving him as a marionette would be no different from abusing him."
    v "However...if Ryou takes action himself, everyone would know about Reina's truth..."
    v "Then I will never be able to save her."
    show V V118
    v "Reina... It's been merely 6 months..."
    show V V19
    v "I think there's still a way...for us to reconcile..."
    stop music
    hide V
    scene black with dissolve
    show text "21:13" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC14 with dissolve
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    ray "Hello...? It's me, Ren."
    ray "How was... *cough... your.... day...? *cough"
    mc "Your cold hasn't gone away."
    ray "Yes... I'm still coughing..."
    ray "It's okay.... I'll get better since I took the elixir savior gave me..."
    ray "When I take the elixir, this weak body will get healthier.... have a stronger mentality... is what savior said."
    ray "I'm sorry I couldn't call as often...."
    ray "I should have taken better care and asked if there was anything uncomfortable...."
    ray "But I was busy the entire day. Even work is just running in circles..."
    ray "Maybe it's because I'm a moron that I don't get the results that I expect."
    menu:
        "You should take care of yourself while working. I'm worried of you, Ren.":
            ray "...Are you worried of me? Really? ... Thanks..."
            ray "But I really need to focus more on work. For the sake of everyone here in Ao Me."
            ray "Since you're also a believer of Ao Me... I shall do my best for you."
            jump u54
        "Savior or whatever you call that person is really good at exploiting labor.":
            ray "No, I'm really glad that I can work for savior. Since by doing so, I was able to meet you..."
            ray "When I think of the place I used to be at....It doesn't even make sense to complain."
            ray "And the word exploitation does not suit savior."
            jump u54
label u54:
    ray "I did a lousy job today that I've disappointed savior..."
    ray "I was sad. I wanted to see you... but I felt so lonely that I couldn't see you the whole day."
    ray "....But I thought of you being in the same place as I am and continued working. That gave thought gave me strength. Thank you. For being here with me..."
    ray "Uh... I kept blabbing on, haven't I? I'm sorry. There are so many things I want to say to you when I talk with you."
    menu:
        "It's okay. It's great that I can talk to you, Ren.":
            ray "Really? It's my first time hearing that... It feels somewhat strange."
            ray "But if you don't feel bad talking with me.. that's a relief."
            jump u55
        "When there are so many things I want to say.... I write it in my diary.":
            ray "..Ah, diary... I am writing one."
            ray "Thought I don't have much time to write in it often.... I mostly scribble a few lines when something important happens."
            ray "The day you first came here, I wrote and entry in my diary. Since it was a very important day for me."
            jump u55
label u55:
    ray "...Huh? Wait."
    ray "I hear footsteps.... It's coming towards my room. Sadly... I must hang up now."
    ray "Sweet dreams. I.. really wanted to say that to you."
    menu:
        "Good night to you as well, Ren.":
            ray "Thanks. Sweet dreams and sleep well."
            ray "I'll pray for you, so that you can sleep comfortably."
            jump u56
        "Good night. I'll see you in my dreams.":
            ray "..Yes. Good night. I'll see in your dreams. I'll be waiting."
            ray "...Haa... Was I a bit weird?"
            ray "But I really mean it. I really wanted to say that to you."
            jump u56
label u56:
    ray "Oh, we should really hang up now. I'll call again."
    ray "Good night! Bye!"
    play sound "music/beep.ogg"
    hide MC
    scene black with dissolve
    call day6 from _call_day6
