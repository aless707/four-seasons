label day1:
    stop music
    scene black with dissolve
    with Pause(0.5)
    show text "The following 4 days will be a warm up with the point system. You will collect good or bad hearts depending on your answers, but they will not effect your game. Starting from Day 5 the points will reset and will effect the game outcome." with dissolve
    with Pause(10.0)
    hide text with dissolve
    with Pause(0.5)
    scene black with dissolve
    with Pause(2.0)
    show text "Day 1" with dissolve
    with Pause(1.0)
    hide text with dissolve
    show text "00:00" with dissolve
    with Pause (1.0)
    hide text with dissolve
    with Pause(2.0)

    play music "music/music2chat-17.ogg"
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    show MC MC15
    "(K is calling)"
    stop sound
    mc "(Pick up.)"
    play sound "music/beep.ogg"
    show MC MC14
    v "Hello? Oh, the phone's working alright."
    v "I told you that I will call you, didn't I?"
    v "Allow me to formally introduce myself. I am K, the head of the RFA."
    v "It's a pleasure to meet you."
    show MC MC11
    menu:
        "Pleasure is mine.":
            v "Oh...thank you. It's great to hear you in person outside the chat room."
            jump v11
        "You must have been surprised to see me.":
            v "Hmm... Yes, it was quite surprising. I'm sure it's the same for the rest of the members."
            v "We've never imagined...to find someone entering our messenger."
            jump v11
    label v11:
        v "I called you because there is something I'd like to ask you."
        v "I already mentioned this in the chat room back there."
        v "Uh...if it does not offend you, may I ask you something?"
        menu:
            "Yeah, sure.":
                v "Thank you for saying that."
                jump v12
            "Could you make it quick?":
                v "Of course. Just a few things to confirm, and it'll be all done."
                v "Thank you for sparing your time."
                jump v12
    label v12:
        v "Can you tell me how you know about Reina?"
        v "I believe it'll be confusing for the rest of the members...if they hear it."
        menu:
            "I got an invitation.":
                v "Invitation...? Could you tell me what exactly was on the invitation?"
                menu:
                    "It's a secret.":
                        v "Hmm... I wonder what it's about."
                        v "But it's alright. You don't have to tell me...if you have to keep it a secret for some reason."
                        jump v13
                    "It told me to hold a party. Nothing more.":
                        v "Oh...I see."
                        v "That's something you already said in the chat room. So there's nothing new."
                        jump v13
            "Reina and I were online friends.":
                        v "Online friends... That's the word you use to refer to friend you met on the Internet, isn't it?"
                        v "I'm actually surprised that Reina met a friend over the Internet."
                        v "And she even talked about parties to her online friend...?"
                        v "I'm Reina's lover, but she never told me anything like that."
                        menu:
                            "You're gravely mistaken if you think lovers would know everything about each other.":
                                v "That's because we did know everything about each other."
                                jump v13
                            "Maybe she told you, but you forgot.":
                                v "That could be the case in common circumstances."
                                v "But I remember even the most trivial things about Reina."
                                v "What she said remains imprinted in my soul, word by word... I don't think I ever forgot anything she told me."
                                jump v13
            "She was an acquaintance of an acquaintance of an acquaintance of my acquaintance.":
                v "...So you two were very distant. If that's true, you wouldn't know anything about what I ask regarding Reina."
                jump v13
    label v13:
        v "Very well, for now. Thank you for answering me."
        v "Hmm... I think that's enough questions for now... And I believe there are limits to the conversation through the phone..."
        menu:
            "You're suspecting me, aren't you? That's why you asked them!":
                v "No, that's not true. I was surprised someone who is a complete stranger to me talked about Reina."
                v "That's why I asked. I'm sorry if I offended you."
                jump v14
            "Let's slowly learn about each other.":
                v "Yes, I plan to. I'd like to know what kind of person you are."
                v "I believe there will be so much to talk about with you."
                jump v14
    label v14:
        v "Since you're here to hold parties, we share a common goal. So I'll help you to the best of my abilities."
        v "I'll tell you one by one...how a party is held."
        v "If you need my help or have any question, please feel free to ask me anytime."
        v "I hope we can spend a good time. And I hope we can spend a good time with the rest of the RFA members as well."
        menu:
            "I'm going to pursue them one by one.":
                v "Pursue...? Umm, that sounds a bit unfamiliar to me."
                v "I'll take it that you'd like to get used to the members."
                jump v15
            "Leave it to me.":
                v "I will. I hope you're a good person."
                jump v15
    label v15:
        v "Now I must leave..."
        v "I look forward...to your future performances."
        play sound "music/beep.ogg"
        stop music
        hide MC with dissolve
        scene black with dissolve
        with Pause(2.0)
        show text "14:08" with dissolve
        with Pause(1.0)
        hide text with dissolve
        with Pause(2.0)
        scene bg5 with dissolve
        play music "music/music2chat-17.ogg"

        call phone_start from _call_phone_start
        call message_start("...", "...") from _call_message_start
        call message("Shun","K is basically the only person who calls me by my birth name.") from _call_message
        call message("Shun","But if it's K, well..") from _call_message_1
        call message("Shun","I'm also ok with Hyun. I'm used to it.") from _call_message_2
        call message("Shun","Though that doesn't let me") from _call_message_3
        call message("Shun","call K by his name...") from _call_message_4
        call screen phone_reply3("What's K's birth name?", "choiced11", "I think it's kind of embarrassing to call you Shun lol", "choiced12", "Can I call you honey?", "choiced13")
    label choiced11:
        call phone_after_menu from _call_phone_after_menu
        call message_start("[name]", "What's K's birth name?") from _call_message_start_1
        call message("Shun","It's Jurou Kim.") from _call_message_5
        call message("Shun","You can find him on the Internet if you type in K the photographer. Haha") from _call_message_6
        jump aftermenud11
    label choiced12:
        call phone_after_menu from _call_phone_after_menu_1
        call message_start("[name]", "I think it's kind of embarrassing to call you Shun lol") from _call_message_start_2
        call message("Shun","Aww, how come - ?") from _call_message_7
        call message("Shun","Don't you feel secured if you hear my name? Oh, maybe that's just me since it's my name.") from _call_message_8
        call message("Shun","I like it. Easy to remember.") from _call_message_9
        jump aftermenud11
    label choiced13:
        call phone_after_menu from _call_phone_after_menu_2
        call message_start("[name]","Can I call you honey?") from _call_message_start_3
        call message("Shun","esy") from _call_message_10
        call message("Shun","yse") from _call_message_11
        call message("Shun","Wait;; that was my hand out-speeding my head.") from _call_message_12
        call message("Shun","I've been single for so long T-T") from _call_message_13
        call message("Shun","I was joking about that yes.") from _call_message_14
        call message("Shun","Umm...") from _call_message_15
        call message("Shun","I think it'd be better to use that term") from _call_message_16
        call message("Shun","when our relationship became sturdy enough to use that term naturally hehe") from _call_message_17
        jump aftermenud11
    label aftermenud11:
        call message("Shun","Name is such a subjective thing, isn't it?") from _call_message_18
        call message("Shun","I think it shows how close you feel to that person.") from _call_message_19
        call message("Shun","oh") from _call_message_20
        call message("Shun","Actually, I'd like to know more about you, [name] -") from _call_message_21
        call message("Shun","What's your hobby? What's your specialty?") from _call_message_22
        call message("SYSTEM","Shun has left the chatroom.") from _call_message_23
        call reply_message("...?") from _call_reply_message
        stop music
        play sound "music/hack.ogg"
        play music "music/music3.ogg"
        call message("SYSTEM","Ren has entered the chatroom.") from _call_message_24
        call message("Ren","Hi!") from _call_message_25
        call screen phone_reply("Ren?", "choiced14", "Are you running a quick maintenance?", "choiced15")
    label choiced14:
        call phone_after_menu from _call_phone_after_menu_3
        call message_start("[name]", "Ren?") from _call_message_start_4
        call message("Ren","I see you're enjoying your chat with my AIs ^^") from _call_message_26
        jump aftermenud12
    label choiced15:
        call phone_after_menu from _call_phone_after_menu_4
        call message_start("[name]", "Are you running a quick maintenance?") from _call_message_start_5
        call message("Ren","Oh, no.") from _call_message_27
        call message("Ren","I just dropped by to see you.") from _call_message_28
        jump aftermenud12
    label aftermenud12:
        call message("Ren","Did I mention that I can get inside the game?") from _call_message_29
        call message("Ren","I'll drop by once in a while like this ^^") from _call_message_30
        call message("Ren","Since I can't visit your room, it'd be nice if we can chat through the messenger as well, isn't it?") from _call_message_31
        call screen phone_reply("You scared me!", "choiced16", "When are you going to drop by my room?", "choiced17")
    label choiced16:
        call phone_after_menu from _call_phone_after_menu_5
        call message_start("[name]", "You scared me!") from _call_message_start_6
        call message("Ren","I must have forgotten to give you a heads-up...sorry.") from _call_message_32
        call message_img("Ren","look look", "images/ohyeah.png") from _call_message_img
        call message("Ren","This emoji shows that I'm happy to see you ^^") from _call_message_33
        jump aftermenud13
    label choiced17:
        call phone_after_menu from _call_phone_after_menu_6
        call message_start("[name]", "When are you going to drop by my room?") from _call_message_start_7
        call message("Ren","Hmm...later in the evening, I think.") from _call_message_34
        call message("Ren","I'm held by this task that requires constant monitoring, so...") from _call_message_35
        jump aftermenud13
    label aftermenud13:
        call message("Ren","Though I'm always doing the server check-ups...") from _call_message_36
        call message_img("Ren","I wanted to actually chat with you than to watch you through the log. So here I am.", "images/smile.png") from _call_message_img_1
        call message("Ren","I want to ask you something...") from _call_message_37
        call message("Ren","What do you think about the game so far? How do you like your gameplay?") from _call_message_38
        call message("Ren","They're really wary of you so far, aren't they?") from _call_message_39
        call screen phone_reply("Yes, they seem to be suspicious of me a lot... But I think our relationship will improve soon!", "choiced18", "I think everyone wants to find out my relationship with this person called Reina. What should I do?", "choiced19")
    label choiced18:
        call phone_after_menu from _call_phone_after_menu_7
        call message_start("[name]", "Yes, they seem to be suspicious of me a lot... But I think our relationship will improve soon!") from _call_message_start_8
        call message("Ren","Yes, I see. Just as expected.") from _call_message_40
        call message("Ren","They're all suspecting you, right? Except for Shun and Yoshiro, that is.") from _call_message_41
        call message("Ren","But you're doing very well. Everything will be ok in a few days.") from _call_message_42
        call message("Ren","These characters are first cold") from _call_message_43
        call message("Ren","but slowly open up to you.") from _call_message_44
        call message("Ren","Don't you think that's the beauty of this game?") from _call_message_45
        call message_img("Ren","So keep playing this game. Just keep up what you're doing right now.", "images/smile.png") from _call_message_img_2
        jump aftermenud14
    label choiced19:
        call phone_after_menu from _call_phone_after_menu_8
        call message_start("[name]", "I think everyone wants to find out my relationship with this person called Reina. What should I do?") from _call_message_start_9
        call message("Ren","Ah, they do...? I knew it...") from _call_message_46
        call message("Ren","Just make up a rough answer.") from _call_message_47
        call message("Ren","I can't tell you everything about this game. That'll take away all the fun...") from _call_message_48
        call message("Ren","But remember this -") from _call_message_49
        call message("Ren","There's hardly a truth in what those AIs know ^^;;") from _call_message_50
        call message("Ren","They think what they see is the truth.") from _call_message_51
        call message("Ren","I'm saying that they're from the game. So everything's made up.") from _call_message_52
        call message("Ren","Don't mind what those AIs tell you.") from _call_message_53
        call message("Ren","Just tell them that it's a secret.") from _call_message_54
        jump aftermenud14
    label aftermenud14:
        call message("Ren","It seems you're enjoying the game.") from _call_message_55
        call message("Ren","Oh, who do you like the most so far?") from _call_message_56
        call screen phone_reply4("No one in particular.", "choiced110", "The one who suspects me.", "choiced111", "The one who trust me.", "choiced112", "You, Ren ^^", "choiced113")
    label choiced110:
        call phone_after_menu from _call_phone_after_menu_9
        call message_start("[name]", "No one in particular.") from _call_message_start_10
        call message("Ren","I see....") from _call_message_57
        call message("Ren","So you prefer actual people than AIs, right?") from _call_message_58
        call message("Ren","Thanks for playing this game for me, [name].") from _call_message_59
        jump aftermenud15
    label choiced111:
        call phone_after_menu from _call_phone_after_menu_10
        call message_start("[name]", "The one who suspects me.") from _call_message_start_11
        call message("Ren","So you like meticulous type ^^") from _call_message_60
        call message("Ren","I see what kind of a man's your type. You prefer a man that's meticulous and logical.") from _call_message_61
        call message("Ren","But if you keep looking for such things") from _call_message_62
        call message("Ren","someday you will end up running into self-conflict.") from _call_message_63
        call message_img("Ren","Because 'logic' isn't the only thing that makes up this world.", "images/smile.png") from _call_message_img_3
        jump aftermenud15
    label choiced112:
        call phone_after_menu from _call_phone_after_menu_11
        call message_start("[name]", "The one who trust me.") from _call_message_start_12
        call message("Ren","I admit that men like them are quite charming...") from _call_message_64
        call message("Ren","They're emotional, and that's what touches your heart.") from _call_message_65
        call message("Ren","But if you make emotions your priority, you can disturb and exhaust people around you.") from _call_message_66
        jump aftermenud15
    label choiced113:
        call phone_after_menu from _call_phone_after_menu_12
        call message_start("[name]", "You, Ren ^^") from _call_message_start_13
        call message("Ren","Wow... ^^") from _call_message_67
        call message("Ren","I'm ecstatic...") from _call_message_68
        call message("Ren","I've never met someone who likes me unconditionally!") from _call_message_69
        call message_img("Ren","I hope this isn't a dream. I hope meeting you wasn't a dream.", "images/wink.png") from _call_message_img_4
    label aftermenud15:
        call message("Ren","Oh, right...") from _call_message_70
        call message("Ren","Though it's late did you enjoy your lunch?") from _call_message_71
        call message("Ren","Let me know if you have a preferred menu.") from _call_message_72
        call message("Ren","I'll make it the top priority in the kitchen.") from _call_message_73
        call screen phone_reply3("Could you first tell me what you like?", "choiced114", "Anything, as long as it's meat.", "choiced115", "I'd like vegetables or something healthy.", "choiced116")
    label choiced114:
        call phone_after_menu from _call_phone_after_menu_13
        call message_start("[name]", "Could you first tell me what you like?") from _call_message_start_14
        call message_img("Ren","...Me?", "images/questioning.png") from _call_message_img_5
        call message("Ren","Umm....") from _call_message_74
        call message("Ren","I......") from _call_message_75
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message_img("Ren","It's a secret.", "images/wink.png") from _call_message_img_6
        jump aftermenud16
    label choiced115:
        call phone_after_menu from _call_phone_after_menu_14
        call message_start("[name]", "Anything, as long as it's meat.") from _call_message_start_15
        call message("Ren","Then how about a steak for tonight?") from _call_message_76
        call message("Ren","You know, steak becomes a completely different dish depending on the degree of doneness.") from _call_message_77
        jump aftermenud16
    label choiced116:
        call phone_after_menu from _call_phone_after_menu_15
        call message_start("[name]", "I'd like vegetables or something healthy.") from _call_message_start_16
        call message("Ren","I'll prepare your dinner with health as a priority.") from _call_message_78
        call message("Ren","I like vegetables too ^^") from _call_message_79
        jump aftermenud16
    label aftermenud16:
        call message("Ren","Oh, it was so great chatting with you. Looks like I lost the time for a moment.") from _call_message_80
        call message("Ren","I think I should go now...") from _call_message_81
        call screen phone_reply("See you soon, Ren!", "choiced117", "Bye!", "choiced118")
    label choiced117:
        call phone_after_menu from _call_phone_after_menu_16
        call message_start("[name]", "See you soon, Ren!") from _call_message_start_17
        call message_img("Ren","Yes, let's see again.", "images/ohyeah.png") from _call_message_img_7
        jump aftermenud17
    label choiced118:
        call phone_after_menu from _call_phone_after_menu_17
        call message_start("[name]", "Bye!") from _call_message_start_18
        call message("Ren","Let's see you soon.") from _call_message_82
        jump aftermenud17
    label aftermenud17:
        call message("Ren","Right, if you see that AI called Shun again, please act naturally.") from _call_message_83
        call message_img("Ren","Those AIs don't know that I dropped by.", "images/smile.png") from _call_message_img_8
        call message("Ren","Though it's a shame to leave now...") from _call_message_84
        call message("Ren","Bye.") from _call_message_85
        play sound "music/hack.ogg"
        call message("SYSTEM","Ren has left the chatroom.") from _call_message_86
        call message("SYSTEM","Shun has entered the chatroom.") from _call_message_87
        call message("Shun","Oh") from _call_message_88
        call message("Shun","It's working") from _call_message_89
        call message("Shun","I'm back!") from _call_message_90
        call message_img("Shun","What was that just now?", "images/questioning.png") from _call_message_img_9
        call screen phone_reply("Tell me about it. Is it some sort of a bug?", "choiced119", "Perhaps the developer ran a quick server maintenance of something...", "choiced120")
    label choiced119:
        call phone_after_menu from _call_phone_after_menu_18
        call message_start("[name]", "Tell me about it. Is it some sort of a bug?") from _call_message_start_19
        call message("Shun","So my phone wasn't the only one that did it;;;;") from _call_message_91
        call message("Shun","The chat room froze, and then it threw me out all of a sudden;") from _call_message_92
        call message("Shun","Now it's working;;") from _call_message_93
        call message("...","...") from _call_message_94
        call phone_end from _call_phone_end
        jump u13
    label choiced120:
        call phone_after_menu from _call_phone_after_menu_19
        call message_start("[name]", "Perhaps the developer ran a quick server maintenance of something...") from _call_message_start_20
        call message_img("Shun","Huh?", "images/questioning.png") from _call_message_img_10
        call message("Shun","Did Zero do a check-up or something?;;") from _call_message_95
        call message_img("Shun","Wait, did he touch on something wrong while checking something?", "images/well.png") from _call_message_img_11
        call message("...", "...") from _call_message_96
        call phone_end from _call_phone_end_1
        jump u13

    label u13:
        stop music
        play music "music/music2chat-17.ogg"
        play sound "music/answer.ogg"
        show MC MC11 as dissolve
        mc "(Take call.)"
        play sound "music/beep.ogg"
        ray "Hello? I'm so glad that you picked up right away."
        ray "I just wanted to hear your voice."
        ray "Did you also wait for me to call?"
        menu:
            "Yes. I was waiting.":
                ray "I'm glad that you say that...."
                ray "I'll call often from now on. Wait for my call."
                jump u14
            "Why should I?":
                ray "I see you weren't..."
                ray "I only think of you these days. So I thought you were like me."
                jump u14
            "It's not that but you have a good voice, Ren.":
                ray "Oh... my voice...? My voice good...?"
                ray "I've never heard that before... I don't know what to say but..."
                ray "thank you for the compliment...."
                jump u14
    label u14:
        ray "Talking to you... like this...."
        ray "I really like it...."
        ray "Your voice ringing in my ear... it's so sweet..."
        ray "I'm a bit jealous... that the other AIs can listen to your voice as well."
        menu:
            "Are you already obsessing?":
                ray "Obsess...? Did you feel it like that?"
                ray "It's a misunderstanding. I'll try to be careful that you don't feel it like that."
                jump u15
            "Nothing can be done since it's for the testing.":
                ray "Uh, yeah. You're very mature."
                ray "Don't stay too close with the AIs. They aren't human anyway..."
                jump u15
    label u15:
        ray "Thinking of which, did you wrap up well in the chat room with the AIs?"
        ray "How was it? Do you think that Shun AI.. is suspecting you?"
        menu:
            "I'm not sure about suspecting but I think Shun's a good person.":
                ray" Good person...? You already think Shun as a person."
                ray "Shun is also an AI I created."
                ray "Anyway, that must mean I did a good job in making it...? I'll take it as a compliment."
                jump u16
            "It's obvious that he'll suspect me up to some degree~":
                ray "Yes. You don't need to take it as a big deal."
                ray "He'll open up if you just adequately chime with him."
                jump u16
    label u16:
        ray "Anyway, it hasn't been long since you've started testing the game."
        ray "You must have so many things you want to ask me..."
        ray "You can ask me anytime when you there's something you want to ask."
        menu:
            "I want to know your ideal type, Ren.":
                ray "I said to ask about the game... but you want to ask about me?"
                ray "I am more important than the game... it doesn't feel bad."
                ray "Um... ideal type... I don't know. Ideal type..."
                ray "I've never thought of that. I've never been interested in that...."
                ray "The only person I'm interested in is you...maybe you're my ideal type?"
                jump u17
            "Nothing that comes to mind.":
                ray "Really? You must be in the enjoying phase yet."
                ray "Tell me everything that you want to have fixed... if it's something that's uncomfortable or something you want to know?"
                ray "Your opinion is really important as this game was prepared just for you."
                ray "I'll put your input in getting the right directionality in making the game."
                ray "Tell me later if you don't have anything right now."
                ray "I'll always be beside you."
                jump u17
            "What happens if I delete the game?":
                ray "Why...? Do you plan to delete it...?"
                ray "Tell me if there's a problem. I'll fix it..."
                ray "So can't you test the game out?"
                ray "I did my best in preparing it until you came. There are so many things I've prepared."
                ray "It might not be to your taste yet but...."
                ray "one out of many might be something you like."
                ray "So, why don't you try it a bit more? I mean, it's only been a day."
                jump u17
    label u17:
        ray "I want you to stay in your room."
        ray "That room was prepped up just for you..."
        ray "Wallpaper, ceiling decoration, all the small props, I picked it out myself. Thinking of you."
        ray "So... I want you to stay there. You will stay there, won't you?"
        ray "...You won't leave me, will you?"
        menu:
            "Sure. I'm not going anywhere.":
                ray "Haa... Oh, sorry. I was felt too relieved that I unconsciously... sighed."
                ray "Thank you for saying that... [name]."
                ray "I want you to stay next to me endlessly. I... I'll try my best that you won't get sick of me."
                ray "I'll try to update the game so please enjoy it."
                jump u18
            "I could leave one day.":
                ray "Oh.. don't... don't say such scary things...."
                ray "My heart just sank thinking of you leaving permanently... leaving me and the room behind."
                ray "I'll do better. I'll do my best that you won't even think of leaving."
                jump u18
    label u18:
        ray "I want to trust you. You're the only person... who I said I'll trust with my own will."
        ray "Haa.. If I can, I want to keep talking to you like this."
        ray "I would if I wasn't that busy... but I have lots of things to do right now..."
        ray "I must hang up now. I'll see you later. Bye."
        play sound "music/beep.ogg"
        stop music
        hide MC MC11 with dissolve
        scene black with dissolve
        with Pause(2.0)
        show text "23:25" with dissolve
        with Pause(1.0)
        hide text with dissolve
        with Pause(2.0)
        scene black with dissolve
        play sound "music/knock.ogg"
        ray "It's me, Ren! Mind if I come in?"
        mc "Come in."
        scene bg7 with dissolve
        stop music
        play music "music/music4.ogg"
        show MC MC11 at my_right with dissolve
        show U U11 at my_left with dissolve
        ray "Hi, [name]! I missed you so much, so I couldn't help visiting."
        show U U12
        ray "It's really late but you're awake. What were you doing?"
        menu:
            "I was playing the game.":
                ray "You've been testing to this hour...?"
                ray "You're really pouring your soul into your gameplay. Makes me feel proud as the creator!"
                ray "I like someone who plays a lot of games. I get the feeling that I'll be able to talk to you more comfortably."
                ray "I'll make it even more fun. I'm getting so enthusiastic!"
                jump vnd11
            "I've been waiting for you, Ren.":
                ray "You were waiting for me? Woah, you mean it?"
                ray "You must like me. I like you too."
                ray "I was wondering what you're doing, and I just had to find out."
                jump vnd11
            "I was about to get to bed.":
                ray "Do you usually go to bed at this hour? I see...I'll try to drop by more early."
                ray "But before you fall asleep tonight, play with me a little bit."
                jump vnd11
    label vnd11:
        show U U11
        ray "How was your day? Did you enjoy my game?"
        show U U12
        ray "I wonder if my AIs were entertaining enough."
        menu:
            "It was fun! It felt like talking to actual people instead of AIs.":
                show U U14
                ray "I'm so flattered!"
                ray "I gave a lot of consideration to the realistic aspect, to make it looks like you're talking to actual people as you play."
                ray "If you did feel that AIs are real, my goal is accomplished."
                ray "But don't forget - these AIs are virtually made."
                ray "What's beyond the screen...is nothing but fraud. Fake people giving appropriate answers according to a set pattern..."
                ray "The only actual person here is right in front of you - me."
                jump vnd12
            "The AIs kept suspecting me, so it wasn't fun.":
                show U U14
                ray "Oh...though I'm their developer, they surely are suspicious."
                ray "But it'd be no fun if they like you from the start, would it?"
                ray "The fun lies in the part where those suspicious AIs slowly open up to you."
                ray "If you open your heart and play, they'll grow to like you in no time."
                jump vnd12
            "Don't you have a cheat for this?":
                show U U14
                ray "Ahaha, didn't think you'll be looking for such a thing from day 1..."
                ray "But that will make the game less entertaining."
                ray "Don't you think a truly interesting relationship tends to grow slowly?"
                ray "But of course...you can start a relationship with me with maximum affinity."
                jump vnd12
    label vnd12:
        show U U13
        ray "I'll have the next update with your opinions as a reference."
        show U U13
        ray "You know, I keep thinking that I have no idea how characters like them managed to turn out, though I made all 5 of them."
        show U U12
        ray "Did I add too many flaws to make them more realistic...?"
        ray "I'll make note of your opinions and make the game more fun. I hope you'll give lots of feedback!"
        show U U13
        ray "Anything else you wanna know?"
        menu:
            "Looks like you don't like them, Ren, though you made these AI characters.":
                show U U12
                ray "...No."
                ray "No, I don't like them..."
                ray "Actually, someone else set these characteristics for the AIs. So I can't change what they're like."
                ray "If I talk too much, I'll make a spoiler. But please remember one thing."
                ray "The RFA isn't so good or meaningful as they say..."
                ray "It's a club of deception and hypocrites."
                ray "You'll get it as you play. You'll learn how hypocritical and twisted those AIs are."
                jump vnd13
            "Now that we talked about games, I want to talk about you, Ren.":
                show U U12
                ray "Huh? About me...? Actually, I've never talked much about me."
                ray "Is it because no one has ever asked me about my feelings?"
                ray "For now...I'd like you to think that I'm a cool app developer."
                ray "Oh, and one more thing..."
                ray "I've been feeling so good ever since you came here, [name]!"
                ray "It feels like a dream...that you play my game."
                ray "I'm also so happy that you're interested in me this much."
                ray "Did I just say 'happy?' Ahaha...! It's been a while since I spoke that word."
                jump vnd13
    label vnd13:
        show U U14
        ray "[name], you're the perfect tester for me to complete my masterpiece. I'd like to think so..."
        show U U13
        ray "Please keep playing with the RFA as best as you can."
        show U U14
        ray "Oh, of course, please don't forget about me!"
        show U U17
        ray "The RFA is a bunch of fake people anyways."
        ray "No matter how good those AIs are, you can't deny the fact that I am real in this reality."
        show U U12
        ray "Oh, that took longer than I planned. Talking to you was so entertaining."
        ray "I gotta go back to work. I have some check-up to do."
        show U U13
        ray "Before I go, there's one thing I want to tell you."
        show U U13
        ray "Meeting you today was the best thing in my life, [name]."
        ray "Ok then...I'll see you tomorrow."
        stop music
        scene black with dissolve
        hide U with dissolve
        hide MC with dissolve
        scene bg7 with dissolve
        show MC MC11 with dissolve
        play music "music/music2chat-17.ogg"
        play sound "music/answer.ogg"
        "(K is calling)"
        stop sound
        mc "(Pick up.)"
        play sound "music/beep.ogg"
        v "Hello, coordinator."
        v "I greet you again on the phone like this."
        v "I told you that I'll call time to time in the chat room."
        v "Oh, I hope you weren't trying to sleep, were you?"
        menu:
            "I was trying to sleep but it's okay.":
                v "Oh, you were. I'm sorry."
                v "Then I'll keep this short. It'll only take a short while."
                jump v16
            "Talking to you, K, is always a welcome.":
                v "Thank you for putting it that way."
                v "I think I'll call more often from now on."
                v "I'll be able to talk to with more ease."
                jump v16
    label v16:
        v "Anyway, I wanted to talk to you."
        v "Did you know? Every one of our members can read the chat room."
        v "If you knew that, I thought there might be some uncomfortable things that you didn't want to say in the chat room."
        v "At those times, you can call like this. The the two conversing will know the phone call content."
        v "The members must have explained very well about the party."
        v "They were all waiting to have the party soon."
        v "About the party... the members would have already told you about it... I wanted to talk to you about Reina."
        v "You're someone Reina sent but I figured you wouldn't know how RIka was like in our association."
        menu:
            "Why do we need to talk about someone who's not here anymore?":
                v "Um... the job you'll be doing is what Reina used to do."
                jump v17
            "The person who told me to host the party was Reina.":
                v "Yes. You said that at the very first in the chat room."
                v "The job you'll be doing is what Reina used to do."
                jump v17
    label v17:
        v "It's very crucial in inviting guests to the party."
        v "If there are no guests, the party cannot exist."
        v "Our prior parties were big and grand, very large in scale."
        v "That was because Reina invited many people from various areas."
        v "The reason why she was able to do that was because she truly wanted people to come to the party."
        v "If you treat everyone sincerely, I believe the guests with respond."
        menu:
            "I'll have that in mind. Thank you for your advice.":
                v "I thank you for accepting it in a good way."
                v "My sincerity that you accept it without distortion must have pulled through to you...Haha."
                jump v18
            "I'll take care of it myself.":
                v "Oh...yes. Was I being too intrusive?"
                v "I trust that you'll host the party very well."
                jump v18
    label v18:
        v "Oh... I almost forgot to tell you of the people you'll be hosting the party with."
        v "All of our members are good people."
        v "They treat others with sincerity."
        v "Though we sometimes clash and fight because everyone thinks differently, they're all good people."
        v "I think it'll be faster for you experience it yourself rather than me telling you about them."
        v "The night is very late. It's also bad manners to keep someone up this late. I'll hang up now."
        v "[name], have a good night."
        play sound "music/beep.ogg"
        stop music
        scene black with dissolve
        hide MC with dissolve
        call day2 from _call_day2
