label day2:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 2" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "07:54" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    call phone_start from _call_phone_start_15
    play music "music/music2.ogg"
    call message_start("SYSTEM", "K has entered the chatroom") from _call_message_start_210
    call screen phone_reply3("K? Something's weird about the chat room.", "choiced221", "Uh...are you Ren by any chance?", "choiced222", "I see you've logged in early, K.", "choiced223")
    label choiced221:
        call phone_after_menu from _call_phone_after_menu_197
        call message_start("[name]","K? Something's weird about the chat room.") from _call_message_start_211
        call message("K","Hello, [name].") from _call_message_1736
        call message("K","Something's weird about the chat room...?") from _call_message_1737
        call message("K","You're sharp ^^") from _call_message_1738
        call message("K","It's nothing big, so you don't have to mind it.") from _call_message_1739
        call message("K","You can just chat with me as usual.") from _call_message_1740
        jump aftermenu210
    label choiced222:
        call phone_after_menu from _call_phone_after_menu_198
        call message_start("[name]","Uh...are you Ren by any chance?") from _call_message_start_212
        call message("K","Ren?") from _call_message_1741
        call message("K","You don't see my username correctly?") from _call_message_1742
        call message("K","That Ren must be your friend...") from _call_message_1743
        call message("K","He has a peculiar name.") from _call_message_1744
        call message("K","Now that I'm curious about you...") from _call_message_1745
        call message("K","I'd like to know what kind of a person this Ren is...") from _call_message_1746
        call message("K","and what you think about him...^^") from _call_message_1747
        jump aftermenu210
    label choiced223:
        call phone_after_menu from _call_phone_after_menu_199
        call message_start("[name]","I see you've logged in early, K.") from _call_message_start_213
        call message("K","Welcome, [name].") from _call_message_1748
        call message_img("K","Good morning to you.", "images/smile.png") from _call_message_img_86
        jump aftermenu210
    label aftermenu210:
        call message("K","[name].") from _call_message_1749
        call message("K","You got to chat with the RFA members yesterday.") from _call_message_1750
        call message("K","I think now you get a rough sense of what kind of people they are and what they talk about.") from _call_message_1751
        call message("K","What do you think about them?") from _call_message_1752
        call message("K","Aren't they all so nice...? And aren't they all so stupidly naive?") from _call_message_1753
        call screen phone_reply3("What? Stupid?", "choiced224", "Kind of... But I'm sure we'll be able to hold parties somehow.", "choiced225", "They keep suspecting me all the time. This is tiring and frustrating.", "choiced226")
    label choiced224:
        call phone_after_menu from _call_phone_after_menu_200
        call message_start("[name]","What? Stupid?") from _call_message_start_214
        call message("K","Did I just use a term that can cause a misunderstanding?") from _call_message_1754
        call message("K","But that was no lie.") from _call_message_1755
        call message("K","They are stupid.") from _call_message_1756
        call message("K","I know them better than anyone else.") from _call_message_1757
        call message("K","Trust in me.") from _call_message_1758
        call message("K","It won't hurt you ^^") from _call_message_1759
        jump aftermenu211
    label choiced225:
        call phone_after_menu from _call_phone_after_menu_201
        call message_start("[name]","Kind of... But I'm sure we'll be able to hold parties somehow.") from _call_message_start_215
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("K","You're diligent, [name].") from _call_message_1760
        call message("K","Those members of the RFA") from _call_message_1761
        call message("K","act the same all the time, like machines...") from _call_message_1762
        call message("K","Sometimes I wonder if they are actually capable of thinking.") from _call_message_1763
        call message("K","They really have a whole lot to learn, but I hope you can lead them safe and sound to the destination called party.") from _call_message_1764
        call message("K","I really look forward to the party you'll hold for us.") from _call_message_1765
        jump aftermenu211
    label choiced226:
        call phone_after_menu from _call_phone_after_menu_202
        call message_start("[name]","They keep suspecting me all the time. This is tiring and frustrating.") from _call_message_start_216
        call message("K","They're stupid, so they can't help it.") from _call_message_1766
        call message("K","They can think of only one thing and harbor only one suspicion.") from _call_message_1767
        call message("K","It's all because they're idiots, so I hope you'll be kind to them.") from _call_message_1768
        call message("K","And I hope you can lead the RFA until the parties are held.") from _call_message_1769
        jump aftermenu211
    label aftermenu211:
        call message("K","Mind if I give you a word of advice for you?") from _call_message_1770
        call message_img("K","Don't trust us too much.", "images/smile.png") from _call_message_img_87
        call message("K","Trust isn't always a good thing.") from _call_message_1771
        call message("K","Often you'll find blindness in guise of trust...") from _call_message_1772
        call message("K","Just look at this. Whether I blind them, or whether I shut their mouths, everything is settled with a single effect called friendship.") from _call_message_1773
        call message("K","Oh the RFA! Are there any other friends handier than them?") from _call_message_1774
        call message("K","They mean so much to me...") from _call_message_1775
        call message("K","They're such useful friends to me.") from _call_message_1776
        call message("K","Including you.....") from _call_message_1777
        call screen phone_reply("Something's not right... Are you really K?", "choiced227", "I don't know what you're talking about, but I'm curious what you've got more to say. So let's continue.", "choiced228")
    label choiced227:
        call phone_after_menu from _call_phone_after_menu_203
        call message_start("[name]","Something's not right... Are you really K?") from _call_message_start_217
        call message("K","Does it matter whether I'm really K or not?") from _call_message_1778
        call message("K","So if I change my username, does that make me someone else?") from _call_message_1779
        call message("K","I wonder why you think I'm K.") from _call_message_1780
        call message("K","I told you") from _call_message_1781
        call message("K","not to trust me so much.") from _call_message_1782
        call message("K","I'm the type of man who's more hypocritical than you think ^^") from _call_message_1783
        jump aftermenu212
    label choiced228:
        call phone_after_menu from _call_phone_after_menu_204
        call message_start("[name]","I don't know what you're talking about, but I'm curious what you've got more to say. So let's continue.") from _call_message_start_218
        call message("K","Curiosity is always welcome.") from _call_message_1784
        call message("K","It's important to ask questions about everything and suspect in more than one way.") from _call_message_1785
        call message("K","You should ask a question or two about what kind of association the RFA really is.") from _call_message_1786
        jump aftermenu212
    label aftermenu212:
        call message("K","......") from _call_message_1787
        call message("K","...") from _call_message_1788
        stop music
        play music "music/music4.ogg"
        call message("Ren","...") from _call_message_1789
        call message("Ren","Tada!") from _call_message_1790
        call message("Ren","Ahaha...") from _call_message_1791
        call message("Ren","Surprised?") from _call_message_1792
        call screen phone_reply("You really scared me! ...That was you, Ren?", "choiced229", "I already knew who you are.", "choiced230")
    label choiced229:
        call phone_after_menu from _call_phone_after_menu_205
        call message_start("[name]","You really scared me! ...That was you, Ren?") from _call_message_start_219
        call message_img("Ren","Wow, success!", "images/ohyeah.png") from _call_message_img_88
        call message("Ren","I must've been so good back there.") from _call_message_1793
        call message("Ren","Did you really think I was K? Ahaha!") from _call_message_1794
        call message("Ren","But it feels kind of strange that I managed to mimic that hypocrite so well.") from _call_message_1795
        call message("Ren","anyways") from _call_message_1796
        jump aftermenu213
    label choiced230:
        call phone_after_menu from _call_phone_after_menu_206
        call message_start("[name]","I already knew who you are.") from _call_message_start_220
        call message("Ren","I see. Did my acting suck?") from _call_message_1797
        call message("Ren","But figures. You can hardly find any similarity between that hypocrite and me.") from _call_message_1798
        call message("Ren","It was kind of hard trying to act nice.") from _call_message_1799
        call message("Ren","Oh, I am nice. I am.") from _call_message_1800
        call message("Ren","I'm saying that it was hard to act like a bad person acting badly.") from _call_message_1801
        call message("Ren","You do seem to possess sharp eyes.") from _call_message_1802
        call message("Ren","Nice judgment you have there ^^") from _call_message_1803
        call message("Ren","But I'm actually a bit happy that you recognized me.") from _call_message_1804
        call message_img("Ren","It means that you felt me during that conversation, you know...?", "images/smile.png") from _call_message_img_89
        jump aftermenu213
    label aftermenu213:
        call message("Ren","I wanted to show you, [name], that I can even do this.") from _call_message_1805
        call message("Ren","Of course, I'm not gonna butt in while you're talking with the AIs. So no worries.") from _call_message_1806
        call message("Ren","Anyways, good morning!") from _call_message_1807
        call message("Ren","Did you sleep well last night?") from _call_message_1808
        call screen phone_reply("I slept very well. I think I like my new room. Did you have a good rest last night?", "cd231", "The game was so fun, so I just couldn't take my eyes off.", "cd232")
    label cd231:
        call phone_after_menu from _call_phone_after_menu_207
        call message_start("[name]","I slept very well. I think I like my new room. Did you have a good rest last night?") from _call_message_start_221
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","actually, I didn't really sleep well last night") from _call_message_1809
        call message("Ren","I was worried whether you'd be enjoying my game.") from _call_message_1810
        call message("Ren","But I'm glad that you slept well!") from _call_message_1811
        call message("Ren","And I'm glad that you like your room.") from _call_message_1812
        call message("Ren","I was worried you'll feel trapped in there.") from _call_message_1813
        jump am214
    label cd232:
        call phone_after_menu from _call_phone_after_menu_208
        call message_start("[name]","The game was so fun, so I just couldn't take my eyes off.") from _call_message_start_222
        call message("Ren","oh really?") from _call_message_1814
        call message("Ren","Now I feel proud as the developer.") from _call_message_1815
        call message("Ren","I'm glad that you're enjoying the game...") from _call_message_1816
        call message("Ren","but don't get too emotionally involved with those AIs.") from _call_message_1817
        call message("Ren","They're AIs, anyways.") from _call_message_1818
        call message("Ren","You can't even meet them for real.") from _call_message_1819
        call message("Ren","I hope you'd treat them as characters from a game, nothing more.") from _call_message_1820
        jump am214
    label am214:
        call message("Ren","Actually, I made brunch for you.") from _call_message_1821
        call message_img("Ren","It turned out so nice, so I wanted to give you a sneak peek...", "images/cg3.png") from _call_message_img_90
        call message_img("Ren","I wish you'd enjoy it!", "images/wink.png") from _call_message_img_91
        call screen phone_reply("Thank you, Ren. You're so good at cooking!", "cd233", "I feel a little like a livestock being raised...", "cd234")
    label cd233:
        call phone_after_menu from _call_phone_after_menu_209
        call message_start("[name]","Thank you, Ren. You're so good at cooking!") from _call_message_start_223
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message_img("Ren","Really? Thanks for the compliment.", "images/ohyeah.png") from _call_message_img_92
        call message("Ren","I had to feed myself since I was very young...") from _call_message_1822
        call message("Ren","So I'm used to handling fire.") from _call_message_1823
        call message("Ren","And I'm good at basic skills.") from _call_message_1824
        call message("Ren","Actually...this is the first time") from _call_message_1825
        call message("Ren","I made something wishing my guest will enjoy it.") from _call_message_1826
        call message("Ren","I hope you'll enjoy it to the last bit.") from _call_message_1827
        call message_img("Ren","I'll deliver it to you myself. So I hope you look forward to it.", "images/smile.png") from _call_message_img_93
        jump am215
    label cd234:
        call phone_after_menu from _call_phone_after_menu_210
        call message_start("[name]","I feel a little like a livestock being raised...") from _call_message_start_224
        call message("Ren","Uh...is that so?") from _call_message_1828
        call message("Ren","Livestock? That's a sad choice of word...") from _call_message_1829
        call message("Ren","I only wish to treasure you and protect you...") from _call_message_1830
        call message("Ren","Listen, [name]") from _call_message_1831
        call message("Ren","you're not tired of this place, are you?") from _call_message_1832
        call message("Ren","You're not tired of me, are you?") from _call_message_1833
        call message("Ren","I hope that's not the case...") from _call_message_1834
        call message("Ren","Though it's no fun yet, I'll do better.") from _call_message_1835
        call message("Ren","I still have to practice my cooking, but I'll try harder.") from _call_message_1836
        call message("Ren","I wish you'd stay with me.") from _call_message_1837
        jump am215
    label am215:
        call message("Ren","I'm going to see you soon") from _call_message_1838
        call message("Ren","but I wanted to talk to you as soon as I can") from _call_message_1839
        call message("Ren","So I just couldn't resist dropping by.") from _call_message_1840
        call message("Ren","I'm trying to give you time to yourself as much as possible since I don't wanna interrupt your gameplay.") from _call_message_1841
        call message("Ren","But honestly, I want to know") from _call_message_1842
        call message("Ren","how you find this game so far.") from _call_message_1843
        call message("Ren","Do you like it?") from _call_message_1844
        call screen phone_reply3("I'm so excited! I think I'll have much more fun discovering all the secrets!", "cd235", "I really like the characters. Especially the guy called K!", "cd236", "I'm not sure...", "cd237")
    label cd235:
        call phone_after_menu from _call_phone_after_menu_211
        call message_start("[name]","I'm so excited! I think I'll have much more fun discovering all the secrets!") from _call_message_start_225
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","That's right. You hit the nail on the head.") from _call_message_1845
        call message("Ren","That is the beauty of this game!") from _call_message_1846
        call message("Ren","You know exactly what you have to do ^^") from _call_message_1847
        call message("Ren","You can just carry on with your ideas.") from _call_message_1848
        jump am216
    label cd236:
        call phone_after_menu from _call_phone_after_menu_212
        call message_start("[name]","I really like the characters. Especially the guy called K!") from _call_message_start_226
        call message("Ren","K?") from _call_message_1849
        call message("Ren","...I'm not sure what you like so much about that one.") from _call_message_1850
        call message("Ren","Though I'm his creator, K is a bit...yeah, you know.") from _call_message_1851
        call message("Ren","Didn't you feel that he's so frustrating") from _call_message_1852
        call message("Ren","while you were talking to him?") from _call_message_1853
        call message("Ren","He gives all the lip service you can think of, but in reality, he does nothing at all.") from _call_message_1854
        call message("Ren","He says it's all for the sake of members, but actually, his very existence is what creates conflict within the RFA.") from _call_message_1855
        call message("Ren","Oh, right.") from _call_message_1856
        jump am16
    label cd237:
        call phone_after_menu from _call_phone_after_menu_213
        call message_start("[name]","I'm not sure...") from _call_message_start_227
        call message("Ren","You don't like it?") from _call_message_1857
        call message("Ren","Those AIs must have been so annoying...") from _call_message_1858
        call message("Ren","But it's okay to talk to me now, right?") from _call_message_1859
        call message("Ren","Please let me know later what you don't like about this game.") from _call_message_1860
        call message("Ren","I'll try to fix it to fit your taste.") from _call_message_1861
        call message("Ren","No, I will fix it. So please don't say that you'll quit... Please...") from _call_message_1862
        call message("Ren","Umm...") from _call_message_1863
        jump am216
    label am216:
        call message("Ren","Did you notice by any chance?") from _call_message_1864
        call message_img("Ren","That AI called K is hiding something!", "images/angry.png") from _call_message_img_94
        call message("Ren","Perhaps he's trying to hide it because he's still wary of you...") from _call_message_1865
        call message("Ren","But he'll tell you what he's hiding once his love meter increases and he opens up to you.") from _call_message_1866
        call message("Ren","He might whisper to you what the RFA has been hiding.") from _call_message_1867
        call message("Ren","Discovering the secret that others were panting after is definitely a special thing, I'd say.") from _call_message_1868
        call message("Ren","Don't you want to know what those secrets will eventually lead to?") from _call_message_1869
        call message("Ren","...Take a look at K.") from _call_message_1870
        call message("Ren","Take a good look and ask him several questions.") from _call_message_1871
        call message("Ren","He's the one who holds the majority of information within the RFA.") from _call_message_1872
        call message("Ren","But he definitely has a secret! One he will tell once he opens up to you!") from _call_message_1873
        call message("Ren","I added thriller elements here and there within the game.") from _call_message_1874
        call screen phone_reply("They're really AIs...right? They feel so real.", "cd238", "This game is complicated...", "cd239")
    label cd238:
        call phone_after_menu from _call_phone_after_menu_214
        call message_start("[name]","They're really AIs...right? They feel so real.") from _call_message_start_228
        call message("Ren","It's an honor that you think they're real.") from _call_message_1875
        call message("Ren","But no matter how well-made they are, they are no match for real people.") from _call_message_1876
        call message("Ren","I'm sure you already know") from _call_message_1877
        call message("Ren","that even though you're having fun with those AIs, you belong in this world where I am.") from _call_message_1878
        call message("Ren","You're not at the world beyond the phone screen. You're in the same world as I am.") from _call_message_1879
        call message("Ren","Don't forget that...") from _call_message_1880
        jump am217
    label cd239:
        call phone_after_menu from _call_phone_after_menu_215
        call message_start("[name]","This game is complicated...") from _call_message_start_229
        call message("Ren","More you have to discover, more entertaining the game gets. Don't you agree?") from _call_message_1881
        call message("Ren","It'll be fun to hunt down and uncover the secrets one by one.") from _call_message_1882
        call message("Ren","A plain romance isn't bad, but love that sprouts from danger and secret is also good, don't you think?") from _call_message_1883
        call message("Ren","Of course, the main purpose of this game would be the romance with those AIs. But I hope you'd enjoy other elements as well.") from _call_message_1884
        jump am217
    label am217:
        call message("Ren","[name], you have no idea what I've gone through for this moment...") from _call_message_1885
        call message("Ren","Not that you have to know that!") from _call_message_1886
        call message_img("Ren","I just want you to enjoy the stage I poured my soul into.", "images/happy.png") from _call_message_img_95
        call screen phone_reply("I think you're really warmhearted man, Ren.", "cd240", "How much longer do I have to play this game?", "cd241")
    label cd240:
        call phone_after_menu from _call_phone_after_menu_216
        call message("[name]","I think you're really warmhearted man, Ren.") from _call_message_1887
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","Really?") from _call_message_1888
        call message("Ren","...Nobody ever told me that.") from _call_message_1889
        call message("Ren","My guardians used to scold me a lot...when I was young.") from _call_message_1890
        call message("Ren","I cried every day, and scolding was a routine... Everyone would condemn and blame me...") from _call_message_1891
        call message("Ren","Thank you for saying that.") from _call_message_1892
        call message("Ren","I want to be a warmhearted man to you...always.") from _call_message_1893
        call message("Ren","I wish I could do that forever...") from _call_message_1894
        jump am218
    label cd241:
        call phone_after_menu from _call_phone_after_menu_217
        call message_start("[name]","How much longer do I have to play this game?") from _call_message_start_230
        call message("Ren","You'll get to see the ending depending on how you play it...but what's the matter?") from _call_message_1895
        call message("Ren","Do you want to quit it already? Or do you want to see character endings?") from _call_message_1896
        jump am218
    label am218:
        call message("Ren","Oh") from _call_message_1897
        call message("Ren","Oh no...") from _call_message_1898
        call message("Ren","I wanted to talk to you some more, but I have tons of work to do.") from _call_message_1899
        call message("Ren","Once I fetch you your meal, I have to be stuck in the development room all day long. Again.") from _call_message_1900
        call message("Ren","There's this person I'm preparing a surprise gift for.") from _call_message_1901
        call screen phone_reply("Who is that?", "cd242", "It's not K, is it?", "cd243")
    label cd242:
        call phone_after_menu from _call_phone_after_menu_218
        call message_start("[name]","Who is that?") from _call_message_start_231
        call message("Ren","Hmm...who is it? A man who needs a bit of a shocker, should I say?") from _call_message_1902
        call message("Ren","There's this guy...that needs a bit of a scare.") from _call_message_1903
        call message("Ren","No, actually it may look like a scare, but perhaps it's really close to diversion...") from _call_message_1904
        call message("Ren","Anyways, I can't tell you who it is. But there is this guy who needs that.") from _call_message_1905
        jump am219
    label cd243:
        call phone_after_menu from _call_phone_after_menu_219
        call message_start("[name]","It's not K, is it?") from _call_message_start_232
        call message("Ren","K?") from _call_message_1906
        call message("Ren","What do you mean, [name]?") from _call_message_1907
        call message_img("Ren","He's an AI. He doesn't exist.", "images/angry.png") from _call_message_img_96
        call message("Ren","You shouldn't mistake virtual world for reality...") from _call_message_1908
        call message("Ren","You know what I mean, don't you?") from _call_message_1909
        call message("Ren","Don't forget, you belong in this world.") from _call_message_1910
        call message("Ren","You belong in this world with me.") from _call_message_1911
        call message("Ren","So don't get too involved with those virtual characters.") from _call_message_1912
        call message("Ren","Just treat them as much as you need to reach your goal. You only have to use them, ok?") from _call_message_1913
        jump am219
    label am219:
        call message("Ren","I can't take this anymore. I miss you even more now that I'm chatting with you.") from _call_message_1914
        call message("Ren","Above all, I want to make you a nice warm breakfast.") from _call_message_1915
        call message("Ren","I'll find you before your brunch gets cold...") from _call_message_1916
        call screen phone_reply("I'll be waiting.", "cd244", "I'm starving. Hurry up.", "cd245")
    label cd244:
        call phone_after_menu from _call_phone_after_menu_220
        call message_start("[name]","I'll be waiting.") from _call_message_start_233
        call message("Ren","Yep! I hope you'd like it.") from _call_message_1917
        call message("Ren","There's no time to type this, I'll be on my way.") from _call_message_1918
        jump am220
    label cd245:
        call phone_after_menu from _call_phone_after_menu_221
        call message_start("[name]","I'm starving. Hurry up.") from _call_message_start_234
        call message("Ren","Oh sorry. You must be hungry.") from _call_message_1919
        call message("Ren","I didn't know that time just flew away. I was too focused on our chat.") from _call_message_1920
        call message("Ren","I'll be right there.") from _call_message_1921
        jump am220
    label am220:
        call message_img("Ren","So just give me a moment, [name].", "images/wink.png") from _call_message_img_97
        call message("SYSTEM", "Ren has left the chatroom.") from _call_message_1922
        call phone_end from _call_phone_end_16
        stop music
        scene black with dissolve
        scene bg8 with dissolve
        play music "music/music3.ogg"
        show S S11 with dissolve
        s "K, can we talk right now? Oh...I didn't really find anything yet. Yeah, I'm surprised, too."
        show S S12
        s "Yeah, I'm still searching. This is no ordinary hacker... I'm dying to know just where he's from."
        s "But most importantly, the fact that this hacker knows Reina's name is... Huh? Oh...I know."
        show S S13
        s "But, are you sure I don't have to say anything...?"
        s "No one can trust [name]. But no one can stay away from [name], either... They'll get even more suspicious if they find out about this."
        show S S14
        s "Whatever the intention is, if this is related to this hacker, it could be dangerous. I don't understand why you'd say that... What if something bad happens?"
        show S S13
        s "Are you trying...not to disturb [name]'s side?"
        show S S11
        s "The other members would get super-suspicious in no time if you tell them that [name]'s okay but don't give them a solid proof for that."
        s "Sayaka happens to be searching separately... And she also keeps saying that she can't find anything about [name]."
        show S S15
        s "But the fact that I'm the only one keeping it secret is..."
        show S S11
        s "I think somebody installed a separate APK file to gain access."
        show S S12
        s "There could be several possible reasons why I can't track down this hacker...but what I'm most curious about is how this hacker managed to find our server and connect."
        s "The last resort? Nothing can be done once the messenger server is gone... Uh... Well, this is not the time to use that yet."
        show S S16
        s "Huh? Me? What about me so suddenly?"
        show S S11
        s "Don't worry about my health... You know that the fact that I'm still breathing is a bonus."
        show S S13
        s "You, on the other hand, seem to be logging in really late. Are you getting enough sleep?"
        s "I'm worried, too. I can't imagine what it's like for you..."
        show S S14
        s "Me? Well... I'm used to it. This isn't the first time, you know?"
        show S S11
        s "You know, I once pulled an all-nighter at my desk, working nonstop for like 5 days. But I'm still walking and talking."
        show S S17
        s "Oh... Well, that's right. I did basically fall dead on my bed for two days afterward. Haha."
        show S S16
        s "Umm...huh?"
        s "Wait a sec, K. Something's weird... What's wrong with this?"
        show S S12
        s "I gotta go. Let's talk later!"
        play sound "music/beep.ogg"
        show S S16
        s "What is this? Update?"
        show S S12
        s "I'm the updater here. Who the hell would run the update?"
        show S S18
        s "Ah, man...! Is it a hacker again?!"
        show S S16
        s "...He sent a message."
        s "Let's have a party...?"
        hide S with dissolve
        scene black with dissolve
        scene bg9 with dissolve
        show V V15 with dissolve
        v "Sayaka didn't get anything, and Levan didn't get anything..."
        show V V16
        v "But still, I can't shut [name] out. That would be an even bigger trigger for that side..."
        show V V15
        v "What should I do...?"
        show V V16
        v "I should think about this on my way."
        hide V with dissolve
        scene black with dissolve
        stop music
        show text "09:37" with dissolve
        with Pause (1.0)
        hide text with dissolve
        with Pause(2.0)
        scene bg5 with dissolve
        call phone_start from _call_phone_start_16
        play music "music/music4.ogg"
        call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_235
        call message_img("Ren","The administrator is here - ^^", "images/ohyeah.png") from _call_message_img_98
        call screen phone_reply3("Is this a maintenance time?", "cd246", "Perfect timing. I was about to report a bug.", "cd247", "The user is here -", "cd248")
    label cd246:
        call phone_after_menu from _call_phone_after_menu_222
        call message_start("[name]","Is this a maintenance time?") from _call_message_start_236
        call message_img("Ren","Huh?","images/questioning.png") from _call_message_img_99
        call message("Ren","Oh, no no.") from _call_message_1923
        call message("Ren","I usually run the maintenance when you're not playing.") from _call_message_1924
        call message_img("Ren","I shouldn't interrupt your gameplay, you know?", "images/smile.png") from _call_message_img_100
        jump am221
    label cd247:
        call phone_after_menu from _call_phone_after_menu_223
        call message_start("[name]","Perfect timing. I was about to report a bug.") from _call_message_start_237
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","Wow, you're really into it, aren't you?") from _call_message_1925
        call message_img("Ren","But let's put this talk aside for a bit. I dropped by to chat with you.", "images/wink.png") from _call_message_img_101
        jump am221
    label cd248:
        call phone_after_menu from _call_phone_after_menu_224
        call message_start("[name]","The user is here -") from _call_message_start_238
        call message_img("Ren","Ahaha", "images/ohyeah.png") from _call_message_img_102
        call message("Ren","That was a decent counter.") from _call_message_1926
        call message("Ren","You really are a funny girl.") from _call_message_1927
        call message("Ren","How do you like the game so far?") from _call_message_1928
        jump am221
    label am221:
        call message("Ren","Do you enjoy your chats with the AIs I built?") from _call_message_1929
        call message("Ren","Did you...enjoy your lunch?") from _call_message_1930
        call message("Ren","I wanna know what you're doing right now...") from _call_message_1931
        call message("Ren","I wanna pay you a visit, but I couldn't...^^;") from _call_message_1932
        call message("Ren","I've been stuck in the development room all this time...") from _call_message_1933
        call screen phone_reply3("What's the development room?", "cd249", "Can I drop by for a bit?", "cd250", "Did you have lunch, Ren?", "cd251")
    label cd249:
        call phone_after_menu from _call_phone_after_menu_225
        call message_start("[name]","What's the development room?") from _call_message_start_239
        call message("Ren","Umm...it's a place where I develop and maintain that game you're playing.") from _call_message_1934
        call message("Ren","It's stuffed with computers... It's not a very nice place.") from _call_message_1935
        call message("Ren","But I'll make sure to decorate it to fit your taste in case you drop by.") from _call_message_1936
        jump am222
    label cd250:
        call phone_after_menu from _call_phone_after_menu_226
        call message_start("[name]","Can I drop by for a bit?") from _call_message_start_240
        call message("Ren","Like I said, it's confidential... Not yet. I told you on the first day.") from _call_message_1937
        call message("Ren","You can visit when you later gain authority to access confidential stuff.") from _call_message_1938
        call message("Ren","I'm sorry, but hold on for a bit. Ok?") from _call_message_1939
        call message("Ren","It's a shame that I can't invite you to my place yet.") from _call_message_1940
        jump am222
    label cd251:
        call phone_after_menu from _call_phone_after_menu_227
        call message_start("[name]","Did you have lunch, Ren?") from _call_message_start_241
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","Huh?") from _call_message_1941
        call message("Ren","Umm...I...") from _call_message_1942
        call message("Ren","Not yet. I was too busy.") from _call_message_1943
        call message("Ren","No one has ever asked me if I had lunch. I feel...") from _call_message_1944
        call message("Ren","happy...") from _call_message_1945
        jump am222
    label am222:
        call message("Ren","To be honest") from _call_message_1946
        call message("Ren","I'm working on things other than app development in this building.") from _call_message_1947
        call message("Ren","I'm doing this and that - a lot of things.") from _call_message_1948
        call message("Ren","So I'm always busy.") from _call_message_1949
        call message("Ren","I want to take greater care of you, but I can't. Sorry about that.") from _call_message_1950
        call screen phone_reply("It's okay. I like your game.", "cd252", "You must be really talented. ", "cd253")
    label cd252:
        call phone_after_menu from _call_phone_after_menu_228
        call message_start("[name]","It's okay. I like your game.") from _call_message_start_242
        call message("Ren","Wow, you like it? Thank god.") from _call_message_1951
        call message("Ren","But...") from _call_message_1952
        call message("Ren","I'm actually a bit jealous...") from _call_message_1953
        call message("Ren","I kept thinking of you while I was working...") from _call_message_1954
        call message("Ren","But your mind was occupied by that game? This feels a bit...") from _call_message_1955
        call message("Ren","Am I not attractive enough...?") from _call_message_1956
        call message("Ren","No. I shouldn't do this, should I?") from _call_message_1957
        jump am223
    label cd253:
        call phone_after_menu from _call_phone_after_menu_229
        call message_start("[name]","You must be really talented.") from _call_message_start_243
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","That's right. I have a lot of things to do at this place.") from _call_message_1958
        call message("Ren","At this place, I can prove myself competent and worthy.") from _call_message_1959
        call message("Ren","I'm happy that you realize that.") from _call_message_1960
        call message("Ren","Your words make me happier than anyone else's did.") from _call_message_1961
        call message_img("Ren","I should take a screenshot of what you said. I'm going to set it as my background image.", "images/ohyeah.png") from _call_message_img_103
        jump am223
    label am223:
        call message("Ren","Oh right.") from _call_message_1962
        call message("Ren","Don't you feel bored inside your room all day?") from _call_message_1963
        call message("Ren","You haven't been outside yet, have you?") from _call_message_1964
        call message("Ren","There's a beautiful garden out there...") from _call_message_1965
        call message_img("Ren","I took this picture few days before you came.", "images/cg4.png") from _call_message_img_104
        call message("Ren","I wish we could take a walk in there one day...") from _call_message_1966
        call message("Ren","That's right. Why not have a tea time with just the two of us?") from _call_message_1967
        call message("Ren","have ice cream when it's hot") from _call_message_1968
        call message_img("Ren","and have a nice cup of tea when it's cold.", "images/wink.png") from _call_message_img_105
        call message("Ren","[name]") from _call_message_1969
        call message("Ren","Thanks for staying here for me today as well!") from _call_message_1970
        call message("Ren","I hope you won't leave...until the parties are held.") from _call_message_1971
        call message("Ren","I want to make a lot of memories here with you.") from _call_message_1972
        call message("Ren","Oh") from _call_message_1973
        call message("Ren","I didn't interrupt you from talking to those AIs, did I?") from _call_message_1974
        call screen phone_reply("Actually....you did. Kind of.", "cd254", "No, I like you more than those AIs.", "cd255")
    label cd254:
        call phone_after_menu from _call_phone_after_menu_230
        call message_start("[name]","Actually....you did. Kind of.") from _call_message_start_244
        show bh at heart_pos with dissolve
        hide bh with dissolve
        call message_img("Ren","I did...?", "images/sad.png") from _call_message_img_106
        call message("Ren","I didn't mean to interrupt you.") from _call_message_1975
        call message("Ren","I just wished to play with you...") from _call_message_1976
        call message("Ren","I don't want to lose you to them.") from _call_message_1977
        jump am224
    label cd255:
        call phone_after_menu from _call_phone_after_menu_231
        call message_start("[name]","No, I like you more than those AIs.") from _call_message_start_245
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","Really?") from _call_message_1978
        call message("Ren","You mean it?") from _call_message_1979
        call message("Ren","I'm actually scared... What if I lose you to those AIs?") from _call_message_1980
        call message("Ren","I'm glad that you think so.") from _call_message_1981
        call message("Ren","I too like you more than those AIs.") from _call_message_1982
        jump am224
    label am224:
        call message("Ren","Those AIs and the game are important...But to me, our relationship is just as important.") from _call_message_1983
        call message("Ren","And...whether you're happy is important.") from _call_message_1984
        call message("Ren","You're my precious tester. One who believed in me.") from _call_message_1985
        call message("Ren","Only a very few people ever believed in me...in this world.") from _call_message_1986
        call message("Ren","But no one ever trusted all my words wholeheartedly.") from _call_message_1987
        call message("Ren","You know,") from _call_message_1988
        call message("Ren","I want to know what you talk about with those AIs.") from _call_message_1989
        call message("Ren","So I keep finding myself surfing through the logs.") from _call_message_1990
        call message("Ren","Are they keeping you entertained?") from _call_message_1991
        call message("Ren","I hope you don't get bored with this game!") from _call_message_1992
        call message("Ren","They have small bugs") from _call_message_1993
        call message("Ren","I'm worried they might make you bored...") from _call_message_1994
        call message("Ren","or hurt you...") from _call_message_1995
        call message("Ren","I'm afraid you might lose your interest in this game and leave... I don't want that to happen.") from _call_message_1996
        call screen phone_reply("I'm not bored. I get to talk to you like this from time to time. ", "cd256", "It did get kind of boring...", "cd257")
    label cd256:
        call phone_after_menu from _call_phone_after_menu_232
        call message_start("[name]","I'm not bored. I get to talk to you like this from time to time.") from _call_message_start_246
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message_img("Ren","I'm so happy.", "images/ohyeah.png") from _call_message_img_107
        call message("Ren","Actually, the happiest time of my day is when I talk to you.") from _call_message_1997
        call message("Ren","I like seeing you in the messenger, but I like seeing you in person even more.") from _call_message_1998
        jump am225
    label cd257:
        call phone_after_menu from _call_phone_after_menu_233
        call message_start("[name]","It did get kind of boring...") from _call_message_start_247
        call message("Ren","Oh no...") from _call_message_1999
        call message("Ren","What should I do? How can I make this game more entertaining?") from _call_message_2000
        call message("Ren","How can I make you enjoy it?") from _call_message_2001
        call message("Ren","I need you to hold the parties...") from _call_message_2002
        call message("Ren","I can't let you leave...") from _call_message_2003
        jump am225
    label am225:
        call message("Ren","Yes, that's right.") from _call_message_2004
        call message("Ren","That whitehead AI started bombarding you with a bunch of whatever-that-means, didn't he?") from _call_message_2005
        call message("Ren","Saying god or whatever...") from _call_message_2006
        call message("Ren","That's actually a bug...") from _call_message_2007
        call message("Ren","K also lies a lot because of a bug...") from _call_message_2008
        call message("Ren","Others have small bugs as well") from _call_message_2009
        call message("Ren","but those two are the worst ^^;") from _call_message_2010
        call message("Ren","They keep talking rubbish because of their bugs...") from _call_message_2011
        call message("Ren","Their world isn't real anyway, is it?") from _call_message_2012
        call message("Ren","You can make anything in a virtual world.") from _call_message_2013
        call message("Ren","A genius hacker, a CEO-in-line...") from _call_message_2014
        call message("Ren","And I'm the one who creates those AIs.") from _call_message_2015
        call message("Ren","I can make a person rich or poor with just a single word.") from _call_message_2016
        call message("Ren","You know that...I'm much greater than the whitehead, right?") from _call_message_2017
        call screen phone_reply3("Of course. You're much more amazing than him.", "cd258", "I thought you were their creator. Do you have to have them that much?", "cd259", "But they sounded right...", "cd260")
    label cd258:
        call phone_after_menu from _call_phone_after_menu_234
        call message_start("[name]","Of course. You're much more amazing than him.") from _call_message_start_248
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","I knew you'd know that!") from _call_message_2018
        call message_img("Ren","That's right. I'm more amazing than him.", "images/ohyeah.png") from _call_message_img_108
        call message("Ren","I need you to acknowledge that...") from _call_message_2019
        jump am226
    label cd259:
        call phone_after_menu from _call_phone_after_menu_235
        call message_start("[name]","I thought you were their creator. Do you have to hate them that much?") from _call_message_start_249
        call message("Ren","Oh...") from _call_message_2020
        call message("Ren","Ahaha") from _call_message_2021
        call message("Ren","You think so?") from _call_message_2022
        call message("Ren","I don't really hate them.") from _call_message_2023
        call message("Ren","Rather than hate...they're like a task for me to finish.") from _call_message_2024
        call message("Ren","That's the reason why I devoted years to this project...") from _call_message_2025
        jump am226
    label cd260:
        call phone_after_menu from _call_phone_after_menu_236
        call message_start("[name]","But they sounded right...") from _call_message_start_250
        call message("Ren","That's all trick.") from _call_message_2026
        call message("Ren","They're all rubbish. Be careful.") from _call_message_2027
        call message("Ren","Even if you agree with them, everything will feel pointless once you turn off that app.") from _call_message_2028
        call message("Ren","And that feeling will last forever if they're gone...") from _call_message_2029
        jump am226
    label am226:
        call message("Ren","Once you discover their secrets, your emptiness will be gone. So will mine...") from _call_message_2030
        call message("Ren","Oh, I gotta go now.") from _call_message_2031
        call message("Ren","I have a meeting with an important guest.") from _call_message_2032
        call message("Ren","But one day") from _call_message_2033
        call message("Ren","you'll get to meet my guest too.") from _call_message_2034
        call message("Ren","But I miss you as I'm saying goodbye...") from _call_message_2035
        call message("Ren","But I gotta finish this task first.") from _call_message_2036
        call message("Ren","I'll call you again as soon as I'm done.") from _call_message_2037
        call message("Ren","Have a wonderful day!") from _call_message_2038
        call message("Ren","Now goodbye.") from _call_message_2039
        call message("SYSTEM", "Ren has left the chatroom.") from _call_message_2040
        call phone_end from _call_phone_end_17
        stop music
        scene black with dissolve
        play music "music/music3.ogg"
        scene bg4 with dissolve
        show V V17 with dissolve
        v "About this message that Levan got...to have a party..."
        show V V18
        v "We don't know what would happen if we disregard it...Are they monitoring our chat room or something?"
        v "Haa... There's barely a thing we found out...but the other side seems to be in a hurry..."
        show V V11
        v "And when it comes to [name] in particular..."
        show V V18
        v "telling the others to just trust me without a good reason is no different from playing around with them."
        v "However...there's no telling what they'll do if we respond with suspicion and attack."
        show V V11
        v "Until the wind changes in our favor...it'd be best to act naturally and friendly..."
        show V V19
        v "I guess there's no choice...but to proceed with the parties... This needs to get done when there's time for the process."
        show V V11
        v "All that happened...must be wrapped up."
        show V V18
        v "[name]... I dearly hope you're not a bad person. Though we don't know everything about you... I really wish you're not a bad person."
        hide V with dissolve
        stop music
        scene black with dissolve
        show text "13:57" with dissolve
        with Pause (1.0)
        hide text with dissolve
        with Pause(2.0)
        scene black with dissolve
        play music "music/music9.ogg"
        scene cg5 with dissolve
        v "Reina, I still don't get what you're trying to imply."
        v "I'm not sure why you sent [name] to us, or what you're trying to tell us."
        v "I'm sure I'd get my answers if I hold your hand and look into your eyes..."
        v "I can still clearly see your face in my head, Reina. But your face in this picture keeps fading away."
        v "Someday I wouldn't be able to even recognize our memoirs."
        v "...I still do not regret my decision."
        v "But I'm afraid...that someday I might not be able to remember your face."
        v "Reina. My one and only love."
        v "Is it warm over there? Are you now free from agony and sorrow?"
        v "Just hang in there...my love."
        v "I'm coming..."
        v "I'm coming to that place you're waiting for me...."
        scene black with dissolve
        stop music
        scene bg10 with dissolve
        play music "music/music3.ogg"
        show V V22 with dissolve
        whooo "......."
        believer "Who are you?"
        show V V210
        whooo "Believer Number A306."
        show V V22
        believer "State the password."
        show V V210
        whooo "...Shroud your vision, and you shall be saved."
        show V V22
        believer "Oh, you're a member of the information team, Room 1206. Come on in, fellow Believer."
        show V V210
        whooo "For eternal paradise."
        show V V22
        believer "For eternal paradise!"
        whooo "......."
        scene black with dissolve
        show V V211
        v "Reina...I hope this path can lead me to you...."
        hide V with dissolve
        stop music
        scene black with dissolve
        show text "18:26" with dissolve
        with Pause (1.0)
        hide text with dissolve
        with Pause(2.0)
        scene bg11 with dissolve
        play music "music/music3.ogg"
        show V V22 with dissolve
        believer "Number A306, did you hear that? About Number C276 whom we had the ceremony together."
        believer "I told you something was weird about him back there."
        believer "He exhibited some strange behaviors, so we had to hold a cleansing ceremony for him."
        show V V24
        v "...Cleansing ceremony? I've never heard about that part."
        believer "Oh, it must have taken place when you were outside for member recruitment."
        show V V22
        believer "It's the second cleansing ceremony that took place ever since the Ao Me was founded."
        believer "There were signs of corruption in Number C276, but our savior held the ceremony in person."
        show V V27
        v "In person...?"
        show V V22
        believer "Yeah. I think it was because...the last ceremony had trouble while Mr. Ren was taking care of it."
        believer "Number C276 and the savior took the ceremony together, and he's become completely different."
        show V V23
        believer "I've met him too. And he's a completely different person!"
        believer "He's become more devout than anyone else. I knew it. Our savior is something..."
        show V V22
        v "...Thanks for filling me in. But where is the savior, by the way?"
        believer "The savior frequently visits the information room these days. I think it's to work on something important with Mr. Ren."
        believer "Though I'm not sure if they're still together in that room...But why do you have to know?"
        show V V24
        v "Oh...we soon have to prepare for the regular ceremony. So if something happens, we must report it at once."
        show V V22
        believer "Hmm...Number A306."
        believer "You're going out often these days, and your mind seems to be somewhere else... You're not seeking for corruption, are you?"
        show V V29
        v "Of course not."
        show V V22
        believer "That's right. Don't forget your ceremony, and don't forget to pray. You might wanna recall the prayer you had with the savior."
        show V V21
        v "...Thank you for the advice. I'll keep that in mind."
        show V V22
        believer "Now I gotta go, Number A306. I have to finish preparing for the ceremony."
        stop music
        whooo "Wait. Out of my way, you all."
        hide V
        scene black with dissolve
        play music "music/music4.ogg"
        v "Uh...Yes."
        ray "I don't want these flowers in ruins! They're for someone very precious."
        believer "Mr. Ren! Please, go on through."
        ray "...Hang on. You there... Have we ever met...?"
        v "...That's what people often tell me."
        ray "What's your number, Believer?"
        v "It's A306."
        ray "A306...? You're from the information team."
        v "That's right, sir."
        ray "...Hmm."
        ray "Oh, this isn't the time. She might be waiting for me."
        ray "Now, excuse me."
        v "...For eternal paradise."
        ray "For eternal paradise."
        stop music
        scene black with dissolve
        scene bg7 with dissolve
        show MC MC11 with dissolve
        play music "music/music2chat-17.ogg"
        play sound "music/answer.ogg"
        mc "(Pick up.)"
        play sound "music/beep.ogg"
        ray "Where are you right now?"
        ray "I was at your room just a few minutes ago, but you weren't there..."
        ray "Were you away by any chance?"
        menu:
            "I did some sightseeing in the other room.":
                ray "I see. So we missed each other. Like I said, you can explore that floor whenever you want."
                jump u25
            "You were in my room? Why?":
                ray "I wanted to make sure you are doing okay. And there was something I wanted to give you."
                jump u25
            "You went in my room while I was gone?":
                ray "I'm sorry. The door didn't open even though I knocked, so I was worried and had to look..."
                ray "From now on I'll wait outside. So please don't be mad..."
                jump u25
    label u25:
        ray "Umm... If you're in your room right now, could you take a look on your bed? I brought you some flowers."
        ray "I picked only the pretty and fresh ones for you. I hope you like them."
        menu:
            "Thanks for the flowers. Did you make it yourself?":
                ray "Yeah. I was thinking about you as I made it... I'm glad you like it...!"
                ray "This is nothing. I want to give you something even better."
                jump u26
            "Rather than flowers, I prefer something to eat...":
                ray "Really...? Then next time let's eat something you like. Together!"
                ray "I wanna try what you like."
                jump u26
    label u26:
        ray "What could be better than flowers...? I'll try to come up with something better!"
        ray "Anyways, it would've been better if I could put those flowers in those tiny hands of yours. It's kind of a shame."
        ray "Umm...though it's not included in that bouquet, there's this really pretty flower called anemone. Have you ever seen it?"
        menu:
            "Yeah, through pictures.":
                ray "Yeah. It looks pretty even on pictures..."
                jump u27
            "Next time I'd like to have anemone, too.":
                ray "No. I will never give it to you."
                ray "Sorry for sounding so strict... But I'd hate to give it to you..."
                jump u27
    label u27:
        ray "The flower itself is pretty, but its language is so sad. So I don't wanna give it to you."
        ray "Anemone has several meanings in the languages of flowers, but the most well-known one is 'betrayal.'"
        ray "Even thinking about it makes my heart ache... And that term doesn't suit you, either..."
        menu:
            "I will never betray you.":
                ray "Thank you... You're so thoughtful."
                ray "I'll also trust you."
                jump u28
            "We'll see whether or not it suits me or not.":
                ray "Don't say that... I trust you."
                ray "So don't you ever think...that you can betray me."
                jump u28
    label u28:
        ray "I hope your room becomes full of flowery scents."
        ray "And I hope you can wake up to flower scents and feel good."
        ray "I hope you'll feel good as you stay here..."
        ray "I want you to be happy as long as you stay here."
        ray "Uh, just a sec."
        ray "...Looking for me? Alright."
        ray "Someone important is looking for me, so I have to go. Let's chat again. Bye."
        play sound "music/beep.ogg"
        stop music
        hide MC with dissolve
        scene black with dissolve
        show text "22:38" with dissolve
        with Pause (1.0)
        hide text with dissolve
        with Pause(2.0)
        scene bg7 with dissolve
        call phone_start from _call_phone_start_17
        play music "music/music4.ogg"
        call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_251
        call message("Ren","Oh, [name]!") from _call_message_2041
        call message("Ren","You're here!") from _call_message_2042
        call message("Ren","I went to your room just a while ago") from _call_message_2043
        call message("Ren","and you weren't there, [name]...") from _call_message_2044
        call message("Ren","Where have you been?") from _call_message_2045
        call message("Ren","Sorry, I can't let you out of that floor yet.") from _call_message_2046
        call message("Ren","Please give me some more time until your safety is secured ^^") from _call_message_2047
        call message("Ren","And...") from _call_message_2048
        call message("Ren","I don't know if you realized") from _call_message_2049
        call message("Ren","but I left some flowers on your bed.") from _call_message_2050
        call message("Ren","For you...") from _call_message_2051
        call screen phone_reply("They're beautiful. Thank you so much, Ren.", "cd261", "Thanks, but I don't like flowers...", "cd262")
    label cd261:
        call phone_after_menu from _call_phone_after_menu_237
        call message_start("[name]","They're beautiful. Thank you so much, Ren.") from _call_message_start_252
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","No, not at all.") from _call_message_2052
        call message("Ren","Actually, I wanted to spend more time with you...") from _call_message_2053
        call message("Ren","but I can't. This is my apology.") from _call_message_2054
        call message("Ren","I didn't know what flowers you like, so I kept deliberating.") from _call_message_2055
        call message("Ren","I'm glad you like them!") from _call_message_2056
        call message("Ren","I've never gave flowers as a gift.") from _call_message_2057
        call message("Ren","If you like flowers") from _call_message_2058
        call message("Ren","next time I'll given even bigger and prettier flowers.") from _call_message_2059
        call message_img("Ren","I wish I can give them to you in person...", "images/wink.png") from _call_message_img_109
        jump am227
    label cd262:
        call phone_after_menu from _call_phone_after_menu_238
        call message_start("[name]","Thanks, but I don't like flowers...") from _call_message_start_253
        call message("Ren","Oh...I see.") from _call_message_2060
        call message("Ren","Next time I'd better prepare something else other than flowers.") from _call_message_2061
        call message("Ren","I'd like you to tell me what you like.") from _call_message_2062
        call message("Ren","I want to give you what you want, but I'm not sure what I should do. I've never tried giving gifts...") from _call_message_2063
        call message("Ren","I wish to give you something better next time.") from _call_message_2064
        call message("Ren","I wish to give you what you want...") from _call_message_2065
        call message("Ren","I want to see you happy.") from _call_message_2066
        jump am227
    label am227:
        call message("Ren","I'll try rescheduling so that I can visit you more often.") from _call_message_2067
        call message("Ren","I analyzed the logs from the AIs today") from _call_message_2068
        call message("Ren","and I noticed some significant shift in their behaviors.") from _call_message_2069
        call message("Ren","K decided to host parties, you know?") from _call_message_2070
        call message("Ren","It means you're leading the game towards the right direction.") from _call_message_2071
        call message_img("Ren","Because holding parties is one of the main goals of this game...!", "images/happy.png") from _call_message_img_110
        call message("Ren","Now we're getting started!") from _call_message_2072
        call screen phone_reply("I think those AIs are not very suspicious of me now. I think this will get even better!", "cd263", "You mean I'm just getting started? When will I ever see the ending?", "cd264")
    label cd263:
        call phone_after_menu from _call_phone_after_menu_239
        call message_start("[name]","I think those AIs are not very suspicious of me now. I think this will get even better!") from _call_message_start_254
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","You're right. It's getting better.") from _call_message_2073
        call message("Ren","So just keep up with your gameplay.") from _call_message_2074
        call message_img("Ren","You'll find yourself met with the best ending if you keep up.", "images/smile.png") from _call_message_img_111
        jump am228
    label cd264:
        call phone_after_menu from _call_phone_after_menu_240
        call message_start("[name]","You mean I'm just getting started? When will I ever see the ending?") from _call_message_start_255
        call message("Ren","oh") from _call_message_2075
        call message("Ren","I mean that...") from _call_message_2076
        call message("Ren","Umm...what I'm saying is that there are other delicacies to enjoy other than the ending.") from _call_message_2077
        call message("Ren","You're on the right track.") from _call_message_2078
        call message("Ren","Just keep up the good work until you reach the party") from _call_message_2079
        jump am228
    label am228:
        call message("Ren","The beauty of this game is the party.") from _call_message_2080
        call message("Ren","So the key is how you hold parties.") from _call_message_2081
        call message("Ren","oh") from _call_message_2082
        call message("Ren","I'll drop it here. Otherwise, I'll spoil the game for you.") from _call_message_2083
        call message("Ren","I wonder if you got a lot of party emails.") from _call_message_2084
        call message("Ren","You can get more tomorrow, so good luck ^^") from _call_message_2085
        call message("Ren","I wish you'd invite a lot of entertaining guests...") from _call_message_2086
        call message("Ren","I think...it'd be fun if there are a variety of guests.") from _call_message_2087
        call message("Ren","Though it's a party in a game, I wanna get an invitation too!") from _call_message_2088
        call message("Ren","I can bring a surprise drink for everyone to enjoy ^^") from _call_message_2089
        call message("Ren","And if I get to go...I wish to go as a couple with you.") from _call_message_2090
        call message("Ren","Because...you're mine.") from _call_message_2091
        call message("Ren","but") from _call_message_2092
        call message("Ren","then I would interrupt your pursuit of a character, right?") from _call_message_2093
        call screen phone_reply3("I too wish you were my partner.", "cd265", "I wish you would focus on developing the game, developer.", "cd266", "I'm not yours.", "cd267")
    label cd265:
        call phone_after_menu from _call_phone_after_menu_241
        call message_start("[name]","I too wish you were my partner.") from _call_message_start_256
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","really?!") from _call_message_2094
        call message("Ren","You mean it?") from _call_message_2095
        call message("Ren","I'm so happy.") from _call_message_2096
        call message("Ren","That's right.") from _call_message_2097
        call message("Ren","You're a person, just like me.") from _call_message_2098
        call message("Ren","A real person.") from _call_message_2099
        call message("Ren","So you're on my side, not theirs.") from _call_message_2100
        call message("Ren","That's right. Haha.") from _call_message_2101
        jump am229
    label cd266:
        call phone_after_menu from _call_phone_after_menu_242
        call message_start("[name]","I wish you would focus on developing the game, developer.") from _call_message_start_257
        call message("Ren","Ah, of course...") from _call_message_2102
        call message("Ren","Of course. I'll do my best.") from _call_message_2103
        call message("Ren","I wish you'd have fun...") from _call_message_2104
        call message("Ren","But I'm a bit frustrated...") from _call_message_2105
        call message("Ren","I feel like those AIs are monopolizing you.") from _call_message_2106
        jump am229
    label cd267:
        call phone_after_menu from _call_phone_after_menu_243
        call message_start("[name]", "I'm not yours.") from _call_message_start_258
        call message("Ren","Oh...") from _call_message_2107
        call message("Ren","Did I offend you?") from _call_message_2108
        call message("Ren","Then...let me rephrase that...") from _call_message_2109
        call message("Ren","I wish you'd choose me.") from _call_message_2110
        call message("Ren","I'm sorry if I offended you...") from _call_message_2111
        call message("Ren","But I'd like you to know this...") from _call_message_2112
        jump am229
    label am229:
        call message("Ren","They're virtual characters.") from _call_message_2113
        call message("Ren","So don't get so attached to them.") from _call_message_2114
        call message("Ren","They'll be reset once the party is over, anyway...") from _call_message_2115
        call message("Ren","I am really glad...") from _call_message_2116
        call message("Ren","that you're testing this game.") from _call_message_2117
        call message("Ren","I'm sure I'm saying this a lot") from _call_message_2118
        call message("Ren","but I'd like you to know that it's because I'm so glad.") from _call_message_2119
        call screen phone_reply3("I am hearing that often. It's getting boring.", "cd268","I can see how happy you are.", "cd269", "If you're so glad, why not materialize it than speak it?", "cd270")
    label cd268:
        call phone_after_menu from _call_phone_after_menu_244
        call message_start("[name]","I am hearing that often. It's getting boring.") from _call_message_start_259
        call message("Ren","Ah...yes.") from _call_message_2120
        call message("Ren","No matter how pleasurable it is to your ears, you'll get tired if you get it too much.") from _call_message_2121
        call message("Ren","But it's because I'm so happy...") from _call_message_2122
        call message("Ren","so don't hate it so much.") from _call_message_2123
        call message("Ren","I'll try to stay away from what you don't like...") from _call_message_2124
        jump am230
    label cd269:
        call phone_after_menu from _call_phone_after_menu_245
        call message_start("[name]","I can see how happy you are.") from _call_message_start_260
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","[name], I think you are a very special person.") from _call_message_2125
        call message("Ren","You always tell me what I want to hear...") from _call_message_2126
        call message("Ren","like you're hacking my mind...") from _call_message_2127
        call message("Ren","But my arms are always open for a hacking like this.") from _call_message_2128
        call message("Ren","You're free to wreck my head.") from _call_message_2129
        call message("Ren","As long as I can hear your compliments.") from _call_message_2130
        call message("Ren","I feel somewhat comfortable deep inside when I'm talking to you.") from _call_message_2131
        call message("Ren","I've never felt like this before...") from _call_message_2132
        jump am230
    label cd270:
        call phone_after_menu from _call_phone_after_menu_246
        call message_start("[name]","If you're so glad, why not materialize it than speak it?") from _call_message_start_261
        call message_img("Ren","Uh...materialize?", "images/questioning.png") from _call_message_img_112
        call message("Ren","Should we grab a fancy dinner?") from _call_message_2133
        call message("Ren","Oh, wait. I'll give you something nice.") from _call_message_2134
        call message("Ren","I should give you something that you can keep for long.") from _call_message_2135
        call message("Ren","Something that will last forever...") from _call_message_2136
        call message("Ren","Ok. I'll think about what I can give you.") from _call_message_2137
        jump am230
    label am230:
        call message("Ren","[name].") from _call_message_2138
        call message("Ren","You'll stay here together too, right?") from _call_message_2139
        call message_img("Ren","This place would be more fun than your home.", "images/ohyeah.png") from _call_message_img_113
        call message("Ren","I'm sure it's frustrating that you can't go out...") from _call_message_2140
        call message("Ren","but I'll try to fix that asap!") from _call_message_2141
        call message("Ren","I wish you'd stay here and help me.") from _call_message_2142
        call message("Ren","There's no one else who can help me.") from _call_message_2143
        call message("Ren","You're the only one.") from _call_message_2144
        call message("Ren","If you're bothered that your house will be left empty too long") from _call_message_2145
        call message("Ren","could you stay at least until you hold the party?") from _call_message_2146
        call message("Ren","You gotta end the game you finished, don't you think?") from _call_message_2147
        call message_img("Ren","And you must be curious with whom you'll turn out as a couple.", "images/smile.png") from _call_message_img_114
        call screen phone_reply3("I wanna be close to you, Ren.", "cd271", "Yeah, I can't stop caring about K.", "cd272", "Ok, but only until the party is held.", "cd273")
    label cd271:
        call phone_after_menu from _call_phone_after_menu_247
        call message_start("[name]","I wanna be close to you, Ren.") from _call_message_start_262
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","Me...?") from _call_message_2148
        call message("Ren","In that case, you should stay here.") from _call_message_2149
        call message("Ren","If you decide to pursue me...") from _call_message_2150
        call message("Ren","You'll have to stay with me even after the game is over... Is that ok with you?") from _call_message_2151
        call message("Ren","Think about it.") from _call_message_2152
        call message("Ren","There's plenty of time ^^") from _call_message_2153
        call message("Ren","...") from _call_message_2154
        call message("Ren","No...actually I don't want you to think about it.") from _call_message_2155
        call message("Ren","I hope you won't change my mind...") from _call_message_2156
        jump am231
    label cd272:
        call phone_after_menu from _call_phone_after_menu_248
        call message_start("[name]","Yeah, I can't stop caring about K.") from _call_message_start_263
        call message("Ren","Oh...so that's your type.") from _call_message_2157
        call message("Ren","Frustrating, always talking who-knows-wha") from _call_message_2158
        call message("Ren","making a bunch of secrets to everyone") from _call_message_2159
        call message("Ren","forcing everyone to trust him...") from _call_message_2160
        call message("Ren","K, huh...?") from _call_message_2161
        call message("Ren","Don't you wanna know what he's hiding?") from _call_message_2162
        call message("Ren","If you like him") from _call_message_2163
        call message("Ren","why don't you try to find out what he's hiding?") from _call_message_2164
        call message("Ren","I bet he's hiding the most in the RFA.") from _call_message_2165
        call message_img("Ren","So try uncovering what he's hiding.", "images/smile.png") from _call_message_img_115
        jump am231
    label cd273:
        call phone_after_menu from _call_phone_after_menu_249
        call message_start("[name]","Ok, but only until the party is held.") from _call_message_start_264
        call message("Ren","Ok...") from _call_message_2166
        call message("Ren","Yes, please do. For now.") from _call_message_2167
        call message("Ren","But let me know if you change your mind.") from _call_message_2168
        call message("Ren","If you decide to stay longer, it's more than welcome.") from _call_message_2169
        call message("Ren","Then I'll be able to enjoy happiness...") from _call_message_2170
        call message("Ren","for at least a day more...") from _call_message_2171
        jump am231
    label am231:
        call message("Ren","Oh") from _call_message_2172
        call message("Ren","remember that I told you I'll be seeing someone important today...?") from _call_message_2173
        call message("Ren","I didn't talk about you yet.") from _call_message_2174
        call message("Ren","But...I will soon") from _call_message_2175
        call message("Ren","and I'm going to let you go outside too.") from _call_message_2176
        call message("Ren","Oh, it's nothing weird. So don't worry.") from _call_message_2177
        call message("Ren","I didn't bring you here in secret...") from _call_message_2178
        call message("Ren","because civilians can enter this place as well.") from _call_message_2179
        call message("Ren","Although getting out of here...could be a bit tricky.") from _call_message_2180
        call message("Ren","I'm the one who brought you here, so you're under my administration.") from _call_message_2181
        call screen phone_reply("...This isn't some sort of illegal pyramid scheme, is it?", "cd274", "I'm counting on you. Please take care of it fast.", "cd275")
    label cd274:
        call phone_after_menu from _call_phone_after_menu_250
        call message_start("[name]","...This isn't some sort of illegal pyramid scheme, is it?") from _call_message_start_265
        call message_img("Ren","Pyramid scheme?", "images/questioning.png") from _call_message_img_116
        call message("Ren","No, we're not.") from _call_message_2182
        call message("Ren","We have no intention to get you involved in something like that.") from _call_message_2183
        call message("Ren","I only wish to keep you entertained.") from _call_message_2184
        call message("Ren","I wish you'd be happier with this game.") from _call_message_2185
        call message_img("Ren","That's my purpose.", "images/wink.png") from _call_message_img_117
        jump am232
    label cd275:
        call phone_after_menu from _call_phone_after_menu_251
        call message_start("[name]","I'm counting on you. Please take care of it fast.") from _call_message_start_266
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","....") from _call_message_2186
        call message("Ren","I can't explain this feeling when someone trusts me.") from _call_message_2187
        call message("Ren","How should I put this?") from _call_message_2188
        call message("Ren","I feel this strong urge that I should do it, do it quick, do it well.") from _call_message_2189
        call message("Ren","It's odd, but it's not bad...") from _call_message_2190
        call message("Ren","You can count on me, [name].") from _call_message_2191
        call message_img("Ren","I'll do my best. Since you trust me.", "images/smile.png") from _call_message_img_118
        jump am232
    label am232:
        call message("Ren","Oh...") from _call_message_2192
        call message("Ren","I have to go now...") from _call_message_2193
        call message("Ren","the server seems unstable all of a sudden...damn...") from _call_message_2194
        call screen phone_reply("We didn't chat much... You're leaving already?", "cd276", "You should go now and look after the server.", "cd277")
    label cd276:
        call phone_after_menu from _call_phone_after_menu_252
        call message_start("[name]","We didn't chat much... You're leaving already?") from _call_message_start_267
        show gh at heart_pos with dissolve
        hide gh with dissolve
        call message("Ren","I wanted to chat with you some more too, It's too bad...") from _call_message_2195
        call message("Ren","But somebody could attack the server...") from _call_message_2196
        call message("Ren","It'd be bad if someone attacks the server") from _call_message_2197
        call message("Ren","while you're playing.") from _call_message_2198
        call message("Ren","I should set the firewall again.") from _call_message_2199
        jump am233
    label cd277:
        call phone_after_menu from _call_phone_after_menu_253
        call message_start("[name]","You should go now and look after the server.") from _call_message_start_268
        call message("Ren","Yes.") from _call_message_2200
        call message("Ren","I can't allow any error in that game.") from _call_message_2201
        call message("Ren","I'll be quick.") from _call_message_2202
        jump am233
    label am233:
        call message("Ren","Let's chat again.") from _call_message_2203
        call message("Ren","Next time I wanna chat with you face to face.") from _call_message_2204
        call message("Ren","Bye...") from _call_message_2205
        call message("Ren","My [name].") from _call_message_2206
        call message("SYSTEM", "Ren has left the chatroom.") from _call_message_2207
        call phone_end from _call_phone_end_18
        stop music
        show MC MC11 with dissolve
        play music "music/music2chat-17.ogg"
        play sound "music/answer.ogg"
        mc "(Pick up.)"
        play sound "music/beep.ogg"
        v "Hello, [name]."
        v "Can you talk right now?"
        v "Actually, there are few things I'd like to ask you."
        menu:
            "What is it?":
                v "It's nothing big."
                jump v28
            "You're not interrogating me, are you?":
                v "Oh, no, no. That was not at all my intention. I'd just like to know more about you."
                v "And I think I'd be able to trust you after I know more about you."
                jump v28
    label v28:
        v "I'd like to ask you this question...because I'd like to trust you."
        v "I'm sure the other members already asked you a lot..."
        v "But...could you tell me about the place you're staying?"
        v "Or...what do you see around you?"
        menu:
            "I can't  tell you that.":
                v "Umm...alright...I won't force you to tell me. If you can't tell me...then that can't be helped..."
                jump v29
            "It's...dark.":
                v "You mean it's too dark to see anything...?"
                v "Look closely. Isn't there anything you can see in the dark? How dark is it? Don't you have any light?"
                v "Uh...oh, dear. My apologies. I sounded like I'm scolding, didn't I...?"
                jump v29
            "Even if I tell you, you're not gonna believe me, are you?":
                v "That's not true. Did I offend you by any chance?"
                v "If that's the case, I'd like to apologize..."
                jump v29
    label v29:
        v "I was just...I was curious about you. And I was worried whether you're safe."
        v "I wasn't trying to gain something from you, just in case you're wondering..."
        v "Umm...allow me to ask one last thing. Did you get any gift of some sort at that place?"
        menu:
            "I got a bouquet.":
                v "A bouquet...? There'll be no problem if it's just a collection of flowers... Still, I'm worried."
                jump v210
            "Love...?":
                v "Did you just say...love?"
                v "Then may I ask you who is giving you that love and what kind of love it is?"
                v "Oh, wait. Are you...joking right now? Haha..."
                jump v210
            "I don't want to talk about it.":
                v "Could you tell me when you feel like you can? It doesn't matter when, and you can tell it to anyone among us."
                jump v210
    label v210:
        v "I've seen this news before."
        v "A person lost conscious after drinking something from a stranger, so we should watch ourselves."
        v "So if you get a gift from a stranger..."
        v "or feel that something's fishy, I suggest you take a look again, even at things you've taken for granted."
        menu:
            "I can take care of myself.":
                v "Uh...alright. Then I hope nothing goes wrong with you."
                jump v211
            "Yes, I'll do as you say.":
                v "It was just a word of advice out of concern... Thanks for hearing me so kindly."
                jump v211
    label v211:
        v "Since you've joined us, now you're one of the RFA."
        v "I truly hope you'll be safe."
        v "Oh, pardon me. I must leave now. Somebody's coming. Now excuse me."
        play sound "music/beep.ogg"
        stop music
        hide MC with dissolve
        scene black with dissolve
        call day3 from _call_day3
