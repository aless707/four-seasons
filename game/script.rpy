﻿# Characters
define mc = Character("[name]", who_color="#0066ee")
define y = Character("Yoshiro", who_color="#31ff26")
define g = Character("Shun", who_color="#8b8b83")
define h = Character("Ryou", who_color="#a59aed")
define vw = Character("Hiroki", who_color="#d7a97d")
define j = Character("Sayaka", who_color="#cfb740")
define whooo = Character("???", who_color="#ffb300")
define savior = Character("Savior", who_color="#dabb70")
define ray = Character("Ren", who_color="#cc3a9f")
define ray_who = Character("Ren?", who_color="#cc3a9f")
define ray_whooo = Character("???", who_color="#cc3a9f")
define unknown = Character("Unknown", who_color="#cc3a9f")
define v = Character("K", who_color="#99d8c8")
define u = Character("Sora", who_color="#cc3a9f")
define u_who = Character("Sora?", who_color="#cc3a9f")
define yu = Character("Young Sora", who_color="#cc3a9f")
define mother = Character("Mother", who_color="#ffb300")
define teacher = Character("Teacher", who_color="#ffb300")
define believer = Character("Believer", who_color="#ffb300")
define pr = Character("Prime Minister", who_color="#ffb300")
define driver = Character("Driver", who_color="#ffb300")
define prosecutor = Character("Prosecutor", who_color="#ffb300")
define police = Character("Police", who_color="#ffb300")
define io = Character("Investigation Officer", who_color="#ffb300")
define iu = Character("Intelligence Unit", who_color="#ffb300")
define news = Character("News", who_color="#ffb300")
define elly = Character("Shiro", who_color="#ffb300")
define s = Character("000", who_color="#fe2626")
define saeyoung = Character("Souta", who_color="#fe2626")
define ys = Character("Young Souta", who_color="#fe2626")
define r = Character("Reina", who_color="#d3d316")

define flash = Fade(0.1, 0.0, 0.5, color="#fff")

# The game starts here.

label start:
    stop music

    scene black
    show MC MC11
    $name = renpy.input("What is your name?")
    $name = name.strip()

    $good_end = 0
    $bad_end = 0

    $ heart_pos = Position(xpos=0.5, ypos=0.8)

    #show gh at heart_pos with dissolve
    #hide gh with dissolve
    #$good_end += 1

    #show bh at heart_pos with dissolve
    #hide bh with dissolve
    #$bad_end +=1

    hide MC with dissolve

    transform my_left:
        xalign .3 yalign 1.0
    transform my_right:
        xalign .7 yalign 1.0

    call prologue from _call_prologue


    return
