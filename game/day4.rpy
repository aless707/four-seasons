label day4:
    stop music
    scene black with dissolve
    with Pause(2.0)
    show text "Day 4" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    show text "00:16" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_34
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_601
    call message_img("Ren","Do you remember this flower, [name]?", "images/cg7.png") from _call_message_img_292
    call screen phone_reply("That's the one we saw in the garden!", "cd41", "That's the one we saw in the corridor!", "cd42")
label cd41:
    call phone_after_menu from _call_phone_after_menu_570
    call message_start("[name]","That's the one we saw in the garden!") from _call_message_start_602
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","I knew you'd remember!") from _call_message_4806
    call message("Ren","I thought you found this flower particularly pretty") from _call_message_4807
    call message("Ren","so I took a sneak pic...") from _call_message_4808
    call message("Ren","I hope you like it.") from _call_message_4809
    jump am41
label cd42:
    call phone_after_menu from _call_phone_after_menu_571
    call message_start("[name]","That's the one we saw in the corridor!") from _call_message_start_603
    call message_img("Ren","Huh?", "images/questioning.png") from _call_message_img_293
    call message("Ren","Umm; no.") from _call_message_4810
    call message("Ren","I thought you liked this one...") from _call_message_4811
    call message("Ren","Guess I was wrong.") from _call_message_4812
    call message("Ren","Perhaps it's not the one that you liked. Did I make a mistake...?") from _call_message_4813
    jump am41
label am41:
    call message("Ren","I wished we could walk for longer...") from _call_message_4814
    call message("Ren","But the time would just fly away from us...") from _call_message_4815
    call message("Ren","Actually") from _call_message_4816
    call message("Ren","I almost grabbed you when you were going into your room...") from _call_message_4817
    call message("Ren","without even realizing it myself.") from _call_message_4818
    call screen phone_reply("I would've been glad if you really did grab me.", "cd43", "Umm, I think you got too excited ^^;", "cd44")
label cd43:
    call phone_after_menu from _call_phone_after_menu_572
    call message_start("[name]","I would've been glad if you really did grab me.") from _call_message_start_604
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","You wanted me to do that?") from _call_message_4819
    call message("Ren","For real?!") from _call_message_4820
    call message("Ren","I thought you'd freak out if I did that") from _call_message_4821
    call message("Ren","so I was holding back...") from _call_message_4822
    call message("Ren","but there was no need for me to hold back?") from _call_message_4823
    call message("Ren","...I'm so happy.") from _call_message_4824
    call message("Ren","B""ecause it means that our hearts were in agreement...") from _call_message_4825
    call message("Ren","that you wanted to be with me some more too.") from _call_message_4826
    jump am42
label cd44:
    call phone_after_menu from _call_phone_after_menu_573
    call message_start("[name]","Umm, I think you got too excited ^^;") from _call_message_start_605
    call message("Ren","Uh, yeah. You're right.") from _call_message_4827
    call message("Ren","I'm sorry...") from _call_message_4828
    call message("Ren","I should try to calm down") from _call_message_4829
    call message("Ren","but it's too hard.") from _call_message_4830
    call message("Ren","I really enjoyed taking a walk with you in the garden, so...") from _call_message_4831
    call message("Ren","it's difficult me to remain calm.") from _call_message_4832
    jump am42
label am42:
    call message("Ren","When I close my eyes to get some sleep") from _call_message_4833
    call message("Ren","I keep thinking about you back in the garden.") from _call_message_4834
    call message("Ren","I keep thinking...") from _call_message_4835
    call message("Ren","if you're asleep") from _call_message_4836
    call message("Ren","what kind of dream you're dreaming") from _call_message_4837
    call message("Ren","and what you thought of our walk, stuff like that.") from _call_message_4838
    call message("Ren","I know I'm not supposed to think like this...") from _call_message_4839
    call message("Ren","but I wished") from _call_message_4840
    call message("Ren","I could program your mind") from _call_message_4841
    call message("Ren","I wish I could code your mind...") from _call_message_4842
    call message("Ren","so that you'll like me and me only.") from _call_message_4843
    call message("Ren","I'd love to create a virtual world free from") from _call_message_4844
    call message("Ren","sadness and pain and") from _call_message_4845
    call message("Ren","stay there with just you.") from _call_message_4846
    call message("Ren","We'll do only happy things everyday where nothing bad") from _call_message_4847
    call message("Ren","can ever trespass...") from _call_message_4848
    call message("Ren","in my virtual world...") from _call_message_4849
    call message("Ren","I'd wish for nothing else if only we") from _call_message_4850
    call message("Ren","could be happy together...") from _call_message_4851
    call screen phone_reply("That sounds like a dream... ", "cd45", "That sounds a bit creepy...", "cd46")
label cd45:
    call phone_after_menu from _call_phone_after_menu_574
    call message_start("[name]","That sounds like a dream...") from _call_message_start_606
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yep...that's the world I dream of.") from _call_message_4852
    call message("Ren","I'm so happy that") from _call_message_4853
    call message("Ren","we're in agreement.") from _call_message_4854
    call message("Ren","I want to someday program that world.") from _call_message_4855
    call message("Ren","Even the idea of just the two of us...") from _call_message_4856
    call message_img("Ren","makes me shiver with happiness.", "images/wink.png") from _call_message_img_294
    call message("Ren","But it's only a dream.") from _call_message_4857
    call message("Ren","A fantasy that I can never grab...") from _call_message_4858
    jump am43
label cd46:
    call phone_after_menu from _call_phone_after_menu_575
    call message_start("[name]","That sounds a bit creepy...") from _call_message_start_607
    call message("Ren","Umm...maybe.") from _call_message_4859
    call message("Ren","I know. It's unrealistic...") from _call_message_4860
    call message("Ren","how can you leave the real world behind?") from _call_message_4861
    call message("Ren","What was I saying just now...?") from _call_message_4862
    jump am43
label am43:
    call message("Ren","this is all just ridiculous fantasy, isn't it?") from _call_message_4863
    call message("Ren","To be honest...") from _call_message_4864
    call message("Ren","I think I'm getting a little jealous") from _call_message_4865
    call message("Ren","reading your conversation with the AIs.") from _call_message_4866
    call message("Ren","I think it's because") from _call_message_4867
    call message("Ren","you're too happy and amazing.") from _call_message_4868
    call message("Ren","I think") from _call_message_4869
    call message("Ren","you could use your time") from _call_message_4870
    call message("Ren","for something better than those AIs.") from _call_message_4871
    call screen phone_reply("I think all members of the RFA are fabulous...", "cd47", "I would've had a hard time playing this game if this wasn't a game that you gave to me to test.", "cd48")
label cd47:
    call phone_after_menu from _call_phone_after_menu_576
    call message_start("[name]","I think all members of the RFA are fabulous...") from _call_message_start_608
    call message("Ren","Oh, really?") from _call_message_4872
    call message("Ren","I see.") from _call_message_4873
    call message("Ren","Honestly...I don't know.") from _call_message_4874
    call message("Ren","....") from _call_message_4875
    call message("Ren","It's a bit shocking that you think they're amazing.") from _call_message_4876
    jump am44
label cd48:
    call phone_after_menu from _call_phone_after_menu_577
    call message_start("[name]","I would've had a hard time playing this game if this wasn't a game that you gave to me to test.") from _call_message_start_609
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yeah, I'm the creator") from _call_message_4877
    call message("Ren","but even I think they stink.") from _call_message_4878
    call message("Ren","You're bearing those boring AIs because 'I' am the creator...? I'm happy to hear that. And I'm sorry.") from _call_message_4879
    call message("Ren","It's sometimes such a pain to talk to them, isn't it?") from _call_message_4880
    jump am44
label am44:
    call message("Ren","Those idiots...") from _call_message_4881
    call message("Ren","They're all busy about the party") from _call_message_4882
    call message("Ren","but they didn't even decide on the date yet...") from _call_message_4883
    call message("Ren","I don't think they're really organized.. ^^;") from _call_message_4884
    call message("Ren","Or are they") from _call_message_4885
    call message("Ren","keeping the date a secret from you") from _call_message_4886
    call message("Ren","since they don't believe you yet?") from _call_message_4887
    call screen phone_reply("I think they're making another secret behind my back... ", "cd49", "I'm sure K will tell me his plan.", "cd410")
label cd49:
    call phone_after_menu from _call_phone_after_menu_578
    call message_start("[name]","I think they're making another secret behind my back...") from _call_message_start_610
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","I know, right? I agree.") from _call_message_4888
    call message("Ren","Geez...") from _call_message_4889
    call message("Ren","you must be disappointed.") from _call_message_4890
    call message("Ren","I hope you can uncover their secret") from _call_message_4891
    call message("Ren","[name].") from _call_message_4892
    call message("Ren","Then you'd feel less disappointed...") from _call_message_4893
    jump am45
label cd410:
    call phone_after_menu from _call_phone_after_menu_579
    call message_start("[name]","I'm sure K will tell me his plan.") from _call_message_start_611
    call message("Ren","Umm...I hope so.") from _call_message_4894
    call message("Ren","But you can never know") from _call_message_4895
    call message("Ren","what that guy's thinking.") from _call_message_4896
    call message("Ren","His thoughts are so elusive.") from _call_message_4897
    call message("Ren","He has a lot of secrets") from _call_message_4898
    call message("Ren","and he also lied a lot") from _call_message_4899
    call message("Ren","to keep them hidden.") from _call_message_4900
    call message("Ren","Watch yourself, [name]. Don't let him get you.") from _call_message_4901
    jump am45
label am45:
    call message("Ren","What do you think...") from _call_message_4902
    call message("Ren","about the RFA party?") from _call_message_4903
    call message("Ren","This concept of helping somebody with fundraising") from _call_message_4904
    call message("Ren","through charity parties") from _call_message_4905
    call message("Ren","is actually all over the world") from _call_message_4906
    call message("Ren","outside the games, you know?") from _call_message_4907
    call screen phone_reply("I think it's good that I get to help those in need.", "cd411", "I think it's a bit hypocritical... ", "cd412")
label cd411:
    call phone_after_menu from _call_phone_after_menu_580
    call message_start("[name]","I think it's good that I get to help those in need.") from _call_message_start_612
    call message("Ren","Yeah") from _call_message_4908
    call message("Ren","That's why people say when they do that.") from _call_message_4909
    call message("Ren","But do you think that actually helps?") from _call_message_4910
    jump am46
label cd412:
    call phone_after_menu from _call_phone_after_menu_581
    call message_start("[name]","I think it's a bit hypocritical...") from _call_message_start_613
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message_img("Ren","I know, right?", "images/smile.png") from _call_message_img_295
    call message("Ren","Our minds are similar, [name].") from _call_message_4911
    jump am46
label am46:
    call message("Ren","I don't like such...") from _call_message_4912
    call message("Ren","donation habit.") from _call_message_4913
    call message("Ren","I don't think that a one-time financial support") from _call_message_4914
    call message("Ren","can make a person completely happy.") from _call_message_4915
    call screen phone_reply("But that could be the gateway to salvation.", "cd413", "No, money can't solve everything.", "cd414")
label cd413:
    call phone_after_menu from _call_phone_after_menu_582
    call message_start("[name]","But that could be the gateway to salvation.") from _call_message_start_614
    call message("Ren","Oh, [name]....") from _call_message_4916
    call message_img("Ren","You sound like K.", "images/well.png") from _call_message_img_296
    call message("Ren","He didn't brainwash you by any chance, did he?") from _call_message_4917
    call message("Ren","That AI may sound convincing") from _call_message_4918
    call message("Ren","but his words are not completely true.") from _call_message_4919
    call message("Ren","Don't trust him too much, [name].") from _call_message_4920
    call message("Ren","Even though the gateway is wide open") from _call_message_4921
    call message("Ren","there a plenty of people that find themselves in greater misfortune in the end.") from _call_message_4922
    jump am47
label cd414:
    call phone_after_menu from _call_phone_after_menu_583
    call message("[name]","No, money can't solve everything.") from _call_message_4923
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yes, you're right.") from _call_message_4924
    call message("Ren","A financial donation is basically nothing different") from _call_message_4925
    call message("Ren","from self-satisfaction.", "images/smile.png") from _call_message_4926
    call message("Ren","You need more than that") from _call_message_4927
    call message("Ren","to make a person truly happy.") from _call_message_4928
    jump am47
label am47:
    call message("Ren","They would be happy for a moment") from _call_message_4929
    call message("Ren","but not for long...") from _call_message_4930
    call message("Ren","True salvation' is not something you can achieve with money.") from _call_message_4931
    call message("Ren","It can come true only when") from _call_message_4932
    call message("Ren","a person puts aside all stereotypes") from _call_message_4933
    call message("Ren","and wish a noble and pure wish") from _call_message_4934
    call message("Ren","to treat happiness as the topmost priority...") from _call_message_4935
    call message("Ren","And my AIs also hunger for salvation...") from _call_message_4936
    call message("Ren","but they're living very unstable lives") from _call_message_4937
    call message("Ren","trapped in the stereotypes of reality.") from _call_message_4938
    call message("Ren","Say, Yoshiro, for example,") from _call_message_4939
    call message("Ren","that AI is set to mourn the death of his precious.") from _call_message_4940
    call message("Ren","But is he really sad") from _call_message_4941
    call message("Ren","because of somebody's death?") from _call_message_4942
    call message("Ren","Or is he sad because death still remains a mystery to him?") from _call_message_4943
    call screen phone_reply("I think he has an inner problem.", "cd415", "I think the period of unsteady anxiety is meaningful if it is for the sake of overcoming one's pain.", "cd416")
label cd415:
    call phone_after_menu from _call_phone_after_menu_584
    call message_start("[name]","I think he has an inner problem.") from _call_message_start_615
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yes") from _call_message_4944
    call message("Ren","That character is very nervous and unstable.") from _call_message_4945
    call message("Ren","He has no idea who he is") from _call_message_4946
    call message("Ren","or where he wants to go.") from _call_message_4947
    call message("Ren","He's trapped in the thought that somebody has to decide them for him.") from _call_message_4948
    call message("Ren","So that's why he's unstable.") from _call_message_4949
    call message("Ren","He's incapable of making choices for himself.") from _call_message_4950
    call message("Ren","So he lets others choose for him") from _call_message_4951
    call message("Ren","and depend on their choices without any question.") from _call_message_4952
    call message("Ren","But that somebody is no longer with him, so there's no way for him to avoid his fear.") from _call_message_4953
    call message("Ren","And that's why he wants to blame someone else to feel secure.") from _call_message_4954
    call message("Ren","But there's no one for him to really blame.") from _call_message_4955
    jump am48
label cd416:
    call phone_after_menu from _call_phone_after_menu_585
    call message_start("[name]","I think the period of unsteady anxiety is meaningful if it is for the sake of overcoming one's pain.") from _call_message_start_616
    call message("Ren","Do you think time can save someone?") from _call_message_4956
    call message("Ren","What if a person's misfortune stays forever?") from _call_message_4957
    call message("Ren","Don't you think someone has to save them?") from _call_message_4958
    call message("Ren","When a person is so fatigued that they can't even raise their hands,") from _call_message_4959
    call message("Ren","don't you think someone's gotta grab and force them to rise if they have to?") from _call_message_4960
    call message("Ren","You should break free from your prejudice, [name].") from _call_message_4961
    call message("Ren","Some people") from _call_message_4962
    call message("Ren","cannot even hope") from _call_message_4963
    call message("Ren","for happiness.") from _call_message_4964
    call message("Ren","Those people need salvation.") from _call_message_4965
    jump am48
label am48:
    call message("Ren","Of course") from _call_message_4966
    call message("Ren","I already have an algorithm") from _call_message_4967
    call message_img("Ren","ready to save AI Yoshiro.", "images/happy.png") from _call_message_img_297
    call message("Ren","It's saying hidden") from _call_message_4968
    call message("Ren","only because it's not the time yet.") from _call_message_4969
    call message("Ren","But you'll gradually find out") from _call_message_4970
    call message("Ren","in the future.") from _call_message_4971
    call message("Ren","You can save Ryou, the man who does not believe in love,") from _call_message_4972
    call message("Ren","Shun, nervous about his own worth,") from _call_message_4973
    call message("Ren","the whitehead with what-in-the-world identity,") from _call_message_4974
    call message("Ren","Yoshiro, who wants to live tucked in his safe zone while waiting for someone to make decisions for him,") from _call_message_4975
    call message("Ren","and Sayaka, who agonizes over the image her society wants from her.") from _call_message_4976
    call message("Ren","You'll get to save...") from _call_message_4977
    call message("Ren","all of them!") from _call_message_4978
    call message("Ren","You'll release the happiness oppressed deep down in their hearts") from _call_message_4979
    call message("Ren","and make it come true!") from _call_message_4980
    call message("Ren","Well?") from _call_message_4981
    call message("Ren","Sounds fun, doesn't it?") from _call_message_4982
    call screen phone_reply("Alright! I'll play until the end.", "cd417", "That is a happy ending...right?", "cd418")
label cd417:
    call phone_after_menu from _call_phone_after_menu_586
    call message_start("[name]","Alright! I'll play until the end.") from _call_message_start_617
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Yes!") from _call_message_4983
    call message("Ren","Please do.") from _call_message_4984
    call message("Ren","You're the only one") from _call_message_4985
    call message("Ren","who can save those AIs.") from _call_message_4986
    call message("Ren","You won't get to change somebody's life") from _call_message_4987
    call message("Ren","out of this place.") from _call_message_4988
    call message("Ren","So please hold the party.") from _call_message_4989
    call message("Ren","If you hold the party") from _call_message_4990
    call message("Ren","you can save countless people suffering") from _call_message_4991
    call message("Ren","along with those AIs.") from _call_message_4992
    jump am49
label cd418:
    call phone_after_menu from _call_phone_after_menu_587
    call message_start("[name]","That is a happy ending...right?") from _call_message_start_618
    call message("Ren","Of course.") from _call_message_4993
    call message("Ren","I guarantee.") from _call_message_4994
    call message("Ren","You'd get to teach those") from _call_message_4995
    call message("Ren","happiness-ignorant AIs what happiness is.") from _call_message_4996
    call message("Ren","...[name], trust in my words.") from _call_message_4997
    call message("Ren","I believe in") from _call_message_4998
    call message("Ren","what you do can save those AIs.") from _call_message_4999
    call message_img("Ren","You'll make a miracle if you believe.", "images/smile.png") from _call_message_img_298
    jump am49
label am49:
    call message("Ren","Oh... By the way, [name]") from _call_message_5000
    call message("Ren","What...about you?") from _call_message_5001
    call message("Ren","Don't you also feel sad, agonized, or nervous") from _call_message_5002
    call message("Ren","just like someone else?") from _call_message_5003
    call message("Ren","you can become an official resident of this building.") from _call_message_5004
    call message("Ren","There is an enrollment procedure though...") from _call_message_5005
    call message("Ren","Well?") from _call_message_5006
    call message("Ren","What do you think?") from _call_message_5007
    call message("Ren","when you read this?") from _call_message_5008
    call screen phone_reply("Sure, I'll think about it.", "cd419", "What's the enrollment procedure?", "cd420")
label cd419:
    call phone_after_menu from _call_phone_after_menu_588
    call message_start("[name]","Sure, I'll think about it.") from _call_message_start_619
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren","Really?") from _call_message_5009
    call message("Ren","I hope you'll make a good decision.") from _call_message_5010
    call message("Ren","You only have to trust me") from _call_message_5011
    call message("Ren","just like when you first came here.") from _call_message_5012
    call message("Ren","There's nothing difficult") from _call_message_5013
    call message("Ren","Just trust me and hold my hand and follow me!") from _call_message_5014
    call message_img("Ren","I'll take care of the rest.", "images/wink.png") from _call_message_img_299
    jump am410
label cd420:
    call phone_after_menu from _call_phone_after_menu_589
    call message_start("[name]","What's the enrollment procedure?") from _call_message_start_620
    call message("Ren","Oh, nothing.") from _call_message_5015
    call message("Ren","You'll go through a ceremony that shows we are one...") from _call_message_5016
    call message("Ren","and plant a seed of happiness inside you.") from _call_message_5017
    call message("Ren","When that seed grows") from _call_message_5018
    call message("Ren","into a tree and bears a fruit") from _call_message_5019
    call message("Ren","that fruit will become your happiness.") from _call_message_5020
    call message("Ren","And when you become happy like that...") from _call_message_5021
    call message("Ren","I'll be happy too.") from _call_message_5022
    jump am410
label am410:
    call message_img("Ren","It feels great talking to you about this...", "images/happy.png") from _call_message_img_300
    call message("Ren","I really want you to become our resident") from _call_message_5023
    call message("Ren","asap") from _call_message_5024
    call message("Ren","I think I'll like you even more if you become one.") from _call_message_5025
    call message("Ren","I hope we can find that day soon...") from _call_message_5026
    call message("Ren","I can hardly wait.") from _call_message_5027
    call message("Ren","Oh") from _call_message_5028
    call message("Ren","I got to go.") from _call_message_5029
    call message("Ren","The server's getting unstable suddenly.") from _call_message_5030
    call message("Ren","I can't let you") from _call_message_5031
    call message("Ren","lose connection while you're playing.") from _call_message_5032
    call message("Ren","I gotta fix this fast.") from _call_message_5033
    call message("Ren","When") from _call_message_5034
    call message("Ren","are you going to sleep, [name]?") from _call_message_5035
    call message("Ren","Ha""ve a good night") from _call_message_5036
    call message("Ren","my precious [name].") from _call_message_5037
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_5038
    call phone_end from _call_phone_end_35
    stop music
    scene black with dissolve
    show text "08:15" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene black with dissolve
    play music "music/music3.ogg"
    v "Levan. It's me, K..."
    v "You didn't pick up, so I'm leaving a message."
    v "I read the message you left."
    v "I understand you're terribly concerned."
    v "Since you said even your skills aren't enough to figure out who [name] is...not to mention what happened past midnight."
    v "First I need every file you have. Send them all to my email. It doesn't matter if they're encrypted. Send everything...!"
    v "And...about...[name]..."
    v "I'll deal with it..."
    v "I was gathering information on my own, and there's nothing strange about her. Nor did I find any strange sign...."
    v "It doesn't seem like she approached us with ill intent regarding the RFA or parties."
    v "Though I wish we could share our information, this doesn't seem like a good time to do that... Sorry for keeping secrets from you as well."
    v "I wanted to talk to you through the phone. What a shame."
    v "I know you're busy, and I'm sorry for burdening you..."
    v "But I want you to know that I'm always grateful to you."
    v "Watch yourself, and if anything happens to [name] or the RFA, call me right away."
    v "That's it. I hope you'll call me back."
    play sound "music/beep.ogg"
    "(K hung up)"
    scene bg14 with dissolve
    show V V21 with dissolve
    v "..."
    show V V22
    v "The area of her activity is strictly limited, and there are 4 Believers watching her room. And there's no particular action detected."
    v "I couldn't catch anything suspicious in chatrooms as well. She doesn't look like a bad person..."
    show V V211
    v "...Looks like she really is being used."
    show V V21
    v "I'll be sure when I meet her in person...."
    show V V28
    v "But my identity wouldn't allow me to do that."
    show V V21
    v "If I don't approach you, you might be in danger, [name]. You're being used."
    show V V28
    v "Please...don't drive people to the cliff anymore. You already had me. Can't you be happy with that...?"
    show V V216
    v "I must resolve everything before my eyes get any worse..."
    hide V with dissolve
    stop music
    scene black with dissolve
    show text "12:00" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg5 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_35
    call message_start("SYSTEM", "Ren has entered the chatroom") from _call_message_start_621
    call message("Ren", "[name]....!") from _call_message_5039
    call message("Ren", "I'm not doing so well right now...") from _call_message_5040
    call message("Ren", "This headache is just killing me...") from _call_message_5041
    call message("Ren", "and my med wouldn't work either...") from _call_message_5042
    call screen phone_reply("Does it hurt a lot? I'm worried...", "cd421", "You should go to the hospital!", "cd422")
label cd421:
    call phone_after_menu from _call_phone_after_menu_590
    call message_start("[name]", "Does it hurt a lot? I'm worried...") from _call_message_start_622
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yeah...") from _call_message_5043
    call message("Ren", "this headache comes and goes once in a while...") from _call_message_5044
    call message("Ren", "Is this a side effect from thinking too much...?") from _call_message_5045
    jump am411
label cd422:
    call phone_after_menu from _call_phone_after_menu_591
    call message_start("[name]", "You should go to the hospital!") from _call_message_start_623
    call message("Ren", "Oh..I wish... Actually, there's a hospital in this building.") from _call_message_5046
    call message("Ren", "But my Believ") from _call_message_5047
    call message("Ren", "I mean;;") from _call_message_5048
    call message("Ren", "my doctor isn't in his office now...") from _call_message_5049
    call message("Ren", "God, my head is killing me.") from _call_message_5050
    call message("Ren", "I don't know what I should do.") from _call_message_5051
    jump am411
label am411:
    call message("Ren", "My body probably grown used to the painkiller. I've been taking it as much as my daily meal.") from _call_message_5052
    call message("Ren", "Or...") from _call_message_5053
    call message("Ren", "is it because my willpower isn't strong enough?") from _call_message_5054
    call message("Ren", "[name]") from _call_message_5055
    call message("Ren", "I hope you'll tell me if you're sick...") from _call_message_5056
    call message("Ren", "I'd hate and rather die to see you") from _call_message_5057
    call message("Ren", "going through the same pain I did...") from _call_message_5058
    call screen phone_reply("Yes, I'll make sure I tell you...!", "cd423", "I can take care of myself.", "cd424")
label cd423:
    call phone_after_menu from _call_phone_after_menu_592
    call message_start("[name]", "Yes, I'll make sure I tell you...!") from _call_message_start_624
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yes") from _call_message_5059
    call message("Ren", "I know better than anyone else") from _call_message_5060
    call message("Ren", "what it's like to be sick...") from _call_message_5061
    jump am412
label cd424:
    call phone_after_menu from _call_phone_after_menu_593
    call message_start("[name]", "I can take care of myself.") from _call_message_start_625
    call message("Ren", "That sounds like...") from _call_message_5062
    call message("Ren", "like you don't need me.") from _call_message_5063
    call message("Ren", "It's a bit sad...") from _call_message_5064
    call message("Ren", "I hope you won't be as sick as I am.") from _call_message_5065
    call message("Ren", "I want to help you...") from _call_message_5066
    call message("Ren", "Please understand that, [name]...") from _call_message_5067
    jump am412
label am412:
    call message("Ren", "You've seen the log, right?") from _call_message_5068
    call message("Ren", "Looks like AI 000 found my trace.") from _call_message_5069
    call message("Ren", "And he's apparently investigating like a madman...") from _call_message_5070
    call message("Ren", "They're complete idiots...") from _call_message_5071
    call message("Ren", "But no need to mind them!") from _call_message_5072
    call message("Ren", "This is all just a bug that arises") from _call_message_5073
    call message("Ren", "because I made them too realistic.") from _call_message_5074
    call message("Ren", "One day...") from _call_message_5075
    call message("Ren", "I'm gonna fix all of those bugs...") from _call_message_5076
    call message("Ren", "And K logged in a moment ago, didn't he?") from _call_message_5077
    call message("Ren", "I see that he's trying to gain your trust and use you to find out what he wants...") from _call_message_5078
    call message("Ren", "when he's still hiding loads of things from you.") from _call_message_5079
    call message("Ren", "Isn't that how you feel?") from _call_message_5080
    call screen phone_reply("Yes...this game could be over if I tell them, right?", "cd425", "I don't think K is a bad person.", "cd426")
label cd425:
    call phone_after_menu from _call_phone_after_menu_594
    call message_start("[name]", "Yes...this game could be over if I tell them, right?") from _call_message_start_626
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yes, it could...") from _call_message_5081
    call message("Ren", "You know you shouldn't let K get you, right?") from _call_message_5082
    call message("Ren", "He's trying to trick you") from _call_message_5083
    call message("Ren", "saying that he's going to help you...") from _call_message_5084
    call message("Ren", "Fascinating, isn't it? He's just an AI...") from _call_message_5085
    jump am413
label cd426:
    call phone_after_menu from _call_phone_after_menu_595
    call message_start("[name]", "I don't think K is a bad person.") from _call_message_start_627
    call message("Ren", "I set him to speak in a kind manner...") from _call_message_5086
    call message("Ren", "so that's probably why he seems kind.") from _call_message_5087
    call message("Ren", "I programmed him like that on purpose") from _call_message_5088
    call message("Ren", "while I was making a variety of characters.") from _call_message_5089
    call message("Ren", "So he's actually not a kind person...") from _call_message_5090
    call message("Ren", "So don't let him get you, [name]...") from _call_message_5091
    jump am413
label am413:
    call message("Ren", "So I'm starting to get a little annoyed...") from _call_message_5092
    call message("Ren", "My head is trying to kill me, and those AIs are also acting like idiots...") from _call_message_5093
    call message("Ren", "Huh, I'm the developer of the game, but I'm annoyed by my own game...") from _call_message_5094
    call message("Ren", "Perhaps you wouldn't like that...!") from _call_message_5095
    call message("Ren", "I think Ryou's is the least unfavorable one") from _call_message_5096
    call message("Ren", "among the RFA's AIs.") from _call_message_5097
    call message("Ren", "Though it bothers me a little that he's friends with K.") from _call_message_5098
    call message("Ren", "Ryou is running a gigantic company...") from _call_message_5099
    call message("Ren", "so his analytic and calm way of thinking") from _call_message_5100
    call message("Ren", "will be of great help to us in the future.") from _call_message_5101
    call screen phone_reply("Us...?", "cd427", "Do you want me to pursue that AI? Would that help you?", "cd428")
label cd427:
    call phone_after_menu from _call_phone_after_menu_596
    call message_start("[name]", "Us...?") from _call_message_start_628
    call message("Ren", "oh") from _call_message_5102
    call message("Ren"," I mean... He'll be useful in my research") from _call_message_5103
    call message("Ren", "in analyzing the data.") from _call_message_5104
    call message_img("Ren", "He's the type of the AI that's easy to draw out the patterns, so he'd be good for research... ^^;;", "images/smile.png") from _call_message_img_301
    jump am414
label cd428:
    call phone_after_menu from _call_phone_after_menu_597
    call message_start("[name]", "Do you want me to pursue that AI? Would that help you?") from _call_message_start_629
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "No") from _call_message_5105
    call message("Ren", "you don't have to do that...") from _call_message_5106
    call message_img("Ren", "You're free to choose whatever you want.", "images/smile.png") from _call_message_img_302
    call message("Ren", "But I'm so glad that you're willing to help me.") from _call_message_5107
    call message("Ren", "Thank you, [name]...") from _call_message_5108
    jump am414
label am414:
    call message("Ren", "Anyways, watch out for K, [name].") from _call_message_5109
    call message("Ren", "It bothers me that he's trying to dig out information from you...") from _call_message_5110
    call message("Ren", "If you trust him and do as he tells you...") from _call_message_5111
    call message("Ren", "he'll eventually betray you and lead you to a bad ending...") from _call_message_5112
    call message("Ren", "I'd hate to see you sad") from _call_message_5113
    call message("Ren", "with the game suddenly over in the middle...") from _call_message_5114
    call screen phone_reply("I can start all over again!", "cd429", "I wonder what the happy ending is like! I'll keep the secret.", "cd430")
label cd429:
    call phone_after_menu from _call_phone_after_menu_598
    call message_start("[name]", "I can start all over again!") from _call_message_start_630
    call message("Ren", "But it'd be so sad if the data is all gone...") from _call_message_5115
    call message("Ren", "you devoted all your time and heart to them...") from _call_message_5116
    call message("Ren", "but they'll forget you...") from _call_message_5117
    jump am415
label cd430:
    call phone_after_menu from _call_phone_after_menu_599
    call message_start("[name]", "I wonder what the happy ending is like! I'll keep the secret.") from _call_message_start_631
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yes...") from _call_message_5118
    call message("Ren", "promise me you will, [name].") from _call_message_5119
    jump am415
label am415:
    call message("Ren", "Since you've started playing...") from _call_message_5120
    call message("Ren", "you should have a happy ending...") from _call_message_5121
    call message("Ren", "And...") from _call_message_5122
    call message("Ren", "I hope you'd continue staying here") from _call_message_5123
    call message("Ren", "if possible.") from _call_message_5124
    call message("Ren", "I'll be able to do that if you agree...") from _call_message_5125
    call message("Ren", "Did you think about what I told you early in the morning?") from _call_message_5126
    call message("Ren", "About that contract...that will let you stay here.") from _call_message_5127
    call screen phone_reply("What benefits do I get if I make the contract?", "cd431", "I want to be with you if I can!", "cd432")
label cd431:
    call phone_after_menu from _call_phone_after_menu_600
    call message_start("[name]", "What benefits do I get if I make the contract?") from _call_message_start_632
    call message("Ren", "You're free to roam wherever you want in this place") from _call_message_5128
    call message("Ren", "and you'll be able to take part in what we do...") from _call_message_5129
    call message("Ren", "to make someone happy.") from _call_message_5130
    call message("Ren", "I really...") from _call_message_5131
    call message("Ren", "really want to stay here with you.") from _call_message_5132
    call message("Ren", "Forever on and on...") from _call_message_5133
    call message("Ren", "Could you give it a thought...") from _call_message_5134
    call message("Ren", "by this evening?") from _call_message_5135
    jump am416
label cd432:
    call phone_after_menu from _call_phone_after_menu_601
    call message_start("[name]", "I want to be with you if I can!") from _call_message_start_633
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Really? You mean it!?") from _call_message_5136
    call message("Ren", "I'm so glad...I'm so thrilled...") from _call_message_5137
    call message("Ren", "I think my headache was gone for a moment back there...") from _call_message_5138
    call message("Ren", "because of what you just said.") from _call_message_5139
    call message("Ren", "Please don't change your mind until this evening") from _call_message_5140
    call message("Ren", "ok?") from _call_message_5141
    call message("Ren", "I hope playing with me is more fun") from _call_message_5142
    call message("Ren", "than playing with those AIs.") from _call_message_5143
    call message("Ren", "...I want to be a better person.") from _call_message_5144
    jump am416
label am416:
    call message("Ren", "Actually") from _call_message_5145
    call message("Ren", "there is an AI that's even worse than that K guy.") from _call_message_5146
    call message("Ren", "We already talked about this, so perhaps you already realized who it is.") from _call_message_5147
    call screen phone_reply3("You mean Yoshiro?", "cd433", "000!", "cd434", "Sayaka! The chief assistant!", "cd435")
label cd433:
    call phone_after_menu from _call_phone_after_menu_602
    call message_start("[name]", "You mean Yoshiro?") from _call_message_start_634
    call message("Ren", "Oh, no.") from _call_message_5148
    call message("Ren", "That one has a hard time controlling his emotions") from _call_message_5149
    call message("Ren", "but that's why he's safe...") from _call_message_5150
    call message("Ren", "he's honest with what he's feeling.") from _call_message_5151
    call message("Ren", "He's got more to show than hide...") from _call_message_5152
    call message("Ren", "so we can analyze him and make our preparations.") from _call_message_5153
    call message("Ren", "But 000 is the worst. He's the guy who uses false emotions as his weapons...") from _call_message_5154
    jump am417
label cd434:
    call phone_after_menu from _call_phone_after_menu_603
    call message_start("[name]", "000!") from _call_message_start_635
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "That's right...!") from _call_message_5155
    call message("Ren", "I knew you'd feel the same!") from _call_message_5156
    call message("Ren", "I'm so glad...") from _call_message_5157
    call message("Ren", "000 is the worst AI...") from _call_message_5158
    jump am417
label cd435:
    call phone_after_menu from _call_phone_after_menu_604
    call message_start("[name]", "Sayaka! The chief assistant!") from _call_message_start_636
    call message("Ren", "No...") from _call_message_5159
    call message("Ren", "actually, she's more like an innocent victim.") from _call_message_5160
    call message("Ren", "But don't you think it's kind of stupid") from _call_message_5161
    call message("Ren", "that she wholeheartedly trusts K?") from _call_message_5162
    call message("Ren", "Ryou may trust in K because they're childhood friends.") from _call_message_5163
    call message("Ren", "But that's not the case for her, you know?") from _call_message_5164
    call message("Ren", "The really bad AI is not innocent...") from _call_message_5165
    call message("Ren", "He's the one who tortured and distorted someone's life on purpose...") from _call_message_5166
    call message("Ren", "It's 000...") from _call_message_5167
    jump am417
label am417:
    call message("Ren", "000 is a monster... He's incapable of feeling sorrow.") from _call_message_5168
    call message("Ren", "He was programmed completely differently from me.") from _call_message_5169
    call message("Ren", "......") from _call_message_5170
    call message("Ren", "Yes, that's right!") from _call_message_5171
    call message("Ren", "I've got an idea.") from _call_message_5172
    call message_img("Ren", "[name], do you like surprises?", "images/ohyeah.png") from _call_message_img_303
    call message("Ren", "I'm the administrator of the game") from _call_message_5173
    call message("Ren", "so I can control the situations for the AIs whenever I want.") from _call_message_5174
    call message("Ren", "It's a kit for surprise events.") from _call_message_5175
    call message("Ren", "I am the creator of that game") from _call_message_5176
    call message("Ren", "so I can insert whatever information I want...") from _call_message_5177
    call screen phone_reply("What kind of a surprise is it?", "cd436", "Doesn't that mean it's more work for you, Ren?", "cd437")
label cd436:
    call phone_after_menu from _call_phone_after_menu_605
    call message_start("[name]", "What kind of a surprise is it?") from _call_message_start_637
    call message("Ren", "Oh...") from _call_message_5178
    jump am418
label cd437:
    call phone_after_menu from _call_phone_after_menu_606
    call message_start("[name]", "Doesn't that mean it's more work for you, Ren?") from _call_message_start_638
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "It is, but I'd like to keep you entertained!") from _call_message_5179
    jump am418
label am418:
    call message("Ren", "But the details are secret!") from _call_message_5180
    call message("Ren", "You'll get to see a very entertaining surprise") from _call_message_5181
    call message("Ren", "related to AI 000 ^^") from _call_message_5182
    call message("Ren", "I'll make sure you won't be bored...") from _call_message_5183
    call message("Ren", "Not for the AIs but for this game that I made...") from _call_message_5184
    call message_img("Ren", "I hope you'll continue to stay by my side.", "images/smile.png") from _call_message_img_304
    call screen phone_reply("Yes, I'd like to stay with you...and continue playing this game you make for me.", "cd438", "But I'll get to enjoy the game only when I keep a good relationship with the AIs.", "cd439")
label cd438:
    call phone_after_menu from _call_phone_after_menu_607
    call message_start("[name]", "Yes, I'd like to stay with you...and continue playing this game you make for me.") from _call_message_start_639
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "I'm so happy") from _call_message_5185
    call message("Ren", "with what you said...") from _call_message_5186
    call message("Ren", "I wish..someday we can do that.") from _call_message_5187
    call message("Ren", "I hope you won't change my mind") from _call_message_5188
    call message("Ren", "until that day comes.") from _call_message_5189
    call message("Ren", "I'm so happy that I'm actually afraid.") from _call_message_5190
    call message("Ren", "I'm afraid that") from _call_message_5191
    call message("Ren", "I'd say a single word wrong tomorrow") from _call_message_5192
    call message("Ren", "and make you change your mind...") from _call_message_5193
    jump am419
label cd439:
    call phone_after_menu from _call_phone_after_menu_608
    call message_start("[name]", "But I'll get to enjoy the game only when I keep a good relationship with the AIs.") from _call_message_start_640
    call message("Ren", "Yep...that's the irony.") from _call_message_5194
    call message("Ren", "You'll have to get close to them to find out what they're hiding.") from _call_message_5195
    call message("Ren", "But...") from _call_message_5196
    call message("Ren", "I hope you won't get too attached to them.") from _call_message_5197
    jump am419
label am419:
    call message("Ren", "oh...") from _call_message_5198
    call message("Ren", "I think I gotta go now, [name].") from _call_message_5199
    call message("Ren", "My painkiller seems to have arrived...") from _call_message_5200
    call message("Ren", "But I feel a little better") from _call_message_5201
    call message("Ren", "maybe it's because I've told my heart to you...") from _call_message_5202
    call message("Ren", "But who knows when this headache will torture me again when you're not here...?") from _call_message_5203
    call screen phone_reply("Take your pills now! I hope you get better!", "cd440", "Bye now!", "cd441")
label cd440:
    call phone_after_menu from _call_phone_after_menu_609
    call message_start("[name]", "Take your pills now! I hope you get better!") from _call_message_start_641
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yep! Thank you.") from _call_message_5204
    call message_img("Ren", "I feel that I'll get better in no time thanks to your energy.", "images/wink.png") from _call_message_img_305
    jump am420
label cd441:
    call phone_after_menu from _call_phone_after_menu_610
    call message_start("[name]", "Bye now!") from _call_message_start_642
    jump am420
label am420:
    call message("Ren", "I'll get going now.") from _call_message_5205
    call message("Ren", "Don't forget - be careful of the bad ending. Alright...?") from _call_message_5206
    call message("Ren", "Bye, my [name].") from _call_message_5207
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_5208
    call phone_end from _call_phone_end_36
    stop music
    scene black with dissolve
    show text "14:41" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg8 with dissolve
    play music "music/music3.ogg"
    show S S11 with dissolve
    s "K... Your message is... Haaaa..."
    show S S19
    s "He keeps telling me to trust [name], but I'm strangely getting nothing about her!"
    show S S14
    s "Just what is the reason he keeps telling me to trust [name]? I'm sure there's no external informant as good as I am..."
    show S S15
    s "K....I can trust you, right? Seriously...I have a bad feeling. This trace of infiltration at midnight seems ominous, and I haven't even discovered who is the hacker that sent [name] into this chatroom..."
    show S S16
    s "Huh? An email. Who sent this? Not many people know about this account."
    play music "music/music12.ogg"
    s "Sent by... Unknown."
    show S S12
    s "...It's...a newspaper article."
    s "There is a flood of reports on the prime minister's illegitimate children...."
    show S S15
    s "Right when the prime minister declares himself as a candidate for the presidential election, the scandal unleashes itself as if it were waiting for this moment...."
    s "Prime minister.... Prime minister..."
    show S S16
    s "Who is this...?"
    s "Just who in the world would know about me...?"
    hide S with dissolve
    stop music
    scene black with dissolve
    show text "17:36" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music4.ogg"
    scene bg5 with dissolve
    show MC MC11 at my_right with dissolve
    play sound "music/knock.ogg"
    "(knocking sound)"
    whooo "It's me. Ren."
    ray "Can I come in?"
    mc "Come on in."
    "..."
    show U U11 at my_left with dissolve
    ray "Hehe... Listen. Did you look at the chatroom?"
    show U U14
    ray "Didn't something fun take place?"
    menu:
        "You mean this email incident with 000?":
            ray "That's the one...!"
            ray "I ran all this way here in case you missed it."
            jump u41
        "I saw it! It's fun to see AI 000 flustered.":
            ray "I feel great seeing you smile..."
            jump u41
label u41:
    ray "I'm the one who did it... They must be going crazy finding out who it is."
    show U U11
    ray "I didn't expect them...to be so skittish."
    show U U14
    ray "They can't find me... My algorithms' perfect."
    show U U11
    menu:
        "They feel like actual people responding to a grave incident. They feel so realistic. What did you send them?":
            ray "Hehe..."
            jump u42
        "I didn't really feel good seeing them so anxious.":
            ray "Perhaps. Maybe that's because you pity them... But I already told you."
            ray "He's the worst AI here. I hope you'd think like that too, [name]."
            jump u42
label u42:
    show U U13
    ray "I sent AI 000...pieces of his past that he longs to forget."
    show U U14
    ray "I just couldn't watch him enjoying his life all by himself...freed from his gruesome past."
    show U U11
    ray "I knew I should've studied hard every day... Now I can bend and break those AIs however I want!"
    show U U14
    ray "[name]...You gotta have unfathomable twists and turns to make your life interesting. Don't you agree?"
    show U U17
    ray "The dark secrets reveal themselves, and your secret sweetheart suddenly tells you how he feels...!"
    menu:
        "Actually, I'd prefer life that can be predicted.":
            ray "I did too..."
            ray "I wished life would continue to run peacefully so that I can see how it would turn and flow."
            ray "But my environment would jerk and spin regardless of my will...like how tomorrow you can't tell whether your mother would yell at you or give you a candy."
            ray "That's why I decided to change myself as well."
            ray "I decided to change myself enormously...to the point others can't predict."
            jump u43
        "Yes, it was a surprise. But that's why I like it.":
            ray" Yes, I think I also got a little taste of what that feeling is like just recently."
            ray "I learned that I must change...in order to be memorable...and in order to avoid being a scapegoat of an unfair misfortune."
            jump u43
label u43:
    ray "Because I was told...that I can never have what I want...that I'll be the only one to get hurt if I do nothing."
    show U U13
    ray "So... [name], I don't want to treat you timidly."
    show U U12
    ray "Of course...I'm a bit shy...but I want to fix that."
    show U U17
    ray "I want to be more daring to you...bolder."
    ray "Because then I'd be able to stay here longer with you...!"
    show U U11
    ray "If you decide to stay here...I'll be much bolder and exciting person."
    ray "I'll try my best to be a person who can keep you entertained and make you happy...!"
    show U U13
    ray "You know what...? I didn't even know how to use the computer a few years ago. But...I worked really hard...and I made this much progress...!"
    show U U17
    ray "Stay here with me forever... Please?"
    ray "Once it gets dark, you'll make a contract to be our official member. You must...!"
    show U U11
    ray "I'll get everything ready. Alright?"
    menu:
        "I'm not sure about that...":
            ray "Hmm? What's that? I didn't catch it..."
            jump u44
        "Okay.":
            jump u44
label u44:
    show U U12
    ray "Wait, there's no time to lose. I should get ready now!"
    stop music
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    show text "17:36" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(K is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    v "[name], it's me."
    v "Umm...do you have a minute to talk?"
    play sound "music/break.ogg"
    menu:
        "Yes, of course.":
            jump v41
        "No, I'm busy.":
            jump v41
label v41:
    v "...Why can't I hear anything?"
    v "...Hello? [name]? Can you hear me?"
    menu:
        "Yes, I can!":
            v "Did you talk just now? Uh, I can't hear very well."
            v "Is there something wrong with the signal here? Should I move somewhere else?"
            jump v42
        "K! You're stupid!":
            v "Pardon? I'm sorry, what was that...? I can't hear you."
            v "But why do I feel like somebody just said something bad about me...? Is it just my imagination?"
            jump v42
        "K! I love you!":
            v "What about view so suddenly? Is there something about the view around there?"
            v "...I can't hear you at all. What should I do? At this rate, I can't talk to you..."
            jump v42
label v42:
    v "[name], could you put yourself on hold for a bit? Let me move to the windows!"
    stop sound
    v "...Hello? Can you hear me?!!!!"
    menu:
        "Yes, I can hear you!!!":
            v "Me, too!"
            v "Now it's working. Back there the words were all disconnected."
            jump v43
        "I'm not deaf -":
            v "Oh, sorry. I'm kind of loud, right?"
            v "Oh...now the phone is working fine!"
            jump v43
label v43:
    v "Anyways, it's a relief now that I hear you better."
    v "I hope the phone will work when I call Levan later on."
    menu:
        "You can keep talking to me, can't you?":
            v "I'd love to...but Levan's having a hard time right now."
            v "I'm the only one who knows what's going on with him, and he relies on me a lot."
            v "I should provide support for him."
            jump v44
        "Please take care of him.":
            v "Yes, I'm the one who has at least a glimpse of what he's going through. So I should do my best."
            v "Thank you for caring for him with me."
            jump v44
        "You should take this opportunity to get a new phone.":
            v "Hmm..should I? But Levan said this phone is better than the ones currently on the market, since he took precautions to prevent bugging..."
            v "I should talk to Levan about this."
            jump v44
label v44:
    v "Oh, just a moment."
    v "Alright...understood. I'll drop by the intelligence team."
    v "...Hello? Did you hear what I just said...?"
    menu:
        "No, not really.":
            v "The signal must have been bad again."
            v "It was nothing important."
            jump v45
        "Intelligence team...?":
            v "It's nothing, so there's no need to mind. Just forget about it."
            jump v45
label v45:
    v "Oh, since the signal might fluctuate again, so I should call Levan when it's still working."
    v "Now I must leave. I'll talk to you again."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "18:42" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music2chat-17.ogg"
    show MC MC11 with dissolve
    play sound "music/answer.ogg"
    "(000 is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up.)"
    s "Hey, it's me."
    s "What are you doing? Oh, right."
    s "It's almost dinner... Did you eat dinner?"
    menu:
        "I did. What about you?":
            s "Good to know that you're eating."
            s "I didn't eat yet. I don't feel like eating."
            jump s41
        "Why do you sound like that?":
            s "My voice..? What about my voice? Is it weird?"
            menu:
                "It sounds so cool.":
                    s "What...? That's so random. And I've never heard that before..."
                    s "But thanks for the compliment."
                    jump s41
                "You sound so tired.":
                    s "You're sharp..."
                    s "I mean, no. I'm fine."
                    jump s41
        "Not yet -":
            s "I hope you don't skip your meals."
            s "Well...but I didn't get dinner, either."
            jump s41
label s41:
    s "And I have this craving for ice cream today."
    s "I've always liked ice creams."
    s "Take a bite, and your mouth is full of sweetness. You feel better once you eat it."
    s "Popsicle, ice cream on a cone... I ate all kinds of ice creams."
    s "And what kind of ice cream do you like? Popsicle? Cone?"
    menu:
        "I like to chew on my popsicle.":
            s "Once I bit my tongue while eating a popsicle, and it hurt so bad."
            s "I unintentionally got to enjoy blood-flavored ice cream... But I ate another one."
            jump s42
        "I like soft ice cream. The one you get on a cone.":
            s "Same here."
            s "It melts away as soon as your tongue touches it. That's the best."
            jump s42
        "Ice cream is for kids.":
            s "...Do you think so?"
            s "I thought it's something that everyone likes..."
            jump s42
label s42:
    s "You know, I feel like I'm in a whole new world when I'm eating an ice cream."
    s "Perhaps ice cream paralyzes this part in the brain that holds the sense of reality, since it's way too sweet."
    s "But I don't know the details... It's just a theory."
    s "When I was young, I used to laugh whenever I had ice cream, no matter how hard I cried before that."
    s "By the way, when I was young, I used to eat this ice cream that you split in half... The one you can share with your friend, you know? That was my favorite."
    menu:
        "Who did you share it with?":
            s "A person most precious to me. A person I miss so much in the entire world..."
            s "Who is that person...? It's a secret."
            jump s43
        "Next time, share with me!":
            s "Let's do that, when chance allows it."
            s "But if I split it wrong....you can have the bigger side."
            jump s43
        "Nobody asked you to tell me that.":
            s "Looks like I did all the talking. I was reminded of my childhood."
            s "Sorry to say all these things...when you didn't ask me."
            jump s43
label s43:
    s "Phew...my head is full of thoughts."
    s "You know, for some reason I talk about a bunch of things on my mind when I'm with you. I heard there was this class for being a good listener... You didn't take that class, did you?"
    s "Ugh...this won't do. I think I'll end up saying something I shouldn't. I'm hanging up now."
    s "I'll see you after I cool down a bit."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "21:53" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    play music "music/music3.ogg"
    scene bg10 with dissolve
    show U U12 with dissolve
    believer "Mr. Ren, how long do you plan on keeping that woman in there?"
    ray "What's the matter? Is there something wrong with her?"
    believer "No.... It's just that as far as I know, she didn't have her ceremony yet. We can't let an outsider stay here forever, can we?"
    show U U14
    ray "Oh... We'll hold her ceremony soon. Before midnight."
    believer "Should I tell the savior?"
    show U U13
    ray "She already knows... The only thing we need now is that girl's choice."
    show U U12
    believer "Then I'll tell the savior that you're taking care of this."
    show U U13
    ray "Fine..."
    show U U14
    ray "Now she'll be with me, won't she? She'll be with me forever... Uh... But what if she changes her mind?"
    show U U16
    ray" What if she suddenly hates me and pushes me away...?"
    show U U17
    ray "No. She came all this way here for me... She'll understand me better than anyone else."
    show U U15
    ray "...Huh? You there. Do you have something to tell me?"
    show U U12
    whooo "N-no. Please go ahead, Mr. Ren."
    show U U13
    ray "What if...she says no...?"
    show U U12
    ray "The elixir of salvation tastes foul, so she might not want to take it..."
    show U U17
    ray "Ah, I should add more flavor and fragrance to it...! I'm going to make hers special... Oh! There's no time to lose if she's going to have her ceremony by tonight!"
    scene black with dissolve
    hide U with dissolve
    play music "music/music12.ogg"
    show V V217 with dissolve
    v "Elixir of salvation...? He's going to use it on [name] tonight!?"
    show V V27
    v "...There's no doubt she's being used."
    show V V216
    v "I must hurry. There isn't much time...!"
    show V V27
    v "I can't leave her here... Tonight is the only time I can save her... ."
    show V V28
    v "I should've discussed in advance with Levan if I'd known this was coming... Would I be able to save her all by myself?"
    show V V216
    v "Haaa... I couldn't tell anyone about this. The secret has got out of my hands..."
    show V V28
    v "But Ren...? Why wouldn't he use his actual name?"
    show V V23
    v "Of course... I'm sure he hates that name...to the point he wants to forsake his birth name."
    v "He must be the one...who sent Levan that email about the prime minister..."
    show V V29
    v "Levan is still suffering from anxiety."
    v "I should send him the coordinates of this place just in case..."
    show V V21
    v "...But I cannot allow that boy to confront Levan. Never."
    hide V with dissolve
    stop music
    scene bg7 with dissolve
    show MC MC11 with dissolve
    play music "music/music2chat-17.ogg"
    play sound "music/answer.ogg"
    "(Ren is calling.)"
    play sound "music/beep.ogg"
    mc "(Pick up)"
    ray "Hello? I wonder what you're doing."
    ray "Actually...I called because I wanted to see you."
    ray "It's a beautiful night."
    ray "What are you doing?"
    menu:
        "I was playing.":
            ray "Oh, so you were playing my game."
            ray "Thanks for playing until this hour. I prepared it so hard for you."
            ray "I feel so proud whenever you enjoy it so much."
            ray "You make me think...that everything I've done for you is all worthy."
            jump u45
        "And what were you doing?":
            ray "Me? I was checking for the last time on something very important."
            ray "It's something that needs great care and attention until the very last moment."
            jump u45
label u45:
    ray "For some reason, I can relax as I listen to you. My body was all tensed until now, actually."
    ray "Ugh...but listening to you makes me miss you even more."
    ray "I'd love to work right next to you, but then I might interrupt your gameplay."
    ray "I'm holding myself back all the time. But when my patience ends, then I call you like I did now."
    ray "Haaa...please, give me more."
    menu:
        "I miss you, too.":
            ray "Haha... I see."
            ray "Funny, isn't it? I call you when I miss you, so that I can get rid of this longing for you even for little. But when I hear you, I miss you even more."
            ray "Is it the same for you? Do you miss me even more when you listen to me? If that's the case, I'll be so happy..."
            ray "Because when you think that you miss me, then you're thinking about me. At least at that moment."
            jump u46
        "You're listening to an automated speaker. Please leave your message after the beep. Beep.":
            ray "Uh....what? Oh...are you kidding?"
            ray "Ahaha. That was kind of scary. I thought you hung up so suddenly. You're so good at pretending to be a machine. There's one more note to take down about you."
            ray "I feel kind of tired after getting surprised then laughing... But I feel good."
            jump u46
label u46:
    ray "Ugh, I feel good... Oh, right. There's something I wanted to ask you."
    mc "What is it?"
    ray "Do you hate bitter stuff? No, wait. I think everyone hates bitter stuff."
    ray "So I'm asking...are you the type of person who can't handle bitter stuff? You'd prefer something sweet, wouldn't you?"
    ray "I'd like to make it fit your taste."
    menu:
        "I'm good with bitter stuff! I can drink coffee without sugar!":
            ray "Wow, really? You're amazing. Actually, this is a secret just for you... I'm really bad at bitter stuff."
            ray "So after I eat something better, I need to get a candy. Or an ice cream."
            ray "So you're good with bitter tastes? Good. Then there's no need to worry."
            jump u47
        "Sweet is the best. Best are the desserts!":
            ray "So you like dessert food. Umm... Is there any way I can make it taste more like a cake...?"
            ray "I'll make it sweet enough to make your tongue melt."
            ray "So that you can smile without realizing after you take it."
            jump u47
        "I don't care about the taste. Anything is good as long as it's food.":
            ray "Really? So you eat everything. It's not easy to like bitter stuff..."
            ray "Since you tell me whatever taste is fine, I feel a little better."
            ray "I was worried that you'd hate it because it doesn't taste good."
            jump u47
label u47:
    ray" Oh, look at the time, I think I have to go now."
    menu:
        "But Ren, is your headache better?":
            ray "Headache...? Oh...hahaha... You're caring for me, right?"
            ray "Sorry for the sudden laughter... Actually, I'm so happy I don't know what to do."
            ray "Nobody has ever cared for me when I'm sick..."
            ray "Whenever I was sick, I had only myself to rely on. I didn't want people to hate me, calling me an annoying kid..."
            ray "I'm so happy whenever you care for me. Thank you...thank you so much."
            jump u48
        "Thanks for the call.":
            ray "Yeah, and thank you, too. It was great talking to you. My head was killing me until now, but now I think I'm feeling better."
            ray "I thought your voice can soothe mind only. But it also soothes the pain."
            ray "Oh, another job to do... I want to talk to you some more..."
            ray "Phew... But there's no choice."
            jump u48
label u48:
    ray "Let me tell you something good before I hang up. I think I can see you very soon."
    ray "And there's something I'd like to hear from you. I hope you'd give me a good answer."
    ray "You'll get just more curious if I talk any more, won't you? Gotta go now."
    ray "I'll see you soon."
    play sound "music/beep.ogg"
    stop music
    hide MC with dissolve
    scene black with dissolve
    show text "22:49" with dissolve
    with Pause(1.0)
    hide text with dissolve
    with Pause(2.0)
    scene bg7 with dissolve
    play music "music/music4.ogg"
    call phone_start from _call_phone_start_36
    call message_start("SYSTEM", "Ren has entered the chatroom.") from _call_message_start_643
    call message("Ren", "[name]! I've been waiting for this moment...") from _call_message_5209
    call screen phone_reply("Am I going to make the contract now!", "cd442", "You're here...", "cd443")
label cd442:
    call phone_after_menu from _call_phone_after_menu_611
    call message_start("[name]"," Am I going to make the contract now!") from _call_message_start_644
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message_img("Ren", "Yep! So you were also waiting!", "images/smile.png") from _call_message_img_306
    jump am421
label cd443:
    call phone_after_menu from _call_phone_after_menu_612
    call message_start("[name]", "You're here...") from _call_message_start_645
    call message("Ren", "Thanks for your patience...^^") from _call_message_5210
    jump am421
label am421:
    call message("Ren", "Now it's time to make the contract to become our official resident...") from _call_message_5211
    call message("Ren", "Have you given a thought about it?") from _call_message_5212
    call screen phone_reply("I haven't made my choice yet...", "cd444", "I decided to stay here with you.", "cd445")
label cd444:
    call phone_after_menu from _call_phone_after_menu_613
    call message_start("[name]", "I haven't made my choice yet...") from _call_message_start_646
    call message("Ren", "I heard that taking a little walk is good for you if you have too many thoughts.") from _call_message_5213
    call message("Ren", "I'm constantly haunted by anxiety attack") from _call_message_5214
    call message("Ren", "whenever I curl up in silence...") from _call_message_5215
    call message("Ren", "so I tend to move a lot. I even force myself to move if I have to ^^") from _call_message_5216
    jump am422
label cd445:
    call phone_after_menu from _call_phone_after_menu_614
    call message_start("[name]", "I decided to stay here with you.") from _call_message_start_647
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "You won't change my mind...will you?") from _call_message_5217
    call message("Ren", "Thank you...") from _call_message_5218
    call message_img("Ren", "I'm so glad..!", "images/wink.png") from _call_message_img_307
    jump am422
label am422:
    call message("Ren", "Make your contract, and we'll be able to take a walk everyday!") from _call_message_5219
    call message("Ren", "Listen, [name]...") from _call_message_5220
    call message("Ren", "You know what?") from _call_message_5221
    call message("Ren", "I heard that the thought that lasts just before you fall asleep") from _call_message_5222
    call message("Ren", "determines how you feel the next day...") from _call_message_5223
    call message("Ren", "I""'m reminded of the person I hate the most before I fall asleep.") from _call_message_5224
    call message("Ren", "And that thought tortures me even till next morning...") from _call_message_5225
    call message("Ren", "to the point, I don't want to open my eyes...") from _call_message_5226
    call message("Ren", "But I hope that's not the case for you.") from _call_message_5227
    call message("Ren", "I want to make you fall asleep every night with happy thoughts") from _call_message_5228
    call message("Ren", "once you start living here...") from _call_message_5229
    call message("Ren", "Because...") from _call_message_5230
    call message("Ren", "...I've actually fallen for you.") from _call_message_5231
    call screen phone_reply("Me too... I like you too, Ren.", "cd446", "This is so sudden...", "cd447")
label cd446:
    call phone_after_menu from _call_phone_after_menu_615
    call message_start("[name]", "Me too... I like you too, Ren.") from _call_message_start_648
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Me too... I like you so much...!") from _call_message_5232
    call message("Ren", "Even if I open my eyes with agonizing thoughts") from _call_message_5233
    call message("Ren", "my heart becomes peaceful when I think of you.") from _call_message_5234
    call message("Ren", "Even if my heart clenches in my chest") from _call_message_5235
    call message("Ren", "I feel like I can breathe again...when I think of you.") from _call_message_5236
    jump am423
label cd447:
    call phone_after_menu from _call_phone_after_menu_616
    call message_start("[name]", "This is so sudden...") from _call_message_start_649
    call message("Ren", "Oh...I'm terribly sorry if I was rushing.") from _call_message_5237
    call message("Ren", "I'll be nice and slow...") from _call_message_5238
    call message("Ren", "But I'd really like you to make your contract tonight..!") from _call_message_5239
    jump am423
label am423:
    call message("Ren", "[name]....") from _call_message_5240
    call message("Ren", "You're not gonna betray me, are you?") from _call_message_5241
    call message("Ren", "I hope you won't trust those AIs") from _call_message_5242
    call message("Ren", "more than me, for example...") from _call_message_5243
    call message("Ren", "I hope such a sad thing won't happen...") from _call_message_5244
    call screen phone_reply("Those AIs...they're the programs you made...right?", "cd448", "I won't betray you. I promise.", "cd449")
label cd448:
    call phone_after_menu from _call_phone_after_menu_617
    call message_start("[name]", "Those AIs...they're the programs you made...right?") from _call_message_start_650
    call message("Ren", "What's the matter?") from _call_message_5245
    call message("Ren", "Did one of them say something over the phone?") from _call_message_5246
    call message("Ren", "Did they tell you something weird?") from _call_message_5247
    call message("Ren", "[name], you didn't let them get you") from _call_message_5248
    call message("Ren", "did you? You don't hate me, do you?") from _call_message_5249
    jump am424
label cd449:
    call phone_after_menu from _call_phone_after_menu_618
    call message_start("[name]", "I won't betray you. I promise.") from _call_message_start_651
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "You have no idea how glad I am whenever you tell me that.") from _call_message_5250
    call message_img("Ren", "Please keep our promise...[name].", "images/smile.png") from _call_message_img_308
    jump am424
label am424:
    call message("Ren", "Throughout my life, all of the people") from _call_message_5251
    call message("Ren", "who betrayed me were unwelcome and uninvited guests...") from _call_message_5252
    call message("Ren", "But you're not one of those people, [name]..") from _call_message_5253
    call message("Ren", "You came here for me.") from _call_message_5254
    call message("Ren", "You chose to.") from _call_message_5255
    call message("Ren", "So...") from _call_message_5256
    call message("Ren", "now I can hope, right?") from _call_message_5257
    call message("Ren", "I can hope that you're nothing like those who betrayed me...right?") from _call_message_5258
    call screen phone_reply("Yes, please trust me.", "cd450", "I think you should trust yourself some more rather than expect me to trust you.", "cd451")
label cd450:
    call phone_after_menu from _call_phone_after_menu_619
    call message_start("[name]", "Yes, please trust me.") from _call_message_start_652
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Yes...I will.") from _call_message_5259
    call message("Ren", "My trust has been met with nothing but betrayal so far...") from _call_message_5260
    call message("Ren", "But...I'll trust you...") from _call_message_5261
    call message("Ren", "So please don't hurt me, [name]...") from _call_message_5262
    jump am425
label cd451:
    call phone_after_menu from _call_phone_after_menu_620
    call message_start("[name]", "I think you should trust yourself some more rather than expect me to trust you.") from _call_message_start_653
    call message("Ren", "Trust myself...?") from _call_message_5263
    call message("Ren", "I...don't think I can do that yet.") from _call_message_5264
    call message("Ren", "I'm not sure if I'm good at anything") from _call_message_5265
    call message("Ren", "other than computers...") from _call_message_5266
    call message("Ren", "But I feel like I'm a fantastic person") from _call_message_5267
    call message("Ren", "if I think that you like me!") from _call_message_5268
    jump am425
label am425:
    call message("Ren", "Do you know what I think of everyday?") from _call_message_5269
    call message("Ren", "I want you to be my side...") from _call_message_5270
    call message("Ren", "I want you to give me all of your sadness and pain..") from _call_message_5271
    call message("Ren", "I want to say here with you forever..") from _call_message_5272
    call message("Ren", "If you stay here...") from _call_message_5273
    call message("Ren", "I want to protect you...") from _call_message_5274
    call message("Ren", "from everything bad to you,") from _call_message_5275
    call message("Ren", "from morning when you wake up until night when you fall asleep...") from _call_message_5276
    call message("Ren", "I want to") from _call_message_5277
    call message("Ren", "protect you") from _call_message_5278
    call message("Ren", "I really want to protect you...") from _call_message_5279
    call message("Ren", "Do you understand...how I feel?") from _call_message_5280
    call screen phone_reply("Of course. I'm so touched, Ren...", "cd452", "You sound a bit...sad.", "cd453")
label cd452:
    call phone_after_menu from _call_phone_after_menu_621
    call message_start("[name]"," Of course. I'm so touched, Ren...") from _call_message_start_654
    show gh at heart_pos with dissolve
    hide gh with dissolve
    call message("Ren", "Uh...I'm not used to praises") from _call_message_5281
    call message("Ren", "so I'm not sure how") from _call_message_5282
    call message("Ren", "I should describe this feeling...") from _call_message_5283
    call message("Ren"," My heart feels kind of...ticklish!") from _call_message_5284
    jump am426
label cd453:
    call phone_after_menu from _call_phone_after_menu_622
    call message_start("[name]", "You sound a bit...sad.") from _call_message_start_655
    call message("Ren", "...I do?") from _call_message_5285
    call message("Ren", "I knew it.") from _call_message_5286
    call message("Ren", "You can also see how pathetic I am inside...") from _call_message_5287
    call message("Ren", "But I..") from _call_message_5288
    call message("Ren", "I'll try to be a better person.") from _call_message_5289
    call message("Ren", "I'll try much harder") from _call_message_5290
    call message("Ren", "to become a braver person...!") from _call_message_5291
    jump am426
label am426:
    call message("Ren", "...I wish you'd stay with me...") from _call_message_5292
    call message("Ren", "I'll come for you now...") from _call_message_5293
    call message("Ren", "T""ell me your choice, [name]...") from _call_message_5294
    call message("Ren", "Ok...") from _call_message_5295
    call message("Ren", "I'm coming for you..!") from _call_message_5296
    call message("SYSTEM", "Ren has left the chatroom.") from _call_message_5297
    call phone_end from _call_phone_end_37
    show MC MC11 at my_right with dissolve
    show U U11 at my_left with dissolve
    ray "Were you waiting for me, [name]...?"
    ray "Now all you have to do is take the pledge to our contract. Then we can stay here forever."
    show U U18
    ray "It just feels like a dream."
    ray "I can't believe it's actually come true... I knew it. Everything I believed in was right."
    ray "I knew I wouldn't regret listening to savior."
    ray "Savior told me that I can be useful if I work hard here at Ao Me."
    show U U19
    ray "When I was young, I was good for nothing."
    show U U18
    ray "But now, [name], I can entertain you at least for a moment."
    show U U11
    ray "I'm...so happy. I'm so happy I'm not sure if this is real or not."
    show U U110
    ray "I'm so happy to see you smiling. That's why I'm afraid it will all go away like a dream..."
    show U U19
    ray "This isn't a dream, is it?"
    ray "I'm not going to wake up to find myself alone in my cold, dark room with nothing but computers, am I?"
    show U U110
    menu:
        "This isn't a dream. Trust me.":
            ray "I feel safe thanks to your warm hand."
            ray "It assures me this is real."
            ray "Please keep doing that. Hold on to me when I'm not sure what is happening......"
            jump u49
        "If this is a dream, we can stay in it forever!":
            ray "Haha, yes... If this a dream, I hope we'll dream being happy together..."
            ray "[name]... Will you promise me to stay with me in this dream?"
            jump u49
label u49:
    show U U12
    ray "[name]..."
    ray "Like I told you before, if you officially take the pledge according to the Ao Me's rules, you're free to roam around this place."
    ray "To take the pledge, you must go through a certain procedure."
    show U U111
    ray "But that procedure might be painful...depending on the person."
    show U U19
    ray "It was painful...for me."
    show U U111
    ray "Oh, why don't we talk somewhere else?"
    ray "I'm sure you were frustrated to stay locked in here. Let's look at the flowers and chat."
    show U U11
    menu:
        "Okay, Ren.":
            ray "Thanks. Now let's walk."
            ray "There are lights in the garden to allow people to take a walk during nights as well."
            jump u410
        "Can't we chat here?":
            ray "We can...but I'd like to chat in the garden. This is something important."
            ray "It won't take long, so don't worry."
            jump u410
label u410:
    show U U12
    ray "This way, [name]."
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    show U U19 at my_left with dissolve
    show MC MC11 at my_right with dissolve
    ray "...I don't want you to be in pain after drinking the elixir...like me."
    mc "What was that?"
    show U U111
    ray "Oh, I'll tell you in the garden."
    hide U with dissolve
    hide MC with dissolve
    scene bg13 with dissolve
    show U U19 at my_left with dissolve
    show MC MC11 at my_right with dissolve
    ray "I want to come here with you during the day, but my work always takes too long."
    show U U111
    ray "It usually ends when you would be asleep..."
    ray "You know, [name], you've accepted me the way I am. You always have for the past few days."
    show U U13
    ray "I've never had a chance to feel...what it's like to have someone accept me."
    show U U111
    ray "So I've been thinking about it all day long."
    show U U12
    ray "And I thought...you would trust me no matter what I tell you."
    ray "Even if I tell you something that is completely out of the rules of the world you know...you'll trust me...wouldn't you?"
    show U U111
    ray "[name]... Will you believe me no matter what I tell you?"
    show U U110
    menu:
        "Yes, I will.":
            ray "...You didn't hesitate at all."
            ray "You are such an amazing person... You're nothing like those people I've known so far."
            jump u411
        "Well, it can't hurt me, can it? Let's give it a go!":
            ray "Huh? Go? Go where...?"
            ray "...You're saying that you'll believe in me, right? If that's case..."
            jump u411
label u411:
    ray "I'll trust you, too, [name]."
    show U U13
    ray "I'll trust you...more than anyone else."
    show U U12
    ray "[name].. Don't you wanna know what this place is?"
    ray "You're supposed to take a pledge after a special ceremony..."
    ray "but you also get a rundown on what this place is really about."
    show U U111
    ray "And for this ceremony...you get to drink a specially made elixir for clean..."
    ray "I mean, for an orientation. Yes, that's it."
    show U U12
    ray "And then you officially become one of us."
    show U U111
    ray "But to some people, the side effect is just too strong, so it could be painful."
    show U U19
    ray "To be honest...I'm scared that you'll be in pain after drinking the elixir. Just like me."
    ray "If you're in pain, my heart will be in greater pain."
    show U U111
    ray "We make people drink the elixir in order to show them the truth."
    ray "Then they'll believe in us and become faithful followers."
    show U U18
    ray "But you... You already trust me and believe in this place so much...even without the elixir."
    ray "So I think you don't need the elixir or the orientation."
    show U U111
    ray "But if someone asks you, you must tell them that you took the elixir. Alright?"
    show U U19
    menu:
        "What elixir?":
            ray "It's the elixir that makes people happy and makes them see the truth."
            ray "The world we live in is full of lies. That's why it's so hard to see the truth."
            ray "But with the elixir, we can see the truth that's waiting for us under our noses."
            jump u412
        "Ren, is it painful if you take that elixir?":
            ray "Yes... I think the side effect is really bad for me."
            ray "My head hurts, my heart throbs... It's so difficult to work while the elixir is in effect."
            ray "So I used to take it a lot in the past, but now I take it only when I really, really need it."
            jump u412
label u412:
    show U U12
    ray "Anyways, it's our secret that you didn't go through the orientation, okay?"
    show U U111
    ray "And now, I'll tell you what this place is."
    ray "Remember what I told you before? I told you that this place makes games."
    ray "But honestly...there are more things going on in this place."
    ray "One of our activities that is close to a game would be..."
    show U U11
    ray "Ah, yes. Providing delight to people tired of life, should I say?"
    ray "We do something fun and very meaningful at this place."
    show U U111
    ray "The name for this place is Magenuta. People here call this place Magenuta, the paradise of dreams and hopes."
    show U U11
    ray "Sounds like a world in the games, right?"
    show U U111
    ray "This is where people wounded from the outside world gather."
    ray "Magenuta is the only place they can be happy at."
    hide U111
    show U U13
    ray "I, too...had terrible wounds when I first came here."
    show U U12
    ray" But after the cleansing, I learned how to push behind the trauma of the past and step forward."
    show U U110
    ray "And in the end, I was able to meet you."
    show U U111
    ray "Everyone at the Magenuta is like me."
    ray "They ended up here like destiny with profound wounds and traumas."
    ray "We all were born as the weaker in the world where only the fittest can survive and the stronger preys on the weaker."
    show U U13
    ray "Only the strong ones can survive in the outside world."
    ray "Do you think those born weak should live as preys until death?"
    show U U12
    ray "This is the one and only place for such people."
    ray "That's why we call this place Magenuta the paradise."
    show U U13
    ray "People from the outside world are completely different from us..."
    ray "So I understand why they can't understand us."
    show U U12
    ray "I was told that it's only logical - by their standards - that there are so many people who deny us."
    ray "So that's why everyone has to live according to their rules."
    ray "The strong ones live by their rules, and the weak ones live by theirs."
    ray "And this is the rule we've chosen."
    ray "Only those who follow our rules will get to join the 'Ao Me' and live here at Magenuta."
    ray "Each of us has a role in this place."
    ray "And my role is to protect the Magenuta and"
    ray "Ao Me's belief from the strong ones from the outside world."
    show U U111
    menu:
        "So you mean your game is...":
            ray "Yes... The chatting game you've been doing...is actually not a game."
            ray "I'm sorry I couldn't tell you sooner."
            ray "I told you it's a game because I was afraid you would hate or attack us like the people from the outside world."
            jump u413
        "So am I now part of the Ao Me?":
            ray "Yes. You're supposed to take the elixir for the clean...I mean, orientation..."
            ray "But like I said, I trust you, and I'm sure you'll trust me..."
            ray "So you don't have to go through that procedure."
            jump u413
label u413:
    ray "And about the people...from that messenger you're doing..."
    show U U12
    ray "I mean the RFA. They're real."
    ray "And they're a threat to the Ao Me."
    ray "The people from the RFA are actually existing people..."
    show U U111
    ray "Uh...but not all people of the RFA are dangerous. There are only two dangerous people there."
    show U U12
    ray "They're named K and 000... You should beware of them."
    show U U16
    ray "Those two hurt me and savior even before the Ao Me was created."
    ray "We came to need this place because we couldn't take the pain they gave us."
    show U U19
    ray "I'll give you the details later... This story is so painful for me to remember."
    show U U111
    show MC MC13
    menu:
        "Savior...?":
            ray "Savior is the one who founded the Ao Me."
            ray "You will get to meet the savior soon."
            jump u414
        "You have to tell me next time, okay?":
            ray "Yes, I will. I want to tell you everything someday..."
            jump u414
label u414:
    ray "The reason why I hacked the RFA chat room was to protect this place and to save the members of the RFA. They're being tricked by K."
    ray "You've seen K making secrets to the rest of the RFA, didn't you...?"
    show U U16
    ray "He's a liar. You should never trust him."
    ray "At first, he pretended to save me... But that wasn't his plan."
    ray "He was planning to throw me into the bottom of the abyss."
    show U U19
    show MC MC14
    ray "I realized that only after I suffering near-death..."
    ray "I would have lost my will to live if it weren't for my savior."
    show U U111
    ray "I don't know the rest of the RFA very well....but I kind of agree with my savior."
    ray "I agree that any foolish soul who is being exploited by K deserves salvation."
    ray "That's why we decided to make an innocent person, a person who knows nothing about the RFA or the Ao Me, to contact them."
    ray "And that person is you, [name]."
    ray "You had to move them in order to figure out how the RFA collects information and data."
    ray "Thankfully, you've done so well, so RFA decided to hold a party."
    ray "And all the data related to the party, which is the core of the RFA, started to move."
    show U U19
    ray "I'm so, so sorry I told you that this is a game..."
    ray "To be honest, I had zero expectation on the tester."
    ray "My savior told me multiple times..."
    ray "That since we're bringing someone outside Magenuta into this...we must not get our hopes up."
    ray "My savior told me to never forget that people from outside world like K and 000 can turn their backs on us and hurt us anytime."
    show U U110
    show MC MC16
    ray "But [name]...I realized you're special."
    ray "You're the type of person...who can be with us."
    show U U18
    ray "I'm so glad I got to meet you."
    ray "There was nothing but endless fight in my life... But now I have a haven called you."
    show U U110
    ray "[name]...if you help me...just a bit more...we can save the RFA as well..."
    ray "Then don't you think I can live my life completely differently?"
    ray "You know...life with more comfort."
    show U U111
    show MC MC11
    ray "...Do you think that's possible for me?"
    ray "My savior told me that this fight wouldn't take forever..."
    ray "That this fight will come to an end...if I work hard...really hard."
    ray "That day will soon come...don't you think?"
    show U U12
    ray "So [name]...please continue this game a little longer."
    ray "Once you hold the party...my savior's and my goal will be complete."
    show U U110
    ray "And after that..."
    scene black with dissolve
    hide U
    whooo "...So that was what she made you do."
    hide U
    show U U15 at my_left
    show MC MC15 at my_right
    ray "...Who's there...?"
    scene bg13 with dissolve
    play music "music/music2.ogg"
    show MC MC15 at my_right
    show U U15 at my_left
    show V V29 with dissolve
    whooo "Sora..."
    whooo "[name], I've come to rescue you... But it appears I'm too late."
    ray "...You...?"
    ray "How come you're...?"
    show U U16
    ray "Ugh...I knew it. I knew this wouldn't go well. No matter what I do, there will always be someone getting in my way."
    ray "This happens all the time."
    show V V210
    whooo "Sora...please calm down."
    show V V22
    menu:
        "Who are you...?":
            whooo "We talked over the phone. It appears you don't remember my voice, [name]."
            v "I'm...K...the representative of the RFA."
            jump u415
        "K...?":
            v Y"ou remember my voice."
            v "I'm terribly sorry this is where we meet, [name]."
            jump u415
        "Sora...?":
            v "It seemed he's under the name Ren at this place, though..."
            ray "K... How did you get in here? Stop trying to torment us!"
            v "It's all just a misunderstanding, Sora..."
            ray "Don't call me like that... Ugh... My head!"
            mc "Ren, are you alright?"
            ray "I'm...fine..."
            ray "[name]... I'll explain about my name later. Don't believe anything that man tells you!"
            jump u415
label u415:
    show V V21
    v "I infiltrated this place after determining that you're in danger..."
    show V V22
    v "but I believe taking you out of this place at the given time is impossible."
    v "According to what I overheard..."
    v "you already believe more in him than in the RFA."
    show U U15
    ray "Of course, she won't believe you! You're a liar...!"
    show U U16
    ray "[name], stay back...! Don't ever get close to him. He's dangerous..."
    show V V29
    v "Sora...it pains me to see you. I'm sure this is all my fault..."
    show U U15
    ray "Don't say something like that! You don't feel guilty at all! I'm not falling for that this time. Never."
    v "I'm not the one who's lying to you. Please...can't you listen to me?"
    show U U16
    ray "Get away! Get away. Get away. Get away from me!"
    show U U15
    ray "Don't you dare touch [name]! I will never let you get away with it if you do...!"
    v "You must be the one who hacked the messenger. You learned how to hack after your brother, didn't you...?"
    v "Why did you have to go that far...?"
    show U U16
    ray "No... I'm going to beat that whitehead."
    ray "My savior told me that I have to...!"
    mc "After your 'brother?' What does he mean...?"
    show V V22
    v "[name]...I'll tell you now. You will find out eventually."
    v "This boy whom you call 'Ren' and 000 are twins."
    show U U15
    ray "I have no such thing as a brother. I threw that part away from my life years ago...!"
    show V V29
    v "Sora..."
    v "You even brought a complete stranger to this...?"
    show V V23
    v "I promised Levan you would be safe..."
    show U U16
    ray "Liar. That's all lie. Get away from me. Please...!"
    show V V29
    v "Sora..."
    show U U15
    ray "Please, please get away. Just go away...from my sight!"
    hide U
    hide MC
    hide V
    scene black with dissolve
    v "Sora..."
    ray "...What...is this...?"
    v "I know you were heartbroken as much as you missed him."
    v "This is what you really looked like. I hope you would keep that in mind at least."
    ray "No...no. Don't give me this. My savior will be so mad if I take it."
    ray "You never gave me this. I did not see anything."
    v "...You're shivering in fear while trapped by her side. My heart is wrenching in pain..."
    ray "You're going to torment us all...just like you did to my savior... You can't fool me."
    v "I'm so sorry things turned out like this, Sora..."
    ray "Help...somebody...help!"
    menu:
        "K, you should leave Ren alone for now.":
            jump u416
        "Hello?! We have an intruder! Somebody help!!":
            jump u416
label u416:
    v "[name]...I'm terribly sorry this is how our encounter turned out."
    whooo "Over there! An intruder!"
    scene bg13 with dissolve
    show U U15 at my_left
    show MC MC15 at my_right
    show V V29
    v "I'm sure our minds aren't the same, but I hope you can see what is truly for his sake..."
    show V V210
    v "I hope someday you will be able to see what is true...[name]."
    v "I heard everything that took place between you two."
    show V V23
    v "I suggest it would be better to pretend nothing happened in the chat rooms. For Sora's sake."
    show V V22
    v "I hope we meet again..."
    hide V
    hide MC
    hide U
    scene white with dissolve
    show MC MC15 at my_right
    show U U15 at my_left
    ray "Huff... Huff..."
    show U U16
    ray "Is he...gone?"
    show U U111
    ray "He's gone, right...? [name], you're safe, too, right?"
    scene bg13 with dissolve
    show U U12 at my_left
    show MC MC17 at my_right
    ray "Sob..."
    show U U113
    ray "I...I'm too weak. I hate myself... I really hate myself..."
    ray "Maybe it's impossible... Maybe I can never...make you ha..."
    show U U112
    ray "Uh..no. Forget that."
    show U U19
    show MC MC14
    ray "I'll walk you to your room...[name]."
    ray "I understand...even if you hate me...because of how pathetic I am."
    show U U111
    ray "Please calm down in there, and let's talk again...once you feel you're up for it..."
    show U U112
    ray "I'm sorry I couldn't protect you...[name]."
    hide U with dissolve
    hide MC with dissolve
    scene black with dissolve
    stop music
    call day5 from _call_day5
