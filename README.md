#About

"Four Seasons" is a 2D drama, romance, story rich, mystey visual novel for Windows PC.

Created by third year Vytautas Magnus university (VMU) students:

* Sandra Pučkoriūtė - art, programming

* Ieva Vaitasiūtė - concept art

*Story is taken from a mobile game "Mystic Messenger" by Cheritz Corporation.* It was taken for developing skills related to video game creation purposes.

*Used music, sounds and background photos taken from royalty free websites.*


## Screenshots

![Title screen](https://i.imgur.com/AvXgate.png)
![Insert name](https://i.imgur.com/9O8WnEi.png)

![Chat mode with choices](https://i.imgur.com/3guXhaw.png)
![Save screen](https://i.imgur.com/OeVd0du.png)

![Visual novel mode](https://i.imgur.com/pTJzOCM.png)
![Visual novel mode with choices](https://i.imgur.com/S7K8eX6.png)

![Emojies in chat mode](https://i.imgur.com/YX7IUAE.png)
![Ending indicator](https://i.imgur.com/haca7um.png)

## Tools

* Ren'py (Python) - game creation, programming

* Paint Tool SAI, Photoshop CC 2018 - art, background filter


## Story
One day a girl gets a mysterious message from an unknown number. Turns out, the unkown person is a game developer who needs her help to test his game. He, who lives somewhere in the mountains, persuades the girl to join him with the testing. When they meet, the unknown person connects her to an app, where a group of people are already chatting!

In order to find out the truth about the mysterious developer and his entire story, the girl will have to go through obstacles and make important desicions.

*The player will often be presented with different chat choices that lead to different ends of the game.*